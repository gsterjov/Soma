
#ifndef SOMA_ASSET_ENTITY_H_
#define SOMA_ASSET_ENTITY_H_


#include <Soma/Config.h>
#include <Soma/String.h>
#include <Soma/Vri.h>
#include <Soma/SmartPointer.h>

#include <Soma/Entity.h>


namespace Soma
{

	/* forward declarations */
	class Asset;
	
	
	
	/**
	 * Asset entity.
	 * 
	 * The asset entity hides the details of loading complex entities such
	 * as a player character or vehicle which rely on an intricate
	 * interconnection of components and resources. It does this by loading
	 * an Asset resource and adding the appropriate components to the entity.
	 * 
	 * An example of an asset entity would be a character's head, the
	 * physics body defining collision boundaries and a material detailing
	 * how it should be rendered.
	 * 
	 * @ingroup components
	 */
	class SOMA_API AssetEntity : public Entity
	{
	public:
		/**
		 * Named constructor.
		 * @param name The name of the asset entity.
		 */
		explicit AssetEntity (const String& name);
		
		/**
		 * Asset path constructor.
		 * 
		 * @param name The name of the asset entity.
		 * @param vri The path to the asset.
		 */
		AssetEntity (const String& name, const Vri& vri);
		
		/**
		 * Asset resource constructor.
		 * 
		 * @param name The name of the asset entity.
		 * @param asset The asset resource to instantiate.
		 */
		AssetEntity (const String& name, const smart_ptr<Asset>& asset);
		
		
		/**
		 * Destructor.
		 */
		virtual ~AssetEntity ();
		
		
		
		/**
		 * Get the asset resource being used by the entity.
		 * @return The asset resource to be instantiated.
		 */
		const smart_ptr<Asset>& getAsset() const { return mAsset; }
		
		
		/**
		 * Set the asset to be used.
		 * @param vri The path to the asset resource.
		 */
		void setAsset (const Vri& vri);
		
		/**
		 * Set the asset to be used.
		 * @param asset The asset resource to instantiate.
		 */
		void setAsset (const smart_ptr<Asset>& asset);
		
		
		
		/** Log source name. */
		static const String LogSource;
		
		
		
	private:
		smart_ptr<Asset> mAsset;
	};

}


#endif /* SOMA_ASSET_ENTITY_H_ */
