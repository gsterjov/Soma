
#ifndef SOMA_SINGLETON_H_
#define SOMA_SINGLETON_H_


#include <cassert>

#include <Soma/Config.h>


namespace Soma
{

	/**
	 * The base singleton class.
	 * 
	 * Singletons ensure only one instance of a class can be instantiated
	 * throughout the application process. In this way we can better manage
	 * shared resources such as files, rendering, input, and so on.
	 */
	template <class T>
	class SOMA_API Singleton
	{
	public:
		/**
		 * Returns a pointer to the instance. If it has not been instantiated
		 * a NULL pointer will be returned.
		 * 
		 * @return A pointer to the instance of the singleton.
		 */
		static T* instancePtr () { return mInstance; }
		
		/**
		 * Returns a reference to the instance. If it has not been instantiated
		 * the assertion will fail in debug mode.
		 * 
		 * @return The instance of the singleton.
		 */
		static T& instance ()
		{
			assert (mInstance);
			return *mInstance;
		}
		
		
	protected:
		/**
		 * Protected Singleton constructor.
		 * 
		 * The singleton constructor ensures an instance of the specified type
		 * doesn't already exist and if it does the assertion will fail
		 * in debug mode.
		 */
		Singleton ()
		{
			assert (!mInstance);
			mInstance = static_cast<T*> (this);
		}
		
		
		/**
		 * Protected Singleton destructor.
		 * 
		 * The singleton destructor clears the stored instance pointer to make
		 * sure that it cannot be used without returning an error and to allow
		 * the singleton being instantiated again.
		 */
		virtual ~Singleton () { mInstance = 0; }
		
		
	private:
		static T* mInstance;
	};
	
	
	
	/**
	 * The singleton instance pointer.
	 */
	template<class T> T* Singleton<T>::mInstance = 0;

}


#endif /* SOMA_SINGLETON_H_ */
