
#ifndef SOMA_WORLD_COMPONENT_H_
#define SOMA_WORLD_COMPONENT_H_


#include <Soma/Config.h>
#include <Soma/String.h>
#include <Soma/Component.h>
#include <Soma/SmartPointer.h>


namespace Soma
{

	/* forward declarations */
	class World;
	
	
	
	/**
	 * World component class.
	 * 
	 * @ingroup components
	 */
	class SOMA_API WorldComponent : public Component
	{
	public:
		/**
		 * Destructor.
		 */
		virtual ~WorldComponent () {}
		
		
		
		/**
		 * @copydoc Component::getType
		 */
		virtual const String& getType() const = 0;
		
		
		/**
		 * @copydoc Component::getOwner
		 */
		weak_ptr<Composite> getOwner() const;
		
		
		/**
		 * @copydoc Component::setOwner
		 */
		void setOwner (const weak_ptr<Composite>& owner);
		
		
		/**
		 * @copydoc Component::load
		 */
		virtual bool load () = 0;		
		
		/**
		 * @copydoc Component::reload
		 */
		virtual bool reload () = 0;
		
		/**
		 * @copydoc Component::unload
		 */
		virtual void unload () = 0;
		
		/**
		 * @copydoc Component::isLoaded
		 */
		virtual bool isLoaded() const = 0;
		
		
		
	protected:
		/**
		 * The world composite that owns this component.
		 */
		weak_ptr<World> mOwner;
	};

}


#endif /* SOMA_WORLD_COMPONENT_H_ */
