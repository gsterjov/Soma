
#ifndef SOMA_AUDIO_ALSA_H_
#define SOMA_AUDIO_ALSA_H_


#include <alsa/asoundlib.h>

#include <Soma/Config.h>
#include <Soma/String.h>
#include <Soma/Audio/Output.h>


namespace Soma {
namespace Audio {

	/**
	 * ALSA device for audio output.
	 */
	class SOMA_API Alsa : public Output
	{
	public:
		/**
		 * Constructor.
		 */
		Alsa ();
		
		/**
		 * Destructor
		 */
		~Alsa ();
		
		
		/**
		 * @copydoc Output::getName
		 */
		const String& getName() const { return mName; }
		
		
		/**
		 * @copydoc Output::open
		 */
		bool open ();
		
		/**
		 * @copydoc Output::close
		 */
		void close ();
		
		/**
		 * @copydoc Output::isOpened
		 */
		bool isOpened() const { return mOpened; }
		
		
		/**
		 * @copydoc Output::write
		 */
		int write (char* buffer, int length);
		
		
		
		/**
		 * Log source name.
		 */
		static const String LogSource;
		
		
		
	private:
		static const String mName;
		
		bool mOpened;
		
		snd_pcm_t* mPcm;
		
		snd_pcm_uframes_t mBufferSize;
		snd_pcm_uframes_t mPeriodSize;
		size_t mFrameSize;
		
		
		bool set_hw_params ();
		bool set_sw_params ();
	};

}}


#endif /* SOMA_AUDIO_ALSA_H_ */
