
#ifndef SOMA_AUDIO_RING_BUFFER_H_
#define SOMA_AUDIO_RING_BUFFER_H_


#include <stdexcept>
#include <iterator>

#include <Soma/Config.h>


namespace Soma {
namespace Audio {

	/**
	 * A ring buffer container.
	 */
	template <typename T, typename Allocator = std::allocator<T> >
	class SOMA_API RingBuffer
	{
	public:
		/* iterators */
		class iterator;
		class const_iterator;
		
		typedef std::reverse_iterator<iterator> reverse_iterator;
		typedef std::reverse_iterator<const_iterator> const_reverse_iterator;
		
		
		/**
		 * STL typedefs.
		 */
		typedef T                                         value_type;
		typedef Allocator                                 allocator_type;
		typedef RingBuffer<T, Allocator>                  self_type;
		typedef typename allocator_type::size_type        size_type;
		typedef typename allocator_type::difference_type  difference_type;
		typedef typename allocator_type::reference        reference;
		typedef typename allocator_type::const_reference  const_reference;
		typedef typename allocator_type::pointer          pointer;
		typedef typename allocator_type::const_pointer    const_pointer;
		
		
		
		/**
		 * Constructor.
		 */
		explicit RingBuffer (size_type capacity,
		                     const allocator_type& alloc = allocator_type());
		
		/**
		 * Destructor.
		 */
		~RingBuffer ();
		
		
		/**
		 * Get the container allocator.
		 */
		allocator_type get_allocator() const { return mAllocator; }
		
		
		
		/**
		 * The amount of items in the ring buffer.
		 */
		size_type size() const { return !mFront ? 0 : (mBack > mFront ? mBack : mBack + mCapacity) - mFront; }
		
		
		/**
		 * The maxmimum size possible of the ring buffer.
		 */
		size_type max_size() const { return mAllocator.max_size(); }
		
		/**
		 * Is the ring buffer empty?
		 */
		bool empty() const { return !mFront; }
		
		/**
		 * The maximum amount of items the ring buffer can hold.
		 */
		size_type capacity() const { return mCapacity; }
		
		
		/**
		 * The start of the ring buffer.
		 */
		iterator begin() { return iterator (*this, 0); }
		
		/**
		 * The end of the ring buffer.
		 */
		iterator end() { return iterator (*this, size()); }
		
		/**
		 * The start of the ring buffer.
		 */
		const_iterator begin() const { return const_iterator (*this, 0); }
		
		/**
		 * The end of the ring buffer.
		 */
		const_iterator end() const { return const_iterator (*this, size()); }
		
		
		/**
		 * The start of the reversed ring buffer.
		 */
		reverse_iterator rbegin() { return reverse_iterator (end()); }
		
		/**
		 * The end of the reversed ring buffer.
		 */
		reverse_iterator rend() { return reverse_iterator (begin()); }
		
		/**
		 * The start of the reversed ring buffer.
		 */
		const_reverse_iterator rbegin() const { return const_reverse_iterator (end()); }
		
		/**
		 * The end of the reversed ring buffer.
		 */
		const_reverse_iterator rend() const { return const_reverse_iterator (begin()); }
		
		
		
		/**
		 * Push an item into the ring buffer.
		 */
		bool push_back (const value_type& value);
		
		
		/**
		 * Pop an item at the front of the ring buffer.
		 */
		void pop_front();
		
		
		/**
		 * Remove all items from the ring buffer.
		 */
		void clear();
		
		
		/**
		 * Write an array of data to the ring buffer.
		 */
		void write (pointer data, size_type length);
		
		
		
		/**
		 * Get data at the front of the ring buffer.
		 */
		reference front() { assert(mFront); return *mFront; }
		
		/**
		 * Get data at the front of the ring buffer.
		 */
		const_reference front() const { assert(mFront); return *mFront; }
		
		/**
		 * Get data at the back of the ring buffer.
		 */
		reference back() { assert(mFront); return *wrap (mBack, -1); }
		
		/**
		 * Get data at the back of the ring buffer.
		 */
		const_reference back() const { assert(mFront); return *wrap (mBack, -1); }
		
		
		
		/**
		 * Get data at the specified index.
		 */
		reference operator[] (size_type index)
		{
			return *wrap (mFront, index);
		}
		
		/**
		 * Get data at the specified index.
		 */
		const_reference operator[] (size_type index) const
		{
			return *wrap (mFront, index);
		}
		
		/**
		 * Get data at the specified index.
		 */
		reference at (size_type index)
		{
			if (index >= size()) throw std::out_of_range ("Index out of range");
			return (*this)[index];
		}
		
		/**
		 * Get data at the specified index.
		 */
		const_reference at (size_type index) const
		{
			if (index >= size()) throw std::out_of_range ("Index out of range");
			return (*this)[index];
		}
		
		
		
	private:
		/* ring buffer properties */
		size_type      mCapacity;
		allocator_type mAllocator;
		pointer        mBuffer;
		pointer        mFront;
		pointer        mBack;
		
		
		
		/**
		 * Increments the pointer wrapping it around when needed.
		 */
		pointer wrap (pointer ptr, difference_type offset) const
		{
			difference_type diff = (ptr - mBuffer) + offset;
			
			/* wrap around to the end */
			if (diff < 0) return (mBuffer + mCapacity) + diff;
			
			/* wrap around to the start */
			else if (diff >= mCapacity) return mBuffer + (diff - mCapacity);
			
			/* go to the next slot */
			else return mBuffer + diff;
		}
		
		
		
		/* disable copy and assignment */
		typedef RingBuffer class_type;
		RingBuffer (const class_type&);
		class_type& operator= (const class_type&);
	};
	
	
	
	
	/* constructor */
	template <typename T, typename Allocator>
	RingBuffer<T, Allocator>::RingBuffer (size_type capacity,
	                                      const allocator_type& alloc)
	: mCapacity (capacity),
	  mAllocator (alloc),
	  mBuffer (mAllocator.allocate (capacity)),
	  mFront (0),
	  mBack (mBuffer)
	{
		
	}
	
	
	/* destructor */
	template <typename T, typename Allocator>
	RingBuffer<T, Allocator>::~RingBuffer ()
	{
		clear ();
		mAllocator.deallocate (mBuffer, mCapacity);
	}
	
	
	
	
	/* push item */
	template <typename T, typename Allocator>
	bool RingBuffer<T, Allocator>::push_back (const value_type& value)
	{
		/* destroy stale data */
		if (mFront && mFront == mBack)
			mAllocator.destroy (mBack);
		
		/* set data */
		mAllocator.construct (mBack, value);\
		
		
		/* get next empty spot */
		value_type* next = wrap (mBack, 1);
		
		
		/* empty buffer */
		if (!mFront)
		{
			mFront = mBack;
			mBack = next;
			return true;
		}
		
		/* full buffer */
		else if (mFront == mBack)
		{
			/* overwrite data */
			mFront = mBack = next;
			return false;
		}
		
		/* space available */
		else
		{
			mBack = next;
			return true;
		}
	}
	
	
	
	/* pop item */
	template <typename T, typename Allocator>
	void RingBuffer<T, Allocator>::pop_front ()
	{
		assert (mFront);
		
		/* destroy data */
		mAllocator.destroy (mFront);
		
		/* get next item spot */
		value_type* next = wrap (mFront, 1);
		
		/* no items left */
		if (next == mBack) mFront = 0;
		
		/* go to next item */
		else mFront = next;
	}
	
	
	
	/* clear buffer */
	template <typename T, typename Allocator>
	void RingBuffer<T, Allocator>::clear ()
	{
		/* destroy all items */
		if (mFront)
		{
			do
			{
				mAllocator.destroy (mFront);
				mFront = wrap (mFront, 1);
			}
			while (mFront != mBack);
		}
		
		mFront = 0;
	}
	
	
	
	
	/* write data */
	template <typename T, typename Allocator>
	void RingBuffer<T, Allocator>::write (pointer data, size_type length)
	{
		assert (length < mCapacity);
		
		
		for (int i = 0; i < length; ++i)
		{
			push_back (data[i]);
		}
		
		
		
		/* get available space from the front to the end */
//		if (!mFront) mFront = mBuffer;
//		difference_type chunk = (mBuffer + mCapacity) - mFront;
//		
//		/* copy into available space */
//		if (chunk >= length)
//		{
//			std::copy (data, data + length, mFront);
////			memcpy (mFront, data, length);
//			
//			mBack = mFront + length;
//		}
//		
//		/* copy and wrap around for available space */
//		else
//		{
//			std::copy (data, data + chunk, mFront);
//			std::copy (data + chunk, data + length, mBuffer);
//			
////			memcpy (mFront, data, chunk);
////			memcpy (mBuffer, data + chunk, length - chunk);
//			
//			mBack = mBuffer + (length - chunk);
//		}
	}
	
	
	
	
	
	
	/**
	 * Ring buffer random access iterator.
	 */
	template <typename T, typename Allocator>
	class RingBuffer<T, Allocator>::iterator : public std::iterator <std::random_access_iterator_tag,
	                                                                 value_type,
	                                                                 difference_type,
	                                                                 pointer,
	                                                                 reference>
	{
	public:
		/**
		 * STL iterator typedefs.
		 */
		typedef RingBuffer<T>                  parent_type;
		typedef typename parent_type::iterator self_type;
		
		
		/**
		 * Constructor.
		 */
		iterator (parent_type& parent, size_type index)
		: mParent (parent),
		  mIndex(index)
		{
		}
		
		
		/**
		 * Increment operator.
		 */
		self_type& operator++ ()
		{
			++mIndex;
			return *this;
		}
		
		/**
		 * Post increment operator.
		 */
		self_type& operator++ (int)
		{
			self_type old (*this);
			operator++();
			return *old;
		}
		
		/**
		 * Decrement operator.
		 */
		self_type& operator-- ()
		{
			--mIndex;
			return *this;
		}
		
		/**
		 * Post decrement operator.
		 */
		self_type& operator-- (int)
		{
			self_type old (*this);
			operator--();
			return *old;
		}
		
		
		/**
		 * Dereference operator.
		 */
		reference operator* () const { return mParent[mIndex]; }
		
		/**
		 * Indirection operator.
		 */
		pointer operator-> () const { return &(mParent[mIndex]); }
		
		
		/**
		 * Equality operator.
		 */
		bool operator== (const self_type& other) const
		{
			return &mParent == &other.mParent && mIndex == other.mIndex;
		}
		
		/**
		 * Inequality operator.
		 */
		bool operator!= (const self_type& other) const
		{
			return !(other == *this);
		}
		
		
	private:
		parent_type& mParent;
		size_type mIndex;
	};
	
	
	
	
	
	/**
	 * Ring buffer random access constant iterator.
	 */
	template <typename T, typename Allocator>
	class RingBuffer<T, Allocator>::const_iterator : public std::iterator <std::random_access_iterator_tag,
	                                                                       value_type,
	                                                                       difference_type,
	                                                                       const_pointer,
	                                                                       const_reference>
	{
	public:
		/**
		 * STL iterator typedefs.
		 */
		typedef RingBuffer<T>                        parent_type;
		typedef typename parent_type::const_iterator self_type;
		
		
		/**
		 * Constructor.
		 */
		const_iterator (parent_type& parent, size_type index)
		: mParent (parent),
		  mIndex(index)
		{
		}
		
		
		/**
		 * Increment operator.
		 */
		self_type& operator++ ()
		{
			++mIndex;
			return *this;
		}
		
		/**
		 * Post increment operator.
		 */
		self_type& operator++ (int)
		{
			self_type old (*this);
			operator++();
			return *old;
		}
		
		/**
		 * Decrement operator.
		 */
		self_type& operator-- ()
		{
			--mIndex;
			return *this;
		}
		
		/**
		 * Post decrement operator.
		 */
		self_type& operator-- (int)
		{
			self_type old (*this);
			operator--();
			return *old;
		}
		
		
		/**
		 * Dereference operator.
		 */
		const_reference operator* () const { return mParent[mIndex]; }
		
		/**
		 * Indirection operator.
		 */
		const_pointer operator-> () const { return &(mParent[mIndex]); }
		
		
		/**
		 * Equality operator.
		 */
		bool operator== (const self_type& other) const
		{
			return &mParent == &other.mParent && mIndex == other.mIndex;
		}
		
		/**
		 * Inequality operator.
		 */
		bool operator!= (const self_type& other) const
		{
			return !(other == *this);
		}
		
		
	private:
		parent_type& mParent;
		size_type mIndex;
	};


}}


#endif /* SOMA_AUDIO_RING_BUFFER_H_ */
