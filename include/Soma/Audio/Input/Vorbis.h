
#ifndef SOMA_AUDIO_VORBIS_H_
#define SOMA_AUDIO_VORBIS_H_


#include <Soma/Config.h>
#include <Soma/String.h>
#include <Soma/Audio/Input.h>


/* forward declarations */
struct OggVorbis_File;


namespace Soma {
namespace Audio {

	/**
	 * Vorbis input decoder.
	 */
	class SOMA_API Vorbis : public Input
	{
	public:
		/**
		 * Constructor.
		 */
		Vorbis ();
		
		/**
		 * Destructor.
		 */
		~Vorbis ();
		
		
		
		/**
		 * @copydoc Input::open
		 */
		bool open (const String& path);
		
		/**
		 * @copydoc Input::close
		 */
		void close ();
		
		/**
		 * @copydoc Input::isOpened
		 */
		bool isOpened() { return mOpened; }
		
		
		
		/**
		 * @copydoc Input::read
		 */
		int read (float* buffer, int samples);
		
		
		
		/**
		 * Log source name.
		 */
		static const String LogSource;
		
		
		
	private:
		OggVorbis_File* mFile;
		
		bool mOpened;
	};

}}


#endif /* SOMA_AUDIO_VORBIS_H_ */
