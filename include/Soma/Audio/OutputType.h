
#ifndef SOMA_AUDIO_OUTPUT_TYPE_H_
#define SOMA_AUDIO_OUTPUT_TYPE_H_


#include <list>

#include <Soma/Config.h>
#include <Soma/String.h>
#include <Soma/Audio/Output.h>


namespace Soma {
namespace Audio {


	struct OutputType;
	
	/**
	 * Output type list.
	 */
	typedef std::list<OutputType*> OutputTypes;
	
	
	/**
	 * Gets a list of all registered outputs.
	 */
	extern OutputTypes& getOutputTypes();
	
	
	
	/**
	 * Structure defining the type of an output.
	 */
	struct OutputType
	{
		/**
		 * Auto registering constructor.
		 */
		OutputType () { getOutputTypes().push_back (this); }
		
		/**
		 * Destructor.
		 */
		virtual ~OutputType () {}
		
		
		/**
		 * The name of the output.
		 */
		virtual String getName() = 0;
		
		/**
		 * Create an instance of the output.
		 */
		virtual Output* create() = 0;
		
		/**
		 * Destroy an instance of the output.
		 */
		virtual void destroy (Output* output) = 0;
	};


}}



#endif /* SOMA_AUDIO_OUTPUT_TYPE_H_ */
