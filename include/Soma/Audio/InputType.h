
#ifndef SOMA_AUDIO_INPUT_TYPE_H_
#define SOMA_AUDIO_INPUT_TYPE_H_


#include <list>

#include <Soma/Config.h>
#include <Soma/String.h>
#include <Soma/Audio/Input.h>


namespace Soma {
namespace Audio {


	struct InputType;
	
	/**
	 * Input type list.
	 */
	typedef std::list<InputType*> InputTypes;
	
	
	/**
	 * Gets a list of all registered inputs.
	 */
	extern InputTypes& getInputTypes();
	
	
	
	/**
	 * Structure defining the type of an input.
	 */
	struct InputType
	{
		/**
		 * Auto registering constructor.
		 */
		InputType () { getInputTypes().push_back (this); }
		
		/**
		 * Destructor.
		 */
		virtual ~InputType () {}
		
		
		/**
		 * The name of the input.
		 */
		virtual String getName() = 0;
		
		/**
		 * Create an instance of the input.
		 */
		virtual Input* create() = 0;
		
		/**
		 * Destroy an instance of the input.
		 */
		virtual void destroy (Input* input) = 0;
	};


}}



#endif /* SOMA_AUDIO_INPUT_TYPE_H_ */
