
#ifndef SOMA_AUDIO_INPUT_H_
#define SOMA_AUDIO_INPUT_H_


#include <Soma/Config.h>
#include <Soma/String.h>
#include <Soma/SmartPointer.h>


namespace Soma {
namespace Audio {

	/**
	 * Audio input reader.
	 */
	class SOMA_API Input : public ReferenceCounter
	{
	public:
		/**
		 * Constructor.
		 */
		Input () {}
		
		/**
		 * Destructor.
		 */
		virtual ~Input () {}
		
		
		
		/**
		 * Open a file.
		 * @return True if successful, false otherwise.
		 */
		virtual bool open (const String& path) = 0;
		
		/**
		 * Close a file.
		 */
		virtual void close () = 0;
		
		/**
		 * Is the file open?
		 * @return True if opened, false otherwise.
		 */
		virtual bool isOpened() = 0;
		
		
		
		/**
		 * Read samples into the buffer.
		 * 
		 * @param buffer The buffer to store decoded samples in.
		 * @param samples The maximum amount of samples to read.
		 * 
		 * @return The actual amount of samples decoded and read.
		 */
		virtual int read (float* buffer, int samples) = 0;
	};

}}


#endif /* SOMA_AUDIO_INPUT_H_ */
