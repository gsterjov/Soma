
#ifndef SOMA_AUDIO_OUTPUT_H_
#define SOMA_AUDIO_OUTPUT_H_


#include <Soma/Config.h>
#include <Soma/String.h>
#include <Soma/SmartPointer.h>


namespace Soma {
namespace Audio {

	/**
	 * Audio output.
	 */
	class SOMA_API Output : public ReferenceCounter
	{
	public:
		/**
		 * Constructor.
		 */
		Output () {}
		
		/**
		 * Destructor.
		 */
		virtual ~Output () {}
		
		
		/**
		 * Get output name.
		 * @return The name of the output.
		 */
		virtual const String& getName() const = 0;
		
		
		/**
		 * Open the output device.
		 * @return True if successful, false otherwise.
		 */
		virtual bool open () = 0;
		
		/**
		 * Close the output device.
		 */
		virtual void close () = 0;
		
		/**
		 * Is the output device open?
		 * @return True if opened, false otherwise.
		 */
		virtual bool isOpened() const = 0;
		
		
		
		/**
		 * Write the buffer to the output device.
		 */
		virtual int write (char* buffer, int length) = 0;
	};

}}


#endif /* SOMA_AUDIO_OUTPUT_H_ */
