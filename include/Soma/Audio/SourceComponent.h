
#ifndef SOMA_AUDIO_SOURCE_H_
#define SOMA_AUDIO_SOURCE_H_


#include <Soma/Config.h>
#include <Soma/String.h>
#include <Soma/EntityComponent.h>


namespace Soma {
namespace Audio {


	/* forward declarations */
	class Input;
	
	
	
	/**
	 * Audio sample buffer.
	 */
	struct SOMA_API SampleBuffer
	{
		int length;
		float* samples;
	};
	
	
	
	
	/**
	 * The source of a 3D positioned sound.
	 * 
	 * @properties
	 * @prop{media-file, String, Read/Write, The loaded media file}
	 * @prop{playing, bool, Read, Is the source currently playing?}
	 * @endproperties
	 * 
	 * @ingroup components
	 */
	class SOMA_API SourceComponent : public EntityComponent
	{
	public:
		/**
		 * Constructor.
		 */
		SourceComponent ();
		
		/**
		 * Destructor.
		 */
		~SourceComponent ();
		
		
		/**
		 * @copydoc EntityComponent::getType
		 */
		const String& getType() const { return Type; }
		
		
		
		/**
		 * @copydoc EntityComponent::load
		 */
		bool load () { return true; }
		
		/**
		 * @copydoc EntityComponent::reload
		 */
		bool reload () { return true; }
		
		/**
		 * @copydoc EntityComponent::unload
		 */
		void unload () {}
		
		/**
		 * @copydoc EntityComponent::unload
		 */
		bool isLoaded () const { return true; }
		
		
		
		/**
		 * Play the specified media.
		 * @param path The path to the media to play.
		 */
		bool play (const String& path);
		
		/**
		 * Stop playing the current media.
		 */
		void stop ();
		
		
		/**
		 * Read samples from the audio source.
		 * @param samples Maximum amount of samples to read.
		 * @return A buffer containing audio samples from the source.
		 */
		const SampleBuffer& read (int samples);
		
		
		/**
		 * Is the source currently playing media.
		 * @return True if playing, false otherwise.
		 */
		bool isPlaying() { return mPlaying; }
		
		
		
		/** Log source name. */
		static const String LogSource;
		
		/** Component type name. */
		static const String Type;
		
		
		
	private:
		bool mPlaying;
		
		smart_ptr<Input> mInput;
		SampleBuffer mBuffer;
	};

}}


#endif /* SOMA_AUDIO_SOURCE_H_ */
