
#ifndef SOMA_AUDIO_MIXER_H_
#define SOMA_AUDIO_MIXER_H_


#include <vector>

#include <Soma/Config.h>
#include <Soma/SmartPointer.h>
#include <Soma/Audio/RingBuffer.h>

/* FIXME: replace pthread with a cross platform one */
#include <pthread.h>


namespace Soma {
namespace Audio {


	/* forward declarations */
	class Output;
	class SourceComponent;
	
	
	/**
	 * Sound mixer.
	 */
	class SOMA_API Mixer : public ReferenceCounter
	{
	public:
		/**
		 * Constructor.
		 */
		Mixer ();
		
		/**
		 * Destructor.
		 */
		~Mixer ();
		
		
		/**
		 * Set the output to write the final audio data on.
		 * @param output An audio output.
		 */
		void setOutput (const smart_ptr<Output>& output) { mOutput = output; }
		
		
		/**
		 * Add a source to be mixed into the final output.
		 * @param source A source to mix in.
		 */
		void addSource (const smart_ptr<SourceComponent>& source);
		
		
		/**
		 * Remove a source from the final output.
		 * @param source The source to remove from the mix.
		 */
		void removeSource (const smart_ptr<SourceComponent>& source);
		
		
		/**
		 * Mix the current sources and output audio samples.
		 */
		void mix ();
		void mix2();
		
		
		/**
		 * Constantly drains the mixer buffer.
		 */
		void _drainLoop ();
		
		
		
	private:
		bool mRunning;
		pthread_t mThread;
		pthread_cond_t mCond;
		pthread_mutex_t mMutex;
		
		smart_ptr<Output> mOutput;
		
		
		std::vector<smart_ptr<SourceComponent> > mSources;
		
		std::vector<int> mLengths;
		RingBuffer<float> mBuffer;
	};

}}


#endif /* SOMA_AUDIO_MIXER_H_ */
