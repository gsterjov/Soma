
#ifndef SOMA_AUDIO_LISTENER_H_
#define SOMA_AUDIO_LISTENER_H_


#include <Soma/Config.h>
#include <Soma/String.h>
#include <Soma/SmartPointer.h>


namespace Soma {
namespace Audio {

	/**
	 * The listener of 3D positioned sounds.
	 */
	class SOMA_API Listener : public ReferenceCounter
	{
	public:
		/**
		 * Constructor.
		 * @param name The name of the listener.
		 */
		explicit Listener (const String& name);
		
		/**
		 * Destructor.
		 */
		~Listener ();
		
		
		/**
		 * The listener name.
		 * @return The name of the listener.
		 */
		const String& getName() { return mName; }
		
		
	private:
		String mName;
	};

}}


#endif /* SOMA_AUDIO_SOURCE_H_ */
