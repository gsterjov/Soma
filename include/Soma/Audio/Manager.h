
#ifndef SOMA_AUDIO_MANAGER_H_
#define SOMA_AUDIO_MANAGER_H_


#include <Soma/Config.h>
#include <Soma/Singleton.h>
#include <Soma/SubSystem.h>
#include <Soma/Configurable.h>
#include <Soma/SmartPointer.h>


namespace Soma {
namespace Audio {


	/* forward declarations */
	class Mixer;
	class Output;
	
	
	/**
	 * The 3D position audio system.
	 * 
	 * The audio manager is responsible for synchronising the various audio
	 * sources throughout the scene and queueing buffers for mixing in the
	 * listeners perspective. The Soma Audio System can be separated into three
	 * logical parts, the high-level 3D positioning and playback layer, the
	 * effects and filters layer and finally the low-level system dependent
	 * audio API layer.
	 */
	class SOMA_API Manager : public Singleton<Manager>,
	                         public SubSystem,
	                         public Configurable
	{
	public:
		/**
		 * Constructor.
		 */
		Manager ();
		
		/**
		 * Destructor.
		 */
		~Manager ();
		
		
		/**
		 * Initialises the audio manager.
		 * 
		 * Once initialised the audio device will be ready to receive input
		 * and process the circular buffer until it is shutdown. However, the
		 * input is handled transparently through the use of higher level
		 * features such as audio Sources and Listeners.
		 * 
		 * @return True if successful, false otherwise.
		 */
		bool initialise ();
		
		/**
		 * Reinitialises the audio manager.
		 * 
		 * When a setting is changed the audio device must be brought down and
		 * started up again for any changes to take effect. Though effects
		 * and filters can be changed on the fly without the need for
		 * reinitialisation, low-level settings on the audio device can only
		 * be applied once it has been closed and opened again as the
		 * input buffer depends on the device settings.
		 * 
		 * @return True if successful, false otherwise.
		 */
		bool reinitialise ();
		
		/**
		 * Shuts down the audio manager.
		 * 
		 * Shutting down the audio manager will close the audio device and
		 * effectively stop any audio processing to occur. However, unlike
		 * other subsystems, it does not invalidate the objects created within
		 * the Audio namespace and can continue to be used despite not having
		 * any real effect on the output.
		 */
		void shutdown ();
		
		/**
		 * Is the audio manager initialised?
		 * @return True if initialised, false otherwise.
		 */
		bool isInitialised() { return mInitialised; }
		
		/**
		 * Steps through the audio 'scene graph' within the elapsed time.
		 * 
		 * It is important to note that this does not in any way control
		 * the output of audio. Rather, it simply updates the audio sources
		 * and listeners in order to calculate the final 3D positional output.
		 * This will effectively give the '3D' effect to audio sources that
		 * within the listeners hearing.
		 */
		void step ();
		
		
		
		/* Configurable implementation */
		bool save    (ConfigInterface& config);
		bool restore (const ConfigInterface& config);
		
		
		
		/**
		 * Get the currently active output.
		 */
		const smart_ptr<Output>& getOutput() { return mOutput; }
		
		/**
		 * Get the currently active mixer.
		 */
		const smart_ptr<Mixer>& getMixer() { return mMixer; }
		
		
		
		/** Log source name. */
		static String LogSource;
		
		
		
	private:
		bool mInitialised;
		
		smart_ptr<Mixer> mMixer;
		smart_ptr<Output> mOutput;
	};

}}


#endif /* SOMA_AUDIO_MANAGER_H_ */
