
#ifndef SOMA_LOG_H_
#define SOMA_LOG_H_


#include <Soma/Config.h>
#include <Soma/String.h>
#include <Soma/Singleton.h>

#include <fstream> /* ofstream */

#ifdef SOMA_DEBUG
#include <iostream>
#endif


namespace Soma
{

	/**
	 * Exposes global logging functions.
	 * 
	 * The Log class allows anyone to log messages to a log file and to the
	 * standard output.
	 */
	class SOMA_API Log : public Singleton<Log>
	{
	public:
		/**
		 * Log verbosity level.
		 */
		enum Level
		{
			TRACE   = 0,  /** Trace information. */
			DEBUG   = 1,  /** Debug information. */
			NOTIFY  = 2,  /** Notification information. */
			WARNING = 3,  /** Warning information. */
			ERROR   = 4,  /** Error information. */
			NONE    = 5   /** Disable logging. */
		};
		
		
		/**
		 * Constructor.
		 * 
		 * The Log class must first be instantiated before it can be
		 * used. The Engine class automatically creates the Log once it is
		 * instantiated so most would only create a Log class if they want to
		 * change the default properties of a log such as the file name.
		 * 
		 * @param filename The name of the log file to write in.
		 */
		explicit Log (const String& filename = "Soma.log");
		
		/**
		 * Destructor.
		 * Closes the log file upon destruction.
		 */
		virtual ~Log ();
		
		
		/**
		 * Set how much information to output.
		 * @param verbosity The verbosity of the log output.
		 */
		void setVerbosity (Level verbosity) { mVerbosity = verbosity; }
		
		
		/**
		 * Sends a trace message to the log output.
		 * 
		 * @param source The source of the trace message.
		 * @param trace The trace message to log.
		 */
		void sendTrace (const String& source, const String& trace);
		
		/**
		 * Sends a debug message to the log output.
		 * 
		 * @param source The source of the debug message.
		 * @param debug The debug message to log.
		 */
		void sendDebug (const String& source, const String& debug);
		
		/**
		 * Sends an information message to the log output.
		 * 
		 * @param source The source of the information message.
		 * @param message The message to log.
		 */
		void sendMessage (const String& source, const String& message);
		
		/**
		 * Sends a warning message to the log output.
		 * 
		 * @param source The source of the warning.
		 * @param warning The warning to log.
		 */
		void sendWarning (const String& source, const String& warning);
		
		/**
		 * Sends an error message to the log output.
		 * 
		 * @param source The source of the error.
		 * @param error The error to log.
		 */
		void sendError (const String& source, const String& error);
		
		
		
		/**
		 * Convenience function. Sends a trace message to the log output.
		 * 
		 * @param source The source of the trace message.
		 * @param trace The trace message to log.
		 */
		static void Trace (const String& source, const String& trace)
		{
#ifdef SOMA_DEBUG
			Log::instance().sendTrace (source, trace);
#endif
		}
		
		
		/**
		 * Convenience function. Sends a debug message to the log output.
		 * 
		 * @param source The source of the debug message.
		 * @param debug The debug message to log.
		 */
		static void Debug (const String& source, const String& debug)
		{
#ifdef SOMA_DEBUG
			Log::instance().sendDebug (source, debug);
#endif
		}
		
		
		/**
		 * Convenience function. Sends an information message to the log output.
		 * 
		 * @param source The source of the information message.
		 * @param message The message to log.
		 */
		static void Message (const String& source, const String& message)
		{ Log::instance().sendMessage (source, message); }
		
		
		/**
		 * Convenience function. Sends a warning message to the log output.
		 * 
		 * @param source The source of the warning message.
		 * @param warning The warning to log.
		 */
		static void Warning (const String& source, const String& warning)
		{ Log::instance().sendWarning (source, warning); }
		
		
		/**
		 * Convenience function. Sends an error message to the log output
		 * 
		 * @param source The source of the error message.
		 * @param error The error to log.
		 * 
		 * @return Always false as a convenience to calling functions.
		 */
		static bool Error (const String& source, const String& error)
		{ Log::instance().sendError (source, error); return false; }
		
		
		
		/**
		 * Log source name.
		 */
		static String LogSource;
		
		
	private:
		std::ofstream mFile;
		Level mVerbosity;
	};

}


#endif /* SOMA_LOG_H_ */
