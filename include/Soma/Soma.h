
#ifndef SOMA_H_
#define SOMA_H_


#include <Soma/Types.h>
#include <Soma/String.h>
#include <Soma/SmartPointer.h>

#include <Soma/Engine.h>
#include <Soma/Exception.h>

#include <Soma/Context.h>
#include <Soma/World.h>

#include <Soma/Entity.h>
#include <Soma/AssetEntity.h>


#include <Soma/GUI/GUI.h>
#include <Soma/Input/Input.h>
#include <Soma/Visual/Visual.h>


#endif /* SOMA_H_ */
