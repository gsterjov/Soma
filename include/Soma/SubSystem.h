
#ifndef SOMA_SUBSYSTEM_H_
#define SOMA_SUBSYSTEM_H_


#include <Soma/Config.h>


namespace Soma
{

	/**
	 * Abstract subsystem class.
	 * 
	 * An engine subsystem is a class which provides external functionality
	 * to the application. They can either be self contained systems running
	 * in the background or fully fledged systems that open up an entire
	 * namespace such as the Renderer subsystem enabling the Graphics
	 * namespace and thus providing the ability to use the graphics card.
	 * 
	 * Often a subsystem will expose access to a shared resource such as audio,
	 * input and graphics but it is in no way limited to it. In essence
	 * anything that needs a controlled initialisation should implement the
	 * SubSystem interface.
	 */
	class SOMA_API SubSystem
	{
	public:
		/**
		 * Destructor.
		 */
		virtual ~SubSystem () {}
		
		
		/**
		 * Initialise the subsystem.
		 * 
		 * This will load the system into a working order and all features
		 * supported by it should be functional.
		 * 
		 * @return True if successful, false otherwise.
		 */
		virtual bool initialise () = 0;
		
		/**
		 * Reinitialise the subsystem.
		 * 
		 * This will effectively reload the system settings without
		 * requiring it to be shutdown() and initialise()'ed again.
		 * 
		 * @return True if successful, false otherwise.
		 */
		virtual bool reinitialise () = 0;
		
		/**
		 * Shutdown the subsystem.
		 * 
		 * Shutting down a system shoud invalidate any features directly
		 * implemented by the system and as such all calls involving the
		 * subsystem will be undefined.
		 */
		virtual void shutdown () = 0;
		
		
		/**
		 * Is the subsystem already initialised?
		 * @return True if initialised, false otherwise.
		 */
		virtual bool isInitialised() = 0;
		
		
		
		/**
		 * Process a single game tick.
		 * 
		 * Systems that need to be kept in sync with the game loop should
		 * implement this method as it will be called on every tick during
		 * the entire runtime of the engine, ie. whenever Engine::step()
		 * is called. This effectively allows for a subsystem to be 'injected'
		 * into the engine main loop with very little effort.
		 * 
		 * Note, however, that not all systems might need to be kept in
		 * sync with the game loop thus a default implementation is defined.
		 */
		virtual void step () {}
	};

}


#endif /* SOMA_SUBSYSTEM_H_ */
