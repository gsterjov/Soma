
#ifndef SOMA_WORLD_H_
#define SOMA_WORLD_H_


#include <map>
#include <vector>

#include <Soma/Config.h>
#include <Soma/String.h>
#include <Soma/Composite.h>


namespace Soma
{

	/* forward declarations */
	class Entity;
	
	
	/** A list of entities. */
	typedef std::vector<smart_ptr<Entity> > EntityList;
	
	
	
	/**
	 * The world class.
	 * 
	 * The World class is where everything takes place. It defines a simple
	 * coordinate system which objects from various subsystems use for its
	 * placement within the world. In essence this class acts more like a
	 * generic world whereby other worlds are encapsulated such as the physics,
	 * graphics and audio worlds. If the subsystem is available it will work
	 * seamlessly due to this higher abstraction of the world concept.
	 * 
	 * @ingroup components
	 */
	class SOMA_API World : public Composite
	{
	public:
		/**
		 * Named constructor.
		 * @param name The name of this world.
		 */
		explicit World (const String& name);
		
		/**
		 * Constructor with components.
		 * 
		 * Allows easy construction of a world by providing a list of
		 * components to add and load after creation.
		 * 
		 * @param name The name of the world.
		 * @param types The types of components to add separated by a comma.
		 */
		World (const String& name, const String& types);
		
		/**
		 * Destructor.
		 * Upon destructor all entities will be unloaded.
		 */
		~World ();
		
		
		/**
		 * Get the name of this world.
		 * @return The world's name.
		 */
		const String& getName() const { return mName; }
		
		
		/**
		 * Get an entity from the world.
		 * 
		 * @param name The name of the entity to retrieve.
		 * @return The entity if found, NULL reference otherwise.
		 */
		smart_ptr<Entity> getEntity (const String& name) const;
		
		/**
		 * Get a list of all the entities in the world.
		 * @return A list of entities.
		 */
		EntityList getEntities() const;
		
		
		
		/**
		 * Add an entity to the world.
		 * 
		 * @note If the world is already loaded it will automatically load the
		 * entity to keep it in sync.
		 * 
		 * @param entity The entity to add.
		 */
		void add (const smart_ptr<Entity>& entity);
		
		/**
		 * Remove an entity from the world.
		 * @note Removing an entity will also unload it.
		 * 
		 * @param entity The entity to remove.
		 */
		void remove (const smart_ptr<Entity>& entity);
		
		/**
		 * Remove an entity from the world by name.
		 * @note Removing an entity will also unload it.
		 * 
		 * @param name The name of the entity to remove.
		 */
		void remove (const String& name);
		
		
		
		/**
		 * @copydoc Composite::load
		 */
		bool load ();
		
		/**
		 * @copydoc Composite::reload
		 */
		bool reload ();
		
		/**
		 * @copydoc Composite::unload
		 */
		void unload ();
		
		
		
		/**
		 * A new entity was added to the world.
		 * @param entity The newly added entity.
		 */
		Event<void, const smart_ptr<Entity>&> EntityAdded;
		
		/**
		 * An entity was removed from the world.
		 * @param entity The removed entity.
		 */
		Event<void, const smart_ptr<Entity>&> EntityRemoved;
		
		
		
		
		/** Log source name. */
		static const String LogSource;
		
		
		
	private:
		String mName;
		
		/* objects */
		typedef std::map<String, smart_ptr<Entity> > EntityMap;
		EntityMap mEntities;
	};

}


#endif /* SOMA_WORLD_H_ */
