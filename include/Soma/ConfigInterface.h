
#ifndef SOMA_CONFIG_INTERFACE_H_
#define SOMA_CONFIG_INTERFACE_H_


#include <Soma/Config.h>
#include <Soma/Types.h>
#include <Soma/String.h>


namespace Soma
{

	/**
	 * Abstract configuration class.
	 * 
	 * The ConfigInterface class is inherited to provide a generic interface
	 * to classes that provide configuration persistence. In this way a
	 * subsystem can implement the Configurable class and still provide
	 * a flexible means of saving those settings since its configuraiton will
	 * be saved with a class implementing ConfigInterface. As such any
	 * subsystem can offer a means to save onto disk, network or any other way.
	 * 
	 * @see Configurable::save() and Configurable::restore()
	 */
	class SOMA_API ConfigInterface
	{
	public:
		/**
		 * Saves configuration values.
		 * 
		 * Though some implementations may write immediately when a
		 * configuration is set (eg. databases), calling save will write
		 * all changes (if necessary) to the configuration output. Because
		 * many configuration values may be set it is recommended that
		 * configuration implementors cache values and write them in a batch
		 * once save is called.
		 * 
		 * @return True if successful, false otherwise.
		 */
		virtual bool save () = 0;
		
		/**
		 * Restores previously saved configuration values.
		 * @return True if successful, false otherwise.
		 */
		virtual bool restore () = 0;
		
		
		
		/**
		 * Check to see if the name already exists.
		 * 
		 * Looks for the name in the configuration and determines if it
		 * already exists with a value applied.
		 * 
		 * @param name The configuration name to look for.
		 * @return True if found, false otherwise.
		 */
		virtual bool exists (const String& name) const = 0;
		
		
		
		/**
		 * Retrieves the value of the configuration.
		 * 
		 * This will return the value of the configuration specified by its
		 * name and if no configuration is found then its default value
		 * will be returned instead.
		 * 
		 * @param name The name of the configuration.
		 * @param defaultValue The value to return if the configuration name 
		 * doesn't already exist.
		 * 
		 * @return The configuration value.
		 */
		virtual int get (const String& name, int defaultValue) const = 0;
		
		/**
		 * @copydoc ConfigInterface::get(const String& name, int defaultValue) const
		 */
		virtual uint get (const String& name, uint defaultValue) const = 0;
		
		/**
		 * @copydoc ConfigInterface::get(const String& name, int defaultValue) const
		 */
		virtual bool get (const String& name, bool defaultValue) const = 0;
		
		/**
		 * @copydoc ConfigInterface::get(const String& name, int defaultValue) const
		 */
		virtual float get (const String& name, float defaultValue) const = 0;
		
		/**
		 * @copydoc ConfigInterface::get(const String& name, int defaultValue) const
		 */
		virtual String get (const String& name, const char* defaultValue) const = 0;
		
		/**
		 * @copydoc ConfigInterface::get(const String& name, int defaultValue) const
		 */
		virtual String get (const String& name, const String& defaultValue) const = 0;
		
		
		
		/**
		 * Sets the configuration value.
		 * 
		 * This will set the value of the configuration specified by its name
		 * and if no configuration is found by that name it will be created.
		 * 
		 * @param name The name of the configuration.
		 * @param value The configuration value to set.
		 * 
		 * @return True if successful, false otherwise..
		 */
		virtual bool set (const String& name, int value) = 0;
		
		/**
		 * @copydoc ConfigInterface::set(const String& name, int value)
		 */
		virtual bool set (const String& name, uint value) = 0;
		
		/**
		 * @copydoc ConfigInterface::set(const String& name, int value)
		 */
		virtual bool set (const String& name, bool value) = 0;
		
		/**
		 * @copydoc ConfigInterface::set(const String& name, int value)
		 */
		virtual bool set (const String& name, float value) = 0;
		
		/**
		 * @copydoc ConfigInterface::set(const String& name, int value)
		 */
		virtual bool set (const String& name, const char* value) = 0;
		
		/**
		 * @copydoc ConfigInterface::set(const String& name, int value)
		 */
		virtual bool set (const String& name, const String& value) = 0;
	};

}


#endif /* SOMA_CONFIG_INTERFACE_H_ */
