
#ifndef SOMA_INPUT_ACTION_H_
#define SOMA_INPUT_ACTION_H_


#include <Soma/Config.h>
#include <Soma/String.h>
#include <Soma/Event.h>
#include <Soma/SmartPointer.h>


namespace Soma {
namespace Input {


	/* forward declarations */
	class Binding;
	class State;
	class Context;
	
	
	/**
	 * Input action.
	 * 
	 * Action is an abstraction of lower-level input events which associates
	 * a given input sequence with a specific action to be processed by the
	 * action holder. This allows us to approach controls with a view on
	 * actions rather than devices effectively allowing for much more
	 * flexibility when it comes to input devices.
	 */
	class SOMA_API Action : public ReferenceCounter
	{
	public:
		/**
		 * Constructor.
		 */
		Action ();
		
		/**
		 * Destructor.
		 */
		virtual ~Action ();
		
		
		/**
		 * Get the name of this action.
		 * @return The action name.
		 */
		virtual const String& getName() = 0;
		
		/**
		 * Has the action been activated?
		 * @return True if activated, false otherwise.
		 */
		virtual bool isActive() = 0;
		
		
		
		/**
		 * Get the context this action is attached to.
		 * @return The attached context.
		 */
		const smart_ptr<Context>& getContext() const { return mContext; }
		
		
		/**
		 * Set the context to attach this action to.
		 * @param context The context to attach to.
		 */
		void setContext (const smart_ptr<Context>& context) { mContext = context; }
		
		
		
		/**
		 * Add a binding for monitoring by the action.
		 * 
		 * This function can be called many times on a single action to allow
		 * alternative bindings to be utilised as well as the first one.
		 * 
		 * @param binding The binding instance to monitor.
		 */
		virtual void addBinding (Binding* binding) = 0;
		
		/**
		 * Remove a binding from the action.
		 * @param binding The binding instance to stop monitoring.
		 */
		virtual void removeBinding (Binding* binding) = 0;
		
		
		
		/**
		 * Bind action to the specified input sequence.
		 * 
		 * This is a convenience method to allow for easy input binding
		 * with any device by using a simple string of the form
		 * [device]/[input], where device is a device name and input is the
		 * input name. ie:
		 * 
		 * @code
		 * quit->bind ("Keyboard/Escape");
		 * quit->bind ("Keyboard/Left Control", "Keyboard/Q");
		 * @endcode
		 * 
		 * It can be called multiple times to create alternative bindings.
		 * 
		 * @param input The device input to bind this action to.
		 */
		void bind (const String& input);
		
		/**
		 * @copydoc Action::bind
		 */
		void bind (const String& input1,
		           const String& input2);
		
		/**
		 * @copydoc Action::bind
		 */
		void bind (const String& input1,
		           const String& input2,
		           const String& input3);
		
		
		
		
		/**
		 * The action has been activated.
		 * @param action The activated action.
		 */
		Event<void, const weak_ptr<Action>&> Activated;
		
		/**
		 * The action has been deactivated.
		 * @param action The deactivated action.
		 */
		Event<void, const weak_ptr<Action>&> Deactivated;
		
		
		
		/** Log source name. */
		static const String LogSource;
		
		
		
	private:
		smart_ptr<Context> mContext;
		
		/**
		 * Finds a state within the device.
		 * 
		 * @param input The input of the form [device]/[input]
		 * @return The device state if found, NULL otherwise.
		 */
		State* findState (const String& input);
	};

}}


#endif /* SOMA_INPUT_ACTION_H_ */
