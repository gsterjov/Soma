
#ifndef SOMA_INPUT_TRIGGER_ACTION_H_
#define SOMA_INPUT_TRIGGER_ACTION_H_


#include <vector>

#include <Soma/Config.h>
#include <Soma/Input/Action.h>
#include <Soma/Input/Binding.h>


namespace Soma {
namespace Input {

	/**
	 * Trigger Action.
	 * 
	 * A trigger action activates whenever a binding has all states activated.
	 */
	class SOMA_API TriggerAction : public Action,
	                               public Binding::Listener
	{
	public:
		/**
		 * Named constructor.
		 */
		explicit TriggerAction (const String& name = "");
		
		/**
		 * Destructor.
		 */
		~TriggerAction ();
		
		
		/**
		 * @copydoc Action::isActive
		 */
		const String& getName() { return mName; }
		
		/**
		 * @copydoc Action::isActive
		 */
		bool isActive() { return mActive; }
		
		
		/**
		 * @copydoc Action::addBinding
		 */
		void addBinding (Binding* binding);
		
		/**
		 * @copydoc Action::removeBinding
		 */
		void removeBinding (Binding* binding);
		
		
		
		/**
		 * Log source name.
		 */
		static const String LogSource;
		
		
	private:
		String mName;
		bool mActive;
		
		std::vector<Binding*> mBindings;
		
		
		/* binding event handlers */
		void bindingActivated (Binding* binding);
		void bindingDeactivated (Binding* binding);
	};

}}


#endif /* SOMA_INPUT_TRIGGER_ACTION_H_ */
