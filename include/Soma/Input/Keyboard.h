
#ifndef SOMA_INPUT_KEYBOARD_H_
#define SOMA_INPUT_KEYBOARD_H_


#include <OISKeyboard.h>

#include <Soma/Config.h>
#include <Soma/String.h>
#include <Soma/Input/Device.h>


namespace Soma {
namespace Input {


	/* forward declarations */
	class State;
	
	
	/**
	 * Keycode enumeration.
	 */
	typedef OIS::KeyCode KeyCode;
	
	
	/**
	 * A keyboard device.
	 */
	class SOMA_API Keyboard : public Device,
	                          public OIS::KeyListener
	{
	public:
		/**
		 * Constructor.
		 */
		explicit Keyboard (OIS::Keyboard* keyboard);
		
		/**
		 * Destructor.
		 */
		~Keyboard ();
		
		
		/**
		 * @copydoc Device::getType
		 */
		Type getType() { return Device::KEYBOARD; }
		
		/**
		 * @copydoc Device::getName
		 */
		const String& getName() { return mName; }
		
		
		/**
		 * Get an input key state associated with the key code.
		 * @return The associated KeyState.
		 */
		State* getState (KeyCode key) { return mKeyStates[key]; }
		
		/**
		 * @copydoc Device::getState
		 */
		State* getState (const String& name);
		
		
		
		/**
		 * @copydoc Device::capture
		 */
		void capture () { mKeyboard->capture(); }
		
		
		
		/**
		 * Log source name.
		 */
		static const String LogSource;
		
		
		
	private:
		String mName;
		OIS::Keyboard* mKeyboard;
		
		std::map<String, KeyCode> mKeyNames;
		std::map<KeyCode, State*> mKeyStates;
		
		
		/**
		 * Creates and adds a state to the key map.
		 * @param code The key code to add.
		 * @param name The name of the key.
		 */
		void addState (KeyCode code, const String& name);
		
		
		/* keyboard event handlers */
		bool keyPressed (const OIS::KeyEvent& ev);
		bool keyReleased (const OIS::KeyEvent& ev);
	};

}}


#endif /* SOMA_INPUT_KEYBOARD_H_ */
