
#ifndef SOMA_INPUT_MOUSE_H_
#define SOMA_INPUT_MOUSE_H_


#include <OISMouse.h>

#include <Soma/Config.h>
#include <Soma/String.h>
#include <Soma/Input/Device.h>


namespace Soma {
namespace Input {

	/**
	 * A mouse device.
	 */
	class SOMA_API Mouse : public Device,
	                       public OIS::MouseListener
	{
	public:
		/**
		 * Constructor.
		 */
		explicit Mouse (OIS::Mouse* mouse);
		
		/**
		 * Destructor.
		 */
		~Mouse ();
		
		
		/**
		 * @copydoc Device::getType
		 */
		Type getType() { return Device::MOUSE; }
		
		/**
		 * @copydoc Device::getName
		 */
		const String& getName() { return mName; }
		
		
		/**
		 * @copydoc Device::getState
		 */
		State* getState (const String& name) { return 0; }
		
		
		/**
		 * @copydoc Device::capture
		 */
		void capture () { mMouse->capture(); }
		
		
		
		/**
		 * Log source name.
		 */
		static const String LogSource;
		
		
		
	private:
		String mName;
		OIS::Mouse* mMouse;
		
		/* mouse event handlers */
		bool mouseMoved (const OIS::MouseEvent& ev);
		bool mousePressed (const OIS::MouseEvent& ev, OIS::MouseButtonID id);
		bool mouseReleased (const OIS::MouseEvent& ev, OIS::MouseButtonID id);
	};

}}


#endif /* SOMA_INPUT_KEYBOARD_H_ */
