
#ifndef SOMA_INPUT_CONTEXT_H_
#define SOMA_INPUT_CONTEXT_H_


#include <map>

#include <Soma/Config.h>
#include <Soma/String.h>
#include <Soma/SmartPointer.h>
#include <Soma/ContextComponent.h>


/* forward declarations */
namespace OIS { class InputManager; }


namespace Soma {	
namespace Input {


	/* forward declarations */
	class Device;
	
	
	/**
	 * Input context.
	 * 
	 * An input context is the 'range' or 'container' of devices and actions
	 * within a specific area. This allows us to create as many input contexts
	 * attached to a specific window which then only processes input within 
	 * that particular window. As such we can separate actions and devices
	 * from the input system and then attach them to each context to enable
	 * functionality for that window.
	 * 
	 * One prime example of utilising multiple contexts is an editor where
	 * the preview window requires different actions and belongs to a different
	 * window than the editor window, although they could even share the same
	 * actions and devices by simply adding them to each context.
	 * 
	 * @ingroup components
	 */
	class SOMA_API Context : public ContextComponent
	{
	public:
		/**
		 * Constructor.
		 */
		Context ();
		
		/**
		 * Destructor.
		 */
		~Context ();
		
		
		
		/**
		 * @copydoc ContextComponent::getType
		 */
		const String& getType() const { return Type; }
		
		
		/**
		 * Get the attached device, if any.
		 * @return The attached device if found, NULL otherwise.
		 */
		Device* getDevice (const String& name) { return mDevices[name]; }
		
		
		
		/**
		 * Grab devices.
		 * 
		 * Sets whether the context has an exclusive grab of the devices on
		 * the system.
		 * 
		 * @param enabled Whether or not devices are exclusively grabbed.
		 */
		void setGrab (bool enabled) { mGrab = enabled; }
		
		/**
		 * Are devices exlusively grabbed?
		 * @return True if grabbed, false otherwise.
		 */
		bool isGrab () { return mGrab; }
		
		
		
		/**
		 * @copydoc ContextComponent::load
		 */
		bool load ();
		
		/**
		 * @copydoc ContextComponent::reload
		 */
		bool reload ();
		
		/**
		 * @copydoc ContextComponent::unload
		 */
		void unload ();
		
		/**
		 * @copydoc ContextComponent::isLoaded
		 */
		bool isLoaded () const { return mLoaded; }
		
		
		
		/**
		 * Read and process the input buffer of all attached devices.
		 */
		void capture ();
		
		
		
		/** Log source name. */
		static const String LogSource;
		
		/** Subcontext type. */
		static const String Type;
		
		
		
	private:
		bool mGrab;
		bool mLoaded;
		
		
		/* devices */
		typedef std::map<String, Device*> DeviceMap;
		DeviceMap mDevices;
		
		/* device manager */
		OIS::InputManager* mManager;
		
		
		void createManager (size_t window_id);
		
		
		/* event handlers */
		void onBroadcast (const CompositeMessage& message);
	};

}}


#endif /* SOMA_INPUT_CONTEXT_H_ */
