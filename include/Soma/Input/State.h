
#ifndef SOMA_INPUT_STATE_H_
#define SOMA_INPUT_STATE_H_


#include <vector>

#include <Soma/Config.h>
#include <Soma/String.h>


namespace Soma {
namespace Input {


	/* forward declarations */
	class Device;
	
	

	/**
	 * Input state.
	 * 
	 * States represent a specific input state such as on/off or axis position.
	 * All input must be represented as a state to allow for independent
	 * assigning of specific input keys/axis/types to a binding without
	 * setting arbitrary limits such as max buttons or keyboard locale types.
	 */
	class SOMA_API State
	{
	public:
		/**
		 * State event listener.
		 */
		struct Listener
		{
			/**
			 * The state has been activated.
			 * @param state The state in question.
			 */
			virtual void stateActivated (State* state) {}
			
			/**
			 * The state has been deactivated.
			 * @param state The state in question.
			 */
			virtual void stateDeactivated (State* state) {}
		};
		
		
		/**
		 * Constructor.
		 */
		State (Device* device, String name);
		
		/**
		 * Destructor.
		 */
		virtual ~State ();
		
		
		/**
		 * Get the device this state belongs to.
		 * @return The Device handling this state.
		 */
		Device* getDevice() { return mDevice; }
		
		
		/**
		 * Get the name of this state.
		 * @return The state's name.
		 */
		String getName() { return mName; }
		
		
		/**
		 * Activate the state.
		 */
		void activate ();
		
		/**
		 * Deactivate the state.
		 */
		void deactivate ();
		
		
		/**
		 * Is the state active?
		 * @return True if active, false otherwise.
		 */
		bool isActive() { return mActive; }
		
		
		/**
		 * Add an event listener to the state.
		 * @param listener An event listener instance.
		 */
		void addListener (Listener* listener);
		
		
		/**
		 * Remove an event listener from the state.
		 * @param listener An event listener instance.
		 */
		void removeListener (Listener* listener);
		
		
		
		/**
		 * Log source name.
		 */
		static const String LogSource;
		
		
	private:
		bool mActive;
		
		Device* mDevice;
		String mName;
		
		std::vector<Listener*> mListeners;
	};

}}


#endif /* SOMA_INPUT_STATE_H_ */
