
#ifndef SOMA_INPUT_MANAGER_H_
#define SOMA_INPUT_MANAGER_H_


#include <map>

#include <Soma/Config.h>
#include <Soma/String.h>
#include <Soma/Singleton.h>
#include <Soma/SubSystem.h>
#include <Soma/Configurable.h>
#include <Soma/SmartPointer.h>


/* forward declarations */
namespace OIS { class InputManager; }


namespace Soma {
namespace Input {


	/* forward declarations */
	class Context;
	
	
	/**
	 * Input system manager.
	 * 
	 * The input Manager class handles all input operations abstracting
	 * the low-level platform specific code for devices allowing for
	 * consistent input notifications across systems. In essence it loads
	 * the input devices and hooks it into the Engine game loop to poll for
	 * any buffered events and the pass them on to the appropriate handler.
	 */
	class SOMA_API Manager : public Singleton<Manager>,
	                         public SubSystem,
	                         public Configurable
	{
	public:
		/**
		 * Constructor.
		 */
		Manager ();
		
		/**
		 * Destructor.
		 */
		~Manager ();
		
		
		
		/**
		 * Add a context to the input manager.
		 * @param context The input context to manage.
		 */
		void addContext (Context* context);
		
		/**
		 * Remove a context from the input manager.
		 * @param context The input context to stop managing.
		 */
		void removeContext (Context* context);
		
		
		
		/**
		 * Initialises the input manager.
		 * @return True if sucessful, false otherwise.
		 */
		bool initialise ();
		
		/**
		 * Reinitialises the input manager.
		 * @return True if successful, false otherwise.
		 */
		bool reinitialise ();
		
		/**
		 * Shutdown the input manager.
		 */
		void shutdown ();
		
		/**
		 * Is the input manager initialised?
		 * @return True if initialised, false otherwise.
		 */
		bool isInitialised () { return mInitialised; }
		
		/**
		 * Read and process the input buffer.
		 */
		void step ();
		
		
		
		/**
		 * @copydoc Configurable::save
		 */
		bool save (ConfigInterface& config);
		
		/**
		 * @copydoc Configurable::restore
		 */
		bool restore (const ConfigInterface& config);
		
		
		
		/** Log source name. */
		static const String LogSource;
		
		
		
	private:
		bool mInitialised;
		
		
		/* contexts */
		typedef std::vector<Context*> ContextList;
		ContextList mContexts;
	};

}}


#endif /* SOMA_INPUT_MANAGER_H_ */
