
#ifndef SOMA_INPUT_DEVICE_H_
#define SOMA_INPUT_DEVICE_H_


#include <Soma/Config.h>
#include <Soma/String.h>


namespace Soma {
namespace Input {


	/* forward declarations */
	class State;
	
	
	
	/**
	 * Input device.
	 * 
	 * Input device abstraction which allows us to use any device with any
	 * action regardless of any bindings as an action can have many bindings
	 * which are exclusive to an input device.
	 */
	class SOMA_API Device
	{
	public:
		/**
		 * Device type.
		 */
		enum Type
		{
			KEYBOARD,
			MOUSE,
			JOYSTICK
		};
		
		
		/**
		 * Destructor.
		 */
		virtual ~Device () {}
		
		
		/**
		 * Get device type.
		 * @return The Type of the Device.
		 */
		virtual Type getType() = 0;
		
		/**
		 * Get device name.
		 * @return The name of the Device.
		 */
		virtual const String& getName() = 0;
		
		
		/**
		 * Get an input state associated with the device.
		 * 
		 * @param name The name of the input state.
		 * @return The associated input state.
		 */
		virtual State* getState (const String& name) = 0;
		
		
		/**
		 * Capture buffered input.
		 * 
		 * This will retrieved the first pending event from the input device
		 * invoking the listeners which handle the triggering of actions.
		 * Ideally this should be called by the InputManager on each frame.
		 */
		virtual void capture () = 0;
		
	};

}}


#endif /* SOMA_INPUT_DEVICE_H_ */
