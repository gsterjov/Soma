
#ifndef SOMA_INPUT_BINDING_H_
#define SOMA_INPUT_BINDING_H_


#include <vector>

#include <Soma/Config.h>
#include <Soma/Input/State.h>


namespace Soma {
namespace Input {
	
	
	/**
	 * Device binding.
	 * 
	 * The Binding class holds information about the device input needed to
	 * trigger the action holding the binding. By decoupling actions, bindings
	 * and devices we can provide many alternative bindings either from the
	 * same or different device for a single action. It also allows us to
	 * specify different kinds of actions which uses the same binding but
	 * processes the input in a different way to determine whether or not
	 * it should be triggered.
	 */
	class SOMA_API Binding : public State::Listener
	{
	public:
		/**
		 * Binding event listener.
		 */
		struct Listener
		{
			/**
			 * The binding has been activated.
			 * @param binding The binding in question.
			 */
			virtual void bindingActivated (Binding* binding) {}
			
			/**
			 * The binding has been deactivated.
			 * @param binding The binding in question.
			 */
			virtual void bindingDeactivated (Binding* binding) {}
		};
		
		
		/**
		 * Constructor.
		 */
		Binding ();
		
		/**
		 * Destructor.
		 */
		~Binding ();
		
		
		/**
		 * Add a state to the bindable.
		 * 
		 * The bindable merely holds each state and notifies its listeners
		 * when a state has been activated or when all states are activated.
		 * What to do once the states are processed depends on the specific
		 * Action which created the binding.
		 */
		void add (State* state);
		
		/**
		 * Remove a state from the bindable.
		 * 
		 * Removing a state will effectively stop all notifications from the
		 * state and the binding will be determined by its remaining states.
		 */
		void remove (State* state);
		
		
		/**
		 * Activate binding.
		 * 
		 * Manually activate the binding triggering any actions associated
		 * with it.
		 */
		void activate ();
		
		/**
		 * Deactivate binding.
		 * 
		 * Manually deactivate the binding stopping any triggered actions
		 * associated with it.
		 */
		void deactivate ();
		
		
		/**
		 * Is the binding active?
		 * @return True if active, false otherwise.
		 */
		bool isActive() { return mActive; }
		
		
		/**
		 * Add an event listener to the binding.
		 * @param listener An event listener instance.
		 */
		void addListener (Listener* listener);
		
		
		/**
		 * Remove an event listener from the bingind.
		 * @param listener An event listener instance.
		 */
		void removeListener (Listener* listener);
		
		
		/**
		 * The binding states.
		 * @return A list of States used for binding.
		 */
		const std::vector<State*>& getStates() { return mStates; }
		
		
		
		/**
		 * Log source name.
		 */
		static const String LogSource;
		
		
	private:
		bool mActive;
		int mActiveCount;
		
		std::vector<State*> mStates;
		std::vector<Listener*> mListeners;
		
		
		/* state event handlers */
		void stateActivated (State* state);
		void stateDeactivated (State* state);
	};

}}


#endif /* SOMA_INPUT_BINDING_H_ */
