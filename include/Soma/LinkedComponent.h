
#ifndef SOMA_LINKED_COMPONENT_H_
#define SOMA_LINKED_COMPONENT_H_


#include <Soma/Config.h>
#include <Soma/String.h>
#include <Soma/SmartPointer.h>


namespace Soma
{

	/* forward declarations */
	class Entity;
	class Component;
	struct ParentChangedEventArgs;
	
	
	
	/**
	 * Linked component.
	 * 
	 * Components that inherit from this class can make use of the complex
	 * process of communicating between components across a number of boundaries
	 * such as components within the same entity, parent entities, child
	 * entities and sibling entities. This component abstracts away the 
	 * communication between entities and components and allows inheriting
	 * components to simply implement various functions which notify them
	 * of any changes of any importance to synchronisation.
	 * 
	 * @note LinkedComponent works with the Entity composite hierarchy and thus
	 * will only be useful to components added to an Entity.
	 * 
	 * @ingroup components
	 */
	class SOMA_API LinkedComponent
	{
	public:
		/**
		 * Destructor.
		 * 
		 * This will call unbind() upon destruction and thus will unlink all
		 * components.
		 */
		virtual ~LinkedComponent () { unbind(); }
		
		
		/** Log source name. */
		static const String LogSource;
		
		
		
	protected:
		/**
		 * Bind to the entity.
		 * 
		 * This will effectively bind the component to the entity synchronising
		 * with its components and hierarchy.
		 * 
		 * @param entity The entity to bind to.
		 */
		void bind (const weak_ptr<Entity>& entity);
		
		
		/**
		 * Unbind the previously bound entity.
		 */
		void unbind ();
		
		
		
		
		/**
		 * Link a sibling component found in the same owner entity.
		 * 
		 * This is called whenever a new component is added to the same entity
		 * that owns this component thus providing the means to link to sibling
		 * components that may provide specific information for the component,
		 * such as a sibling MovableComponent providing 3D positional data
		 * for a SourceComponent, both of which are added to the same entity.
		 * 
		 * @param sibling The sibling component to link.
		 */
		virtual void link (const smart_ptr<Component>& sibling) {}
		
		/**
		 * Unlink a sibling component found in the same owner entity.
		 * 
		 * This is generally called whenever a component is no longer
		 * a sibling of this component as a result of it being removed.
		 * 
		 * @param sibling The sibling component to unlink.
		 */
		virtual void unlink (const smart_ptr<Component>& sibling) {}
		
		
		/**
		 * Link a component found in a parent entity.
		 * 
		 * This is called whenever a component is added to the heirarchy of
		 * this component as a result of the parent component being added to
		 * the parent of the entity, or when an entity was is introduced to the
		 * heirarchy of the entity utilising this component.
		 * 
		 * @param parent The parent component to link.
		 */
		virtual void linkParent (const smart_ptr<Component>& parent) {}
		
		/**
		 * Unlink a component found in a parent entity.
		 * 
		 * This is generally called whenever a component is no longer
		 * in the parent entity and thus should be unlinked. This can happen
		 * when the parent component is removed from its entity, or when
		 * the entity owning the parent component is no longer a parent of the
		 * entity utilising this component.
		 * 
		 * @param parent The parent component to unlink.
		 */
		virtual void unlinkParent (const smart_ptr<Component>& parent) {}
		
		
		/**
		 * Link a component found in a child entity.
		 * 
		 * This is called whenever a component is added to the heirarchy of
		 * this component as a result of the child component or child entity
		 * being added to the entity utilising this component.
		 * 
		 * @param child The child component to link.
		 */
		virtual void linkChild (const smart_ptr<Component>& child) {}
		
		/**
		 * Unlink a component found in a child entity.
		 * 
		 * This is generally called whenever a component is no longer
		 * in the child entity and thus should be unlinked. This can happen
		 * when the child component is removed from its entity, or when
		 * the entity owning the child component is no longer a child of the
		 * entity utilising this component.
		 * 
		 * @param child The child component to unlink.
		 */
		virtual void unlinkChild (const smart_ptr<Component>& child) {}
		
		
		/**
		 * Link a component found in a child entity.
		 * 
		 * This is called whenever a component is added to the heirarchy of
		 * this component as a result of the child component or child entity
		 * being added to the entity utilising this component.
		 * 
		 * @param child The child component to link.
		 */
		virtual void loadChild (const smart_ptr<Component>& child) {}
		
		/**
		 * Unlink a component found in a child entity.
		 * 
		 * This is generally called whenever a component is no longer
		 * in the child entity and thus should be unlinked. This can happen
		 * when the child component is removed from its entity, or when
		 * the entity owning the child component is no longer a child of the
		 * entity utilising this component.
		 * 
		 * @param child The child component to unlink.
		 */
		virtual void unloadChild (const smart_ptr<Component>& child) {}
		
		
		
	private:
		weak_ptr<Entity> mEntity;
		
		
		/* owner event handlers */
		void onOwnerAdded (const weak_ptr<Entity>& owner);
		void onOwnerRemoved (const weak_ptr<Entity>& owner);
		
		
		/* sibling event handlers */
		void onComponentAdded   (const smart_ptr<Component>& component);
		void onComponentRemoved (const smart_ptr<Component>& component);
		
		
		/* parent event handlers */
		void onParentChanged (const ParentChangedEventArgs& args);
		void onParentComponentAdded   (const smart_ptr<Component>& component);
		void onParentComponentRemoved (const smart_ptr<Component>& component);
		
		
		/* child event handlers */
		void onChildAdded   (const weak_ptr<Entity>& child);
		void onChildRemoved (const weak_ptr<Entity>& child);
		void onChildComponentAdded   (const smart_ptr<Component>& component);
		void onChildComponentRemoved (const smart_ptr<Component>& component);
		
		void onChildComponentLoaded   (const smart_ptr<Component>& component);
		void onChildComponentUnloaded (const smart_ptr<Component>& component);
	};

}


#endif /* SOMA_LINKED_COMPONENT_H_ */
