
#ifndef SOMA_ASSET_H_
#define SOMA_ASSET_H_


#include <vector>

#include <Soma/Config.h>
#include <Soma/String.h>
#include <Soma/Types.h>
#include <Soma/SmartPointer.h>

#include <Soma/Resource.h>
#include <Soma/ResourceManager.h>


namespace Soma
{

	/**
	 * Asset resource.
	 * 
	 * An asset is the combination of various objects allowing them to have
	 * a specific hierarchy and to be used in an intuitive way. One example
	 * of an asset is a playable character or a vehicle. Even scenery can
	 * benefit from the use of assets. All higher level interfaces work
	 * with assets as they provide the highest level of encapsulation of low
	 * level entities within the various sub-systems.
	 * 
	 * @ingroup resources
	 */
	class SOMA_API Asset : public Resource
	{
	public:
		/**
		 * Holds information about a particular resource instance.
		 */
		struct Instance
		{
			/** The name of the resource instance */
			String name;
			/** The instance world transform */
			Matrix transform;
			/** The resource to instantiate */
			smart_ptr<Resource> resource;
		};
		
		
		/** A list of resources */
		typedef std::vector<Instance> ResourceList;
		
		
		
		
		/**
		 * Destructor.
		 */
		virtual ~Asset () {}
		
		
		
		/**
		 * Get all defined resources within the asset.
		 * @return A list of resources.
		 */
		const ResourceList& getResources() const { return mResources; }
		
		
		/**
		 * Add a resource to the asset.
		 * @param instance A resource instance descriptor.
		 */
		void addResource (const Instance& instance) { mResources.push_back (instance); }
		
		
		
		/**
		 * @copydoc Resource::getType
		 */
		const String& getType() const { return Type; }
		
		/**
		 * @copydoc Resource::getVri
		 */
		const Vri& getVri() const { return Vri::EMPTY; }
		
		/**
		 * @copydoc Resource::getSize
		 */
		size_t getSize() const;
		
		
		/**
		 * @copydoc Resource::load
		 */
		virtual bool load ();
		
		/**
		 * @copydoc Resource::reload
		 */
		bool reload ();
		
		/**
		 * @copydoc Resource::unload
		 */
		void unload ();
		
		
		
		/** Log source name. */
		static const String LogSource;
		
		/** Resource type. */
		static const String Type;
		
		
		
	protected:
		/**
		 * A list of resources belonging to the asset. This is used to define
		 * a resource and resource instance information for entities that want
		 * to load and instantiate interconnected resources.
		 */
		ResourceList mResources;
	};
	
	
	
	/**
	 * Asset resource manager.
	 */
	class SOMA_API AssetManager : public ResourceManager<Asset> {};

}


#endif /* SOMA_ASSET_H_ */
