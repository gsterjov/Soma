
#ifndef SOMA_SMART_POINTER_H_
#define SOMA_SMART_POINTER_H_


#include <cassert>
#include <iostream>

#include <Soma/Config.h>
#include <Soma/Event.h>


namespace Soma
{

	/**
	 * A reference counter for intrusive smart pointers.
	 * 
	 * All classes that want to benefit from the smart pointer implementation
	 * of Ref and WeakRef must inherit from ReferenceCounter in order to
	 * provide a counting mechanism for the intrusive smart pointer
	 * implementation.
	 */
	class SOMA_API ReferenceCounter
	{
		/* only SmartPointer is allowed to access internal counter implementation */
		template<class T> friend class base_ptr;
		
		
	public:
		ReferenceCounter() : mCount(0) {}
		virtual ~ReferenceCounter() {}
		
		
	private:
		/**
		 * Reference counter.
		 */
		int mCount;
		
		/**
		 * The pointer will be destroyed.
		 */
		Event<void, bool> Destroy;
	};
	
	
	
	
	/**
	 * Base smart pointer class.
	 */
	template <class T>
	class SOMA_API base_ptr
	{
	public:
		/**
		 * Pointer constructor.
		 * 
		 * A constructor which stores the specified pointer and increments
		 * the associated counter. As a result the pointer must inherit from
		 * ReferenceCounter.
		 * 
		 * @param ptr The pointer to maintain.
		 */
		explicit base_ptr (T* ptr) : mPointer(ptr), mCounter(ptr) {}
		
		
		/**
		 * Empty constructor.
		 */
		base_ptr () : mPointer(0), mCounter(0) {}
		
		
		/**
		 * Destructor.
		 */
		virtual ~base_ptr () {}
		
		
		
		
		/* equality comparison operator */
		friend bool operator== (const base_ptr& lhs, const base_ptr& rhs) { return lhs.mPointer == rhs.mPointer; }
		friend bool operator== (const base_ptr& lhs, const T*        rhs) { return lhs.mPointer == rhs; }
		friend bool operator== (const T*        lhs, const base_ptr& rhs) { return lhs == rhs.mPointer; }
		
		/* inequality comparison operator */
		friend bool operator!= (const base_ptr& lhs, const base_ptr& rhs) { return !(lhs == rhs); }
		friend bool operator!= (const base_ptr& lhs, const T*        rhs) { return !(lhs == rhs); }
		friend bool operator!= (const T*        lhs, const base_ptr& rhs) { return !(lhs == rhs); }
		
		/* bool conversion operator */
		operator bool () const { return mPointer != 0; }
		
		
		
		/* dereference operator */
		T& operator* () const { assert (mPointer); return *mPointer; }
		
		
		/* indirection operator */
		T* operator-> () const { assert (mPointer); return mPointer; }
		
		
		/**
		 * Retrieves the stored pointer being referenced.
		 */
		T* getPointer() const { return mPointer; }
		
		
		/**
		 * Gets the current reference count.
		 */
		int getCount() const { return mCounter ? mCounter->mCount : 0; }
		
		
		/**
		 * Does the pointer being referenced exist?
		 * @return True if pointer is valid, false otherwise.
		 */
		bool isValid() const { return mPointer; }
		
		
		
		/**
		 * Releases the reference to the stored pointer.
		 */
		virtual void release () = 0;
		
		
		
		/**
		 * Stream out operator.
		 */
		friend std::ostream& operator<< (std::ostream& ostream,
		                                 const base_ptr& ref)
		{
			ostream << "Pointer: " << ref.mPointer
					<< " (Reference Count: " << ref.getCount() << ")";
			
			return ostream;
		}
		
		
		
	protected:
		/** The reference counter */
		ReferenceCounter* mCounter;
		
		/** The stored pointer */
		T* mPointer;
		

		/* TODO: have the increment decrement functions provide a stack trace
		 * on each function call which can be used for debugging any memory
		 * leaks from lost pointers to circular references. should be available
		 * in debug mode only so as not to hurt performance */
		
		/**
		 * Increment the reference counter.
		 */
		void increment() { assert (mCounter); ++(mCounter->mCount); }
		
		/**
		 * Decrement the reference counter.
		 */
		void decrement() { assert (mCounter); --(mCounter->mCount); }
		
		/**
		 * Reference counter value.
		 */
		int value() { assert (mCounter); return mCounter->mCount; }
		
		
		/**
		 * Notifies all smart pointers that the pointer will be destroyed.
		 */
		void destroy ()
		{
			assert (mCounter);
			mCounter->Destroy.raise (true);
		}
		
		
		/**
		 * Connect to the Destroy event so as to be notified when the pointer
		 * is about to be destroyed.
		 * 
		 * @note Do not call this method when inside the onDestroy() event.
		 */
		void connect ()
		{
			if (mCounter)
				mCounter->Destroy += delegate (this, &base_ptr<T>::onDestroy);
		}
		
		/**
		 * Disconnect from the Destroy event.
		 * 
		 * @note Do not call this method when inside the onDestroy() event.
		 */
		void disconnect ()
		{
			if (mCounter)
				mCounter->Destroy -= delegate (this, &base_ptr<T>::onDestroy);
		}
		
		
		/**
		 * Called before the pointer is about to be destroyed.
		 * 
		 * @note This should not interact with the event in any way as it will
		 * corrupt memory if the delegate store is altered in any way. It is
		 * thus crucial not to call connect() or disconnect().
		 */
		virtual void onDestroy (bool) {}
	};
	
	
	
	
	
	/**
	 * Strong intrusive smart pointer.
	 * 
	 * Ref maintains a strong reference to a pointer. Its main purpose
	 * is memory management in that the stored pointer is freed once it is no
	 * longer being referenced. It also manages the reference counter whenever
	 * the reference is copied over or released. Unlike WeakRef, no
	 * precaution is needed to ensure the validity of the stored pointer as it
	 * will always exist.
	 * 
	 * Due to the smart pointer being intrusive we can free the reference
	 * through a virtual base destructor which allows us to use the smart
	 * pointer with incomplete types.
	 */
	template <class T>
	class SOMA_API smart_ptr : public base_ptr<T>
	{
	public:
		/**
		 * Pointer constructor.
		 * 
		 * A constructor which stores the specified pointer and increments
		 * the associated counter. As a result the pointer must inherit from
		 * ReferenceCounter.
		 * 
		 * @note This also doubles as an implicit conversion constructor thus
		 * allowing a smart pointer to be created by 'assigning' it an existing
		 * pointer. For example,
		 * 
		 * @code
		 * WeakRef<MyClass> myWeakRef = new MyClass();
		 * //or
		 * MyClass* ptr = new MyClass();
		 * WeakRef<MyClass> myWeakRef = ptr;
		 * @endcode
		 * 
		 * @param ptr The pointer to maintain.
		 */
		smart_ptr (T* ptr) : base_ptr<T>(ptr)
		{
			/* increase reference */
			if (this->mCounter) this->increment();
		}
		
		
		/**
		 * Generic copy constructor.
		 * @param ref Any smart pointer to copy.
		 */
		smart_ptr (const base_ptr<T>& ref) : base_ptr<T>(ref)
		{
			/* increase reference */
			if (this->mCounter) this->increment();
		}
		
		
		/**
		 * Copy constructor.
		 * @param ref The strong smart pointer to copy.
		 */
		smart_ptr (const smart_ptr<T>& ref) : base_ptr<T>(ref)
		{
			/* increase reference */
			if (this->mCounter) this->increment();
		}
		
		
		/**
		 * Implicit conversion between smart pointers.
		 * 
		 * Without this we wont be able to cast a pointer to its base
		 * type and still keep the smart pointer wrapping. This effectively
		 * creates a templated conversion operator to allow for implicit
		 * conversions between types and if they are incompatible then a
		 * compiler error will occur
		 * 
		 * @param ref The smart pointer to convert.
		 */
		template <typename fromType>
		smart_ptr (const base_ptr<fromType>& ref) : base_ptr<T>(ref.getPointer())
		{
			/* increase reference */
			if (this->mCounter) this->increment();
		}
		
		
		/**
		 * Default constructor.
		 * A constructor with an empty reference.
		 */
		smart_ptr () {}
		
		
		/**
		 * Destructor.
		 * Upon deletion the reference will be released.
		 */
		virtual ~smart_ptr ()
		{
			release ();
		}
		
		
		
		/**
		 * Copies a generic reference.
		 * 
		 * When the reference is copied over its old reference is decremented 
		 * and released while the new reference is incremented and stored.
		 */
		smart_ptr& operator= (const base_ptr<T>& ref)
		{
			if (ref.getPointer() != this->mPointer)
			{
				/* release current reference */
				release ();
				
				/* copy pointer and increase reference */
				this->mPointer = ref.getPointer();
				this->mCounter = ref.getPointer();
				if (this->mCounter) this->increment();
			}
			
			return *this;
		}
		
		
		
		/**
		 * Copies the specified reference.
		 * 
		 * When the reference is copied over its old reference is decremented 
		 * and released while the new reference is incremented and stored.
		 */
		smart_ptr& operator= (const smart_ptr<T>& ref)
		{
			if (ref.mPointer != this->mPointer)
			{
				/* release current reference */
				release ();
				
				/* copy pointer and increase reference */
				this->mPointer = ref.mPointer;
				this->mCounter = ref.mCounter;
				if (this->mCounter) this->increment();
			}
			
			return *this;
		}
		
		
		/**
		 * Releases the reference to the stored pointer.
		 * 
		 * Strong references not only decrement and nullify the reference but
		 * also free's the stored pointer if the reference counter reaches zero.
		 * Whilst the reference is automatically released upon destruction this
		 * method can be used to force its release.
		 */
		void release ()
		{
			if (this->mPointer)
			{
				assert (this->value() > 0);
				
				
				this->decrement();
				
				/* free pointer */
				if (this->value() == 0)
				{
					/* raise destroyed event */
					this->destroy ();
					
					/* use base destructor to allow incomplete types */
					delete this->mCounter;
				}
				
				/* nullify reference */
				this->mPointer = 0;
				this->mCounter = 0;
			}
		}
	};
	
	
	
	
	/**
	 * Weak intrusive smart pointer.
	 * 
	 * WeakRef maintains a weak reference to a pointer. A weak reference
	 * neither manages the reference counter nor any memory management. Its
	 * sole purpose is to provide compatibility with Ref when using raw
	 * pointers. It further allows the ability to distribute any reference
	 * without incrementing the counter and thus prolonging its life, although
	 * as a result the validity of a weak reference must always be checked
	 * with isValid() before it is used otherwise memory corruption may occur.
	 */
	template <class T>
	class SOMA_API weak_ptr : public base_ptr<T>
	{
	public:
		/**
		 * Pointer constructor.
		 * 
		 * A constructor which stores the specified pointer. As a result the
		 * pointer must inherit from ReferenceCounter.
		 * 
		 * @note This also doubles as an implicit conversion constructor thus
		 * allowing a smart pointer to be created by 'assigning' it an existing
		 * pointer. For example,
		 * 
		 * @code
		 * WeakRef<MyClass> myWeakRef = new MyClass();
		 * //or
		 * MyClass* ptr = new MyClass();
		 * WeakRef<MyClass> myWeakRef = ptr;
		 * @endcode
		 * 
		 * @param ptr The pointer to reference.
		 */
		weak_ptr (T* ptr) : base_ptr<T>(ptr)
		{
			this->connect ();
		}
		
		
		/**
		 * Copy constructor.
		 * @param ref The smart pointer to copy.
		 */
		weak_ptr (const base_ptr<T>& ref) : base_ptr<T>(ref)
		{
			this->connect ();
		}
		
		
		/**
		 * Implicit conversion between smart pointers.
		 * 
		 * Without this we wont be able to cast a pointer to its base
		 * type and still keep the smart pointer wrapping. This effectively
		 * creates a templated conversion operator to allow for implicit
		 * conversions between types and if they are incompatible then a
		 * compiler error will occur
		 * 
		 * @param ref The smart pointer to convert.
		 */
		template <typename fromType>
		weak_ptr (const base_ptr<fromType>& ref) : base_ptr<T>(ref.getPointer())
		{
			this->connect ();
		}
		
		
		/**
		 * Default constructor.
		 * A constructor with an empty reference.
		 */
		weak_ptr () {}
		
		
		/**
		 * Destructor.
		 * Upon deletion the reference will be released.
		 */
		virtual ~weak_ptr ()
		{
			release ();
		}
		
		
		
		/**
		 * Copies the specified reference.
		 */
		weak_ptr& operator= (const base_ptr<T>& ref)
		{
			release ();
			
			/* copy pointer and counter */
			this->mPointer = ref.getPointer();
			this->mCounter = ref.getPointer();
			
			this->connect ();
			return *this;
		}
		
		
		
		/**
		 * Releases the reference to the stored pointer.
		 * 
		 * Weak references simply nullify the reference without any memory
		 * management nor any counter management. Though WeakRef
		 * automatically releases the reference upon destruction this method
		 * can be used to force its release early.
		 */
		void release()
		{
			this->disconnect ();
			this->mPointer = 0;
			this->mCounter = 0;
		}
		
		
		
	private:
		/* shared pointer was destroyed */
		void onDestroy (bool)
		{
			this->mPointer = 0;
			this->mCounter = 0;
		}
	};
	
	
	
	
	
	/**
	 * Dynamic cast smart pointers.
	 * 
	 * This allows smart pointers to be upcasted to a derived type by using
	 * a static_cast on the base pointer type to the new type. Though this can
	 * be achieved by manually casting the pointer into a new smart_ptr type,
	 * this function provides a convenient and clean syntax to achieve it.
	 * 
	 * @return A new smart_ptr to the pointer casted into its derived type.
	 */
	template <typename derivedType, typename baseType>
	smart_ptr<derivedType> ref_cast (const smart_ptr<baseType>& ref)
	{
		assert (ref.getPointer());
		return smart_ptr<derivedType> (static_cast<derivedType*> (ref.getPointer()));
	}
	
	
	/**
	 * Dynamic cast weak smart pointers.
	 * 
	 * This allows smart pointers to be upcasted to a derived type by using
	 * a static_cast on the base pointer type to the new type. Though this can
	 * be achieved by manually casting the pointer into a new weak_ptr type,
	 * this function provides a convenient and clean syntax to achieve it.
	 * 
	 * @return A new weak_ptr to the pointer casted into its derived type.
	 */
	template <typename derivedType, typename baseType>
	weak_ptr<derivedType> ref_cast (const weak_ptr<baseType>& ref)
	{
		assert (ref.getPointer());
		return weak_ptr<derivedType> (static_cast<derivedType*> (ref.getPointer()));
	}

}


#endif /* SOMA_SMART_POINTER_H_ */
