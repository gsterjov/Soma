
#ifndef SOMA_PHYSICS_MANAGER_H_
#define SOMA_PHYSICS_MANAGER_H_


#include <vector>

#include <Soma/Config.h>
#include <Soma/Singleton.h>
#include <Soma/SubSystem.h>
#include <Soma/Configurable.h>
#include <Soma/SmartPointer.h>


namespace Soma {
namespace Physics {

	/* forward declarations */
	class World;
	typedef std::vector<smart_ptr<World> > WorldList;
	
	
	/**
	 * Physics simulation manager.
	 * 
	 * The physics Manager class manages the real-time physics simulation. In
	 * essence it wraps and abstracts the Bullet library to provide a clean and
	 * neat subsystem for the engine. Though the Manager class will be used
	 * little by even the low-level classes it is responsible for loading up the
	 * physics interface and ensuring consistent simulation by hooking into
	 * the Engine game loop.
	 */
	class SOMA_API Manager : public Singleton<Manager>,
	                         public SubSystem,
	                         public Configurable
	{
	public:
		/**
		 * Constructor.
		 */
		Manager ();
		
		/**
		 * Destructor.
		 * 
		 * Upon destruction the manager destroys all worlds add to it and
		 * and brings down the entire physics subsystem as a result. This
		 * includes any instantiated objects among other things. One must be
		 * careful destroying a manager as it will invalidate all objects
		 * belonging to the Physics namespace.
		 */
		virtual ~Manager ();
		
		
		/**
		 * Initialises the physics manager.
		 * 
		 * Upon initialisation the physics manager is primed and ready for
		 * simulating the added worlds in step with the Engine, with the
		 * configurations, and any configurations associated with Worlds,
		 * applied.
		 * 
		 * Unlike other subsystems, such as the Graphics Renderer, the physics
		 * Manager subsystem doesn't need to be initialised for physics
		 * objects to be created and used. Indeed, it is entirely possible
		 * to have a working physics simulation without ever creating the
		 * manager.
		 * 
		 * @return True if successful, false otherwise.
		 */
		bool initialise ();
		
		/**
		 * Reinitialises the physics manager.
		 * 
		 * When a setting is changed the manager needs to be reinitialised to
		 * propagate those sentings to the added worlds. This is particularly
		 * useful when settings are changed whilst the manager is running
		 * and the objects already created within the Physics namespace
		 * shouldn't be destroyed and recreated.
		 * 
		 * @return True if successful, false otherwise.
		 */
		bool reinitialise ();
		
		/**
		 * Shuts down the physics manager.
		 * 
		 * Shutting down the physics manager will effectively stop any
		 * simulation taking place within the added Worlds. It does not,
		 * however, invalidate the worlds or any objects associated with it.
		 */
		void shutdown ();
		
		/**
		 * Is the physics manager initialised?
		 * @return True if initialised, false otherwise.
		 */
		bool isInitialised() { return mInitialised; }
		
		/**
		 * Steps through a frame simulating any physics within the elapsed time.
		 * 
		 * If handling a manual game loop instead of using the Engine class then
		 * the Manager (as well as other subsystems) need to be stepped through
		 * each frame to provide proper timing for the simulation. This will
		 * effectively simulate all added Worlds from the last step processed.
		 */
		void step ();
		
		
		
		/**
		 * @copydoc Configurable::save
		 */
		bool save (ConfigInterface& config);
		
		/**
		 * @copydoc Configurable::restore
		 */
		bool restore (const ConfigInterface& config);
		
		
		
		/**
		 * Add a physics World to the manager.
		 * 
		 * Once a World is added to the manager it effectively becomes
		 * simulated whenever the manager is initialised and running. This
		 * allows worlds to be created arbitrarily whilst using the manager
		 * to synchronize it appropriately with the Engine and its other
		 * subsystems.
		 * 
		 * @param world The physics World to manage.
		 */
		void add (const smart_ptr<World>& world) { mWorlds.push_back (world); }
		
		/**
		 * Get a list of all loaded Worlds.
		 * @return A list of physically simulated Worlds.
		 */
		const WorldList& getWorlds() { return mWorlds; }
		
		
		
		/** Log source name. */
		static const String LogSource;
		
		
		
	private:
		WorldList mWorlds;
		
		bool mInitialised;
	};

}}


#endif /* SOMA_PHYSICS_MANAGER_H_ */
