
#ifndef SOMA_PHYSICS_RIGID_BODY_H_
#define SOMA_PHYSICS_RIGID_BODY_H_


#include <Soma/Config.h>
#include <Soma/Resource.h>
#include <Soma/ResourceManager.h>
#include <Soma/SmartPointer.h>


/* forward declarations */
class btCollisionShape;


namespace Soma {
namespace Physics {

	/**
	 * A rigid body resource containing the shared physics body data.
	 * 
	 * @ingroup resources
	 */
	class SOMA_API RigidBody : public Resource
	{
	public:
		/**
		 * Manual constructor.
		 * @param shape The collision shape to use in the resource.
		 */
		RigidBody (btCollisionShape* shape, float mass);
		
		/**
		 * Empty constructor.
		 */
		RigidBody ();
		
		/**
		 * Destructor.
		 */
		virtual ~RigidBody ();
		
		
		/**
		 * The mass of the rigid body.
		 * @return The rigid body mass.
		 */
		float getMass() { return mMass; }
		
		
		/**
		 * The underlying Bullet collision shape.
		 * @return The Bullet collision shape.
		 */
		btCollisionShape* getShape() { return mShape; }
		
		
		
		/**
		 * @copydoc Resource::getType
		 */
		const String& getType() const { return Type; }
		
		/**
		 * @copydoc Resource::getVri
		 */
		const Vri& getVri() const { return Vri::EMPTY; }
		
		/**
		 * @copydoc Resource::getSize
		 */
		size_t getSize() const;
		
		
		/**
		 * @copydoc Resource::load
		 */
		bool load ();
		
		/**
		 * @copydoc Resource::reload
		 */
		bool reload ();
		
		/**
		 * @copydoc Resource::unload
		 */
		void unload ();
		
		
		
		/** Log source name. */
		static const String LogSource;
		
		/** Resource type. */
		static const String Type;
		
		
	private:
		/* TODO: Shape should be its own resource for performance */
		btCollisionShape* mShape;
		
		float mMass;
	};
	
	
	
	/**
	 * RigidBody resource manager.
	 */
	class SOMA_API RigidBodyManager : public ResourceManager<RigidBody> {};

}}


#endif /* SOMA_PHYSICS_RIGID_BODY_H_ */
