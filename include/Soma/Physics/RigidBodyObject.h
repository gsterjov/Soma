
#ifndef SOMA_PHYSICS_RIGID_BODY_OBJECT_H_
#define SOMA_PHYSICS_RIGID_BODY_OBJECT_H_


#include <Soma/Config.h>
#include <Soma/String.h>
#include <Soma/SmartPointer.h>
#include <Soma/Vri.h>



/* forward declarations */
class btRigidBody;


namespace Soma {

	/* forward declarations */
	class Movable;
	
	
namespace Physics {

	/* forward declarations */
	class World;
	class RigidBody;
	
	
	
	/**
	 * An instance of a rigid body resource.
	 */
	class SOMA_API RigidBodyObject : public ReferenceCounter
	{
	public:
		/**
		 * Resource constructor.
		 * 
		 * @param name The name of the rigid body.
		 * @param body The rigid body resource to create an instance of.
		 */
		RigidBodyObject (const String& name, const smart_ptr<RigidBody>& body);
		
		/**
		 * URI constructor.
		 * Takes a resource path and rigid body name to load from the
		 * RigidBodyManager.
		 * 
		 * @param name The name of the rigid body.
		 * @param uri The path to the resource containing the rigid body.
		 */
		RigidBodyObject (const String& name,
		                 const String& uri);
		
		/**
		 * Empty constructor.
		 * @param name The name of the rigid body.
		 */
		explicit RigidBodyObject (const String& name);
		
		/**
		 * Destructor.
		 */
		virtual ~RigidBodyObject ();
		
		
		/**
		 * The name of the RigidBody instance.
		 * @return The RigidBody instance name.
		 */
		String getName() { return mName; }
		
		
		/**
		 * Attach a movable entity to the rigid body.
		 * 
		 * During the duration of the simulation the movable entity will move
		 * according to the rigid body transformation as a result of the
		 * attachment. In essence this is what makes entities move according
		 * to the simulation.
		 */
		void attach (Movable* movable);
		
		
		/**
		 * Set the rigid body resource.
		 * 
		 * Each instantiation of this object will use the rigid body data found
		 * within the given rigid body resource.
		 * 
		 * @param body The rigid body resource to use.
		 */
		void setRigidBody (const smart_ptr<RigidBody>& body);
		
		/**
		 * Set the rigid body resource.
		 * 
		 * Each instantiation of this object will use the rigid body data found
		 * within the given rigid body resource.
		 * 
		 * @param vri The path to the rigid body resource within an asset.
		 */
		void setRigidBody (const Vri& vri);
		
		
		/**
		 * The underlying Bullet rigid body.
		 * @return The Bullet rigid body.
		 */
		btRigidBody* getBody() { return mBody; }
		
		
		
		/**
		 * Log source name.
		 */
		static const String LogSource;
		
		
	private:
		String mName;
		
		smart_ptr<World> mWorld;
		smart_ptr<RigidBody> mRigidBody;
		
		btRigidBody* mBody;
	};

}}


#endif /* SOMA_PHYSICS_RIGID_BODY_OBJECT_H_ */
