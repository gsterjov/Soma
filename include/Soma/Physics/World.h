
#ifndef SOMA_PHYSICS_WORLD_H_
#define SOMA_PHYSICS_WORLD_H_


#include <list>

#include <Soma/Config.h>
#include <Soma/String.h>
#include <Soma/SmartPointer.h>


/* forward declarations */
class btDiscreteDynamicsWorld;
class btAxisSweep3;
class btCollisionDispatcher;
class btDefaultCollisionConfiguration;
class btSequentialImpulseConstraintSolver;


namespace Soma {

	/* forward declarations */
	class Timer;
	
	
namespace Physics {

	/* forward declarations */
	class RigidBodyObject;
	
	
	/**
	 * A world which handles the physics simulation.
	 */
	class SOMA_API World : public ReferenceCounter
	{
	public:
		/**
		 * Constructor.
		 * @param name The name of the physics world.
		 */
		World (const String& name);
		
		/**
		 * Destructor.
		 */
		virtual ~World ();
		
		
		/**
		 * Get the name of thhe physics world.
		 * @return The physics world's name.
		 */
		const String& getName() const { return mName; }
		
		
		/**
		 * Add a rigid body instance to the physics world.
		 * @param body The rigid body instance to add and simulate.
		 */
		void addRigidBody (const smart_ptr<RigidBodyObject>& body);
		
		
		/**
		 * Get all the child rigid bodies added to the world.
		 * @return A list of RigidBodyObjects.
		 */
		const std::list<smart_ptr<RigidBodyObject> >& getNodes() { return mRigidBodies; }
		
		
		/**
		 * Simulate the physics world from the last step.
		 */
		void step ();
		
		
		/**
		 * The underlying Bullet dynamics world.
		 * @return The Bullet dynamics world.
		 */
		btDiscreteDynamicsWorld* getDynamicsWorld() { return mWorld; }
		
		
		
		/**
		 * Log source name.
		 */
		static const String LogSource;
		
		
	private:
		String mName;
		Timer* mTimer;
		
		std::list<smart_ptr<RigidBodyObject> > mRigidBodies;
		
		
		/* physics world */
		btDiscreteDynamicsWorld* mWorld;
		
		/* physics components */
		btAxisSweep3* mBroadphase;
		btCollisionDispatcher* mDispatch;
		btDefaultCollisionConfiguration* mConfig;
		btSequentialImpulseConstraintSolver* mSolver;
	};

}}


#endif /* SOMA_PHYSICS_WORLD_H_ */
