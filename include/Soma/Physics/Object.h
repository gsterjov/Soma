
#ifndef SOMA_PHYSICS_OBJECT_H_
#define SOMA_PHYSICS_OBJECT_H_


#include <Soma/Config.h>
#include <Soma/SmartPointer.h>


namespace Soma {
namespace Physics {

	/**
	 * Base physics object.
	 */
	class SOMA_API Object : public ReferenceCounter
	{
		
	};

}}


#endif /* SOMA_PHYSICS_OBJECT_H_ */
