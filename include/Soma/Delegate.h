/*
 * Delegate.h
 *
 *  Created on: Jul 31, 2010
 *      Author: sterjov
 */

#ifndef SOMA_DELEGATE_H_
#define SOMA_DELEGATE_H_


#include <cassert>

#include <Soma/Config.h>


namespace Soma
{

	/**
	 * Generic delegate interface.
	 * 
	 * The delegate interface defines the implementation required by all
	 * delegates. As such all delegate types are sure to provide a
	 * consistent use such as comparing and calling delegates.
	 */
	template <class ReturnType, class ArgType>
	class SOMA_API DelegateInterface
	{
	public:
		/**
		 * Compares the delegate's callback function.
		 * @return True if the same, false otherwise.
		 */
		virtual bool equals (const DelegateInterface* delegate) const = 0;
		
		
		/**
		 * Runs the delegate callback function.
		 * This will effectively run the function wrapped by the delegate.
		 * 
		 * @param arg The parameter arguments for the callback.
		 * @return The return value of the callback.
		 */
		virtual ReturnType call (ArgType arg) = 0;
		
		
		/**
		 * Clones the delegate.
		 * @return A deep copy of the delegate object.
		 */
		virtual DelegateInterface* clone() const = 0;
	};
	
	
	
	
	/**
	 * Generic non-static member function delegate.
	 * 
	 * Wraps a non-static member function of the provided object to be used
	 * as a delegate. This is required by the event system if one wants to
	 * provide a callback to a member function whenever an event is emitted but
	 * it can also be used to wrap member functions in general and passed
	 * around for comparisons, execution, and so on.
	 */
	template <class ClassType, class ReturnType, class ArgType>
	class SOMA_API Delegate : public DelegateInterface<ReturnType, ArgType>
	{
	public:
		/**
		 * The type of a non-static member function callback.
		 */
		typedef ReturnType (ClassType::*Callback) (ArgType arg);
		
		
		
		/**
		 * Constructor.
		 * Creates a delegate wrapping a member function callback residing on
		 * the specified object.
		 * 
		 * @param object The object where the member function resides.
		 * @param callback The non-static member function to execute upon an
		 * event emission.
		 */
		Delegate (ClassType* object, Callback callback)
		: mObject (object),
		  mCallback (callback)
		{
		}
		
		
		/**
		 * Copy constructor.
		 * Creates a deep copy of the specified delegate.
		 * 
		 * @param delegate The delegate to copy.
		 */
		Delegate (const Delegate& delegate)
		: mObject (delegate.mObject),
		  mCallback (delegate.mCallback)
		{
		}
		
		
		
		/**
		 * @copydoc DelegateInterface<ReturnType, ArgType>::equals
		 */
		bool equals (const DelegateInterface<ReturnType, ArgType>* delegate) const
		{
			const Delegate* del = dynamic_cast<const Delegate*> (delegate);
			
			if (del)
				return (mObject == del->mObject && mCallback == del->mCallback);
			else
				return false;
		}
		
		
		
		/**
		 * @copydoc DelegateInterface<ReturnType, ArgType>::call
		 */
		ReturnType call (ArgType arg)
		{
			assert (mObject);
			assert (mCallback);
			return (mObject->*mCallback) (arg);
		}
		
		
		
		/**
		 * @copydoc DelegateInterface<ReturnType, ArgType>::clone
		 */
		DelegateInterface<ReturnType, ArgType>* clone() const
		{
			/* call copy constructor */
			return new Delegate (*this);
		}
		
		
		
	private:
		/* the object to call the member function on */
		ClassType* mObject;
		
		/* the member function callback */
		Callback mCallback;
	};
	
	
	
	
	
	/**
	 * Wraps a non-static member function in a delegate.
	 * 
	 * This is a convenience function to allow simple delegate wrapping in the
	 * form of a function rather than manually constructing and handling
	 * delegate instances. It is particularly useful when registering and
	 * unregistering a lot of event callbacks.
	 * 
	 * @param object The class instance to call the callback on.
	 * @param callback The non-static member function to call upon execution.
	 */
	template <class ClassType, class ReturnType, class ArgType>
	Delegate<ClassType, ReturnType, ArgType> delegate (ClassType *object, ReturnType (ClassType::*callback) (ArgType arg))
	{
		return Delegate<ClassType, ReturnType, ArgType> (object, callback);
	}

}


#endif /* SOMA_DELEGATE_H_ */
