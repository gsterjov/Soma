
#ifndef SOMA_ENGINE_H_
#define SOMA_ENGINE_H_


#include <map>
#include <vector>

#include <Soma/Config.h>
#include <Soma/String.h>
#include <Soma/Singleton.h>
#include <Soma/Event.h>


namespace Soma
{

	/* forward declarations */
	class Log;
	class Timer;
	class SubSystem;
	class Context;
	class World;
	
	
	
	/**
	 * The frame tick event argument raised by the game loop.
	 */
	struct SOMA_API TickEventArgs
	{
		/**
		 * The amount of time elapsed since the last tick.
		 */
		float elapsed;
	};
	
	
	
	/**
	 * Low-level Engine class.
	 * 
	 * The Engine class manages the internal game loop and as
	 * such all classes that need to be processed regulary will have to
	 * attach itself to an Engine class.
	 */
	class SOMA_API Engine : public Singleton<Engine>
	{
	public:
		/**
		 * Constructor.
		 * 
		 * The Engine constructor initialises all subsystems needed for operation.
		 * Its possible to initialise all the subsystems manually and forego the
		 * Engine class entirely but a game loop will have to then be created to
		 * support the aforementioned subsystems.
		 */
		Engine ();
		
		/**
		 * Destructor.
		 * 
		 * The Engine destructor shuts down all loaded subsystems and free's any
		 * managed classes. As a result it should be safe to bring the entire
		 * engine down simply by destroying the created Engine class. However, any
		 * subsystems initialised or created outside of the Engine class will still
		 * exist, this is not a fool proof memory clean-up, its merely a
		 * convenience.
		 */
		~Engine ();
		
		
		/**
		 * Initialises the engine and its subsystems.
		 * 
		 * The Engine class coordinates stepping between all the subsystems and
		 * controls the internal game loop providing the appropriate subsystems
		 * with a timing mechanism, crucial for steps that need a 'time since last
		 * tick' delta, Initialisation sets up those subsystems and timer.
		 * 
		 * @return True if successful, false otherwise.
		 */
		bool initialise ();
		
		/**
		 * Reinitialises the engine and its subsystems.
		 * 
		 * This will effectively initialise the subsystems without shutting them
		 * down and reset the timer. When changes are made to a subsystem a
		 * reinitialisation is usually required to apply those changes.
		 * Reinitialising with the Engine class will effectively apply all
		 * changes to all subsystems and provide new timing values as a result
		 * of resetting the timer, which may be necessary for some subsystems
		 * upon reinitialisation.
		 */
		bool reinitialise ();
		
		/**
		 * Shuts down the engine and its subsystems.
		 * 
		 * Once the engine is shut down objects created from any of its subsystems
		 * are no longer valid and should be discarded. This is only necessary
		 * if manually cleaning up or wanting a 'hard reset' by shutting down and
		 * initialising all subsystems again, which may be required as
		 * reinitialise() primarily reloads configurations rather than reloading
		 * the entire subsystem.
		 */
		void shutdown ();
		
		/**
		 * Is the engine initialised?
		 * @return True if initialised, false otherwise.
		 */
		bool isInitialised() { return mInitialised; }
		
		
		/**
		 * Run the game loop.
		 * 
		 * This is a blocking method and will only return once quit() is called.
		 * Since threading is problematic when controlling a large amount of
		 * exclusive subsystems one would normally quit() the game loop by hooking
		 * into its step() process. All that is required for an application to
		 * run the engine is to call initialise() to load the various subsystems
		 * and configurations and then call run() to step through each subsystems
		 * and event frame by frame.
		 */
		void run ();
		
		/**
		 * Stops the game loop.
		 * 
		 * Calling this method is the only way to have a previously game
		 * loop running (call with run() method) to return. It does nothing more
		 * than ensure the run() method no longer calls step() each frame.
		 */
		void quit ();
		
		/**
		 * Processes a single frame.
		 * 
		 * Stepping is the process of creating and outputting one entire frame
		 * within the game loop. It will effectively render the next frame, queue
		 * the audio buffer, simulate physics and step through other subsystems
		 * that need a frame-by-frame approach. Calling step() manually allows
		 * for a manual game loop whilst still utilising the engine and its
		 * automatic subsystem management. Such examples are stepping through
		 * the engine inside a Gtk+ main loop so as to embed the engine in the GUI
		 * toolkit.
		 */
		void step ();
		
		
		/**
		 * Mount a resource path.
		 * 
		 * To be able to load any resources it must firt be found within the
		 * virtual filesystem which hides dependent implementation and pools
		 * together all resource paths so that resources can be accessed
		 * without having to worry about its absolute location.
		 * 
		 * @param path The absolute resource path to mount onto the virtual
		 * filesystem.
		 */
		void mount (const String& path);
		
		/**
		 * Unmount a resource path.
		 * 
		 * Unmounting a previously mounted resource path effectively removes
		 * it from the virtual filesystem ensuring that the resources contained
		 * within the path cannot be accessed.
		 * 
		 * @param path The absolute resource path to unmount from the virtual
		 * filesystem.
		 */
		void unmount (const String& path);
		
		
		
		/**
		 * Set engine loop throttle.
		 * 
		 * Throttling the engine loop will halt the engine loop for the minimum
		 * amount of time set whenever the last step completed under the
		 * specified time, hence we throttle the loop to slow it down and not
		 * waste CPU cycles on tasks that go unnoticed. For example, there is
		 * no point in rendering a frame more than once within 10 milliseconds
		 * since a rate this high is unlikely to be perceived by the human eye.
		 * 
		 * By default the engine loop is throttled at 15 milliseconds.
		 * 
		 * @param time The minimum amount of time to spend in a single engine
		 * loop (in seconds).
		 */
		void setThrottle (float time) { mThrottle = time; }
		
		/**
		 * Get the engine loop throttle time.
		 * @return The minimum time (in seconds) the engine will spend in a 
		 * single loop.
		 */
		float getThrottle() { return mThrottle; }
		
		
		
		/**
		 * Create an engine context.
		 * @param name The name of the newly created context.
		 */
		Context* createContext (const String& name);
		
		/**
		 * Destroy an engine context.
		 * @param name The name of the context to be destroyed.
		 */
		void destroyContext (const String& name);
		
		/**
		 * Destroy an engine context.
		 * @param context The context to destroy.
		 */
		void destroyContext (Context* context);
		
		/**
		 * Retrieve an engine context.
		 * @param name The name of the context to retrieve.
		 * @return The engine context if found, NULL otherwise.
		 */
		Context* getContext (const String& name) const;
		
		
		
		/**
		 * Create a world.
		 * @param name The name of the newly created world.
		 */
		World* createWorld (const String& name);
		
		/**
		 * Destroy a world.
		 * @param name The name of the world to be destroyed.
		 */
		void destroyWorld (const String& name);
		
		/**
		 * Destroy a world.
		 * @param world The world to destroy.
		 */
		void destroyWorld (World* world);
		
		/**
		 * Retrieve a world.
		 * @param name The name of the world to retrieve.
		 * @return The world if found, NULL otherwise.
		 */
		World* getWorld (const String& name) const;
		
		
		
		/**
		 * A single tick of the game loop just completed.
		 * @param args Tick event arguments.
		 */
		Event<void, const TickEventArgs&> Tick;
		
		
		
	private:
		/* subsystems */
		Log* mLog;
		
		/* container types */
		typedef std::vector<SubSystem*> SubSystemList;
		typedef std::map<String, Context*> ContextMap;
		typedef std::map<String, World*> WorldMap;
		
		SubSystemList mSubSystems;
		ContextMap mContexts;
		WorldMap mWorlds;
		
		
		/* variables */
		bool mInitialised;
		bool mRunning;
		Timer* mTimer;
		
		float mThrottle;
	};
}


#endif /* SOMA_ENGINE_H_ */
