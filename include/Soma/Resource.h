/**
 * @defgroup resources Resources.
 * 
 * Resources hold very specific data for a particular use, such as mesh data
 * for a mesh object, audio frames for an audio source, and so on. The data
 * in this case should be 'instanced' and thus a specific resource should
 * always be the only one created and loaded, of which the ResourceManager
 * ensures.
 * 
 * Resources are thus used by an entity or component to instanciate the data
 * and use it as they please without affecting the actual resource in question.
 * Each resource has a very specific data requirement. Some only need audio
 * frames whilst others can handle audio frames, mesh data, physics data and
 * material definitions all in one (such as the asset resource).
 * 
 * To allow for this wide range of functionality resources implement features
 * typical of component, such as type data, to allow a generic use of varying
 * resource types.
 */


#ifndef SOMA_RESOURCE_H_
#define SOMA_RESOURCE_H_


#include <cstddef>  /* size_t */

#include <Soma/Config.h>
#include <Soma/SmartPointer.h>
#include <Soma/Vri.h>


namespace Soma
{

	/**
	 * Engine resource interface.
	 * 
	 * All resources within the engine implement this interface to allow for
	 * proper loading and unloading as well as more sophisticated memory
	 * management if a custom resource manager is implemented.
	 * 
	 * @ingroup resources
	 */
	class SOMA_API Resource : public ReferenceCounter
	{
	public:
		/**
		 * Get the resource type.
		 * @return The type of the resource.
		 */
		virtual const String& getType() const = 0;
		
		/**
		 * Get the virtual resource identifier.
		 * @return The resource identifier.
		 */
		virtual const Vri& getVri() const = 0;
		
		/**
		 * The in-memory size of the resource.
		 * @return The size of the resource memory block.
		 */
		virtual size_t getSize() const = 0;
		
		
		/**
		 * Loads the resource.
		 * @return True if loaded, false otherwise.
		 */
		virtual bool load () = 0;
		
		/**
		 * Reloads the resource without unloading it.
		 * @return True if loaded, false otherwise.
		 */
		virtual bool reload () = 0;
		
		/**
		 * Unloads the resource.
		 */
		virtual void unload () = 0;
	};

}


#endif /* SOMA_RESOURCE_H_ */
