
#ifndef SOMA_SCRIPTING_LUA_VISUAL_MATERIAL_H_
#define SOMA_SCRIPTING_LUA_VISUAL_MATERIAL_H_


#include <Soma/Config.h>
#include <Soma/String.h>
#include <Soma/Vri.h>
#include <Soma/SmartPointer.h>

#include <Soma/Visual/Material.h>



/* forward declarations */
struct lua_State;



namespace Soma {
namespace Scripting {
namespace Lua {


	/**
	 * Lua visual material class.
	 */
	class SOMA_API VisualMaterial : public Visual::Material
	{
	public:
		/**
		 * Constructor.
		 * @param vri The path to the resource within an asset.
		 */
		explicit VisualMaterial (const Vri& vri);
		
		/**
		 * Name constructor.
		 */
		explicit VisualMaterial (const String& name);
		
		/**
		 * Destructor.
		 */
		~VisualMaterial ();
		
		
		/* material resource implementation */
		bool loadMaterial (lua_State* state);
		
		
		
		/** Log source name. */
		static String LogSource;
		
		
		
	private:
		Vri mVri;
		
		
		/* utilities */
		Colour getColour (lua_State* state);
		
		
		/* script parsing */
		void getTechnique (lua_State* state, const String& name);
		
		void getPass (lua_State* state,
		              const String& name,
		              smart_ptr<Visual::Technique>& technique);
	};
	
	
	
	
	/**
	 * Lua material resource format.
	 */
	struct SOMA_API MaterialFormat : public Visual::MaterialManager::Format
	{
		/**
		 * @copydoc Visual::MaterialManager::Format::getExt
		 */
		String getExt() const { return ".lua"; }
		
		/**
		 * @copydoc Visual::MaterialManager::Format::create
		 */
		Visual::Material* create (const Vri& vri) const { return new VisualMaterial (vri); }
	};


}}}



#endif /* SOMA_SCRIPTING_LUA_VISUAL_MATERIAL_H_ */
