
#ifndef SOMA_SCRIPTING_LUA_MATERIAL_H_
#define SOMA_SCRIPTING_LUA_MATERIAL_H_


#include <Soma/Config.h>
#include <Soma/Vri.h>
#include <Soma/SmartPointer.h>



/* forward declarations */
struct lua_State;



namespace Soma {
namespace Scripting {
namespace Lua {


	/* forward declarations */
	class VisualMaterial;
	
	
	
	/**
	 * Lua material class.
	 */
	class SOMA_API Material : public ReferenceCounter
	{
	public:
		/**
		 * Constructor.
		 */
		Material (const Vri& vri);
		
		/**
		 * Destructor.
		 */
		~Material ();
		
		
		
		/**
		 * Get visual material.
		 */
		const smart_ptr<VisualMaterial>& getVisualMaterial() { return mVisualMaterial; }
		
		
		
		/** Log source name. */
		static String LogSource;
		
		
	private:
		Vri mVri;
		
		smart_ptr<VisualMaterial> mVisualMaterial;
		
		
		void load (const Vri& vri);
	};


}}}


#endif /* SOMA_SCRIPTING_LUA_MATERIAL_H_ */
