
#ifndef SOMA_SCRIPTING_LUA_MANAGER_H_
#define SOMA_SCRIPTING_LUA_MANAGER_H_


#include <Soma/Config.h>
#include <Soma/String.h>
#include <Soma/Singleton.h>
#include <Soma/SubSystem.h>
#include <Soma/Vri.h>



/* forward declarations */
struct lua_State;



namespace Soma {
namespace Scripting {
namespace Lua {


	/**
	 * Lua host manager class.
	 */
	class SOMA_API Manager : public Singleton<Manager>,
	                         public SubSystem
	{
	public:
		/**
		 * Constructor.
		 */
		Manager ();
		
		/**
		 * Destructor.
		 */
		~Manager ();
		
		
		/**
		 * @copydoc SubSystem::initialise
		 */
		bool initialise ();
		
		/**
		 * @copydoc SubSystem::reinitialise
		 */
		bool reinitialise ();
		
		/**
		 * @copydoc SubSystem::shutdown
		 */
		void shutdown ();
		
		/**
		 * @copydoc SubSystem::isInitialised
		 */
		bool isInitialised() { return mInitialised; }
		
		
		
		/**
		 * Runs the specified Lua script.
		 * @param vri The path to the script.
		 * @return True if run was successful, false otherwise.
		 */
		bool runScript (const Vri& vri);
		
		
		
		/**
		 * Dump the contents of the virtual stack.
		 */
		void dumpStack ();
		
		
		/**
		 * Get the internal Lua state.
		 * @return The Lua state.
		 */
		lua_State* getState() const { return mState; }
		
		
		
		/** Log source name. */
		static String LogSource;
		
		
		
	private:
		bool mInitialised;
		
		lua_State* mState;
		
		
		bool checkError (int status);
	};


}}}


#endif /* SOMA_SCRIPTING_LUA_MANAGER_H_ */
