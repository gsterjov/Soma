
#ifndef SOMA_CONFIGURABLE_H_
#define SOMA_CONFIGURABLE_H_


#include <Soma/Config.h>
#include <Soma/ConfigInterface.h>


namespace Soma
{

	/**
	 * Abstract settings class.
	 * 
	 * The Configurable class allows inheriting classes to provide a layer
	 * of persistence. All settings are stored in a ConfigInterface which
	 * determine how the settings are persisted. Configurable exposes a
	 * clean and easy way to modify, restore and save settings of any
	 * subsystem within the engine.
	 */
	class SOMA_API Configurable
	{
	public:
		/**
		 * Sets the configuration values to the current settings
		 * 
		 * Calling save will set the current settings to the specified
		 * configuration. It does not save the actual configuration as that is
		 * left to the owner of the class. Rather it saves its settings by
		 * modifying the configuration values, which can then be made
		 * persistent by calling ConfigInterface::save().
		 * 
		 * @param config The configuration to save settings to.
		 * @returns True if successful, false otherwise.
		 */
		virtual bool save (ConfigInterface& config) = 0;
		
		/**
		 * Restores settings from the specified configuration.
		 * 
		 * @param config The configuration to restore settings from.
		 * @returns True if successful, false otherwise.
		 */
		virtual bool restore (const ConfigInterface& config) = 0;
	};

}


#endif /* SOMA_CONFIGURABLE_H_ */
