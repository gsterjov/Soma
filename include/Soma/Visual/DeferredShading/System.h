
#ifndef SOMA_VISUAL_DEFERRED_SHADING_SYSTEM_H_
#define SOMA_VISUAL_DEFERRED_SHADING_SYSTEM_H_


#include <Soma/Config.h>
#include <Soma/SubSystem.h>


/* forward declaration */
namespace Ogre { class Viewport; class CompositorInstance; }


namespace Soma {
namespace Visual {

	/**
	 * Deferred shading system.
	 * 
	 * This system is an alternative to the classical forward rendering
	 * system whereby the light is calculated on each object per light. The
	 * deferred shading system, however, holds lighting information in a
	 * GBuffer which is later used by shaders to compute the lighting allowing
	 * for a faster and higher quality lighting system.
	 */
	class SOMA_API DeferredShading : public SubSystem
	{
	public:
		/**
		 * Constructor.
		 */
		explicit DeferredShading (Ogre::Viewport* viewport);
		
		/**
		 * Destructor.
		 */
		~DeferredShading ();
		
		
		/* SubSystem implementation */
		bool initialise ();
		bool reinitialise ();
		void shutdown ();
		
		bool isInitialised () { return mInitialised; }
		
		
	private:
		bool mInitialised;
		
		Ogre::Viewport* mViewport;
		Ogre::CompositorInstance* mGBuffer;
		Ogre::CompositorInstance* mLighting;
	};

}}


#endif /* SOMA_VISUAL_DEFERRED_SHADING_SYSTEM_H_ */
