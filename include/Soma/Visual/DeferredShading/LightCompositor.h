
#ifndef SOMA_VISUAL_DEFERRED_SHADING_LIGHT_COMPOSITOR_H_
#define SOMA_VISUAL_DEFERRED_SHADING_LIGHT_COMPOSITOR_H_


#include <OgreCompositorInstance.h>
#include <OgreCustomCompositionPass.h>

#include <Soma/Visual/DeferredShading/DeferredLight.h>


namespace Soma {
namespace Visual {

	/**
	 * Deferred light renderer.
	 */
	class LightRenderOperation : public Ogre::CompositorInstance::RenderSystemOperation
	{
	public:
		/**
		 * Constructor.
		 */
		LightRenderOperation (Ogre::CompositorInstance* instance,
		                      const Ogre::CompositionPass* pass);
		
		/**
		 * Destructor.
		 */
		~LightRenderOperation ();
		
		
		/**
		 * Execute the render operation.
		 */
		void execute (Ogre::SceneManager *sm, Ogre::RenderSystem *rs);
		
		
	private:
		Ogre::Viewport* mViewport;
		
		/* light list */
		typedef std::map<Ogre::Light*, DeferredLight*> LightMap;
		LightMap mLights;
	};
	
	
	
	/**
	 * Deferred light compositor.
	 */
	class LightCompositor : public Ogre::CustomCompositionPass
	{
	public:
		/**
		 * Create render operation.
		 */
		Ogre::CompositorInstance::RenderSystemOperation*
		createOperation (Ogre::CompositorInstance* instance,
		                 const Ogre::CompositionPass* pass)
		{
			return new LightRenderOperation (instance, pass);
		}
	};

}}


#endif /* SOMA_VISUAL_DEFERRED_SHADING_LIGHT_COMPOSITOR_H_ */
