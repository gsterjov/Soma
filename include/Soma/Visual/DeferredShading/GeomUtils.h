
#ifndef SOMA_VISUAL_DEFERRED_SHADING_GEOM_UTILS_H_
#define SOMA_VISUAL_DEFERRED_SHADING_GEOM_UTILS_H_

#include <OgreString.h>
#include <OgreVertexIndexData.h>


namespace Soma {
namespace Visual {

	/**
	 * A class for creating common geometry shapes.
	 */
	class GeomUtils
	{
	public:
		
		static void createCone (Ogre::VertexData* vertexData,
		                        Ogre::IndexData* indexData,
		                        float radius,
		                        float height,
		                        int nVerticesInBase);
	};

}}


#endif /* SOMA_VISUAL_DEFERRED_SHADING_GEOM_UTILS_H_ */
