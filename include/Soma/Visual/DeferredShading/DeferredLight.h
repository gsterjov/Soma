
#ifndef SOMA_VISUAL_DEFERRED_SHADING_DEFERRED_LIGHT_H_
#define SOMA_VISUAL_DEFERRED_SHADING_DEFERRED_LIGHT_H_


#include <OgreSimpleRenderable.h>


namespace Soma {
namespace Visual {

	/**
	 * Deferred light geometry.
	 */
	class DeferredLight : public Ogre::SimpleRenderable
	{
	public:
		/**
		 * Constructor.
		 */
		DeferredLight (Ogre::Light* light);
		
		/**
		 * Destructor.
		 */
		~DeferredLight ();
		
		
		/* SimpleRenderable implementation */
		Ogre::Real getBoundingRadius () const;
		Ogre::Real getSquaredViewDepth (const Ogre::Camera* camera) const;
		void getWorldTransforms (Ogre::Matrix4* trans) const;
		
		
	private:
		Ogre::Light* mLight;
		
		float mRadius;
		
		/**
		 * Create a cone.
		 */
		void createCone ();
	};

}}


#endif /* SOMA_VISUAL_DEFERRED_SHADING_DEFERRED_LIGHT_H_ */
