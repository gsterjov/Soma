
#ifndef SOMA_VISUAL_MESH_H_
#define SOMA_VISUAL_MESH_H_


#include <Soma/Config.h>
#include <Soma/Resource.h>
#include <Soma/ResourceManager.h>
#include <Soma/SmartPointer.h>


/* forward declarations */
namespace Ogre { class Mesh; }


namespace Soma {
namespace Visual {

	/**
	 * A mesh resource containing the shared mesh data.
	 * 
	 * @ingroup resources
	 */
	class SOMA_API Mesh : public Resource
	{
	public:
		/**
		 * Manual constructor.
		 * @param mesh The mesh to use as a resource.
		 */
		explicit Mesh (Ogre::Mesh* mesh);
		
		/**
		 * Empty constructor.
		 */
		Mesh ();
		
		/**
		 * Destructor.
		 */
		virtual ~Mesh ();
		
		
		
		/**
		 * @copydoc Resource::getType
		 */
		const String& getType() const { return Type; }
		
		/**
		 * @copydoc Resource::getVri
		 */
		const Vri& getVri() const { return Vri::EMPTY; }
		
		/**
		 * @copydoc Resource::getSize
		 */
		size_t getSize() const;
		
		
		/**
		 * @copydoc Resource::load
		 */
		bool load ();
		
		/**
		 * @copydoc Resource::reload
		 */
		bool reload ();
		
		/**
		 * @copydoc Resource::unload
		 */
		void unload ();
		
		
		
		/**
		 * The underlying OGRE mesh.
		 * @return The OGRE mesh.
		 */
		Ogre::Mesh* getMesh() { return mMesh; }
		
		/**
		 * The underlying OGRE mesh.
		 * @return The OGRE mesh.
		 */
		const Ogre::Mesh* getMesh() const { return mMesh; }
		
		
		
		/** Log source name. */
		static const String LogSource;
		
		/** Resource type. */
		static const String Type;
		
		
		
	protected:
		/**
		 * Create an Ogre::Mesh to be used by the mesh resource.
		 * @return A newly created OGRE mesh.
		 */
		virtual Ogre::Mesh* createMesh () { return 0; }
		
		
	private:
		Ogre::Mesh* mMesh;
	};
	
	
	
	
	/**
	 * Mesh resource manager.
	 */
	typedef ResourceManager<Mesh> MeshManager;
//	class SOMA_API MeshManager : public ResourceManager<Mesh> {};

}}


#endif /* SOMA_VISUAL_MESH_H_ */
