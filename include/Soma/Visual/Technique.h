
#ifndef SOMA_VISUAL_TECHNIQUE_H_
#define SOMA_VISUAL_TECHNIQUE_H_


#include <Soma/Config.h>
#include <Soma/String.h>
#include <Soma/SmartPointer.h>


/* forward declarations */
namespace Ogre { class Technique; }


namespace Soma {
namespace Visual {


	/* forward declarations */
	class Material;
	class Pass;
	
	/** A list of Passes. */
	typedef std::vector<smart_ptr<Pass> > PassList;
	
	
	
	/**
	 * A material technique.
	 * 
	 * A technique contains specific render passes which determine the final
	 * output of a material. Its primary purpose is to group render operations
	 * into one logical setting such as "High" and "Low" or to provide
	 * maximum compatibility to devices which cannot support a specific
	 * technique and so the material cascades into the next technique until
	 * one is found which satisfies the hardware capability.
	 */
	class SOMA_API Technique : public ReferenceCounter
	{
	public:
		/**
		 * Constructor.
		 * @param material The material this technique belongs to.
		 * @param name The name of the new technique
		 */
		Technique (const weak_ptr<Material>& material, const String& name);
		
		
		/**
		 * Technique constructor.
		 * @param technique The core OGRE technique to use.
		 */
		explicit Technique (Ogre::Technique* technique);
		
		
		
		/**
		 * Get the name of the technique.
		 * @return The technique name.
		 */
		const String& getName() const;
		
		/**
		 * Get the specified pass,
		 * 
		 * @param name The name of the pass to retrieve.
		 * @return The pass if found, NULL reference otherwise.
		 */
		smart_ptr<Pass> getPass (const String& name) const;
		
		/**
		 * Get a list of pass in the order they are processed by this technique.
		 * @return A list of passes.
		 */
		PassList getPasses() const;
		
		
		
		/**
		 * Add a new pass to the technique.
		 * 
		 * This will automatically create a new pass with default values and
		 * store it within the technique. The new pass is returned as a
		 * convenience so that it can be modified without having to retrieve
		 * it first.
		 * 
		 * @param name The name of the new pass.
		 * @return The newly created pass.
		 */
		smart_ptr<Pass> add (const String& name);
		
		/**
		 * Remove a pass from the technique by name.
		 * @param name The name of the pass to remove.
		 */
		void remove (const String& name);
		
		/**
		 * Remove the pass from the material.
		 * @param pass The pass to remove.
		 */
		void remove (const smart_ptr<Pass>& pass);
		
		
		
		/**
		 * The underlying OGRE technique.
		 * @return The OGRE technique.
		 */
		Ogre::Technique* getTechnique() { return mTechnique; }
		
		
		
		/** Log source name. */
		static const String LogSource;
		
		
		
	private:
		Ogre::Technique* mTechnique;
	};

}}


#endif /* SOMA_VISUAL_TECHNIQUE_H_ */
