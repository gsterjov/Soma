
#ifndef SOMA_VISUAL_CAMERA_H_
#define SOMA_VISUAL_CAMERA_H_


#include <Soma/Config.h>
#include <Soma/String.h>
#include <Soma/SmartPointer.h>

#include <Soma/EntityComponent.h>
#include <Soma/Visual/Object.h>


/* forward declarations */
namespace Ogre { class Camera; }


namespace Soma {
namespace Visual {

	/**
	 * A camera renders the entire scene its looking at onto the render target
	 * it was added in.
	 * 
	 * @properties
	 * @prop{ near-clip-distance, float, Read/Write, The near clip distance }
	 * @prop{ look-at, Vector, Write, The location to point the camera at }
	 * @endproperties
	 * 
	 * @ingroup components
	 */
	class SOMA_API Camera : public Object
	{
	public:
		/**
		 * Constructor.
		 */
		Camera ();
		
		/**
		 * Destructor.
		 */
		virtual ~Camera ();
		
		
		/**
		 * @copydoc Object::getType
		 */
		const String& getType() const { return Type; }
		
		
		/**
		 * Set near clip distance.
		 * 
		 * The clip distance of a Camera determines when an object should
		 * be culled. Once the distance between an object and a Camera is
		 * less than the clip distance the object is not rendered.
		 */
		void setNearClipDistance (float distance);
		
		/**
		 * Face the camera at the given point.
		 * @param point The point to face.
		 */
		void lookAt (const Vector& point);
		
		/**
		 * Face the camera at the given point.
		 * 
		 * @param x The X axis of the point to face.
		 * @param y The Y axis of the point to face.
		 * @param z The Z axis of the point to face.
		 */
		void lookAt (float x, float y, float z) { lookAt (Vector (x, y, z)); }
		
		
		/**
		 * The underlying OGRE camera.
		 * @return The OGRE camera.
		 */
		Ogre::Camera* getCamera() { return mCamera; }
		
		
		/* Object implementaiton */
		Ogre::MovableObject* getMovableObject();
		
		
		
		/** Log source name. */
		static const String LogSource;
		
		/** Component type name. */
		static const String Type;
		
		
		
	private:
		Ogre::Camera* mCamera;
		
		
		/* unloaded properties */
		typedef std::pair<String, Value> Property;
		typedef std::vector<Property> PropertyList;
		PropertyList mProps;
		
		
		/* object creation/destruction */
		bool create (const weak_ptr<Visual::World>& world);
		void destroy ();
		
		
		/* get/set component property */
		Value getProperty (const String& name) const;
		void  setProperty (const String& name, const Value& value);
		
		
		/* entity event handlers */
		void onComponentAdded   (const smart_ptr<Component>& component);
		void onComponentRemoved (const smart_ptr<Component>& component);
	};

}}


#endif /* SOMA_VISUAL_CAMERA_H_ */
