
#ifndef SOMA_VISUAL_RENDER_TARGET_H_
#define SOMA_VISUAL_RENDER_TARGET_H_


#include <map>
#include <list>

#include <Soma/Config.h>
#include <Soma/String.h>
#include <Soma/SmartPointer.h>
#include <Soma/ContextComponent.h>


/* forward declaration */
namespace Ogre { class RenderWindow; }


namespace Soma {
namespace Visual {

	/* forward declarations */
	class Camera;
	
	
	
	/**
	 * Render target properties.
	 */
	struct RenderTargetProperties
	{
		int width;
		int height;
		bool fullscreen;
		
		bool useCurrentGLContext;
		
		RenderTargetProperties ()
		: width(0), height(0), fullscreen(false), useCurrentGLContext(false)
		{}
	};
	
	
	
	/**
	 * Renderer output base class.
	 * 
	 * A RenderTarget abstracts away the creation of the output while still
	 * providing the implementation of hooking viewports and modifying output
	 * settings. This allows for consistent operation within the Visual
	 * namespace as it only requires a RenderTarget to achieve its goals of
	 * rendering to an output whilst still allowing for very specific output
	 * implementation, such as an embedded GTK+ renderer widget.
	 * 
	 * @ingroup components
	 */
	class SOMA_API RenderTarget : public ContextComponent
	{
	public:
		/**
		 * Constructor.
		 */
		RenderTarget ();
		
		/**
		 * Destructor.
		 */
		virtual ~RenderTarget ();
		
		
		/**
		 * @copydoc ContextComponent::getType
		 */
		const String& getType() const { return Type; }
		
		
		/**
		 * Get unique window ID.
		 * 
		 * The returned window ID is dependent on the windowing system and its
		 * implementation. As such the ID is only useful to subsystems that
		 * are tightly intergrated with the windowing system, such as the Input
		 * subsystem.
		 * 
		 * @return The specific window implementation identifier.
		 */
		size_t getWindowID();
		
		
		
		/**
		 * Load the render output.
		 * 
		 * This will create the entire visual output context where contents
		 * will be rendered on. Once a render target is created all functions
		 * in the RenderTarget class can be freely used.
		 * 
		 * @return True if successful, false otherwise.
		 */
		virtual bool load () = 0;
		
		/**
		 * Reload the render output.
		 * @return True if successful, false otherwise.
		 */
		virtual bool reload () = 0;
		
		/**
		 * Unload the render output.
		 * 
		 * This will bring down the entire visual output context and as such
		 * the Renderer contents will no longer be rendered on this target.
		 * All functions in the RenderTarget will be invalidated and all
		 * associated references to Visual objects will be removed.
		 */
		virtual void unload () = 0;
		
		/**
		 * Is the render output loaded?
		 * @return True if loaded, false otherwise.
		 */
		bool isLoaded () const { return mRenderWindow; }
		
		
		
		/**
		 * Update the render target.
		 */
		void update ();
		
		/**
		 * Resize the target area.
		 */
		void resize (int width, int height);
		
		
		
		/**
		 * The underlying OGRE render target.
		 * @return The OGRE render target.
		 */
		Ogre::RenderWindow* getTarget() { return mRenderWindow; }
		
		
		
		/** Log source name. */
		static const String LogSource;
		
		/** Subcontext type. */
		static const String Type;
		
		
		
	protected:
		/**
		 * The internally created OGRE render window.
		 */
		Ogre::RenderWindow* mRenderWindow;
		
		
		/**
		 * Create the target with the set properties.
		 * 
		 * This is a convenience function to create a render window based on
		 * the supplied properties. It will store the resulting render window
		 * in the mRenderWindow variable which all subclasses have access to.
		 * This is the recommended way to create render targets.
		 * 
		 * @param title The title to display with the target.
		 * @param props The target properties to use when creating.
		 */
		void createTarget (const String& title,
		                   const RenderTargetProperties& props);
		
		
		
	private:
		/* event handlers */
		void onRequest (CompositeMessage& message);
	};

}}


#endif /* SOMA_VISUAL_RENDER_TARGET_H_ */
