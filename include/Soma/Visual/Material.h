
#ifndef SOMA_VISUAL_MATERIAL_H_
#define SOMA_VISUAL_MATERIAL_H_


#include <vector>

#include <Soma/Config.h>
#include <Soma/String.h>
#include <Soma/Resource.h>
#include <Soma/ResourceManager.h>
#include <Soma/SmartPointer.h>


/* forward declarations */
namespace Ogre { class Material; }


namespace Soma {
namespace Visual {


	/* forward declarations */
	class Technique;
	
	/** A list of Techniques. */
	typedef std::vector<smart_ptr<Technique> > TechniqueList;
	
	
	
	/**
	 * A visual material resource.
	 * 
	 * A visual material is applied to a particular mesh object or submesh.
	 * A material itself doesn't define the rendering operations to
	 * apply to the mesh, rather it holds a list of techniques of which the
	 * right technique to use is determined depending on settings and hardware
	 * capabilities. Once the right technique is found each pass in that
	 * technique is rendered giving the final output. Each technique is
	 * iterated in the order it was added to the material which means that
	 * everything after the first technique is a 'fallback' of sorts until
	 * the right one is found.
	 * 
	 * @ingroup resources
	 */
	class SOMA_API Material : public Resource
	{
	public:
		/**
		 * Constructor.
		 * @param name The name of the new material.
		 */
		explicit Material (const String& name);
		
		/**
		 * Material constructor.
		 * @param material The core OGRE material to use.
		 */
		explicit Material (Ogre::Material* material);
		
		/**
		 * Copy constructor.
		 * 
		 * @param material The material to copy.
		 * @param name The name of the material copy.
		 */
		Material (const Material& material, const String& name);
		
		/**
		 * Destructor.
		 */
		virtual ~Material ();
		
		
		
		/**
		 * Get the name of the material.
		 * @return The material name.
		 */
		const String& getName() const;
		
		/**
		 * Get the specified technique,
		 * 
		 * @param name The name of the technique to retrieve.
		 * @return The technique if found, NULL reference otherwise.
		 */
		smart_ptr<Technique> getTechnique (const String& name) const;
		
		/**
		 * Get a list of techniques in the order they are processed by
		 * this material.
		 * 
		 * @return A list of techniques.
		 */
		TechniqueList getTechniques() const;
		
		
		
		/**
		 * Add a new material technique.
		 * 
		 * This will automatically create a new technique with default values
		 * and store it within the material. The new technique is returned as
		 * a convenience so that it can be modified without having to retrieve
		 * it first.
		 * 
		 * @param name The name of the new technique.
		 * @return The newly created technique.
		 */
		smart_ptr<Technique> add (const String& name);
		
		/**
		 * Remove a technique from the material by name.
		 * @param name The name of the technique to remove.
		 */
		void remove (const String& name);
		
		/**
		 * Remove the technique from the material.
		 * @param technique The technique to remove.
		 */
		void remove (const smart_ptr<Technique>& technique);
		
		
		/**
		 * Clears the material remove all techniques.
		 */
		void clear ();
		
		
		
		/**
		 * @copydoc Resource::getType
		 */
		const String& getType() const { return Type; }
		
		/**
		 * @copydoc Resource::getVri
		 */
		const Vri& getVri() const { return Vri::EMPTY; }
		
		/**
		 * @copydoc Resource::getSize
		 */
		size_t getSize() const;
		
		
		/**
		 * @copydoc Resource::load
		 */
		virtual bool load ();
		
		/**
		 * @copydoc Resource::reload
		 */
		virtual bool reload ();
		
		/**
		 * @copydoc Resource::unload
		 */
		virtual void unload ();
		
		
		
		/**
		 * The underlying OGRE material.
		 * @return The OGRE material.
		 */
		Ogre::Material* getMaterial() { return mMaterial; }
		
		
		
		/** Log source name. */
		static const String LogSource;
		
		/** Resource type. */
		static const String Type;
		
		
		
	protected:
		/**
		 * Empty constructor.
		 * 
		 * This is convenient for resources that must first be loaded before
		 * it can know the material name.
		 * 
		 * @note No internal material is created with this constructor thus no
		 * Material members should be called until an internal material exists.
		 */
		Material () : mMaterial(0) {}
		
		
		/**
		 * The internal OGRE material.
		 */
		Ogre::Material* mMaterial;
		
		
		
		/**
		 * Create the internal material with default values.
		 * @param name The name of the material.
		 */
		void create (const String& name);
	};
	
	
	
	
	/**
	 * Material resource manager.
	 */
	typedef ResourceManager<Material> MaterialManager;
//	class SOMA_API MaterialManager : public ResourceManager<Material> {};

}}


#endif /* SOMA_VISUAL_MATERIAL_H_ */
