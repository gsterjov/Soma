
#ifndef SOMA_VISUAL_VIEWPORT_H_
#define SOMA_VISUAL_VIEWPORT_H_


#include <map>

#include <Soma/Config.h>
#include <Soma/String.h>
#include <Soma/SmartPointer.h>
#include <Soma/ContextComponent.h>


/* forward declaration */
namespace Ogre { class Viewport; }


namespace Soma {

	/* forward declarations */
	class Entity;


namespace Visual {

	/* forward declarations */
	class Camera;
	class RenderTarget;
	
	
	
	/**
	 * A render target viewport.
	 * 
	 * A viewport defines the location and dimensions to display a camera's
	 * perspective on the associated render target. This effectively allows
	 * the viewport to be consistent across all RenderTarget's simply by
	 * adding it to a different context.
	 * 
	 * @properties
	 * @prop{ camera, smart_ptr<Camera>, Read/Write, The attached camera }
	 * @prop{ camera-entity, smart_ptr<Entity>, Write, The attached camera entity }
	 * @prop{ background-colour, Colour, Read/Write, The background colour of the viewport }
	 * @endproperties
	 */
	class SOMA_API Viewport : public ContextComponent
	{
	public:
		/**
		 * Constructor.
		 */
		Viewport ();
		
		/**
		 * Destructor.
		 */
		virtual ~Viewport ();
		
		
		/**
		 * @copydoc SubContext::getType
		 */
		const String& getType() const { return Type; }
		
		
		/**
		 * Get the camera attached to viewport.
		 * @return The attached camera if it exists, NULL reference otherwise.
		 */
		const smart_ptr<Camera>& getCamera() { return mCamera; }
		
		
		/**
		 * Get the background colour of the viewport.
		 * @return The background colour.
		 */
		Colour getBackgroundColour() const { return mBackColour; }
		
		
		
		/**
		 * Set the camera to render on the viewport.
		 * @param camera The camera to render.
		 */
		void setCamera (const smart_ptr<Camera>& camera);
		
		/**
		 * Set the camera to render on the viewport.
		 * 
		 * This variant takes an entity and looks for a camera component. If
		 * one isn't found an exception will be thrown.
		 * 
		 * @param entity The entity with a camera component to render.
		 */
		void setCamera (const smart_ptr<Entity>& entity);
		
		
		/**
		 * Set the background colour of the viewport.
		 * @param colour The background colour
		 */
		void setBackgroundColour (const Colour& colour);
		
		/**
		 * Set the background colour of the viewport.
		 * 
		 * @param r The red component of the background.
		 * @param g The green component of the background.
		 * @param b The blue component of the background.
		 */
		void setBackgroundColour (float r, float g, float b)
		{ setBackgroundColour (Colour (r, g, b)); }
		
		
		
		/**
		 * @copydoc ContextComponent::load
		 */
		bool load ();
		
		/**
		 * @copydoc ContextComponent::reload
		 */
		bool reload ();
		
		/**
		 * @copydoc ContextComponent::unload
		 */
		void unload ();
		
		/**
		 * @copydoc ContextComponent::isLoaded
		 */
		bool isLoaded () const { return mLoaded; }
		
		
		
		/**
		 * The underlying OGRE viewport.
		 * @return The OGRE viewport.
		 */
		Ogre::Viewport* getViewport() { return mViewport; }
		
		
		
		/** Log source name. */
		static const String LogSource;
		
		/** Subcontext type. */
		static const String Type;
		
		
		
	private:
		bool mLoaded;
		Colour mBackColour;
		
		smart_ptr<Camera> mCamera;
		smart_ptr<RenderTarget> mRenderTarget;
		
		Ogre::Viewport* mViewport;
		
		
		
		/* get/set subcontext property */
		Value getProperty (const String& name) const;
		void  setProperty (const String& name, const Value& value);
		
		
		/* context events */
		void onComponentAdded   (const smart_ptr<Component>& component);
		void onComponentRemoved (const smart_ptr<Component>& component);
		
		/* camera events */
		void onCameraLoad (const weak_ptr<Component>& component);
		void onCameraUnload (const weak_ptr<Component>& component);
	};

}}


#endif /* SOMA_VISUAL_VIEWPORT_H_ */
