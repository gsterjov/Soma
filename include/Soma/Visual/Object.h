
#ifndef SOMA_VISUAL_OBJECT_H_
#define SOMA_VISUAL_OBJECT_H_


#include <Soma/Config.h>
#include <Soma/SmartPointer.h>

#include <Soma/Visual/VisualComponent.h>


/* forward declarations */
namespace Ogre { class MovableObject; }


namespace Soma {
namespace Visual {

	/* forward declarations */
	class World;
	
	
	
	/**
	 * Base visual object.
	 */
	class SOMA_API Object : public VisualComponent
	{
	public:
		/**
		 * @copydoc VisualComponent::getType
		 */
		virtual const String& getType() const = 0;
		
		
		/**
		 * The underlying OGRE movable object.
		 * @return The OGRE movable object.
		 */
		virtual Ogre::MovableObject* getMovableObject() = 0;
	};

}}


#endif /* SOMA_VISUAL_OBJECT_H_ */
