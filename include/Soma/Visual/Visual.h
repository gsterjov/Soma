
#ifndef SOMA_VISUAL_H_
#define SOMA_VISUAL_H_


#include <Soma/Visual/Manager.h>
#include <Soma/Visual/World.h>
#include <Soma/Visual/Terrain.h>

#include <Soma/Visual/RenderTarget.h>
#include <Soma/Visual/Window.h>
#include <Soma/Visual/Viewport.h>

#include <Soma/Visual/Mesh.h>
#include <Soma/Visual/Material.h>
#include <Soma/Visual/Technique.h>
#include <Soma/Visual/Pass.h>
#include <Soma/Visual/Manager.h>

#include <Soma/Visual/VisualComponent.h>
#include <Soma/Visual/Object.h>
#include <Soma/Visual/Node.h>
#include <Soma/Visual/Camera.h>
#include <Soma/Visual/Light.h>
#include <Soma/Visual/MeshObject.h>


#endif /* SOMA_VISUAL_H_ */
