
#ifndef SOMA_VISUAL_WINDOW_H_
#define SOMA_VISUAL_WINDOW_H_


#include <cstddef>  /* size_t */

#include <Soma/Config.h>
#include <Soma/String.h>
#include <Soma/Visual/RenderTarget.h>


/* forward declarations */
namespace Ogre { class RenderWindow; }


namespace Soma {
namespace Visual {

	/**
	 * A standalone render window.
	 * 
	 * The render window is a self contained window displaying the rendered
	 * output directly. As a result the type of Window created is entirely
	 * dependant on a specific implementation (ie. the X window system). This
	 * class abstracts away the internal implementation details and allows for
	 * a generic window to be created with the appropriately set configuration.
	 * 
	 * Being a RenderTarget it also cleanly integrates with other subsystems
	 * and can be used throughout the engine.
	 * 
	 * @ingroup components
	 */
	class SOMA_API Window : public RenderTarget
	{
	public:
		/**
		 * Constructor.
		 */
		Window ();
		
		/**
		 * Title constructor.
		 * @param title The title to display with the window.
		 */
		explicit Window (const String& title);
		
		/**
		 * Destructor.
		 * This will destroy the window and remove it from the renderer output.
		 */
		virtual ~Window ();
		
		/**
		 * Get the window title.
		 * @return The title displayed with the window.
		 */
		const String& getTitle() const { return mTitle; }
		
		
		/**
		 * Set the window title.
		 * @param title The title to display with the window.
		 */
		void setTitle (const String& title) { mTitle = title; }
		
		
		/**
		 * @copydoc RenderTarget::initialise
		 */
		bool load ();
		
		/**
		 * @copydoc RenderTarget::reload
		 */
		bool reload ();
		
		/**
		 * @copydoc RenderTarget::shutdown
		 */
		void unload ();
		
		
		
		/**
		 * Log source name.
		 */
		static const String LogSource;
		
		
	private:
		String mTitle;
	};

}}


#endif /* SOMA_VISUAL_WINDOW_H_ */
