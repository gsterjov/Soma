
#ifndef SOMA_VISUAL_TERRAIN_H_
#define SOMA_VISUAL_TERRAIN_H_


#include <Soma/Config.h>
#include <Soma/String.h>
#include <Soma/SmartPointer.h>
#include <Soma/WorldComponent.h>
#include <Soma/Vri.h>


/* forward declarations */
namespace Ogre { class TerrainGlobalOptions; class TerrainGroup; class Terrain; }


namespace Soma {
namespace Visual {

	/* forward declarations */
	class Light;
	
	
	
	/**
	 * A terrain base class which provides an interface to access and modify
	 * terrain data.
	 * 
	 * @ingroup components
	 */
	class SOMA_API Terrain : public WorldComponent
	{
	public:
		/**
		 * Constructor.
		 */
		Terrain ();
		
		/**
		 * Destructor.
		 */
		virtual ~Terrain ();
		
		
		/**
		 * @copydoc WorldComponent::getType
		 */
		const String& getType() const { return Type; }
		
		
		/**
		 * Get the global terrain light.
		 * @return The global light being used.
		 */
		const smart_ptr<Light>& getLight() { return mLight; }
		
		/**
		 * Get the terrain height map VRI.
		 * @return The VRI to the height map being used.
		 */
		const Vri& getHeightMap() { return mHeightMap; }
		
		
		
		/**
		 * Set the global light to be used by the terrain.
		 * @param light The light to generate lightmaps with.
		 */
		void setLight (const smart_ptr<Light>& light) { mLight = light; }
		
		/**
		 * Set the terrain height map VRI.
		 * @param vri The VRI to the height map to be used.
		 */
		void setHeightMap (const Vri& vri) { mHeightMap = vri; }
		
		
		
		/**
		 * @copydoc WorldComponent::load
		 */
		bool load () { return mLoaded; }
		bool load2 ();
		
		/**
		 * @copydoc WorldComponent::reload
		 */
		bool reload ();
		
		/**
		 * @copydoc WorldComponent::unload
		 */
		void unload ();
		
		/**
		 * @copydoc WorldComponent::isLoaded
		 */
		bool isLoaded () const { return mLoaded; }
		
		
		
		/** Log source name. */
		static const String LogSource;
		
		/** Subworld type. */
		static const String Type;
		
		
		
	private:
		bool mLoaded;
		
		Vri mHeightMap;
		smart_ptr<Light> mLight;
		
		
		/* terrain variables */
		Ogre::TerrainGlobalOptions* mTerrainGlobals;
		Ogre::TerrainGroup* mTerrainGroup;
		
		bool mImported;
		
		
		/* terrain helper functions */
		void defineTerrain (long x, long y);
		void initBlendMaps (Ogre::Terrain* terrain);
		void configureTerrain (const smart_ptr<Light>& light);
	};

}}


#endif /* SOMA_VISUAL_TERRAIN_H_ */
