
#ifndef SOMA_VISUAL_PASS_H_
#define SOMA_VISUAL_PASS_H_


#include <Soma/Config.h>
#include <Soma/String.h>
#include <Soma/SmartPointer.h>


/* forward declarations */
namespace Ogre { class Pass; }


namespace Soma {
namespace Visual {


	/* forward declarations */
	class Technique;
	
	
	
	/**
	 * A material pass.
	 * 
	 * A pass is a combination of operations which will be applied on each
	 * render. Within every Material Technique there is one or more passes that
	 * are executed in order and as such a pass can be used to extend on a
	 * previous pass render output to create a more robust scene at the cost
	 * of performance. It is in a Pass that specific material settings are
	 * defined and used.
	 */
	class SOMA_API Pass : public ReferenceCounter
	{
	public:
		/**
		 * Constructor.
		 * @param technique The technique this pass should be applied in.
		 * @param name The name of the new pass
		 */
		Pass (const weak_ptr<Technique>& technique, const String& name);
		
		
		/**
		 * Pass constructor.
		 * @param pass The core OGRE pass to use.
		 */
		explicit Pass (Ogre::Pass* pass);
		
		
		
		/**
		 * Get the name of the pass.
		 * @return The pass name.
		 */
		const String& getName() const;
		
		
		
		/**
		 * Lighting is enabled in this pass.
		 * @return True if enabled, false otherwise.
		 */
		bool getLighting ();
		
		
		/**
		 * Set lighthing in this pass.
		 * @param enabled Lighting enabled if true, disabled otherwise.
		 */
		void setLighting (bool enabled);
		
		
		
		/**
		 * The ambient colour.
		 * @return A colour structure with the ambient values.
		 */
		Colour getAmbient ();
		
		/**
		 * The diffuse colour.
		 * @return A colour structure with the diffuse values.
		 */
		Colour getDiffuse ();
		
		/**
		 * The specular colour.
		 * @return A colour structure with the specular values.
		 */
		Colour getSpecular ();
		
		/**
		 * The emissive colour.
		 * @return A colour structure with the emissive values.
		 */
		Colour getEmissive ();
		
		
		/**
		 * Set the ambient colour.
		 * @param colour The colour to set ambient values to.
		 */
		void setAmbient (const Colour& colour);
		
		/**
		 * Set the diffuse colour.
		 * @param colour The colour to set diffuse values to.
		 */
		void setDiffuse (const Colour& colour);
		
		/**
		 * Set the specular colour.
		 * @param colour The colour to set specular values to.
		 */
		void setSpecular (const Colour& colour);
		
		/**
		 * Set the emissive colour.
		 * @param colour The colour to set emissive values to.
		 */
		void setEmissive (const Colour& colour);
		
		
		/**
		 * Set the ambient colour.
		 * @param r The red component of the ambient colour.
		 * @param g The green component of the ambient colour.
		 * @param b The blue component of the ambient colour.
		 */
		void setAmbient (float r, float g, float b);
		
		/**
		 * Set the diffuse colour.
		 * @param r The red component of the diffuse colour.
		 * @param g The green component of the diffuse colour.
		 * @param b The blue component of the diffuse colour.
		 * @param a The alpha component of the diffuse colour.
		 */
		void setDiffuse (float r, float g, float b, float a);
		
		/**
		 * Set the specular colour.
		 * @param r The red component of the specular colour.
		 * @param g The green component of the specular colour.
		 * @param b The blue component of the specular colour.
		 * @param a The alpha component of the specular colour.
		 */
		void setSpecular (float r, float g, float b, float a);
		
		/**
		 * Set the emissive colour.
		 * @param r The red component of the emissive colour.
		 * @param g The green component of the emissive colour.
		 * @param b The blue component of the emissive colour.
		 */
		void setEmissive (float r, float g, float b);
		
		
		
		/**
		 * The underlying OGRE pass.
		 * @return The OGRE pass.
		 */
		Ogre::Pass* getPass() { return mPass; }
		
		
		
		/** Log source name. */
		static const String LogSource;
		
		
		
	private:
		Ogre::Pass* mPass;
	};

}}


#endif /* SOMA_VISUAL_PASS_H_ */
