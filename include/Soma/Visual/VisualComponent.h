
#ifndef SOMA_VISUAL_VISUAL_COMPONENT_H_
#define SOMA_VISUAL_VISUAL_COMPONENT_H_


#include <Soma/Config.h>
#include <Soma/SmartPointer.h>
#include <Soma/EntityComponent.h>


/* forward declarations */
namespace Ogre { class MovableObject; }


namespace Soma {
namespace Visual {

	/* forward declarations */
	class World;
	
	
	
	/**
	 * Base visual object.
	 */
	class SOMA_API VisualComponent : public EntityComponent
	{
	public:
		/**
		 * Constructor.
		 */
		VisualComponent ();
		
		/**
		 * Destructor.
		 */
		virtual ~VisualComponent ();
		
		
		/**
		 * @copydoc EntityComponent::getType
		 */
		virtual const String& getType() const = 0;
		
		
		/**
		 * @copydoc EntityComponent::load
		 */
		bool load ();
		
		/**
		 * @copydoc EntityComponent::reload
		 */
		bool reload ();
		
		/**
		 * @copydoc EntityComponent::unload
		 */
		void unload ();
		
		/**
		 * @copydoc EntityComponent::isLoaded
		 */
		bool isLoaded() const { return mLoaded; }
		
		
		
		/** Log source name. */
		static const String LogSource;
		
		
		
	protected:
		/**
		 * The visual world the component was loaded in.
		 * 
		 * This is primarily used for unloading so that the actual component
		 * wont need to keep a world reference around simply to use it upon
		 * destruction.
		 */
		weak_ptr<Visual::World> mVisualWorld;
		
		
		/**
		 * Create the visual component within the specified visual world.
		 * 
		 * @param world The visual world to create the component in.
		 * @return True if created, false otherwise.
		 */
		virtual bool create (const weak_ptr<Visual::World>& world) = 0;
		
		/**
		 * Destroy the visual component removing it from the world it was
		 * previously created in with create().
		 */
		virtual void destroy () = 0;
		
		
		
	private:
		bool mLoaded;
	};

}}


#endif /* SOMA_VISUAL_VISUAL_COMPONENT_H_ */
