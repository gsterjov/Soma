
#ifndef SOMA_VISUAL_LIGHT_H_
#define SOMA_VISUAL_LIGHT_H_


#include <Soma/Config.h>
#include <Soma/String.h>
#include <Soma/SmartPointer.h>

#include <Soma/EntityComponent.h>
#include <Soma/Visual/Object.h>


/* forward declarations */
namespace Ogre { class Light; }


namespace Soma {
namespace Visual {

	/**
	 * Visual light.
	 * 
	 * @properties
	 * @prop{type, String, Read/Write, The light type - [point, directional, spotlight]}
	 * @prop{direction, Vector, Read/Write, The direction the light is pointing at}
	 * @endproperties
	 * 
	 * @ingroup components
	 */
	class SOMA_API Light : public Object
	{
	public:
		/**
		 * The type of lighting.
		 */
		enum LightType
		{
			POINT,       /** Light distributed evenly in all directions */
			DIRECTIONAL, /** Light distributed globaly in one direction */
			SPOTLIGHT    /** Light distributed in one direction at one point */
		};
		
		
		/**
		 * Constructor.
		 */
		Light ();
		
		/**
		 * Destructor.
		 */
		virtual ~Light ();
		
		
		/**
		 * @copydoc Object::getType
		 */
		const String& getType() const { return Type; }
		
		
		/**
		 * Get the direction the light is pointing at.
		 * @return The light direction.
		 */
		Vector getDirection() const;
		
		
		/**
		 * Set lighting type
		 * @param type The type of lighting to use.
		 */
		void setType (LightType type);
		
		
		/**
		 * Set the direction of the light.
		 * @param direction The light direction.
		 */
		void setDirection (const Vector& direction);
		
		
		
		/**
		 * The underlying OGRE light.
		 * @return The OGRE light.
		 */
		Ogre::Light* getLight() { return mLight; }
		
		
		/* Object implementaiton */
		Ogre::MovableObject* getMovableObject();
		
		
		
		/** Log source name. */
		static const String LogSource;
		
		/** Component type name. */
		static const String Type;
		
		
		
	private:
		Ogre::Light* mLight;
		
		
		/* object creation/destruction */
		bool create (const weak_ptr<Visual::World>& world);
		void destroy ();
		
		
		/* get/set component property */
		Value getProperty (const String& name) const;
		void  setProperty (const String& name, const Value& value);
	};

}}


#endif /* SOMA_VISUAL_LIGHT_H_ */
