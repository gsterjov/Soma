
#ifndef SOMA_VISUAL_NODE_H_
#define SOMA_VISUAL_NODE_H_


#include <list>

#include <Soma/Config.h>
#include <Soma/String.h>
#include <Soma/SmartPointer.h>
#include <Soma/LinkedComponent.h>

#include <Soma/Visual/VisualComponent.h>


/* forward declarations */
namespace Ogre { class SceneNode; }


namespace Soma {
namespace Visual {


	/* forward declarations */
	class Object;
	
	
	
	/**
	 * Scene node component.
	 * 
	 * @properties
	 * @prop{scale, Vector, Read/Write, The scale applied to the visual node}
	 * @endproperties
	 * 
	 * @ingroup components
	 */
	class SOMA_API Node : public LinkedComponent,
	                      public VisualComponent
	{
	public:
		/**
		 * Constructor.
		 */
		Node ();
		
		
		/**
		 * Destructor.
		 */
		virtual ~Node ();
		
		
		/**
		 * @copydoc Object::getType
		 */
		const String& getType() const { return Type; }
		
		
		/**
		 * Add child node.
		 * @param child The child node to add.
		 */
		void add (const smart_ptr<Node>& child);
		
		/**
		 * Remove a child node.
		 * @param child The child node to remove.
		 */
		void remove (const smart_ptr<Node>& child);
		
		
		/**
		 * Attach a movable object to the node.
		 * @param object The movable object to attach.
		 */
		void attach (const smart_ptr<Object>& object);
		
		/**
		 * Detach a movable object from the node.
		 * @param object The movable object to detach.
		 */
		void detach (const smart_ptr<Object>& object);
		
		
		/**
		 * Set the scale to be applied on all attached objects.
		 * @param scale The scale of each axis in units to apply.
		 */
		void setScale (const Vector& scale);
		
		/**
		 * Set the scale to be applied on all attached objects.
		 * 
		 * @param x The scale of the X axis in units to apply.
		 * @param y The scale of the Y axis in units to apply.
		 * @param z The scale of the Z axis in units to apply.
		 */
		void setScale (float x, float y, float z)
		{ setScale (Vector (x, y, z)); }
		
		/**
		 * Get the scale being applied on all attached objects.
		 * @return The scale of each axis in units being applied.
		 */
		const Vector& getScale () { return mScale; }
		
		
		/**
		 * Scale all attached objects relative to its current scale.
		 * @param scale The scale of each axis in units to apply.
		 */
		void scale (const Vector& scale);
		
		/**
		 * Scale all attached objects relative to its current scale.
		 * 
		 * @param x The scale of the X axis in units to apply.
		 * @param y The scale of the Y axis in units to apply.
		 * @param z The scale of the Z axis in units to apply.
		 */
		void scale (float x, float y, float z)
		{ scale (Vector (x, y, z)); }
		
		
		
		/**
		 * Get all the child nodes of this node.
		 * @return A list of Node's.
		 */
		const std::list<smart_ptr<Node> >& getChildren() { return mChildren; }
		
		
		/**
		 * Get all the objects attached to this node.
		 * @return A list of Object's.
		 */
		const std::list<smart_ptr<Object> >& getObjects() { return mObjects; }
		
		
		/**
		 * The underlying OGRE scene node.
		 * @return The OGRE scene node.
		 */
		Ogre::SceneNode* getSceneNode() { return mSceneNode; }
		
		
		
		/** Log source name. */
		static const String LogSource;
		
		/** Component type name. */
		static const String Type;
		
		
		
	private:
		Ogre::SceneNode* mSceneNode;
		
		Vector mScale;
		
		std::list<smart_ptr<Node> > mChildren;
		std::list<smart_ptr<Object> > mObjects;
		
		
		/* object creation/destruction */
		bool create (const weak_ptr<Visual::World>& world);
		void destroy ();
		
		
		/* get/set component property */
		Value getProperty (const String& name) const;
		void  setProperty (const String& name, const Value& value);
		
		
		/* linked components */
		void linkParent   (const smart_ptr<Component>& parent);
		void unlinkParent (const smart_ptr<Component>& parent);
		
		
		/* event handlers */
		void onPositionChanged    (const Vector& position);
		void onOrientationChanged (const Quaternion& orientation);
	};

}}


#endif /* SOMA_VISUAL_NODE_H_ */
