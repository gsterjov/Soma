
#ifndef SOMA_VISUAL_WORLD_H_
#define SOMA_VISUAL_WORLD_H_


#include <list>

#include <Soma/Config.h>
#include <Soma/String.h>
#include <Soma/SmartPointer.h>
#include <Soma/WorldComponent.h>


/* forward declarations */
namespace Ogre { class SceneManager; }


namespace Soma {

	/* forward declarations */
	class Entity;


namespace Visual {

	/* forward declarations */
	class Node;
	
	
	
	/**
	 * A world which handles the visual scene graph.
	 * 
	 * @properties
	 * @prop{ambient-light, Colour, Read/Write, The world ambient light}
	 * @endproperties
	 * 
	 * @ingroup components
	 */
	class SOMA_API World : public WorldComponent
	{
	public:
		/**
		 * Constructor.
		 */
		World ();
		
		/**
		 * Destructor.
		 */
		virtual ~World ();
		
		
		
		/**
		 * @copydoc WorldComponent::getType
		 */
		const String& getType() const { return Type; }
		
		
		/**
		 * Get the ambient colour being applied in the world.
		 * @return The world ambient colour.
		 */
		Colour getAmbientLight() const;
		
		
		/**
		 * Set the ambient colour to apply in the world.
		 * @param colour The ambient colour to use.
		 */
		void setAmbientLight (const Colour& colour);
		
		
		
		/**
		 * Add a node into the world.
		 */
		void add (const smart_ptr<Node>& node);
		
		/**
		 * Remove a node from the world.
		 */
		void remove (const smart_ptr<Node>& node);
		
		
		/**
		 * Get all the child nodes added to the world.
		 * @return A list of Nodes.
		 */
		const std::list<smart_ptr<Node> >& getNodes() { return mNodes; }
		
		
		
		/**
		 * @copydoc WorldComponent::load
		 */
		bool load ();
		
		/**
		 * @copydoc WorldComponent::reload
		 */
		bool reload ();
		
		/**
		 * @copydoc WorldComponent::unload
		 */
		void unload ();
		
		/**
		 * @copydoc WorldComponent::isLoaded
		 */
		bool isLoaded () const { return mSceneManager; }
		
		
		
		
		/**
		 * The underlying OGRE scene manager.
		 * @return The OGRE scene manager.
		 */
		Ogre::SceneManager* getSceneManager() { return mSceneManager; }
		
		
		
		/** Log source name. */
		static const String LogSource;
		
		/** Subworld type. */
		static const String Type;
		
		
		
	private:
		std::list<smart_ptr<Node> > mNodes;
		
		Ogre::SceneManager* mSceneManager;
		
		
		/* get/set component property */
		Value getProperty (const String& name) const;
		void  setProperty (const String& name, const Value& value);
		
		
		/* event handlers */
		void onEntityAdded   (const smart_ptr<Entity>& entity);
		void onEntityRemoved (const smart_ptr<Entity>& entity);
		
		void onNodeLoad   (const weak_ptr<Component>& component);
		void onNodeUnload (const weak_ptr<Component>& component);
	};

}}


#endif /* SOMA_VISUAL_WORLD_H_ */
