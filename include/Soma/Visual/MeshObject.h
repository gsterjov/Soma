
#ifndef SOMA_VISUAL_MESH_OBJECT_H_
#define SOMA_VISUAL_MESH_OBJECT_H_


#include <Soma/Config.h>
#include <Soma/String.h>
#include <Soma/SmartPointer.h>
#include <Soma/Vri.h>
#include <Soma/Visual/Object.h>



/* forward declarations */
namespace Ogre { class Entity; }


namespace Soma {
namespace Visual {

	/* forward declarations */
	class Material;
	class Mesh;
	
	
	
	/**
	 * An instance of a mesh resource.
	 * 
	 * @properties
	 * @prop{resource, smart_ptr<Resource>, Read/Write, The mesh resource to instantiate}
	 * @prop{resource-path, String, Write, The VRI path to a mesh resource to instantiate}
	 * @endproperties
	 * 
	 * @ingroup components
	 */
	class SOMA_API MeshObject : public Object
	{
	public:
		/**
		 * Resource constructor.
		 * @param mesh The mesh resource to create an instance of.
		 */
		explicit MeshObject (const smart_ptr<Mesh>& mesh);
		
		/**
		 * Name constructor.
		 * Takes a asset path and mesh name to load from the MeshManager.
		 * 
		 * @param vri The path to the mesh resource within an asset.
		 */
		explicit MeshObject (const Vri& vri);
		
		/**
		 * Constructor.
		 */
		MeshObject ();
		
		/**
		 * Destructor.
		 */
		virtual ~MeshObject ();
		
		
		/**
		 * @copydoc Object::getType
		 */
		const String& getType() const { return Type; }
		
		
		/**
		 * Set the mesh resource.
		 * 
		 * Each instantiation of this object will use the mesh data found
		 * within the given mesh resource.
		 * 
		 * @param mesh The mesh resource to use.
		 */
		void setMesh (const smart_ptr<Mesh>& mesh);
		
		/**
		 * Set the mesh resource.
		 * 
		 * Each instantiation of this object will use the mesh data found
		 * within the given mesh resource.
		 * 
		 * @param vri The path to the mesh resource within an asset.
		 */
		void setMesh (const Vri& vri);
		
		
		/**
		 * Set the current material on the object.
		 * @param material The material to use.
		 */
		void setMaterial (const smart_ptr<Material>& material);
		
		
		
		/**
		 * The underlying OGRE entity.
		 * @return The OGRE entity.
		 */
		Ogre::Entity* getEntity() { return mEntity; }
		
		
		/* object implementaiton */
		Ogre::MovableObject* getMovableObject();
		
		
		
		/** Log source name. */
		static const String LogSource;
		
		/** Component type name. */
		static const String Type;
		
		
		
	private:
		smart_ptr<Material> mMaterial;
		smart_ptr<Mesh> mMesh;
		
		Ogre::Entity* mEntity;
		
		
		/* object creation/destruction */
		bool create (const weak_ptr<Visual::World>& world);
		void destroy ();
		
		
		/* get/set component property */
		Value getProperty (const String& name) const;
		void  setProperty (const String& name, const Value& value);
		
		
		
		/* entity event handlers */
		void onComponentAdded   (const smart_ptr<Component>& component);
		void onComponentRemoved (const smart_ptr<Component>& component);
	};

}}


#endif /* SOMA_VISUAL_MESH_OBJECT_H_ */
