
#ifndef SOMA_VISUAL_MANAGER_H_
#define SOMA_VISUAL_MANAGER_H_


#include <Soma/Config.h>
#include <Soma/String.h>
#include <Soma/Singleton.h>
#include <Soma/SubSystem.h>
#include <Soma/Configurable.h>


/* external forward declerations */
namespace Ogre { class Root; }


namespace Soma {
namespace Visual {

	/**
	 * Low-level Renderer class.
	 * 
	 * The manager class manages the shared graphics resource. In essence
	 * it wraps and abstracts the Ogre library to provide a clean and neat
	 * subsystem for the engine. Though the manager class will be used little
	 * by even the low-level classes it is responsible for loading up the
	 * visual interface and ensuring consistent rendering by hooking into
	 * the Engine game loop.
	 */
	class SOMA_API Manager : public Singleton<Manager>,
	                         public SubSystem,
	                         public Configurable
	{
	public:
		/**
		 * Rendering backend.
		 * A list of all the supported rendering systems.
		 */
		enum RenderSystem
		{
			RENDER_SYSTEM_OPENGL, /**< OpenGL graphics API */
			RENDER_SYSTEM_DIRECT_X /**< Direct X graphics API */
		};
		
		
		/**
		 * Constructor.
		 * 
		 * The renderer automatically creates the OGRE rendering context upon
		 * construction and loads any required libraries such as OpenGL. Along with
		 * this it also sets the default property values which ultimately determine
		 * the settings of the renderings system.
		 */
		Manager ();
		
		/**
		 * Destructor.
		 * 
		 * Upon destruction the renderer destroys the OGRE rendering context
		 * and brings down the entire graphics subsystem as a result. This includes
		 * any instantiated windows among other things. One must be carefull
		 * destroying a renderer as it will invalidate all objects belonging to
		 * the Visual namespace.
		 */
		virtual ~Manager ();
		
		
		/**
		 * Initialises the renderer.
		 * 
		 * Once the renderer is initialised the entire graphics subsystem can be
		 * used without having to register it with the Renderer. This will
		 * effectively load the rendering system specified by the RenderSystem
		 * property and apply the necessary renderer settings. Once the rendering
		 * system has been configured OGRE is initialised, though without a
		 * resource manager one is limited to what can be achieved.
		 * 
		 * @return True if successful, false otherwise.
		 */
		bool initialise ();
		
		/**
		 * Reinitialises the renderer.
		 * 
		 * When a setting is changed the renderer needs to be reinitialised to
		 * propagate those sentings to the rendering system. This is particularly
		 * useful when visual settings are changed whilst the renderer is running,
		 * as is typical in a 'Change Graphic Options' menu, and the objects
		 * already created within the Visual namespace shouldn't be destroyed.
		 */
		bool reinitialise ();
		
		/**
		 * Shuts down the renderer.
		 * 
		 * Shutting down the renderer also shuts down OGRE and as such any object
		 * created from the Visual subsystem will be invalidated.
		 */
		void shutdown ();
		
		/**
		 * Is the renderer initialised?
		 * @return True if initialised, false otherwise.
		 */
		bool isInitialised ();
		
		/**
		 * Steps through a frame rendering it.
		 * 
		 * If handling a manual game loop instead of using the Engine class then
		 * the Renderer (as well as other subsystems) need to be step through each
		 * frame to provide proper timing and display for the subsystem. This will
		 * effectively render one whole frame from start to finish.
		 */
		void step ();
		
		
		
		/* Configurable implementation */
		bool save    (ConfigInterface& config);
		bool restore (const ConfigInterface& config);
		
		
		
		/**
		 * The rendering backend.
		 * @return The renderering system type.
		 */
		RenderSystem getRenderSystem() { return mRenderSystem; }
		/**
		 * Set the rendering backend to use.
		 * @param value The renderering system type to use.
		 */
		void setRenderSystem (RenderSystem value) { mRenderSystem = value; }
		
		
		/**
		 * Is the renderer fullscreen?
		 * @return True if fullscreen, false otherwise.
		 */
		bool getFullscreen() { return mFullscreen; }
		/**
		 * Set the rendering backend to fullscreen or windowed mode.
		 * @param value True if renderer should be fullscreen, false otherwise.
		 */
		void setFullscreen (bool value) { mFullscreen = value; }
		
		
		/**
		 * Rendering contents on every vertical sync?
		 * @return True if VSynced, false otherwise.
		 */
		bool getVSync() { return mVSync; }
		/**
		 * Set whether to render on every vertical sync.
		 * @param value True if renderering every vsync, false otherwise.
		 */
		void setVSync (bool value) { mVSync = value; }
		
		
		/**
		 * Rendering with gamma conversion.
		 * @return True if gamma conversion is on, false otherwise.
		 */
		bool getGammaConversion() { return mGammaConversion; }
		/**
		 * Set the rendering gamma conversion mode.
		 * @param value True if gamma conversion should be on, false otherwise.
		 */
		void setGammaConversion (bool value) { mGammaConversion = value; }
		
		
		/**
		 * The renderering width size.
		 * @return The resolution width.
		 */
		int getWidth() { return mWidth; }
		/**
		 * Set the resolution width.
		 * @param value The desired resolution width.
		 */
		void setWidth (int value) { mWidth = value; }
		
		
		/**
		 * The renderering height size.
		 * @return The resolution height.
		 */
		int getHeight() { return mHeight; }
		/**
		 * Set the resolution height.
		 * @param value The desired resolution height.
		 */
		void setHeight (int value) { mHeight = value; }
		
		
		/**
		 * Full screen anti-aliasing level.
		 * @return The anti-aliasing level.
		 */
		int getFSAA() { return mFSAA; }
		/**
		 * Set the full screen anti-aliasing level.
		 * @param value The desired anti-aliasing level.
		 */
		void setFSAA (int value) { mFSAA = value; }
		
		
		/**
		 * Monitor frequency.
		 * @return The monitor frequency.
		 */
		int getFrequency() { return mFrequency; }
		/**
		 * Set the monitor frequency.
		 * @param value The desired monitor frequency.
		 */
		void setFrequency (int value) { mFrequency = value; }
		
		
		
		
		/**
		 * The underlying OGRE renderer.
		 * @return OGRE root.
		 */
		Ogre::Root* getRoot() { return mRoot; }
		
		
		
		/**
		 * Log source name.
		 */
		static const String LogSource;
		
		
		
	private:
		Ogre::Root* mRoot;
		
		
		/* renderer properties */
		enum RenderSystem mRenderSystem;
		bool mFullscreen;
		bool mVSync;
		bool mGammaConversion;
		int mWidth;
		int mHeight;
		int mFSAA;
		int mFrequency;
		
		
		/* set renderer options */
		void applySettings ();
	};

}}


#endif /* SOMA_VISUAL_MANAGER_H_ */
