
#ifndef SOMA_MATH_QUATERNION_H_
#define SOMA_MATH_QUATERNION_H_


#include <iostream>

#include <Soma/Config.h>


namespace Soma {
namespace Math {


	/* forward declarations */
	class Radian;
	class Matrix3;
	class Vector3;
	
	
	/**
	 * Quaternion math class representing a rotation.
	 */
	class SOMA_API Quaternion
	{
	public:
		/**
		 * Manual constructor.
		 */
		Quaternion (float w, float x, float y, float z);
		
		/**
		 * 3x3 rotation matrix constructor.
		 */
		explicit Quaternion (const Matrix3& rotation);
		
		/**
		 * Angle/axis constructor.
		 */
		Quaternion (const Radian& angle, const Vector3& axis);
		
		/**
		 * Orthonormal axes constructor.
		 */
		Quaternion (const Vector3& xAxis, const Vector3& yAxis, const Vector3& zAxis);
		
		/**
		 * Empty constructor.
		 */
		Quaternion ();
		
		
		
		/** The W component. */
		float w;
		/** The X axis component. */
		float x;
		/** The Y axis component. */
		float y;
		/** The Z axis component. */
		float z;
		
		
		
		
		/**
		 * Create a rotation matrix from the quaternion.
		 * @return A 3x3 rotation matrix.
		 */
		Matrix3 toRotationMatrix () const;
		
		
		
		
		
		/** Assignment operator. */
		Quaternion& operator= (const Quaternion& rhs);
		
		
		/** Equality operator. */
		bool operator== (const Quaternion& rhs) const;
		
		/** Inequality operator. */
		bool operator!= (const Quaternion& rhs) const;
		
		
		
		/** Addition operator. */
		Quaternion operator+ (const Quaternion& rhs) const;
		
		/** Subtraction operator. */
		Quaternion operator- (const Quaternion& rhs) const;
		
		/** Multiplication operator. */
		Quaternion operator* (const Quaternion& rhs) const;
		/** Multiplication operator. */
		Quaternion operator* (const float rhs) const;
		
		
		/** Negative operator. */
		Quaternion operator- () const;
		
		
		
		/** Stream out operator. */
		friend std::ostream& operator<< (std::ostream& ostream, const Quaternion& ref);
		
		
		
		/**
		 * Predefined quaternion with empty values.
		 * @return A quaternion with components (0, 0, 0, 0).
		 */
		static const Quaternion ZERO;
		
		/**
		 * Predefined quaternion with identity values.
		 * @return A quaternion with components (1, 0, 0, 0).
		 */
		static const Quaternion IDENTITY;
	};

}}


#endif /* SOMA_MATH_QUATERNION_H_ */
