
#ifndef SOMA_MATH_H_
#define SOMA_MATH_H_


#include <cmath>

#include <Soma/Config.h>


namespace Soma {
namespace Math {

	/**
	 * Mathematical functions class.
	 */
	class SOMA_API Math
	{
	public:
		/**
		 * Compute the cosine of an angle.
		 * 
		 * @param angle An angle in radians.
		 * @return The cosine of the angle in radians.
		 */
		static float cos (float angle) { return std::cos (angle); }
		
		/**
		 * Compute the sine of an angle.
		 * 
		 * @param angle An angle in radians.
		 * @return The sine of the angle in radians.
		 */
		static float sin (float angle) { return std::sin (angle); }
		
		/**
		 * Compute the tangent of an angle.
		 * 
		 * @param angle An angle in radians.
		 * @return The tangent of the angle in radians.
		 */
		static float tan (float angle) { return std::tan (angle); }
		
		
		
		/**
		 * Compute the arc cosine of an angle.
		 * In trigonometrics an arc cosine is the inverse of cosine.
		 * 
		 * @param angle An angle within the range [-1, 1].
		 * @return The arc cosine of the angle within the range [0, pi] radians.
		 */
		static float acos (float angle) { return std::acos (angle); }
		
		/**
		 * Compute the arc sine of an angle.
		 * In trigonometrics an arc sine is the inverse of sine.
		 * 
		 * @param angle An angle within the range [-1, 1].
		 * @return The arc sine of the angle within the range [-pi/2, pi/2] radians.
		 */
		static float asin (float angle) { return std::asin (angle); }
		
		/**
		 * Compute the arc tangent of an angle.
		 * In trigonometrics an arc tangent is the inverse of tangent.
		 * 
		 * @note Due to sign ambiguity it cannot be determined in which
		 * quadrant the angle falls by its tangent value alone. Use atan2()
		 * if the quandrant is needed.
		 * 
		 * @param angle An angle in radians.
		 * @return The arc tangent of the angle within the range [-pi/2, pi/2] radians.
		 */
		static float atan (float angle) { return std::atan (angle); }
		
		/**
		 * Compute the arc tangent and quadrant of an angle.
		 * To determine the quadrant the sign of both arguments are used.
		 * 
		 * @note Both arguments cannot be zero else an exception will occur.
		 * 
		 * @param y A y-coordinate.
		 * @param x An x-coordinate.
		 * @return The arc tangent of y/x within the range [-pi, pi] radians.
		 */
		static float atan2 (float y, float x) { return std::atan2 (y, x); }
		
		
		
		/**
		 * Get the natural base-e exponential raised to the power of a value.
		 * 
		 * @param x The factor to raise e to the power of.
		 * @return The value of e to the power of x.
		 */
		static float exp (float x) { return std::exp (x); }
		
		/**
		 * Get the natural base-e logarithm of a value.
		 * A base-e logarithm is the inverse of base-e exponential exp().
		 * 
		 * @param x The value to obtain the natural logarithm of.
		 * @return The natural base-e logarithm of x.
		 */
		static float log (float x) { return std::log (x); }
		
		/**
		 * Get the base-2 logarithm of a value.
		 * 
		 * @param x The value to obtain the logarithm of.
		 * @return The base-2 logarithm of x.
		 */
		static float log2 (float x) { return log(x) / LOG2; }
		
		/**
		 * Get the base-10 logarithm of a value.
		 * 
		 * @param x The value to obtain the logarithm of.
		 * @return The base-10 logarithm of x.
		 */
		static float log10 (float x) { return std::log10 (x); }
		
		/**
		 * Get the logarithm of a value with the specified base.
		 * 
		 * @param base The logarithmic base to use.
		 * @param x The value to obtain the logarithm of.
		 * @return The base-N logarithm of x.
		 */
		static float logN (float base, float x) { return log(x) / log(base); }
		
		
		
		/**
		 * Raise a value to the power of another value.
		 * 
		 * @param base The base value to raise.
		 * @param exponent The exponent value to raise to.
		 * @return The value of base raised to the power of exponent.
		 */
		static float pow (float base, float exponent) { return std::pow (base, exponent); }
		
		/**
		 * Compute the square root of x.
		 * @note The argument cannot be negative else an exception will occur.
		 * 
		 * @param x The value to calculate the square root of.
		 * @return The square root of x.
		 */
		static float sqrt (float x) { return std::sqrt (x); }
		
		/**
		 * Compute the inverse square root of x.
		 * 
		 * @param x The value to calculate the inverse square root of.
		 * @return The inverse square root of x.
		 */
		static float invSqrt (float x) { return 1.0f / sqrt (x); }
		
		
		
		/**
		 * Round a value up.
		 * 
		 * @param x The value to round up.
		 * @return The smallest integral value above x.
		 */
		static float ceil (float x) { return std::ceil (x); }
		
		/**
		 * Round a value down.
		 * 
		 * @param x The value to round down.
		 * @return The largest integral value below x.
		 */
		static float floor (float x) { return std::floor (x); }
		
		/**
		 * Get an absolute value.
		 * 
		 * @param x The value to get the absolute value of.
		 * @return The absolute value of x.
		 */
		static float abs (float x) { return std::abs (x); }
		
		
		
		
		/**
		 * Convert radians to degrees.
		 * 
		 * @param radians The value to convert.
		 * @return The given value in degrees.
		 */
		static float radiansToDegrees (float radians)
		{ return radians * mDegreesPerRadian; }
		
		/**
		 * Convert degrees to radians.
		 * 
		 * @param degrees The value to convert.
		 * @return The given value in radians.
		 */
		static float degreesToRadians (float degrees)
		{ return degrees * mRadiansPerDegree; }
		
		
		
		/**
		 * Value of PI.
		 */
		static const float PI;
		
		/**
		 * Value of log(2).
		 */
		static const float LOG2;
		
		
	private:
		static const float mDegreesPerRadian;
		static const float mRadiansPerDegree;
	};

}}


#endif /* SOMA_MATH_H_ */
