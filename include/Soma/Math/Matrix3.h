
#ifndef SOMA_MATH_MATRIX3_H_
#define SOMA_MATH_MATRIX3_H_


#include <Soma/Config.h>


namespace Soma {
namespace Math {


	/* forward declarations */
	class Matrix4;
	class Vector3;
	
	
	/**
	 * Matrix math class representing 3 rows and columns.
	 */
	class SOMA_API Matrix3
	{
	public:
		/**
		 * Constructor.
		 * Assigns matrix components as [row][column].
		 */
		Matrix3 (float m1_1, float m1_2, float m1_3,
		         float m2_1, float m2_2, float m2_3,
		         float m3_1, float m3_2, float m3_3);
		
		
		/**
		 * 4x4 matrix constructor.
		 * 
		 * Extracts the rotation/scale part of the 4x4 matrix to create
		 * a 3x3 rotation/scaling matrix.
		 */
		explicit Matrix3 (const Matrix4& mat);
		
		
		/**
		 * Empty constructor.
		 */
		Matrix3 () {}
		
		
		
		/**
		 * Transpose matrix.
		 * @return A transposed matrix.
		 */
		Matrix3 transpose () const;
		
		
		/**
		 * Decompose the 3x3 matrix to determine the rotation, scale and shear
		 * values.
		 * 
		 * @param rotation A 3x3 matrix to store the rotation into.
		 * @param scale A vector to store the scale into.
		 * @param shear A vector to store the shear into.
		 */
		void decompose (Matrix3& rotation,
		                Vector3& scale,
		                Vector3& shear) const;
		
		
		
		/** Assignment operator. */
		Matrix3& operator= (const Matrix4& mat);
		
		
		
		/** Index operator */
		float* operator[] (int index);
		/** Index operator */
		const float* operator[] (int index) const;
		
		
		
		/** Equality operator. */
		bool operator== (const Matrix3& mat) const;
		
		/** Inequality operator. */
		bool operator!= (const Matrix3& mat) const;
		
		
		
		
		/** Multiplication operator */
		Matrix3 operator* (const Matrix3& mat) const;
		/** Multiplication operator */
		Vector3 operator* (const Vector3& vec) const;
		
		/** Addition operator */
		Matrix3 operator+ (const Matrix3& mat) const;
		
		/** Subtraction operator */
		Matrix3 operator- (const Matrix3& mat) const;
		
		
		
		/**
		 * Predefined matrix with empty values.
		 * @return An empty matrix.
		 */
		static const Matrix3 ZERO;
		
		/**
		 * Predefined matrix with identity values.
		 * @return An identity matrix.
		 */
		static const Matrix3 IDENTITY;
		
		
		
	private:
		/* matrix [row][column] */
		float m[3][3];
	};

}}


#endif /* SOMA_MATH_MATRIX3_H_ */
