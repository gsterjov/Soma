
#ifndef SOMA_MATH_DEGREE_H_
#define SOMA_MATH_DEGREE_H_


#include <Soma/Config.h>


namespace Soma {
namespace Math {


	/* forward declaration */
	class Radian;
	
	
	/**
	 * Degree math class representing an angle.
	 */
	class SOMA_API Degree
	{
	public:
		/**
		 * Constructor.
		 */
		Degree (float degree);
		
		/**
		 * Radian constructor.
		 */
		Degree (const Radian& radian);
		
		/**
		 * Empty constructor.
		 */
		Degree ();
		
		
		
		/**
		 * Get the degree value.
		 * @return The degree value.
		 */
		float getValue() const;
		
		/**
		 * Gets the value in radians.
		 * @return The value in radians.
		 */
		float toRadians() const;
		
		
		
		/** Conversion operator */
		operator float () const;
		
		
		
		/** Assignment operator. */
		Degree& operator= (const Degree& rhs);
		
		/** Assignment operator. */
		Degree& operator= (const Radian& rhs);
		
		/** Assignment operator. */
		Degree& operator= (const float rhs);
		
		
		
		/** Equality operator. */
		bool operator== (const Degree& rhs) const;
		
		/** Inequality operator. */
		bool operator!= (const Degree& rhs) const;
		
		/** Less-than operator. */
		bool operator< (const Degree& rhs) const;
		
		/** Less-than-or-equal-to operator. */
		bool operator<= (const Degree& rhs) const;
		
		/** Greater-than operator. */
		bool operator> (const Degree& rhs) const;
		
		/** Greater-than-or-equal-to operator. */
		bool operator>= (const Degree& rhs) const;
		
		
		
		/** Addition operator. */
		Degree operator+ (const Degree& rhs) const;
		/** Addition operator. */
		Degree operator+ (const Radian& rhs) const;
		
		/** Subtraction operator. */
		Degree operator- (const Degree& rhs) const;
		/** Subtraction operator. */
		Degree operator- (const Radian& rhs) const;
		
		/** Multiplication operator. */
		Degree operator* (const float rhs) const;
		
		/** Division operator. */
		Degree operator/ (const float rhs) const;
		
		
		
		/** Addition operator. */
		Degree& operator+= (const Degree& rhs);
		/** Addition operator. */
		Degree& operator+= (const Radian& rhs);
		
		/** Subtraction operator. */
		Degree& operator-= (const Degree& rhs);
		/** Subtraction operator. */
		Degree& operator-= (const Radian& rhs);
		
		/** Multiplication operator. */
		Degree& operator*= (const float rhs);
		
		/** Division operator. */
		Degree& operator/= (const float rhs);
		
		
		/** Negative operator. */
		Degree operator- () const;
		
		
		
	private:
		float mDegree;
	};

}}


#endif /* SOMA_MATH_DEGREE_H_ */
