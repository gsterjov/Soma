
#ifndef SOMA_MATH_MATRIX4_H_
#define SOMA_MATH_MATRIX4_H_


#include <iostream>

#include <Soma/Config.h>


namespace Soma {
namespace Math {


	/* forward declarations */
	class Vector3;
	class Matrix3;
	class Quaternion;
	
	
	/**
	 * Matrix math class representing 4 rows and columns.
	 */
	class SOMA_API Matrix4
	{
	public:
		/**
		 * Constructor.
		 * Assigns matrix components as [row][column].
		 */
		Matrix4 (float m1_1, float m1_2, float m1_3, float m1_4,
		         float m2_1, float m2_2, float m2_3, float m2_4,
		         float m3_1, float m3_2, float m3_3, float m3_4,
		         float m4_1, float m4_2, float m4_3, float m4_4);
		
		
		/**
		 * 3x3 matrix constructor.
		 * 
		 * Assigns the rotation/scaling 3x3 matrix to the matrix and zero's
		 * out the translation.
		 */
		explicit Matrix4 (const Matrix3& mat);
		
		
		/**
		 * Quaternion constructor.
		 * 
		 * Assigns the rotation/scaling quaternion to the matrix and zero's
		 * out the translation.
		 */
		explicit Matrix4 (const Quaternion& quat);
		
		
		/**
		 * Empty constructor.
		 */
		Matrix4 () {}
		
		
		
		/**
		 * Is the matrix an affine matrix?
		 * @return True if affine, false otherwise.
		 */
		bool isAffine() const;
		
		
		/**
		 * Transpose matrix.
		 * @return A transposed matrix.
		 */
		Matrix4 transpose () const;
		
		
		/**
		 * Set the translation part of the matrix.
		 * @param vec The translation to set.
		 */
		void setTranslation (const Vector3& vec);
		
		/**
		 * Get the translation part of the matrix.
		 * @return The matrix translation.
		 */
		Vector3 getTranslation() const;
		
		
		/**
		 * Set the scale part of the matrix.
		 * @param vec The scale to set.
		 */
		void setScale (const Vector3& vec);
		
		
		/**
		 * Decompose the matrix into its position/scale/orientation parts.
		 * 
		 * @param position The vector to store the position in.
		 * @param scale The vector to store the scale in.
		 * @param orientation The quaternion to store the orientation in.
		 */
		void decompose (Vector3&    position,
		                Vector3&    scale,
		                Quaternion& orientation) const;
		
		
		
		/** Assignment operator. */
		Matrix4& operator= (const Matrix3& mat);
		
		
		
		/** Index operator */
		float* operator[] (int index);
		/** Index operator */
		const float* operator[] (int index) const;
		
		
		
		/** Equality operator. */
		bool operator== (const Matrix4& mat) const;
		
		/** Inequality operator. */
		bool operator!= (const Matrix4& mat) const;
		
		
		
		
		/** Multiplication operator */
		Matrix4 operator* (const Matrix4& mat) const;
		/** Multiplication operator */
		Vector3 operator* (const Vector3& vec) const;
		
		/** Addition operator */
		Matrix4 operator+ (const Matrix4& mat) const;
		
		/** Subtraction operator */
		Matrix4 operator- (const Matrix4& mat) const;
		
		
		
		/** Stream out operator. */
		friend std::ostream& operator<< (std::ostream& ostream, const Matrix4& ref);
		
		
		
		/**
		 * Predefined matrix with empty values.
		 * @return An empty matrix.
		 */
		static const Matrix4 ZERO;
		
		/**
		 * Predefined matrix with identity values.
		 * @return An identity matrix.
		 */
		static const Matrix4 IDENTITY;
		
		
		
	private:
		/* matrix [row][column] */
		float m[4][4];
	};

}}


#endif /* SOMA_MATH_MATRIX4_H_ */
