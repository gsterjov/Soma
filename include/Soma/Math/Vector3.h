
#ifndef SOMA_MATH_VECTOR3_H_
#define SOMA_MATH_VECTOR3_H_


#include <iostream>

#include <Soma/Config.h>


namespace Soma {
namespace Math {

	/**
	 * Vector math class representing three dimensions.
	 */
	class SOMA_API Vector3
	{
	public:
		/**
		 * Constructor.
		 */
		Vector3 (float x, float y, float z);
		
		/**
		 * Empty constructor.
		 */
		Vector3 ();
		
		
		
		/** The X axis component. */
		float x;
		/** The Y axis component. */
		float y;
		/** The Z axis component. */
		float z;
		
		
		
		
		/** Assignment operator. */
		Vector3& operator= (const Vector3& rhs);
		
		/** Assignment operator. */
		Vector3& operator= (const float rhs);
		
		
		/** Equality operator. */
		bool operator== (const Vector3& rhs) const;
		
		/** Inequality operator. */
		bool operator!= (const Vector3& rhs) const;
		
		
		
		/** Addition operator. */
		Vector3 operator+ (const Vector3& rhs) const;
		/** Addition operator. */
		Vector3 operator+ (const float rhs) const;
		
		/** Subtraction operator. */
		Vector3 operator- (const Vector3& rhs) const;
		/** Subtraction operator. */
		Vector3 operator- (const float rhs) const;
		
		/** Multiplication operator. */
		Vector3 operator* (const Vector3& rhs) const;
		/** Multiplication operator. */
		Vector3 operator* (const float rhs) const;
		
		/** Division operator. */
		Vector3 operator/ (const Vector3& rhs) const;
		/** Division operator. */
		Vector3 operator/ (const float rhs) const;
		
		
		
		/** Addition operator. */
		Vector3& operator+= (const Vector3& rhs);
		/** Addition operator. */
		Vector3& operator+= (const float rhs);
		
		/** Subtraction operator. */
		Vector3& operator-= (const Vector3& rhs);
		/** Subtraction operator. */
		Vector3& operator-= (const float rhs);
		
		/** Multiplication operator. */
		Vector3& operator*= (const Vector3& rhs);
		/** Multiplication operator. */
		Vector3& operator*= (const float rhs);
		
		/** Division operator. */
		Vector3& operator/= (const Vector3& rhs);
		/** Division operator. */
		Vector3& operator/= (const float rhs);
		
		
		/** Negative operator. */
		Vector3 operator- () const;
		
		
		
		/** Stream out operator. */
		friend std::ostream& operator<< (std::ostream& ostream, const Vector3& ref);
		
		
		
		/**
		 * Predefined vector with empty values.
		 * @return A vector with components (0, 0, 0).
		 */
		static const Vector3 ZERO;
		
		/**
		 * Predefined vector with all values containing a normalised one.
		 * @return A vector with components (1, 1, 1).
		 */
		static const Vector3 UNIT_SCALE;
		
		/**
		 * Predefined vector with a normalised value of one at the X axis.
		 * @return A vector with components (1, 0, 0).
		 */
		static const Vector3 UNIT_X;
		
		/**
		 * Predefined vector with a normalised value of one at the Y axis.
		 * @return A vector with components (0, 1, 0).
		 */
		static const Vector3 UNIT_Y;
		
		/**
		 * Predefined vector with a normalised value of one at the Z axis.
		 * @return A vector with components (0, 0, 1).
		 */
		static const Vector3 UNIT_Z;
		
		/**
		 * Predefined vector with a normalised value of negative one at the X axis.
		 * @return A vector with components (-1, 0, 0).
		 */
		static const Vector3 NEGATIVE_UNIT_X;
		
		/**
		 * Predefined vector with a normalised value of negative one at the Y axis.
		 * @return A vector with components (0, -1, 0).
		 */
		static const Vector3 NEGATIVE_UNIT_Y;
		
		/**
		 * Predefined vector with a normalised value of negative one at the Z axis.
		 * @return A vector with components (0, 0, -1).
		 */
		static const Vector3 NEGATIVE_UNIT_Z;
	};

}}


#endif /* SOMA_MATH_VECTOR3_H_ */
