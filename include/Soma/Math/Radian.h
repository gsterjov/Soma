
#ifndef SOMA_MATH_RADIAN_H_
#define SOMA_MATH_RADIAN_H_


#include <Soma/Config.h>


namespace Soma {
namespace Math {


	/* forward declarations */
	class Degree;
	
	
	/**
	 * Radian math class representing an angle.
	 */
	class SOMA_API Radian
	{
	public:
		/**
		 * Constructor.
		 */
		Radian (float radian);
		
		/**
		 * Degree constructor.
		 */
		Radian (const Degree& degree);
		
		/**
		 * Empty constructor.
		 */
		Radian ();
		
		
		
		/**
		 * Get the radian value.
		 * @return The radian value.
		 */
		float getValue() const;
		
		/**
		 * Gets the value in degrees.
		 * @return The value in degrees.
		 */
		float toDegrees() const;
		
		
		
		/** Conversion operator */
		operator float () const;
		
		
		
		
		/** Assignment operator. */
		Radian& operator= (const Radian& rhs);
		
		/** Assignment operator. */
		Radian& operator= (const Degree& rhs);
		
		/** Assignment operator. */
		Radian& operator= (const float rhs);
		
		
		
		/** Equality operator. */
		bool operator== (const Radian& rhs) const;
		
		/** Inequality operator. */
		bool operator!= (const Radian& rhs) const;
		
		/** Less-than operator. */
		bool operator< (const Radian& rhs) const;
		
		/** Less-than-or-equal-to operator. */
		bool operator<= (const Radian& rhs) const;
		
		/** Greater-than operator. */
		bool operator> (const Radian& rhs) const;
		
		/** Greater-than-or-equal-to operator. */
		bool operator>= (const Radian& rhs) const;
		
		
		
		/** Addition operator. */
		Radian operator+ (const Radian& rhs) const;
		/** Addition operator. */
		Radian operator+ (const Degree& rhs) const;
		
		/** Subtraction operator. */
		Radian operator- (const Radian& rhs) const;
		/** Subtraction operator. */
		Radian operator- (const Degree& rhs) const;
		
		/** Multiplication operator. */
		Radian operator* (const float rhs) const;
		
		/** Division operator. */
		Radian operator/ (const float rhs) const;
		
		
		
		/** Addition operator. */
		Radian& operator+= (const Radian& rhs);
		/** Addition operator. */
		Radian& operator+= (const Degree& rhs);
		
		/** Subtraction operator. */
		Radian& operator-= (const Radian& rhs);
		/** Subtraction operator. */
		Radian& operator-= (const Degree& rhs);
		
		/** Multiplication operator. */
		Radian& operator*= (const float rhs);
		
		/** Division operator. */
		Radian& operator/= (const float rhs);
		
		
		/** Negative operator. */
		Radian operator- () const;
		
		
		
	private:
		float mRadian;
	};

}}


#endif /* SOMA_MATH_RADIAN_H_ */
