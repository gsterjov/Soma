
#ifndef SOMA_TYPES_H_
#define SOMA_TYPES_H_


#include <iostream>

/* integer types */
#include <inttypes.h>

/* math unit types */
#include <Soma/Math/Radian.h>
#include <Soma/Math/Degree.h>
#include <Soma/Math/Vector3.h>
#include <Soma/Math/Quaternion.h>
#include <Soma/Math/Matrix4.h>


namespace Soma
{

	/* units */
	typedef Math::Radian     Radian;
	typedef Math::Degree     Degree;
	typedef Math::Vector3    Vector;
	typedef Math::Quaternion Quaternion;
	typedef Math::Matrix4    Matrix;
	
	
	/* unsigned numbers */
	typedef unsigned char  uchar;
	typedef unsigned short ushort;
	typedef unsigned int   uint;
	typedef unsigned long  ulong;
	
	
	/* fixed length numbers */
	typedef int8_t  int8;
	typedef int16_t int16;
	typedef int32_t int32;
	typedef int64_t int64;
	
	typedef uint8_t  uint8;
	typedef uint16_t uint16;
	typedef uint32_t uint32;
	typedef uint64_t uint64;
	
	
	
	/**
	 * A simple colour structure.
	 */
	struct SOMA_API Colour
	{
		float r, g, b, a;
		
		
		Colour () : r(0), b(0), g(0), a(1) {}
		
		Colour (float red, float green, float blue, float alpha = 1)
		{ setColour (red, green, blue, alpha); }
		
		void setColour (float red, float green, float blue, float alpha = 1)
		{
			r = red;
			g = green;
			b = blue;
			a = alpha;
		}
		
		
		
		/**
		 * Stream out operator.
		 */
		friend std::ostream& operator<< (std::ostream& ostream,
		                                 const Colour& colour)
		{
			ostream << "Colour: {R=" << colour.r << ", G=" << colour.g <<
					", B=" << colour.b << ", A=" << colour.a << "}";
			
			return ostream;
		}
	};

}


#endif /* SOMA_TYPES_H_ */
