
#ifndef SOMA_FILE_SYSTEM_POSIX_DIRECTORY_H_
#define SOMA_FILE_SYSTEM_POSIX_DIRECTORY_H_


#include <Soma/FileSystem/Directory.h>


namespace Soma {
namespace FileSystem {

	/**
	 * Concrete implementation of the Directory class using the Posix API.
	 */
	class SOMA_API PosixDirectory : public Directory
	{
	public:
		/**
		 * Constructor.
		 * @param path The path to the directory.
		 */
		PosixDirectory (const String& path);
		
		/**
		 * Destructor.
		 */
		~PosixDirectory ();
		
		
		/**
		 * @copydoc Directory::getPath
		 */
		const String& getPath() const { return mPath; }
		
		
		/**
		 * @copydoc Directory::getName
		 */
		const String& getName() const { return mName; }
		
		
		/**
		 * @copydoc Directory::getDirectories
		 */
		DirectoryMap& getDirectories();
		
		
		/**
		 * @copydoc Directory::getFiles
		 */
		FileMap& getFiles();
		
		
	private:
		String mPath;
		String mName;
		
		DirectoryMap mDirCache;
		FileMap mFileCache;
		
		bool mCached;
		
		
		/**
		 * Build the directory entries cache.
		 */
		void buildCache ();
	};

}}


#endif /* SOMA_FILE_SYSTEM_POSIX_DIRECTORY_H_ */
