
#ifndef SOMA_FILE_SYSTEM_DIRECTORY_H_
#define SOMA_FILE_SYSTEM_DIRECTORY_H_


#include <vector>

#include <Soma/Config.h>
#include <Soma/String.h>
#include <Soma/FileSystem/File.h>


namespace Soma {
namespace FileSystem {


	class Directory;
	
	/**
	 * A Directory map.
	 */
	typedef std::map<String, Directory*> DirectoryMap;
	
	
	/**
	 * A directory within a virtual file system.
	 * 
	 * Virtual directories are logical directories within the virtual file
	 * system which can be used in a consistent way regardless of how and
	 * what that directory might represent. As a result its entirely possible
	 * for a directory to represent a folder within an archive or even a
	 * network location without the need to change how the directory is handled.
	 */
	class SOMA_API Directory
	{
	public:
		/**
		 * Destructor.
		 */
		virtual ~Directory () {}
		
		
		/**
		 * Get directory path.
		 * @return The path to this directory.
		 */
		virtual const String& getPath() const = 0;
		
		
		/**
		 * Get directory name.
		 * @return The name of this directory.
		 */
		virtual const String& getName() const = 0;
		
		
		/**
		 * Get a list of all the sub-directories in this directory.
		 * @return A Directory list.
		 */
		virtual DirectoryMap& getDirectories() = 0;
		
		
		/**
		 * Get a list of all the Files in this directory.
		 * @return A File list.
		 */
		virtual FileMap& getFiles() = 0;
	};

}}


#endif /* DIRECTORY_H_ */
