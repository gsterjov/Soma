
#ifndef SOMA_FILE_SYSTEM_MANAGER_H_
#define SOMA_FILE_SYSTEM_MANAGER_H_


#include <map>

#include <Soma/Config.h>
#include <Soma/String.h>
#include <Soma/Singleton.h>
#include <Soma/SubSystem.h>

#include <Soma/FileSystem/Directory.h>
#include <Soma/FileSystem/File.h>


namespace Soma {
namespace FileSystem {

	/**
	 * A virtual file system.
	 * 
	 * This class abstracts the platform file system and provides a simple
	 * interface to access any resource from the defined root paths. This
	 * allows for 'filters' to be applied during resource manipulation which
	 * allows for accessing archives, encrypted files and remote files
	 * without any perceived difference from the user of the virtual file
	 * system.
	 */
	class SOMA_API Manager : public Singleton <Manager>
	{
	public:
		/**
		 * Constructor.
		 */
		Manager ();
		
		/**
		 * Destructor.
		 */
		~Manager ();
		
		
		
		/**
		 * Mount a path.
		 * 
		 * Mounts the specified path into the virtual file system allowing
		 * it to be accessed from the root directory.
		 * 
		 * @param path A full or relative path to mount.
		 */
		void mount (const String& path);
		
		/**
		 * Unmount a path.
		 * 
		 * Unmounts the specified path removing it from the virtual file system
		 * and thus disallowing any access to it.
		 * 
		 * @param path A full or relative path to unmount.
		 */
		void unmount (const String& path);
		
		
		/**
		 * Find the file from the given virtual path.
		 * @return The File at the virtual path if found, other NULL.
		 */
		File* findFile (const String& path);
		
		
		/**
		 * Get a list of all the directories in the root file system.
		 * @return A Directory list.
		 */
		DirectoryMap& getDirectories();
		
		
		/**
		 * Get a list of all the files in the root file system.
		 * @return A File list.
		 */
		FileMap& getFiles();
		
		
		/**
		 * All currently mounted paths.
		 * @return A list containing the currently mounted paths.
		 */
		const DirectoryMap& getMounts() const { return mMounts; }
		
		
	private:
		DirectoryMap mMounts;
		
		DirectoryMap mDirCache;
		FileMap mFileCache;
		
		bool mCached;
		
		void buildCache ();
	};

}}


#endif /* SOMA_FILE_SYSTEM_MANAGER_H_ */
