
#ifndef SOMA_FILE_SYSTEM_FILE_H_
#define SOMA_FILE_SYSTEM_FILE_H_


#include <map>

#include <Soma/Config.h>
#include <Soma/String.h>


namespace Soma {
namespace FileSystem {


	class File;
	
	/**
	 * A File list.
	 */
	typedef std::map<String, File*> FileMap;
	
	
	/**
	 * A file within a virtual file system.
	 * 
	 * Virtual files are logical files within a virtual file system which
	 * can be used and manipulated in a consistent way much like a virtual
	 * Directory. As a result a virtual file can represent a file within
	 * an archive to a subsection of an encrypted stream without having
	 * to seperately handle unique file representations and requirements.
	 */
	class SOMA_API File
	{
	public:
		/**
		 * Destructor.
		 */
		virtual ~File () {}
		
		
		/**
		 * Get the path to the file.
		 * @return The path to this file.
		 */
		virtual const String& getPath() const = 0;
		
		
		/**
		 * Get the name of the file.
		 * @return The name of this file.
		 */
		virtual const String& getName() const = 0;
	};

}}


#endif /* SOMA_FILE_SYSTEM_FILE_H_ */
