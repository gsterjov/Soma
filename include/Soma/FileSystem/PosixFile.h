
#ifndef SOMA_FILE_SYSTEM_POSIX_FILE_H_
#define SOMA_FILE_SYSTEM_POSIX_FILE_H_


#include <Soma/FileSystem/File.h>


namespace Soma {
namespace FileSystem {

	/**
	 * Concrete implementation of the File class using the Posix API.
	 */
	class SOMA_API PosixFile : public File
	{
	public:
		/**
		 * Constructor.
		 * @param path The full path to the file.
		 */
		PosixFile (const String& path);
		
		/**
		 * Destructor.
		 */
		~PosixFile ();
		
		
		/**
		 * @copydoc File::getPath
		 */
		const String& getPath() const { return mPath; }
		
		/**
		 * @copydoc File::getName
		 */
		const String& getName() const { return mName; }
		
		
	private:
		String mPath;
		String mName;
	};

}}


#endif /* SOMA_FILE_SYSTEM_POSIX_FILE_H_ */
