
#ifndef SOMA_COMPOSITE_H_
#define SOMA_COMPOSITE_H_


#include <map>
#include <vector>

#include <Soma/Config.h>
#include <Soma/String.h>
#include <Soma/Event.h>
#include <Soma/Value.h>
#include <Soma/SmartPointer.h>


namespace Soma
{

	/* forward declarations */
	class Component;
	
	/** A list of components. */
	typedef std::vector<smart_ptr<Component> > ComponentList;
	
	
	
	/**
	 * Composite message for inter-component communication.
	 */
	struct CompositeMessage
	{
		/**
		 * The message type which uniquely identifies it.
		 */
		String type;
		
		/**
		 * The packed value associated with the message.
		 */
		Value value;
		
		
		/**
		 * Value constructor.
		 */
		CompositeMessage (const String& type, Value value)
		: type (type),
		  value (value)
		{
		}
		
		/**
		 * Empty value constructor.
		 */
		CompositeMessage (const String& type) : type(type) {}
	};
	
	
	
	
	/**
	 * A class which is made up of components.
	 * 
	 * A composite class is a base class providing component based design
	 * functionality. It acts as a container of sorts which allows an arbitrary
	 * number of components and further facilitates communication between these
	 * components. As such a composite is the absolute minimum an object must
	 * inherit from in order to benefit from the various features components
	 * can provide.
	 * 
	 * @ingroup components.
	 */
	class SOMA_API Composite : public ReferenceCounter
	{
	public:
		/**
		 * Constructor.
		 */
		Composite ();
		
		/**
		 * Destructor.
		 * 
		 * When the composite is destroyed all components are automatically
		 * unparented and unloaded.
		 */
		virtual ~Composite ();
		
		
		
		/**
		 * Get the name of the composite.
		 * @return The composite name.
		 */
		virtual const String& getName() const = 0;
		
		
		/**
		 * Get a specific component from the composite.
		 * 
		 * @param type The type of the component to retrieve.
		 * @return The component if found, NULL reference otherwise.
		 */
		smart_ptr<Component> getComponent (const String& type) const;
		
		/**
		 * Get a specific component from the provided type.
		 */
		template <typename T>
		smart_ptr<T> getComponent() const
		{
			return ref_cast<T> (getComponent (T::Type));
		}
		
		/**
		 * Get a list of all components.
		 * @return A list of components.
		 */
		ComponentList getComponents() const;
		
		
		
		/**
		 * Get a property value from the specified component.
		 * 
		 * @param type The type of component the property resides in.
		 * @param name The name of the property.
		 * 
		 * @return The value associated with the property if the component
		 * exists, NULL value otherwise.
		 */
		Value getProperty (const String& type, const String& name) const;
		
		/**
		 * Set a property value on the specified component.
		 * 
		 * @param type The type of component the property resides in.
		 * @param name The name of the property to set.
		 * @param value The value to assign to the property.
		 */
		void setProperty (const String& type,
		                  const String& name,
		                  const Value& value);
		
		
		
		/**
		 * Add component.
		 * 
		 * Adding a component gives the specific functionality defined by it to
		 * the composite thus allowing it to utilise and share the component
		 * functionality with other components and even other composites.
		 * 
		 * @param component The component to add.
		 */
		void addComponent (const smart_ptr<Component>& component);
		
		/**
		 * Add components by type.
		 * 
		 * Adding a component gives the specific functionality defined by it to
		 * the composite thus allowing it to utilise and share the component
		 * functionality with other components and even other composites.
		 * 
		 * This version will create a component instance from the type registry
		 * using the types specified in the parameter. The types should be a
		 * list of component types to add separated by a comma. For example,
		 * 
		 * @code
		 * entity->addComponents ("entity.movable, entity.audio.source");
		 * @endcode
		 * 
		 * @param types The types of components to add separated by a comma.
		 */
		void addComponents (const String& types);
		
		/**
		 * Remove component.
		 * 
		 * Removing a component will effectively remove the functionality
		 * provided by the component and the composite will continue to
		 * operate as if the component was never added.
		 * 
		 * @param component The component to remove.
		 */
		void removeComponent (const smart_ptr<Component>& component);
		
		/**
		 * Remove components by type.
		 * 
		 * Removing a component will effectively remove the functionality
		 * provided by the component and the composite will continue to
		 * operate as if the component was never added.
		 * 
		 * This version will remove all types that match the specified types
		 * in the parameter. Each type should be separated by a comma. For
		 * example,
		 * 
		 * @code
		 * entity->removeComponents ("entity.movable, entity.audio.source");
		 * @endcode
		 * 
		 * @param types The types of the components to remove.
		 */
		void removeComponents (const String& types);
		
		
		
		/**
		 * Send a message request to all components.
		 * 
		 * @param type The type of the message requested.
		 * @return A message if request was filled, NULL message otherwise.
		 */
		CompositeMessage request (const String& type)
		{
			CompositeMessage msg (type);
			Request.raise (msg);
			return msg;
		}
		
		
		/**
		 * Broadcast a message to all components.
		 * 
		 * @param type The type of the message.
		 * @param value The value to pack into the message.
		 */
		template <typename T>
		void broadcast (const String& type, T value)
		{
			CompositeMessage msg (type, value);
			Broadcast.raise (msg);
		}
		
		
		
		/**
		 * Load the composite and its components.
		 * 
		 * @note This is an all or nothing operation. If a single component
		 * fails to load for whatever reason all components are unloaded
		 * (if already loaded) and the composite will remain unloaded.
		 * 
		 * @return True if loaded, false otherwise.
		 */
		virtual bool load ()
		{
			if (loadComponents ()) Loaded.raise (this);
			return mLoaded;
		}
		
		/**
		 * Reload the composite and its components.
		 * 
		 * Ideally reloading a composite will only reinitialise its component
		 * data and thus no intensive task such as construction and destruction
		 * of resources would execute. However, the task of reloading is left
		 * entirely up to the component and it can very well be an intensive
		 * task if the requirements call for it.
		 * 
		 * @note This is an all or nothing operation. If a single component
		 * fails to reload for whatever reason all components are unloaded
		 * (if already loaded) and the composite will become unloaded.
		 * 
		 * @return True if reloaded, false otherwise.
		 */
		virtual bool reload ()
		{
			if (reloadComponents ()) Reloaded.raise (this);
			return mLoaded;
		}
		
		/**
		 * Unload the composite and its components.
		 */
		virtual void unload ()
		{
			unloadComponents ();
			Unloaded.raise (this);
		}
		
		/**
		 * Is the composite already loaded?
		 * 
		 * Due to the nature of composite loading and unloading a loaded
		 * composite is a fully loaded one. That is to say that all of its
		 * components are fully loaded also.
		 * 
		 * @return True if loaded, false otherwise.
		 */
		virtual bool isLoaded() { return mLoaded; }
		
		
		
		
		/**
		 * A message request to all components.
		 * @param message The requested message to fill.
		 */
		Event<void, CompositeMessage&> Request;
		
		/**
		 * A message broadcast to all components.
		 * @param message The broadcasted message.
		 */
		Event<void, const CompositeMessage&> Broadcast;
		
		
		/**
		 * A new component was added to the composite.
		 * @param component The newly added component.
		 */
		Event<void, const smart_ptr<Component>&> ComponentAdded;
		
		/**
		 * A component was removed from the composite.
		 * @param component The removed component.
		 */
		Event<void, const smart_ptr<Component>&> ComponentRemoved;
		
		
		/**
		 * The composite was fully loaded.
		 * @param composite The loaded composite.
		 */
		Event<void, const weak_ptr<Composite>&> Loaded;
		
		/**
		 * The composite was fully reloaded.
		 * @param composite The reloaded composite.
		 */
		Event<void, const weak_ptr<Composite>&> Reloaded;
		
		/**
		 * The composite was fully unloaded.
		 * @param composite The unloaded composite.
		 */
		Event<void, const weak_ptr<Composite>&> Unloaded;
		
		
		
		/** Log source name. */
		static const String LogSource;
		
		
		
	protected:
		/**
		 * Component map type indexed by the component type.
		 */
		typedef std::map<String, smart_ptr<Component> > ComponentMap;
		
		/**
		 * Component map.
		 * 
		 * All components added to the composite are stored in this map and
		 * can be retrieved simply with the type of the desired component.
		 */
		ComponentMap mComponents;
		
		
		/**
		 * The load state of the composite.
		 * 
		 * This boolean is used by the *loadComponents functions to indicate
		 * its internal load status and can be freely used by subclasses to
		 * indicate their own load state.
		 * 
		 * @note Be aware that calling any of the *loadComponents functions
		 * can, and most likely will, change this value.
		 */
		bool mLoaded;
		
		
		/**
		 * Load the components within the composite.
		 * 
		 * This will effectively load all components added to the composite
		 * or unload them if a component fails to load.
		 * 
		 * @note This is the default functionality for load() and is provided
		 * here for convenience to deriving classes implementing their own
		 * load() method.
		 * 
		 * @return True if all components loaded, false otherwise.
		 */
		bool loadComponents ();
		
		/**
		 * Reload the components within the composite.
		 * 
		 * This will effectively reload all components added to the composite
		 * or unload them if a component fails to reload.
		 * 
		 * @note This is the default functionality for reload() and is provided
		 * here for convenience to deriving classes implementing their own
		 * reload() method.
		 * 
		 * @return True if all components reloaded, false otherwise.
		 */
		bool reloadComponents ();
		
		/**
		 * Unload the components within the composite.
		 * 
		 * @note This is the default functionality for unload() and is provided
		 * here for convenience to deriving classes implementing their own
		 * unload() method.
		 */
		void unloadComponents ();
	};

}


#endif /* SOMA_COMPOSITE_H_ */
