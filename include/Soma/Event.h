
#ifndef SOMA_EVENT_H_
#define SOMA_EVENT_H_


#include <vector>

#include <Soma/Config.h>
#include <Soma/Delegate.h>
#include <iostream>

namespace Soma
{

	/**
	 * Generic event provider.
	 * 
	 * Events implement the Observer/Subscriber pattern but in a more loosely
	 * coupled way than virtual inheritance. A class adding an Event thus
	 * has the ability to broadcast a request or change to various delegates
	 * (or subscribers). A generic event further extends this ability by
	 * allowing a class to determine an Event's parameters and return type
	 * with no modification or subclassing required. Adding a delegate to
	 * an Event is trivial and does not increase in complexity as an application
	 * grows in size and requirements.
	 * Defining an Event is as easy as declaring
	 * 
	 * @code
	 * Event<bool, int> MyEvent;
	 * @endcode
	 * 
	 * where bool is the return type and int is the parameter.
	 */
	template <typename ReturnType, typename ArgType>
	class SOMA_API Event
	{
	public:
		/**
		 * The type of delegate this event supports.
		 */
		typedef DelegateInterface<ReturnType, ArgType> DelegateType;
		
		
		/**
		 * Constructor.
		 */
		Event () : mRaising(false) {}
		
		
		/**
		 * Destructor.
		 * Automatically free's cloned delegates.
		 */
		~Event ()
		{
			assert (!mRaising);
			
			typename DelegateList::iterator iter;
			
			for (iter = mDelegates.begin(); iter != mDelegates.end(); ++iter)
				delete *iter;
		}
		
		
		
		/**
		 * Get all delegates registered with the event.
		 * @return A list of delegates.
		 */
		const std::vector<DelegateType*>& getDelegates() { return mDelegates; }
		
		
		
		/**
		 * Raise the event.
		 * @param arg The event argument to pass on to the delegates.
		 */
		void raise (ArgType arg)
		{
			mRaising = true;
			
			typename DelegateList::iterator iter;
			
			for (iter = mDelegates.begin(); iter != mDelegates.end(); ++iter)
				(*iter)->call (arg);
			
			mRaising = false;
		}
		
		
		
		/**
		 * Registers a delegate with the event.
		 * @param delegate The delegate to be called when the event is emitted.
		 */
		void add (const DelegateType& delegate)
		{
			assert (!mRaising);
			mDelegates.push_back (delegate.clone());
		}
		
		
		
		/**
		 * Registers a delegate wrapping the specified non-static member
		 * function.
		 * 
		 * This method acts in the same way as add(Delegate<ClassType,
		 * ReturnType, ArgType>& delegate) except that it takes a member
		 * function and the object it resides on instead of a delegate already
		 * wrapping the member function. This is useful when the delegate
		 * wrapper functor is not needed outside the event system.
		 * 
		 * @code
		 * myProvider.MyEvent.add (this, &myClass::myMethod);
		 * @endcode
		 * 
		 * @param object The object where the member function resides.
		 * @param callback The non-static member function to add.
		 * 
		 * @see add(Delegate<ClassType, ReturnType, ArgType>& delegate)
		 */
		template <class ClassType>
		void add (ClassType* object, ReturnType (ClassType::*callback)(ArgType arg))
		{
			Delegate<ClassType, ReturnType, ArgType> delegate (object, callback);
			add (delegate);
		}
		
		
		
		/**
		 * Unregisters a delegate with the event.
		 * 
		 * Removal of a delegate is determined by its callback address
		 * and as such the same delegate functor does not need to be supplied.
		 * All that is required to remove a delegate is for its callback
		 * to be the same and for it to reside in the same instantiated object
		 * if its a non-static member delegate.
		 * 
		 * @param delegate the delegate to remove from the event list.
		 */
		void remove (const DelegateType& delegate)
		{
			assert (!mRaising);
			
			typename DelegateList::iterator iter;
			
			/* look for a delegate match */
			for (iter = mDelegates.begin(); iter != mDelegates.end(); ++iter)
			{
				/* found a matching delegate */
				if (delegate.equals (*iter))
				{
					delete *iter;
					mDelegates.erase (iter);
					
					return;
				}
			}
		}
		
		
		
		/**
		 * Unregisters a delegate wrapping the specified non-static member
		 * function.
		 * 
		 * This method acts in the same way as remove(Delegate<ClassType,
		 * ReturnType, ArgType>& delegate) except that it takes a member
		 * function and the object it resides on instead of a delegate already
		 * wrapping the member function. This is useful when the delegate
		 * wrapper functor is not needed outside the event system.
		 * 
		 * @code
		 * myProvider.MyEvent.remove (this, &myClass::myMethod);
		 * @endcode
		 * 
		 * @param object The object where the member function resides.
		 * @param callback The non-static member function to remove.
		 * 
		 * @see remove(Delegate<ClassType, ReturnType, ArgType>& delegate)
		 */
		template <class ClassType>
		void remove (ClassType* object, ReturnType (ClassType::*callback)(ArgType arg))
		{
			Delegate<ClassType, ReturnType, ArgType> delegate (object, callback);
			remove (delegate);
		}
		
		
		
		/**
		 * @copydoc Event<ReturnType, ArgType>::add
		 */
		Event& operator+= (const DelegateType& delegate)
		{
			add (delegate);
			return *this;
		}
		
		
		/**
		 * @copydoc Event<ReturnType, ArgType>::remove
		 */
		Event& operator-= (const DelegateType& delegate)
		{
			remove (delegate);
			return *this;
		}
		
		
		
	private:
		bool mRaising;
		
		/* the internal delegate list */
		typedef std::vector<DelegateType*> DelegateList;
		DelegateList mDelegates;
	};

}


#endif /* SOMA_EVENT_H_ */
