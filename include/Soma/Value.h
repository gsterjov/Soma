
#ifndef SOMA_VALUE_H_
#define SOMA_VALUE_H_


#include <typeinfo>

#include <Soma/Config.h>
#include <Soma/Exception.h>


namespace Soma
{

	/**
	 * A generic value.
	 */
	class SOMA_API Value
	{
	public:
		/**
		 * Constructor.
		 */
		Value () : mData(0) {}
		
		
		/**
		 * Copy constructor.
		 */
		Value (const Value& ref)
		{
			mData = !ref.isEmpty() ? ref.mData->clone() : 0;
		}
		
		
		/**
		 * Value constructor.
		 * 
		 * By not making the constructor explicit with allow implicit
		 * conversions to a generic value adding cleaner syntax and use.
		 */
		template <typename T>
		Value (T value) : mData(new GenericData <T> (value)) {}
		
		
		/**
		 * Destructor.
		 */
		~Value ()
		{
			clear ();
		}
		
		
		/**
		 * Assignment operator.
		 */
		const Value& operator= (const Value& ref)
		{
			mData = !ref.isEmpty() ? ref.mData->clone() : 0;
			return *this;
		}
		
		
		/**
		 * Get value type.
		 */
		const std::type_info& getType() const
		{
			return mData ? mData->type() : typeid(void);
		}
		
		
		/**
		 * Has the value been set?
		 */
		bool isEmpty() const { return !mData; }
		
		
		
		/**
		 * Clear the value.
		 */
		void clear ()
		{
			if (mData)
			{
				delete mData;
				mData = 0;
			}
		}
		
		
		
		/**
		 * Set generic value.
		 */
		template <typename T>
		void set (T value)
		{
			clear();
			mData = new GenericData <T> (value);
		}
		
		
		
		/**
		 * Get generic value.
		 */
		template <typename T>
		T& get () const
		{
			/* empty value */
			if (!mData)
				SOMA_EXCEPTION ("Value", "Cannot cast value type to the "
						"requested type because the value is empty");
			
			
			/* cast to value type */
			if (typeid(T) == mData->type())
				return static_cast<GenericData<T>*> (mData)->value;
			
			
			/* target type and value type dont match */
			SOMA_EXCEPTION ("Value", "Failed to cast value type to the "
					"requested type");
		}
		
		
		
	private:
		/* data storage */
		struct Data;
		Data* mData;
		
		
		/**
		 * Data storage interface.
		 */
		struct Data
		{
			/**
			 * Get data type.
			 */
			virtual const std::type_info& type() const = 0;
			
			/**
			 * Clone data.
			 */
			virtual Data* clone() const = 0;
		};
		
		
		/**
		 * Generic data storage.
		 */
		template <typename T>
		struct GenericData : Data
		{
			/**
			 * Data value.
			 */
			T value;
			
			
			/**
			 * Value constructor.
			 */
			GenericData (T value) : value(value) {}
			
			
			/**
			 * @copydoc Data::type
			 */
			const std::type_info& type() const { return typeid(value); }
			
			/**
			 * @copydoc Data::clone
			 */
			Data* clone() const { return new GenericData <T> (value); }
		};
	};

}


#endif /* SOMA_VALUE_H_ */
