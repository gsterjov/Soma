
#ifndef SOMA_VRI_H_
#define SOMA_VRI_H_


#include <Soma/Config.h>
#include <Soma/String.h>


namespace Soma
{

	/**
	 * A virtual resource identifier.
	 * 
	 * VRI's are often used in place of a String due to the fact that it can
	 * implicitly convert a String and verify it before the VRI is used. The
	 * need for a VRI syntax is to allow complex interaction with the resource
	 * system as its entirely possible for a single asset to hold many
	 * interelated resources, and referring to individual resources would become
	 * a task in its own without a simple mechanism to abstract it, such as a
	 * virtual resource identifier.
	 */
	class SOMA_API Vri
	{
	public:
		/**
		 * The complete virtual path to the resource.
		 */
		String path;
		
		/**
		 * The name of the asset containing the resource.
		 */
		String asset;
		
		/**
		 * The name of the resource within the asset.
		 */
		String resource;
		
		
		
		/**
		 * VRI Constructor.
		 * 
		 * A VRI is composed of both the asset path and resource name separated
		 * by a hash. For example, the path '/media/asset.dae#mesh' is
		 * considered a valid VRI as well as '/media/asset.dae' which will
		 * automatically use the first resource found within the asset.
		 * 
		 * @param vri A VRI pointing to a specific resource.
		 */
		Vri (const String& vri) : path(vri)
		{
			parse (vri);
		}
		
		
		/**
		 * @copydoc Vri(const String& vri)
		 */
		Vri (const char* vri) : path(vri)
		{
			parse (vri);
		}
		
		
		
		/**
		 * Copy constructor.
		 */
		Vri (const Vri& vri)
		: path     (vri.path),
		  asset    (vri.asset),
		  resource (vri.resource)
		{}
		
		
		/**
		 * Empty constructor.
		 */
		Vri () {}
		
		
		
		/**
		 * Copy operator.
		 */
		Vri& operator= (const Vri& vri)
		{
			path = vri.path;
			asset = vri.path;
			resource = vri.path;
			return *this;
		}
		
		
		/**
		 * Assignment operator.
		 */
		Vri& operator= (const String& vri)
		{
			parse (vri);
			return *this;
		}
		
		
		
		/* equality comparison operator */
		friend bool operator== (const Vri&    lhs, const Vri&    rhs) { return lhs.path == rhs.path; }
		friend bool operator== (const Vri&    lhs, const String& rhs) { return lhs.path == rhs; }
		friend bool operator== (const String& lhs, const Vri&    rhs) { return lhs == rhs.path; }
		
		/* inequality comparison operator */
		friend bool operator!= (const Vri&    lhs, const Vri&    rhs) { return !(lhs == rhs); }
		friend bool operator!= (const Vri&    lhs, const String& rhs) { return !(lhs == rhs); }
		friend bool operator!= (const String& lhs, const Vri&    rhs) { return !(lhs == rhs); }
		
		
		
		/** Special NULL Vri type. */
		static const Vri EMPTY;
		
		
		
	private:
		/* parse VRI path */
		void parse (const String& vri)
		{
			/* pull the asset path and resource name from the uri */
			StringList list = Strings::split (vri, "#");
			
			asset = vri;
			
			if (list.size() > 0) asset = list[0];
			if (list.size() > 1) resource = list[1];
		}
	};

}


#endif /* SOMA_VRI_H_ */
