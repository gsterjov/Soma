
#ifndef SOMA_MOVABLE_H_
#define SOMA_MOVABLE_H_


#include <Soma/Config.h>
#include <Soma/Types.h>


namespace Soma
{

	/**
	 * The movable object interface.
	 * 
	 * Anything that has a world position should implement this interface
	 * if its position can be changed externally. This allows for a consistent
	 * move scheme throughout the engine and further allows for convenient
	 * functions to be used on all objects using this interface.
	 */
	class SOMA_API Movable
	{
	public:
		
		/**
		 * Set the position of the object relative to its parent.
		 * @param position The position of the object.
		 */
		void setPosition (const Vector& position)
		{ _setPosition (position); }
		
		/**
		 * Set the orientation of the object relative to its parent.
		 * @param orientation The orientation of the object.
		 */
		void setOrientation (const Quaternion& orientation)
		{ _setOrientation (orientation); }
		
		
		/**
		 * Get the position of the object relative to its parent.
		 * @return The position of the object.
		 */
		const Vector& getPosition () { return _getPosition (); }
		
		/**
		 * Get the orientation of the object relative to its parent.
		 * @return The orientation of the object.
		 */
		const Quaternion& getOrientation () { return _getOrientation (); }
		
		
		/**
		 * Set the position of the object relative to its parent.
		 * 
		 * @param x The position of the object on the X axis in units.
		 * @param y The position of the object on the Y axis in units.
		 * @param z The position of the object on the Z axis in units.
		 */
		void setPosition (float x, float y, float z)
		{ _setPosition (Vector (x, y, z)); }
		
		
		
		/**
		 * Move the object relative to its current position.
		 * @param translation The amount of units to move along each axis.
		 */
		void translate (const Vector& translation)
		{ _translate (translation); }
		
		/**
		 * Move the object relative to its current position.
		 * 
		 * @param x The amount of units to move the object along the X axis.
		 * @param y The amount of units to move the object along the Y axis.
		 * @param z The amount of units to move the object along the Z axis.
		 */
		void translate (float x, float y, float z)
		{ _translate (Vector (x, y, z)); }
		
		
		
		/**
		 * Rotate the object relative to its current orientation.
		 * @param rotation The rotation to apply.
		 */
		void rotate (const Quaternion& rotation)
		{ _rotate (rotation); }
		
		/**
		 * Rotate the object in radians along the given axis.
		 * 
		 * @param radian The amount of radians to rotate.
		 * @param axis The axis to rotate on.
		 */
		void rotate (const Radian& radian, const Vector& axis)
		{ _rotate (Quaternion (radian, axis)); }
		
		/**
		 * Rotate the object in degrees along the given axis.
		 * 
		 * @param degree The amount of degrees to rotate.
		 * @param axis The axis to rotate on.
		 */
		void rotate (const Degree& degree, const Vector& axis)
		{ _rotate (Quaternion (Radian (degree), axis)); }
		
		
		
		/**
		 * Rotate the object on the X axis in radians.
		 * @param radian The amount of radians to rotate.
		 */
		void pitch (const Radian& radian)
		{ rotate (radian, Vector::UNIT_X); }
		
		/**
		 * Rotate the object on the X axis in degrees.
		 * @param degree The amount of degrees to rotate.
		 */
		void pitch (const Degree& degree)
		{ rotate (degree, Vector::UNIT_X); }
		
		
		/**
		 * Rotate the object on the Y axis in radians.
		 * @param radian The amount of radians to rotate.
		 */
		void yaw (const Radian& radian)
		{ rotate (radian, Vector::UNIT_Y); }
		
		/**
		 * Rotate the object on the Y axis in degrees.
		 * @param degree The amount of degrees to rotate.
		 */
		void yaw (const Degree& degree)
		{ rotate (degree, Vector::UNIT_Y); }
		
		
		/**
		 * Rotate the object on the Z axis in radians.
		 * @param radian The amount of radians to rotate.
		 */
		void roll (const Radian& radian)
		{ rotate (radian, Vector::UNIT_Z); }
		
		/**
		 * Rotate the object on the Z axis in degrees.
		 * @param degree The amount of degrees to rotate.
		 */
		void roll (const Degree& degree)
		{ rotate (degree, Vector::UNIT_Z); }
		
		
		
	protected:
		/**
		 * Set the position of the object relative to its parent. This function
		 * must be implementated by all classes inheriting Movable.
		 * 
		 * @param translation The position of the object.
		 */
		virtual void _setPosition (const Vector& translation) = 0;
		
		/**
		 * Set the orientation of the object relative to its parent. This
		 * function must be implementated by all classes inheriting Movable.
		 * 
		 * @param rotation The orientation of the object.
		 */
		virtual void _setOrientation (const Quaternion& rotation) = 0;
		
		
		/**
		 * Get the position of the object relative to its parent. This function
		 * must be implementated by all classes inheriting Movable.
		 * 
		 * @return The position of the object.
		 */
		virtual const Vector& _getPosition () = 0;
		
		/**
		 * Get the orientation of the object relative to its parent.
		 * @return The orientation of the object.
		 */
		virtual const Quaternion& _getOrientation () = 0;
		
		
		/**
		 * Move the object relative to its current position. This function
		 * must be implementated by all classes inheriting Movable.
		 * 
		 * @param translation The amount of units to move along each axis.
		 */
		virtual void _translate (const Vector& translation) = 0;
		
		/**
		 * Rotate the object relative to its current orientation. This function
		 * must be implementated by all classes inheriting Movable.
		 * 
		 * @param rotation The rotation to apply.
		 */
		virtual void _rotate (const Quaternion& rotation) = 0;
	};

}


#endif /* SOMA_MOVABLE_H_ */
