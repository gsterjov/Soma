
#ifndef SOMA_STRING_H_
#define SOMA_STRING_H_


#include <string>
#include <vector>
#include <sstream>

#include <Soma/Config.h>
#include <Soma/Types.h>


namespace Soma
{

	typedef std::string String;
	typedef std::vector<String> StringList;
	
	
	
	/**
	 * String manipulation class.
	 * 
	 * The Strings class provides static function for convenient manipulation
	 * of strings. Since String is just a typedef of std::string the static
	 * functions have wide applicability.
	 */
	class SOMA_API Strings
	{
	public:
		static String& trim    (String& str);
		static String& toLower (String& str);
		static String& toUpper (String& str);
		
		static StringList split (const String& str, const String& delimiters = "\n");
		
		
		static int    toInt    (const String& str);
		static uint   toUInt   (const String& str);
		static long   toLong   (const String& str);
		static ulong  toULong  (const String& str);
		static bool   toBool   (const String& str);
		static float  toFloat  (const String& str);
		static double toDouble (const String& str);
		
		
		/**
		 * Converts a given size_t into a string.
		 * 
		 * @param val The value to convert.
		 * @return The string equivalent interpreted from the value.
		 */
		template <typename T>
		static String convert (T val)
		{
			std::ostringstream stream (std::ostringstream::out);
			stream << val;
			return stream.str();
		}
		
		
		/**
		 * Boolean conversion specialisation.
		 * 
		 * @param val A boolean to convert.
		 * @return The string "true" if val is true, "false" otherwise.
		 */
		static String convert (bool val)
		{
			return val ? "true" : "false";
		}
	};

}


#endif /* SOMA_STRING_H_ */
