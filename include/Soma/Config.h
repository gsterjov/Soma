
#ifndef SOMA_CONFIG_H_
#define SOMA_CONFIG_H_


#include <Soma/ConfigDefines.h>


/* helpers for shared library support */
#if defined _WIN32 || defined __CYGWIN__
	#define SOMA_HELPER_LIB_IMPORT __declspec(dllimport)
	#define SOMA_HELPER_LIB_EXPORT __declspec(dllexport)
	#define SOMA_HELPER_LIB_LOCAL
#else
	#if __GNUC__ >= 4
		#define SOMA_HELPER_LIB_IMPORT __attribute__ ((visibility("default")))
		#define SOMA_HELPER_LIB_EXPORT __attribute__ ((visibility("default")))
		#define SOMA_HELPER_LIB_LOCAL  __attribute__ ((visibility("hidden")))
	#else
		#define SOMA_HELPER_LIB_IMPORT
		#define SOMA_HELPER_LIB_EXPORT
		#define SOMA_HELPER_LIB_LOCAL
	#endif
#endif



/* define SOMA_API and SOMA_LOCAL */
/* Soma is a shared library */
#ifdef SOMA_SHARED
	/* Soma is being built, export it */
	#ifdef SOMA_EXPORTS
		#define SOMA_API SOMA_HELPER_LIB_EXPORT
	/* Soma is being used, import it */
	#else
		#define SOMA_API SOMA_HELPER_LIB_IMPORT
	#endif /* SOMA_EXPORTS */
	
	#define SOMA_LOCAL SOMA_HELPER_LIB_LOCAL

/* Soma is not a shared library */
#else
	#define SOMA_API
	#define SOMA_LOCAL
#endif /* SOMA_SHARED */



#endif /* SOMA_CONFIG_H_ */
