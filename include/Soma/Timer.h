
#ifndef SOMA_TIMER_H_
#define SOMA_TIMER_H_

#include <Soma/Config.h>
#include <Soma/Types.h>


#ifdef SOMA_SYSTEM_UNIX
#include <time.h>
#endif


namespace Soma
{

	/**
	 * Generic timer.
	 * 
	 * The Timer class provides a simple and efficient way to track elapsed
	 * time either since it was last reset or since a value was last retrieved
	 * giving it 'split time' functionality. It is used by the Engine class
	 * to provide accurate microsecond precision timing to the game loop.
	 */
	class SOMA_API Timer
	{
	public:
		/**
		 * Constructor.
		 */
		Timer ();
		
		/**
		 * Destructor.
		 */
		~Timer ();
		
		
		/**
		 * Resets the timer.
		 * 
		 * A timer value is the time elapsed from the last reset call.
		 * Once reset is called the timer starts at zero again.
		 */
		void reset ();
		
		/**
		 * Blocks the current thread for a set amount of time.
		 * 
		 * This will effectively block the current thread on this function
		 * for the specified amount of time in a cross platform manner. Due to
		 * operating system clock resolution it is possible that a time greater
		 * than specified will be spent sleeping and thus why the actual time
		 * spent is returned.
		 * 
		 * @param time The amount of time (in seconds) to sleep for.
		 * @return The actual time spent sleeping.
		 */
		float sleep (float time);
		
		
		/**
		 * Elapsed time in microseconds.
		 * 
		 * The total elapsed time in microseconds since the last timer reset.
		 * @param splitTime If true the elapsed time since a value was retrieved
		 * will be returned instead of the total time elapsed from a reset.
		 * 
		 * @return The elapsed time in microseconds.
		 */
		float getMicroseconds (bool splitTime = false);
		
		/**
		 * Elapsed time in milliseconds.
		 * 
		 * The total elapsed time in milliseconds since the last timer reset.
		 * @param splitTime If true the elapsed time since a value was retrieved
		 * will be returned instead of the total time elapsed from a reset.
		 * 
		 * @return The elapsed time in milliseconds.
		 */
		float getMilliseconds (bool splitTime = false);
		
		/**
		 * Elapsed time in seconds.
		 * 
		 * The total elapsed time in seconds since the last timer reset.
		 * @param splitTime If true the elapsed time since a value was retrieved
		 * will be returned instead of the total time elapsed from a reset.
		 * 
		 * @return The elapsed time in seconds.
		 */
		float getSeconds (bool splitTime = false);
		
		
	private:
#ifdef SOMA_SYSTEM_UNIX
		timespec mStart, mLast;
#endif
	};

}


#endif /* SOMA_TIMER_H_ */
