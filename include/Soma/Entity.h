
#ifndef SOMA_ENTITY_H_
#define SOMA_ENTITY_H_


#include <map>
#include <list>

#include <Soma/Config.h>
#include <Soma/String.h>
#include <Soma/Event.h>
#include <Soma/Composite.h>
#include <Soma/Movable.h>
#include <Soma/SmartPointer.h>


namespace Soma
{

	/* forward declarations */
	class Entity;
	class World;
	
	
	/** A list of entities. */
	typedef std::vector<smart_ptr<Entity> > EntityList;
	
	
	
	/**
	 * Entity ParentChanged event args.
	 */
	struct ParentChangedEventArgs
	{
		weak_ptr<Entity> oldParent;
		weak_ptr<Entity> newParent;
	};
	
	
	
	/**
	 * An instance of an asset resource.
	 * 
	 * An object combines low-level objects from various sub-systems
	 * into one high level object. This allows specific entities to be
	 * associated with one another regardless of their construction or location.
	 * An example of an object resource would be a character's head, the
	 * physics body defining collision boundaries and a material detailing
	 * how it should be rendered. As such strongly associated resources
	 * can be encapsulated and used to enable weak or strong association
	 * with other objects.
	 * 
	 * @ingroup components
	 */
	class SOMA_API Entity : public Composite,
	                        public Movable
	{
	public:
		/**
		 * Named constructor.
		 * @param name The name of the entity.
		 */
		explicit Entity (const String& name);
		
		/**
		 * Constructor with components.
		 * 
		 * Allows easy construction of an entity by providing a list of
		 * components to add and load after creation.
		 * 
		 * @param name The name of the entity.
		 * @param types The types of components to add separated by a comma.
		 */
		Entity (const String& name, const String& types);
		
		
		/**
		 * Destructor.
		 * Upon destruction the entity children and components will be unloaded.
		 */
		virtual ~Entity ();
		
		
		/**
		 * Get the name of the entity.
		 * @return The entity name.
		 */
		const String& getName() const { return mName; }
		
		
		/**
		 * Get a specific child entity by name.
		 * @param name The name of the child entity to retrieve.
		 * @return The child entity if found, NULL reference otherwise.
		 */
		smart_ptr<Entity> getChild (const String& name) const;
		
		/**
		 * Get a list of all child entities.
		 * @return A list of entities.
		 */
		EntityList getChildren() const;
		
		
		/**
		 * Get the parent of the entity.
		 * @return The entity parent.
		 */
		const weak_ptr<Entity>& getParent() const { return mParent; }
		
		/**
		 * Change the entity parent.
		 * 
		 * This doesn't automatically add or remove itself from the parent's
		 * store, rather it merely maintains a reference to the parent to allow
		 * for inter-component communication as well as keeping a simple
		 * entity heirarchy.
		 * 
		 * @param parent The new entity parent.
		 */
		void setParent (const weak_ptr<Entity>& parent);
		
		
		/**
		 * Get the absolute position derived from parents.
		 * @return The absolute position of the component.
		 */
		Vector getAbsolutePosition() const { return mParentPosition + mPosition; }
		
		/**
		 * Get the absolute orientation derived from parents.
		 * @return The absolute orientation of the component.
		 */
		Quaternion getAbsoluteOrientation() const { return mParentOrientation * mOrientation; }
		
		
		/**
		 * Get the world the entity is currently in.
		 * @return The world containing the entity.
		 */
		const weak_ptr<World> getWorld() const { return mWorld; }
		
		
		/**
		 * Set the world the entity should be in.
		 * @param world The world to place the entity in.
		 */
		void setWorld (const weak_ptr<World>& world);
		
		
		
		/**
		 * Add child entity.
		 * @param child The child entity to add.
		 */
		void addChild (const smart_ptr<Entity>& child);
		
		/**
		 * Remove child entity.
		 * @param child The child entity to remove.
		 */
		void removeChild (const smart_ptr<Entity>& child);
		
		/**
		 * Remove child entity by name.
		 * @param name The name of the child entity to remove.
		 */
		void removeChild (const String& name);
		
		
		
		/**
		 * @copydoc Composite::load
		 */
		bool load ();
		
		/**
		 * @copydoc Composite::reload
		 */
		bool reload ();
		
		/**
		 * @copydoc Composite::unload
		 */
		void unload ();
		
		
		
		/**
		 * Position changed event.
		 * @param position The new absolute position.
		 */
		Event<void, const Vector&> PositionChanged;
		
		/**
		 * Orientation changed event.
		 * @param orientation The new absolute orientation.
		 */
		Event<void, const Quaternion&> OrientationChanged;
		
		
		
		/**
		 * A new child was added to the entity.
		 * @param child The newly added child.
		 */
		Event<void, const weak_ptr<Entity>&> ChildAdded;
		
		/**
		 * A child was removed from the entity.
		 * @param child The removed child.
		 */
		Event<void, const weak_ptr<Entity>&> ChildRemoved;
		
		
		/**
		 * The parent of the entity has changed.
		 * @param oldParent The previous entity parent.
		 * @param newParent The new entity parent.
		 */
		Event<void, const ParentChangedEventArgs&> ParentChanged;
		
		
		
		/** Log source name. */
		static const String LogSource;
		
		
		
	private:
		String mName;
		weak_ptr<Entity> mParent;
		weak_ptr<World> mWorld;
		
		Vector mPosition;
		Quaternion mOrientation;
		
		Vector mParentPosition;
		Quaternion mParentOrientation;
		
		
		/* heirarchy */
		typedef std::map<String, smart_ptr<Entity> > EntityMap;
		EntityMap mChildren;
		
		
		
		/* movable implementation */
		const Vector&     _getPosition()    { return mPosition; }
		const Quaternion& _getOrientation() { return mOrientation; }
		
		void _setPosition (const Vector& position)
		{
			mPosition = position;
			PositionChanged.raise (mPosition);
		}
		void _setOrientation (const Quaternion& orientation)
		{
			mOrientation = orientation;
			OrientationChanged.raise (mOrientation);
		}
		
		void _translate (const Vector& translation)
		{
			mPosition += translation;
			PositionChanged.raise (mPosition);
		}
		void _rotate (const Quaternion& rotation)
		{
//			mOrientation *= rotation;
			OrientationChanged.raise (mOrientation);
		}
		
		
		
		/* event handlers */
		void onParentPositionChanged (const Vector& position) { mParentPosition = position; }
		void onParentOrientationChanged (const Quaternion& orientation) { mParentOrientation = orientation; }
	};

}


#endif /* SOMA_ENTITY_H_ */
