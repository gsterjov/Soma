
#ifndef SOMA_FORMATS_COLLADA_ASSET_H_
#define SOMA_FORMATS_COLLADA_ASSET_H_


#include <Soma/Config.h>
#include <Soma/String.h>
#include <Soma/Vri.h>

#include <Soma/Asset.h>


/* forward declarations */
namespace ColladaParser { class Document; class Node; }


namespace Soma {
namespace Formats {
namespace Collada {


	/**
	 * An asset resource loaded from a Collada document.
	 * 
	 * This class is basically a Soma resource for a 'visual_scene'
	 * field found in the Collada document.
	 */
	class SOMA_API Asset : public Soma::Asset
	{
	public:
		/**
		 * Constructor.
		 * @param vri The path to the asset resource.
		 */
		explicit Asset (const Vri& vri);
		
		/**
		 * Destructor.
		 */
		virtual ~Asset ();
		
		
		/**
		 * @copydoc Soma::Asset::load
		 */
		bool load ();
		
		
	private:
		Vri mVri;
		
		
		/* node parser */
		void parseNode (const ColladaParser::Document& doc,
		                const ColladaParser::Node* node);
		
		
		/* transform parser */
		Matrix parseTransforms (const ColladaParser::Node* node);
	};
	
	
	
	/**
	 * Collada asset resource format.
	 * 
	 * Though most of the time an asset resource
	 * from a Collada document will be added to the resource manager when
	 * the entire document is parsed, by creating a Collada asset resource
	 * format we can allow a single node to be loaded from a document
	 * manually.
	 */
	struct SOMA_API AssetFormat : public AssetManager::Format
	{
		/**
		 * @copydoc AssetManager::Format::getExt
		 */
		String getExt() const { return ".dae"; }
		
		/**
		 * @copydoc AssetManager::Format::create
		 */
		Asset* create (const Vri& vri) const { return new Asset (vri); }
	};


}}}


#endif /* SOMA_FORMATS_COLLADA_ASSET_H_ */
