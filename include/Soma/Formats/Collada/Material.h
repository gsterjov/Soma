
#ifndef SOMA_FORMATS_COLLADA_MATERIAL_H_
#define SOMA_FORMATS_COLLADA_MATERIAL_H_


#include <Soma/Config.h>
#include <Soma/String.h>
#include <Soma/Vri.h>

#include <Soma/Visual/Material.h>


/* forward declarations */
namespace ColladaParser { class Document; class Material; }


namespace Soma {
namespace Formats {
namespace Collada {


	/**
	 * A material resource loaded from a Collada document.
	 * 
	 * This class is basically a Soma resource for a 'material' field found
	 * in the Collada document.
	 */
	class SOMA_API Material : public Visual::Material
	{
	public:
		/**
		 * Constructor.
		 * @param vri The path to the resource within an asset.
		 */
		explicit Material (const Vri& vri);
		
		/**
		 * Empty constructor.
		 */
		Material ();
		
		/**
		 * Destructor.
		 */
		virtual ~Material ();
		
		
		/* Material implementation */
		bool load ();
		
		
		/**
		 * Load given Collada material.
		 * @param material The material to manually load.
		 */
		void loadMaterial (const ColladaParser::Document& doc,
		                   const ColladaParser::Material* material);
		
		
	private:
		Vri mVri;
	};
	
	
	
	/**
	 * Collada material resource format.
	 * 
	 * Though most of the time a material resource from a Collada document
	 * will be added to the resource manager when the entire document is
	 * parsed, by creating a Collada material resource format we can allow a
	 * single material to be loaded from a document manually.
	 */
	struct SOMA_API MaterialFormat : public Visual::MaterialManager::Format
	{
		/**
		 * @copydoc Visual::MaterialManager::Format::getExt
		 */
		String getExt() const { return ".dae"; }
		
		/**
		 * @copydoc Visual::MaterialManager::Format::create
		 */
		Visual::Material* create (const Vri& vri) const { return new Material (vri); }
	};


}}}


#endif /* SOMA_FORMATS_COLLADA_MATERIAL_H_ */
