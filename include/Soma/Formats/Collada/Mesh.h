
#ifndef SOMA_FORMATS_COLLADA_MESH_H_
#define SOMA_FORMATS_COLLADA_MESH_H_


#include <Soma/Config.h>
#include <Soma/String.h>
#include <Soma/Vri.h>

#include <Soma/Visual/Mesh.h>


/* forward declarations */
namespace ColladaParser { class Geometry; }


namespace Soma {
namespace Formats {
namespace Collada {


	/**
	 * A mesh resource loaded from a Collada document.
	 * 
	 * This class is basically a Soma resource for a 'geometry' field found
	 * in the Collada document.
	 */
	class SOMA_API Mesh : public Visual::Mesh
	{
	public:
		/**
		 * Constructor.
		 * @param vri The path to the resource within an asset.
		 */
		explicit Mesh (const Vri& vri);
		
		/**
		 * Empty constructor.
		 */
		Mesh ();
		
		/**
		 * Destructor.
		 */
		virtual ~Mesh ();
		
		
		
		/**
		 * Bind the material symbol to the given material.
		 * 
		 * @param symbol The material symbol to bind to.
		 * @param material The material instance name to be used by the mesh.
		 */
		void bindMaterial (const String& symbol, const String& material);
		
		
		/**
		 * Load given Collada geometry.
		 * 
		 * This will create a mesh with the provided geometry which can then
		 * be used as a normal mesh resource throughout the engine.
		 * 
		 * @param geometry The geometry to manually load.
		 * @param name The name to associate with the created mesh resource.
		 */
		void loadGeometry (const ColladaParser::Geometry* geometry,
		                   const String& name);
		
		
	private:
		Vri mVri;
		Ogre::Mesh* mMesh;
		
		/* material list */
		std::map<String, String> mMaterials;
		
		
		/* create mesh resource */
		Ogre::Mesh* createMesh();
	};
	
	
	
	
	/**
	 * Collada mesh resource format.
	 * 
	 * Though most of the time a mesh resource from a Collada document will be
	 * added to the resource manager when the entire document is parsed, by
	 * creating a Collada mesh resource format we can allow a single geometry
	 * to be loaded from a document manually.
	 */
	struct SOMA_API MeshFormat : public Visual::MeshManager::Format
	{
		/**
		 * @copydoc Visual::MeshManager::Format::getExt
		 */
		String getExt() const { return ".dae"; }
		
		/**
		 * @copydoc Visual::MeshManager::Format::create
		 */
		Visual::Mesh* create (const Vri& vri) const { return new Mesh (vri); }
	};


}}}


#endif /* SOMA_FORMATS_COLLADA_MESH_H_ */
