
#ifndef SOMA_FILE_FORMAT_H_
#define SOMA_FILE_FORMAT_H_


namespace Soma
{
	namespace Formats
	{
	
		enum AssetFormat
		{
			ASSET = 0x01,
			ASSET_OBJECT,
			ASSET_PHYSICS,
			ASSET_MATERIAL,
			ASSET_LAST
		};
		
		
		enum MeshFormat
		{
			MESH = ASSET_LAST,
			MESH_NAME,
			
			MESH_VERTEX_SEMANTIC,
			MESH_VERTEX_SEMANTIC_POSITION,
			MESH_VERTEX_SEMANTIC_NORMAL,
			MESH_VERTEX_SEMANTIC_TEXCOORD,
			
			MESH_VERTEX_LIST,
			MESH_INDEX_LIST,
			
			
			MESH_SUBMESH,
			MESH_SUBMESH_MATERIAL,
			MESH_SUBMESH_SHARED_VERTICES
		};
		
		
		
		
		/* enum string conversion */
		static const char* toString (int id)
		{
			switch (id)
			{
				case ASSET:          return "ASSET";
				case ASSET_OBJECT:   return "ASSET_OBJECT";
				case ASSET_PHYSICS:  return "ASSET_PHYSICS";
				case ASSET_MATERIAL: return "ASSET_MATERIAL";
				
				case MESH:                          return "MESH";
				case MESH_VERTEX_SEMANTIC:          return "MESH_VERTEX_SEMANTIC";
				case MESH_VERTEX_SEMANTIC_POSITION: return "MESH_VERTEX_SEMANTIC_POSITION";
				case MESH_VERTEX_SEMANTIC_NORMAL:   return "MESH_VERTEX_SEMANTIC_NORMAL";
				case MESH_VERTEX_SEMANTIC_TEXCOORD: return "MESH_VERTEX_SEMANTIC_TEXCOORD";
				case MESH_VERTEX_LIST:              return "MESH_VERTEX_LIST";
				case MESH_INDEX_LIST:               return "MESH_INDEX_LIST";
				case MESH_SUBMESH:                  return "MESH_SUBMESH";
				case MESH_SUBMESH_MATERIAL:         return "MESH_SUBMESH_MATERIAL";
				case MESH_SUBMESH_SHARED_VERTICES:  return "MESH_SUBMESH_SHARED_VERTICES";
				
				default: return "UNKNOWN FORMAT ID";
			}
		}
	
	}
}


#endif /* SOMA_FILE_FORMAT_H_ */
