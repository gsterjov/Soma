/**
 * @defgroup components Entity Components.
 * 
 * Entity components can be added to any entity and will automatically
 * interact with other components within its context.
 * 
 * By adding components to entities we allow for functionality to be defined
 * in small objects which can then be applied to any object without tightly
 * coupling the specific entity to the plethora of possible component
 * combinations. As such the components are primarily data driven allowing
 * for even more loose coupling and flexibility in a 'game object'.
 */


#ifndef SOMA_COMPONENT_H_
#define SOMA_COMPONENT_H_


#include <map>

#include <Soma/Config.h>
#include <Soma/String.h>
#include <Soma/Value.h>
#include <Soma/Event.h>
#include <Soma/SmartPointer.h>


namespace Soma
{

	/* forward declarations */
	class Composite;
	struct CompositeMessage;
	
	
	
	/**
	 * The component interface.
	 * 
	 * A component is a small class implementing a specific functionality that
	 * can be reused and added to any composite without strongly coupling the
	 * functionality to a composite and allowing for any number of combinations 
	 * of other components within the composite, thus minimising overhead to a 
	 * minimum based on the functionality the composite requires.
	 * 
	 * @ingroup components
	 */
	class SOMA_API Component : public ReferenceCounter
	{
	public:
		/**
		 * Component OwnerChanged event args.
		 */
		struct OwnerChangedEventArgs
		{
			weak_ptr<Composite> oldOwner;
			weak_ptr<Composite> newOwner;
		};
		
		
		/**
		 * Destructor.
		 */
		virtual ~Component () {}
		
		
		
		/**
		 * Get the type of the component.
		 * @return The component type.
		 */
		virtual const String& getType() const = 0;
		
		
		/**
		 * Get the component owner.
		 * @return The owner if owned, NULL reference otherwise.
		 */
		virtual weak_ptr<Composite> getOwner() const = 0;
		
		
		/**
		 * Set the owner of this component.
		 * @param owner The composite to own the component.
		 */
		virtual void setOwner (const weak_ptr<Composite>& owner) = 0;
		
		
		
		/**
		 * Get a property value from the component.
		 * 
		 * @param name The name of the property.
		 * @return The value associated with the property.
		 */
		virtual Value getProperty (const String& name) const { return Value(); }
		
		/**
		 * Set a property value on the component.
		 * 
		 * @param name The name of the property to set.
		 * @param value The value to assign to the property.
		 */
		virtual void setProperty (const String& name, const Value& value) {}
		
		
		
		/**
		 * Load the component.
		 * @return True if loaded, false otherwise.
		 */
		virtual bool load () = 0;
		
		/**
		 * Reload the component.
		 * @return True if reloaded, false otherwise.
		 */
		virtual bool reload () = 0;
		
		/**
		 * Unload the component.
		 */
		virtual void unload () = 0;
		
		/**
		 * Is the component loaded?
		 * @return True if loaded, false otherwise.
		 */
		virtual bool isLoaded() const = 0;
		
		
		
		/**
		 * Stream out operator.
		 */
		friend std::ostream& operator<< (std::ostream& ostream,
		                                 const Component& ref)
		{
			ostream << "Component: " << ref.getType() << ", "
					<< "Loaded: " << Strings::convert (ref.isLoaded());
			return ostream;
		}
		
		
		
		/**
		 * Property value changed event.
		 * @param value The new property value.
		 */
		Event<void, const Value&> PropertyChanged;
		
		/**
		 * The component was loaded.
		 * @param component The loaded component.
		 */
		Event<void, const weak_ptr<Component>&> Loaded;
		
		/**
		 * The component was unloaded.
		 * @param component The unloaded component.
		 */
		Event<void, const weak_ptr<Component>&> Unloaded;
	};

}


#endif /* SOMA_COMPONENT_H_ */
