
#ifndef SOMA_EXCEPTION_H_
#define SOMA_EXCEPTION_H_


#include <stdexcept>

#include <Soma/Config.h>
#include <Soma/String.h>


namespace Soma {

	/**
	 * Convenience macro for throwing exceptions.
	 * @param subsystem The subsystem name the exception occurred in.
	 * @param message The exception message.
	 */
	#define SOMA_EXCEPTION(subsystem, message) \
		throw Soma::Exception (subsystem, message, __FILE__, __LINE__)
	
	
	
	/**
	 * The base exception class for the entire engine.
	 * 
	 * By having a custom exception class as the root of all exceptions
	 * thrown within the library we can ensure that all exceptions carry
	 * extra data which provides more details on the error that occurred.
	 * 
	 * It is important to keep in mind that allocations are made during the
	 * construction of the exception. As such this class should only be used
	 * when infrequent exceptions occur and when memory/stack overflow are not
	 * the issue being reported by the exception. This will ensure both
	 * performance and stability when using high level exceptions.
	 */
	class SOMA_API Exception : public std::exception
	{
	public:
		/**
		 * Constructor.
		 */
		Exception (const String& subsystem,
		           const String& message,
		           const String& source,
		           int line)
		: mSubsystem (subsystem),
		  mMessage (message),
		  mSource (source),
		  mLine (line)
		{
			mWhat = subsystem + ": " + message + " at line " +
					Strings::convert (line) + " in file '" + source + "'";
		}
		
		/**
		 * Destructor.
		 */
		virtual ~Exception () throw() {}
		
		
		/**
		 * The error message.
		 */
		const char* what() const throw() { return mWhat.c_str(); }
		
		
		/**
		 * The subsystem where the exception occurred.
		 */
		const String& subsystem() const throw() { return mSubsystem; }
		
		/**
		 * The exception message.
		 */
		const String& message() const throw() { return mMessage; }
		
		/**
		 * The source file where the exception occurred.
		 */
		const String& source() const throw() { return mSource; }
		
		/**
		 * The line number where the exception occurred.
		 */
		int line() const throw() { return mLine; }
		
		
	private:
		String mWhat;
		
		String mSubsystem;
		String mMessage;
		String mSource;
		int mLine;
	};

}



#endif /* SOMA_EXCEPTION_H_ */
