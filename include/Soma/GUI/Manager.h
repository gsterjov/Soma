
#ifndef SOMA_GUI_MANAGER_H_
#define SOMA_GUI_MANAGER_H_


#include <map>

#include <Soma/Config.h>
#include <Soma/Singleton.h>
#include <Soma/SubSystem.h>
#include <Soma/Configurable.h>
#include <Soma/SmartPointer.h>


/* forward declarations */
namespace Gorilla { class Silverback; }


namespace Soma {

/* forward declarations */
namespace Visual { class Viewport; }


namespace GUI {

	/* forward declarations */
	class Screen;
	
	
	
	/**
	 * Graphical user interface manager.
	 */
	class SOMA_API Manager : public Singleton<Manager>,
	                         public SubSystem,
	                         public Configurable
	{
	public:
		/**
		 * Constructor.
		 */
		Manager ();
		
		/**
		 * Destructor.
		 */
		~Manager ();
		
		
		/**
		 * @copydoc SubSystem::initialise
		 */
		bool initialise ();
		
		/**
		 * @copydoc SubSystem::reinitialise
		 */
		bool reinitialise ();
		
		/**
		 * @copydoc SubSystem::shutdown
		 */
		void shutdown ();
		
		/**
		 * @copydoc SubSystem::isInitialised
		 */
		bool isInitialised() { return mInitialised; }
		
		
		
		/**
		 * Create a GUI screen on the specified viewport.
		 * 
		 * @param name The name to identify the screen by.
		 * @param atlas The GUI atlas to use while rendering.
		 * @param viewport The viewport to render the screen on.
		 * 
		 * @return A newly created screen.
		 */
		smart_ptr<Screen> createScreen (const String name,
		                                const String atlas,
		                                const smart_ptr<Visual::Viewport>& viewport);
		
		
		
		/**
		 * @copydoc Configurable::save
		 */
		bool save (ConfigInterface& config);
		
		/**
		 * @copydoc Configurable::restore
		 */
		bool restore (const ConfigInterface& config);
		
		
		
		/**
		 * Get the underlying Gorilla silverback instance.
		 * @return The Gorilla silverback instance.
		 */
		Gorilla::Silverback* getGorilla() { return mGorilla; }
		
		
		
		/** Log source name. */
		static const String LogSource;
		
		
		
	private:
		bool mInitialised;
		
		/* gui root */
		Gorilla::Silverback* mGorilla;
		
		
		/* associated map of screens */
		typedef std::map<String, smart_ptr<Screen> > ScreenMap;
		ScreenMap mScreens;
	};

}}


#endif /* SOMA_GUI_MANAGER_H_ */
