
#ifndef SOMA_GUI_LABEL_H_
#define SOMA_GUI_LABEL_H_


#include <Soma/Config.h>
#include <Soma/String.h>

#include <Soma/GUI/Widget.h>


/* forward declarations */
namespace Gorilla { class Caption; }


namespace Soma {
namespace GUI {

	/* forward declarations */
	class Layer;
	
	
	
	/**
	 * A label widget displaying text.
	 */
	class SOMA_API Label : public Widget
	{
	public:
		/**
		 * Constructor.
		 */
		Label (const String& name, const smart_ptr<Layer>& layer);
		
		/**
		 * Destructor.
		 */
		virtual ~Label ();
		
		
		/**
		 * Get the text of the label.
		 * @return The label text.
		 */
		String getText() const;
		
		/**
		 * Set the text of the label.
		 * @param text The text to show on the label.
		 */
		void setText (const String& text);
		
		
		
		/**
		 * @copydoc Widget::setPosition
		 */
		void setPosition (float left, float top);
		
		/**
		 * @copydoc Widget::setLeft
		 */
		void setLeft (float left);
		
		/**
		 * @copydoc Widget::setTop
		 */
		void setTop (float top);
		
		/**
		 * @copydoc Widget::setWidth
		 */
		void setWidth (float width);
		
		/**
		 * @copydoc Widget::setHeight
		 */
		void setHeight (float height);
		
		
		
		/** Log source name. */
		static const String LogSource;
		
		
		
	private:
		Gorilla::Caption* mCaption;
	};

}}


#endif /* SOMA_GUI_LABEL_H_ */
