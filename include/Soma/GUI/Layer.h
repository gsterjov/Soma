
#ifndef SOMA_GUI_LAYER_H_
#define SOMA_GUI_LAYER_H_


#include <Soma/Config.h>
#include <Soma/String.h>
#include <Soma/SmartPointer.h>


/* forward declarations */
namespace Gorilla { class Layer; }


namespace Soma {
namespace GUI {

	/* forward declarations */
	class Screen;
	
	
	
	/* TODO: Layers would best benefit by being a composite so that its gui
	 * elements would be defined as components and can be used with extreme
	 * flexibility.
	 * Not only does this allow it to use any complex created
	 * element but it also allows it to be used with other subsystems such
	 * as placing an element within a 3d scene graph and have it altered by
	 * a visual scene node. */
	
	/**
	 * A GUI layer.
	 */
	class SOMA_API Layer : public ReferenceCounter
	{
	public:
		/**
		 * Constructor.
		 * 
		 * @param The name of the layer.
		 * @param The underlying Gorilla layer to use.
		 */
		Layer (const String& name, Gorilla::Layer* layer);
		
		/**
		 * Destructor.
		 */
		~Layer ();
		
		
		/**
		 * Get the name of the layer.
		 * @return The layer name.
		 */
		const String& getName() const { return mName; }
		
		
		
		/**
		 * Get the underlying Gorilla layer instance.
		 * @return The Gorilla layer instance.
		 */
		Gorilla::Layer* getLayer() { return mLayer; }
		
		
		
		/** Log source name. */
		static const String LogSource;
		
		
		
	private:
		String mName;
		Gorilla::Layer* mLayer;
	};
	
}}


#endif /* SOMA_GUI_LAYER_H_ */
