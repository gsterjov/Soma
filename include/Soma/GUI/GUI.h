
#ifndef SOMA_GUI_H_
#define SOMA_GUI_H_


#include <Soma/GUI/Manager.h>
#include <Soma/GUI/Screen.h>
#include <Soma/GUI/Layer.h>

#include <Soma/GUI/Widget.h>
#include <Soma/GUI/Label.h>


#endif /* SOMA_GUI_H_ */
