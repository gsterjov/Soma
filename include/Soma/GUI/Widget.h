
#ifndef SOMA_GUI_WIDGET_H_
#define SOMA_GUI_WIDGET_H_


#include <Soma/Config.h>
#include <Soma/String.h>
#include <Soma/SmartPointer.h>


namespace Soma {
namespace GUI {

	/**
	 * A GUI widget.
	 */
	class SOMA_API Widget : public ReferenceCounter
	{
	public:
		/**
		 * Constructor.
		 * @param name The name of the widget.
		 */
		Widget (const String& name) : mName(name) {}
		
		/**
		 * Destructor.
		 */
		virtual ~Widget () {}
		
		
		/**
		 * Get the name of the widget.
		 * @return The widget name.
		 */
		const String& getName() const { return mName; }
		
		
		/**
		 * Set the position of the widget.
		 * @param left The distance from the absolute left of the screen.
		 * @param top The distance from the absolute top of the screen.
		 */
		virtual void setPosition (float left, float top) = 0;
		
		/**
		 * Set the left position of the widget.
		 * @param left The distance from the absolute left of the screen.
		 */
		virtual void setLeft (float left) = 0;
		
		/**
		 * Set the top position of the widget.
		 * @param top The distance from the absolute top of the screen.
		 */
		virtual void setTop (float top) = 0;
		
		/**
		 * Set the width of the widget.
		 * @param width The widget width.
		 */
		virtual void setWidth (float width) = 0;
		
		/**
		 * Set the height of the widget.
		 * @param height The widget height.
		 */
		virtual void setHeight (float height) = 0;
		
		
		
	protected:
		/** The widget name. */
		String mName;
	};

}}


#endif /* SOMA_GUI_WIDGET_H_ */
