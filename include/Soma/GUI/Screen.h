
#ifndef SOMA_GUI_SCREEN_H_
#define SOMA_GUI_SCREEN_H_


#include <map>

#include <Soma/Config.h>
#include <Soma/String.h>
#include <Soma/SmartPointer.h>


/* forward declarations */
namespace Gorilla { class Screen; }


namespace Soma {

/* forward declarations */
namespace Visual { class Viewport; }


namespace GUI {

	/* forward declarations */
	class Layer;
	
	
	
	/**
	 * A GUI screen.
	 */
	class SOMA_API Screen : public ReferenceCounter
	{
	public:
		/**
		 * Constructor.
		 * 
		 * @param name The name to identify the screen by.
		 * @param atlas The GUI atlas to use while rendering.
		 * @param viewport The viewport to render the screen on.
		 */
		Screen (const String& name,
		        const String& atlas,
		        const smart_ptr<Visual::Viewport>& viewport);
		
		/**
		 * Destructor.
		 */
		~Screen ();
		
		
		
		/**
		 * Create a GUI layer on the specified viewport.
		 * 
		 * @param name The name to identify the layer by.
		 * @param index The index of the layer within the screen.
		 * 
		 * @return A newly created layer.
		 */
		smart_ptr<Layer> createLayer (const String& name, int index);
		
		
		
		/**
		 * Get the underlying Gorilla screen instance.
		 * @return The Gorilla lcreen instance.
		 */
		Gorilla::Screen* getScreen() { return mScreen; }
		
		
		
		/** Log source name. */
		static const String LogSource;
		
		
		
	private:
		String mName;
		Gorilla::Screen* mScreen;
		
		
		/* associated map of layers */
		typedef std::map<String, smart_ptr<Layer> > LayerMap;
		LayerMap mLayers;
	};

}}


#endif /* SOMA_GUI_SCREEN_H_ */
