
#ifndef SOMA_RESOURCE_MANAGER_H_
#define SOMA_RESOURCE_MANAGER_H_


#include <map>

#include <Soma/Config.h>
#include <Soma/String.h>
#include <Soma/Log.h>
#include <Soma/Singleton.h>
#include <Soma/SmartPointer.h>
#include <Soma/Vri.h>
#include <Soma/Registry.h>

#include <Soma/FileSystem/Manager.h>
#include <Soma/FileSystem/File.h>


namespace Soma
{

	/**
	 * Generic Resource Manager.
	 * 
	 * A resource manager is responsible for finding and loading resources
	 * of a specific type. This is the best way to access engine resources
	 * as it is possible to deploy specific strategies and/or policies
	 * regarding resource loading having the potential to greatly reduce
	 * the cost of loading resources on demand.
	 */
	template <typename ResourceType>
	class SOMA_API ResourceManager : public Singleton <ResourceManager<ResourceType> >
	{
	public:
		/**
		 * Resource format structure.
		 * 
		 * The ResourceManager Format class allows various formats to be used
		 * together without needing to worry about how its loaded or how
		 * its going to be used. This allows for a resource of one format
		 * to be loaded according to its resource type and another to be
		 * loaded in the same way, all within the same ResourceManager.
		 */
		struct SOMA_API Format
		{
			/**
			 * Get the file extension this resource format handles.
			 * @return The supported file extension.
			 */
			virtual String getExt() const = 0;
			
			/**
			 * Create a resource located at the specified path.
			 * 
			 * @param vri The path to the resource within an asset.
			 * @return The newly created but unloaded resource.
			 */
			virtual ResourceType* create (const Vri& vri) const = 0;
		};
		
		
		
		/**
		 * Constructor.
		 * 
		 * This will automatically add all formats that have been registered
		 * at the format registry for this particular resource manager type.
		 */
		ResourceManager ();
		
		
		/**
		 * Get the resource located at the specified path.
		 * 
		 * If the resource has not yet been created it will create one according
		 * to the asset format and store it so that future retrieval of the
		 * resource can simply return the same instance thus saving time on
		 * resource construction and destruction.
		 * 
		 * @param vri The path to the resource within an asset.
		 * @return The desired resource instance.
		 */
		smart_ptr<ResourceType> get (const Vri& vri);
		
		
		/**
		 * Looks for the resource with the given virtual resource identifier.
		 * 
		 * Unlike get() this method only looks for the resource and returns it
		 * if found. It will <b>not</b> create the resource and return it. It
		 * primarily serves as a utility to help resources determine if one
		 * should be created manually or simply retrieved from the store.
		 * 
		 * However, there is no reason why it cannot be used to simply determine
		 * if a resource exists within the resource manager store.
		 * 
		 * @param vri The path to the resource within an asset.
		 * @return The desired resource if found, NULL reference otherwise.
		 */
		smart_ptr<ResourceType> find (const Vri& vri) const;
		
		
		
		/**
		 * Manual add a resource to the store which can then be used as if it
		 * was created by the resource manager itself.
		 * 
		 * @param vri The identifier to associate the resource with.
		 * @param resource The manual resource to add.
		 */
		void add (const Vri& vri, const smart_ptr<ResourceType>& resource);
		
		
		/**
		 * Add a ResourceManager Format to the list of supported resource
		 * formats. The format will then allow the ResourceManager to create
		 * resources of the specified format type.
		 * 
		 * @param format The resource format to add.
		 */
		void addFormat (const Format* format);
		
		
	private:
		/* map types */
		typedef std::map<String, const Format*> FormatMap;
		typedef std::map<String, smart_ptr<ResourceType> > ResourceMap;
		
		FormatMap mFormats;
		ResourceMap mResources;
	};
	
	
	
	
	
	
	
	/* constructor */
	template <typename ResourceType>
	ResourceManager<ResourceType>::ResourceManager ()
	{
		typedef Registry<ResourceManager<ResourceType>::Format> FormatRegistry;
		
		/* get all registered format types */
		typename FormatRegistry::TypeMap::const_iterator iter;
		typename FormatRegistry::TypeMap types = FormatRegistry::getTypes();
		
		/* create and add the format handler */
		for (iter = types.begin(); iter != types.end(); ++iter)
			addFormat (iter->second->create());
	}
	
	
	
	
	/* retrieve the resource instance */
	template <class ResourceType>
	smart_ptr<ResourceType> ResourceManager<ResourceType>::get (const Vri& vri)
	{
		/* get already created resource */
		smart_ptr<ResourceType> resource = mResources[vri.path];
		
		/* create new resource */
		if (!resource)
		{
			/* get resource extension */
			String ext = vri.asset.substr (vri.asset.find_last_of ("."));
			
			
			/* get format handler */
			const Format* format = mFormats[ext];
			
			/* resource format not supported */
			if (!format)
			{
				Log::Error ("ResourceManager", "Cannot find a format capable "
						"of handling '" + ext + "' files.");
				
				return resource;
			}
			
			
			/* determine full path */
			FileSystem::Manager* man = FileSystem::Manager::instancePtr();
			FileSystem::File* file = man->findFile (vri.asset);
			
			/* cannot find file */
			if (!file)
			{
				Log::Error ("ResourceManager", "Cannot find the asset '" +
						vri.asset + "' within the filesystem mounts. Make sure "
						"the root directory of the asset is mounted");
				
				return resource;
			}
			
			
			/* create new resource */
			resource = format->create (file->getPath() + "#" + vri.resource);
			mResources[vri.path] = resource;
		}
		
		
		return resource;
	}
	
	
	
	
	/* look for the resource instance */
	template <class ResourceType>
	smart_ptr<ResourceType> ResourceManager<ResourceType>::find (const Vri& vri) const
	{
		typename ResourceMap::const_iterator iter = mResources.find (vri.path);
		return iter != mResources.end() ? iter->second : smart_ptr<ResourceType>();
	}
	
	
	
	
	/* add a manual resource */
	template <typename ResourceType>
	void ResourceManager<ResourceType>::add (const Vri& vri,
	                                         const smart_ptr<ResourceType>& resource)
	{
		mResources[vri.path] = resource;
	}
	
	
	
	/* add a resource format */
	template <typename ResourceType>
	void ResourceManager<ResourceType>::addFormat (const Format* format)
	{
		mFormats[format->getExt()] = format;
	}

}


#endif /* SOMA_RESOURCE_MANAGER_H_ */
