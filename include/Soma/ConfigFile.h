
#ifndef SOMA_CONFIG_FILE_H_
#define SOMA_CONFIG_FILE_H_


#include <map>

#include <Soma/Config.h>
#include <Soma/Types.h>
#include <Soma/String.h>
#include <Soma/ConfigInterface.h>


namespace Soma
{

	/**
	 * Configuration persistence class.
	 * 
	 * The ConfigFile class reads and writes config files with a readable
	 * syntax and provides functionality to easily retrieve the configuration
	 * settings, though it is not limited to configuration alone. At heart
	 * its a key-value serializer.
	 * 
	 * The ConfigFile syntax allows settings to be placed into groups which
	 * means the name of a setting will be the group and the option
	 * concatenated by a ".", for example "graphics.fullscreen" retrieves
	 * the "fullscreen" setting in the "graphics" group.
	 * 
	 * The syntax of a config file is designed to be easily read and modified
	 * by any text document. A typical config file looks like this:
	 * 
	 * @code
	 * graphics
	 * {
	 * 		system "OpenGL"
	 * 		fullscreen "false"
	 * 		display_frequency "60"
	 * }
	 * @endcode
	 */
	class SOMA_API ConfigFile : public ConfigInterface
	{
	public:
		
		/**
		 * Constructor.
		 * Sets the configuration file to use.
		 * 
		 * @param file Either a relative of absolute path to the config file.
		 */
		explicit ConfigFile (const String& file);
		
		/**
		 * Empty constructor.
		 */
		ConfigFile ();
		
		/**
		 * Destructor.
		 */
		virtual ~ConfigFile ();
		
		
		/**
		 * Writes the current configuration to the specified output file.
		 * @return True if successful, false otherwise.
		 */
		bool save ();
		
		/**
		 * Writes the current configuration to the specified output stream.
		 * 
		 * @param stream The stream to write the configuration to.
		 * @return True if successful, false otherwise.
		 */
		bool save (std::ostream& stream);
		
		/**
		 * Reads the configuration file and caches the values within.
		 * @return True if successful, false otherwise.
		 */
		bool restore ();
		
		/**
		 * Reads the configuration file stream and caches the values within.
		 * 
		 * @param stream The stream to read the configuration file from.
		 * @return True if successful, false otherwise.
		 */
		bool restore (std::istream& stream);
		
		
		
		/* ConfigInterface implementation */
		bool exists (const String& name) const;
		
		int    get (const String& name, int           defaultValue) const;
		uint   get (const String& name, uint          defaultValue) const;
		bool   get (const String& name, bool          defaultValue) const;
		float  get (const String& name, float         defaultValue) const;
		String get (const String& name, const char*   defaultValue) const;
		String get (const String& name, const String& defaultValue) const;
		
		bool set (const String& name, int           value);
		bool set (const String& name, uint          value);
		bool set (const String& name, bool          value);
		bool set (const String& name, float         value);
		bool set (const String& name, const char*   value);
		bool set (const String& name, const String& value);
		
		
		
	private:
		String mFile;
		std::map<String, String> mContents;
		
		
		/**
		 * Parses the specified string loading the key-value pairs into a map.
		 * @param buf The contents of the loaded config file.
		 */
		void parse (const String& buf);
	};

}


#endif /* SOMA_CONFIG_FILE_H_ */
