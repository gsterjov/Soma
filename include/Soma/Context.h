
#ifndef SOMA_CONTEXT_H_
#define SOMA_CONTEXT_H_


#include <Soma/Config.h>
#include <Soma/String.h>
#include <Soma/Composite.h>


namespace Soma
{

	/**
	 * Context interface.
	 * 
	 * A context allows subsystems to group functionality by state. This is
	 * needed, for example, by the input and graphics subsystem to allow their
	 * components to act on a specific context, which can be many. As such
	 * details like window handles for input catching and window/target
	 * creation are abstracted away into a single context. It is further
	 * possible to have two contexts working simultaneously without fear of
	 * corrupting data or resource locks. Indeed, it may even be possible to
	 * simple switch components from one context to another depending on the
	 * implementation of said context.
	 * 
	 * @ingroup components
	 */
	class SOMA_API Context : public Composite
	{
	public:
		/**
		 * Named constructor.
		 * @param name The name of the context.
		 */
		explicit Context (const String& name);
		
		/**
		 * Constructor with components.
		 * 
		 * Allows easy construction of a context by providing a list of
		 * components to add and load after creation.
		 * 
		 * @param name The name of the context.
		 * @param types The types of components to add separated by a comma.
		 */
		Context (const String& name, const String& types);
		
		/**
		 * Destructor.
		 */
		~Context ();
		
		
		/**
		 * Get the name of the context.
		 * @return The context name.
		 */
		const String& getName() const { return mName; }
		
		
		
		/** Log source name. */
		static const String LogSource;
		
		
		
	private:
		String mName;
	};

}


#endif /* SOMA_CONTEXT_H_ */
