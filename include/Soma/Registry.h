
#ifndef SOMA_REGISTRY_H_
#define SOMA_REGISTRY_H_


#include <map>

#include <Soma/Config.h>
#include <Soma/String.h>
#include <Soma/Singleton.h>


namespace Soma
{

	/**
	 * Convenience macro to register a type with a specific registry.
	 * 
	 * @param baseType The registry type the type is compatible with.
	 * @param type The type to register.
	 * @param name The name to associate the type with.
	 */
	#define REGISTER_TYPE(baseType, type, name) \
		static RegistryType<baseType, type> Type_##type (name);
	
	
	
	/**
	 * Generic registry class.
	 * 
	 * The registry class is used to hold and retrieve systems/components
	 * in a loosely coupled manner. As such specific objects can be retrieved
	 * by a unique name alone and its memory is managed by the specific type
	 * which registers itself to the registry class.
	 */
	template <typename T>
	class SOMA_API Registry
	{
	public:
		/**
		 * Generic registry type class.
		 * 
		 * A registry type is ultimately a destriptor for a specific kind of
		 * type within a registry. It allows for simple association of types
		 * with data (such as type name) as well as a generic way to create and
		 * destroy types without knowledge of the type's information. As such
		 * all types must fit the type requirements of the registry holding it.
		 */
		struct SOMA_API Type
		{
			/**
			 * Get the name of the registry type.
			 * @return The registry type name.
			 */
			virtual const String& getName() const = 0;
			
			
			/**
			 * Creates an instance of the registry type.
			 * @return A newly created registry type.
			 */
			virtual T* create () const = 0;
			
			
			/**
			 * Destroys an instance of the registry type.
			 * @param instance The registry type to destroy.
			 */
			virtual void destroy (T* instance) const = 0;
		};
		
		
		
		/**
		 * A list of registered types.
		 */
		typedef std::map<String, Type*> TypeMap;
		
		
		/**
		 * Get all the registry types.
		 * @return The registry's type map.
		 */
		static TypeMap& getTypes()
		{
			static TypeMap types;
			return types;
		}
		
		
		/**
		 * Add registry type.
		 * @param type The registry type to add.
		 */
		static void add (Type* type)
		{
			getTypes()[type->getName()] = type;
		}
		
		/**
		 * Remove registry type.
		 * @param name The name of the registry type to remove.
		 */
		static void remove (const String& name)
		{
			getTypes().erase (name);
		}
		
		
		/**
		 * Find a specific registry type.
		 * 
		 * @param name The name of the registry type to look for.
		 * @return The registry type if found, NULL otherwise.
		 */
		static Type* find (const String& name)
		{
			TypeMap& types = getTypes();
			typename TypeMap::const_iterator iter = types.find (name);
			
			/* return type if found */
			return iter != types.end() ? iter->second : 0;
		}
	};
	
	
	
	/**
	 * Registry type class.
	 * 
	 * This class encapsulates everything needed by the registry system in
	 * a single line of instantiation to facilitate automatic registration
	 * with the macro REGISTER_TYPE(). It associates the type T with the
	 * registry Registry<BaseType> upon creation. This is the recommended way
	 * to register types when not using the REGISTER_TYPE() macro as it cuts
	 * back on significant overhead in defining a type.
	 */
	template <typename BaseType, typename T>
	struct SOMA_API RegistryType : Registry<BaseType>::Type
	{
		/**
		 * Auto registering constructor.
		 * 
		 * The constructor automatically registers the type with the registry
		 * thus allowing the type to be added implicitly upon initialisation
		 * rather than a separate function call to register it.
		 * 
		 * This is especially useful when initialising the type as a static
		 * variable, thus registering the type before any code is actually
		 * executed. In this way we allow any type from any module to be
		 * visible to the engine.
		 * 
		 * @param name The name to associate with the registry type.
		 */
		RegistryType (const String& name) : mName(name)
		{
			Registry<BaseType>::add (this);
		}
		
		/**
		 * Destructor.
		 */
		virtual ~RegistryType ()
		{
			Registry<BaseType>::remove (mName);
		}
		
		
		/**
		 * @copydoc Registry<BaseType>::Type::getName
		 */
		const String& getName() const { return mName; }
		
		
		/**
		 * @copydoc Registry<BaseType>::Type::create
		 */
		BaseType* create () const { return new T(); }
		
		
		/**
		 * @copydoc Registry<BaseType>::Type::destroy
		 */
		void destroy (BaseType* instance) const { delete instance; }
		
		
	private:
		String mName;
	};

}


#endif /* SOMA_REGISTRY_H_ */
