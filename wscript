import os
import sys
sys.path.insert (0, ".")

from waflib import Utils
from waflib.Configure import ConfigurationContext
from waflib.Build import BuildContext


class ExtractBin (ConfigurationContext):
	'''extract dependencies into the build directory'''
	cmd = 'extract-bin'
	fun = 'extract_bin'


class Test (BuildContext):
	'''build and run the test suite'''
	cmd = 'test'
	fun = 'test'



top = "."
out = "build"
includes = "include"


def extract_bin (ctx):
	ctx.recurse ("libs")


def test (bld):
	bld.recurse ("test")


def options (opt):
	opt.load ("compiler_c")
	opt.load ("compiler_cxx")

	# hacky way to get os dependent options
	opt.__dict__["dest_os"] = Utils.unversioned_sys_platform()

	opt.recurse ("libs")
	opt.recurse ("src")


def configure (conf):
	# include source headers and generated headers
	src_inc_path = os.path.join (conf.path.abspath(), includes)
	bld_inc_path = os.path.join (conf.path.get_bld().abspath(), includes)

	conf.env.append_value ("CXXFLAGS", ['-fPIC'])
	conf.env.append_value ('INCLUDES', [src_inc_path, bld_inc_path])

	conf.load ("compiler_c")
	conf.load ("compiler_cxx")

	conf.recurse ("libs")
	conf.recurse ("src")


def build (bld):
	bld.recurse ("libs")
	bld.recurse ("src")