
#ifndef SOMA_MOCK_COMPONENT_H_
#define SOMA_MOCK_COMPONENT_H_


#include <gmock/gmock.h>
#include <Soma/Component.h>


namespace Soma {
namespace Test {


class MockComponent : public Component
{
public:
	MOCK_CONST_METHOD0 (getType,  const String&    ());
	MOCK_CONST_METHOD0 (getOwner, CompositeWeakRef ());
	
	MOCK_METHOD1 (setOwner, void (const CompositeWeakRef& owner));
	
	MOCK_CONST_METHOD1 (getProperty, Value (const String& name));
	MOCK_METHOD2       (setProperty, void  (const String& name, const Value& value));
};


}}


#endif /* SOMA_MOCK_COMPONENT_H_ */
