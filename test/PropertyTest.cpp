/*
 * PropertyTest.cpp
 *
 *  Created on: Jul 23, 2010
 *      Author: sterjov
 */

#include <gtest/gtest.h>
#include <Soma/Property.h>


namespace Soma {
namespace Test {


	class PropertyTestDummy
	{
	public:
		PropertyTestDummy() : val(0) {}
		int val;
		
		
		int getProp (int arg) { return val; }
		void setProp (int arg) { val = arg; }
	};
	
	
	
	class PropertyTest : public testing::Test
	{
	public:
		PropertyTest ()
		: mInt (mIntVal, 0),
		  mUInt (mUIntVal, 0),
		  mBool (mBoolVal, false),
		  mDummy (mDummyVal),
		  mDummyPtr (mDummyPtrVal, 0)
		{}
		
	protected:
		/* variables */
		Property<int> mInt;
		Property<uint> mUInt;
		Property<bool> mBool;
		
		Property<PropertyTestDummy> mDummy;
		Property<PropertyTestDummy*> mDummyPtr;
		
		
		
		int mIntVal;
		uint mUIntVal;
		bool mBoolVal;
		
		PropertyTestDummy mDummyVal;
		PropertyTestDummy* mDummyPtrVal;
	};
	
	
	
	
//	/* primitive property accessors */
//	TEST_F (PropertyTest, GetSetPrimitives)
//	{
//		/* set values */
//		mInt.set (1);
//		mUInt.set (1);
//		mBool.set (true);
//		
//		/* test stored property */
//		EXPECT_EQ (1, mInt.get());
//		EXPECT_EQ (1, mUInt.get());
//		EXPECT_EQ (true, mBool.get());
//	}
//	
//	
//	
//	
//	/* class property accessors */
//	TEST_F (PropertyTest, GetSetClasses)
//	{
//		PropertyTestDummy dummy;
//		
//		/* set dummy class to the property */
//		dummy.val = 1;
//		mDummy.set (dummy);
//		
//		/* sanity check */
//		EXPECT_EQ (1, mDummy.get().val);
//		
//		
//		/* change local dummy value */
//		dummy.val = 2;
//		
//		/* property value should neither be changed nor the address
//		 * match the local dummy since it should invoke the copy constructor
//		 * when we set it on the property */
//		EXPECT_NE (&dummy, &mDummy.get());
//		EXPECT_EQ (1, mDummy.get().val);
//		
//		
//		/* get dummy class, should invoke copy constructor */
//		PropertyTestDummy retDummy = mDummy.get();
//		
//		/* change returned dummy class value */
//		retDummy.val = 2;
//		
//		/* again, value shouldn't have changed nor the address be the same */
//		EXPECT_NE (&dummy, &mDummy.get());
//		EXPECT_EQ (1, mDummy.get().val);
//	}
//	
//	
//	
//	
//	/* pointer property accessors */
//	TEST_F (PropertyTest, GetSetPointers)
//	{
//		PropertyTestDummy* dummy = new PropertyTestDummy ();
//		
//		/* set dummy pointer to the property */
//		dummy->val = 1;
//		mDummyPtr.set (dummy);
//		
//		/* sanity check */
//		EXPECT_EQ (1, mDummyPtr.get()->val);
//		
//		
//		/* change local dummy value */
//		dummy->val = 2;
//		
//		/* dummy pointer should copy the pointer address and the value
//		 * should be changed to the local dummy version */
//		EXPECT_EQ (dummy, mDummyPtr.get());
//		EXPECT_EQ (2, mDummyPtr.get()->val);
//		
//		
//		/* get dummy pointer */
//		PropertyTestDummy* retDummy = mDummyPtr.get();
//		
//		/* change returned dummy pointer value */
//		retDummy->val = 3;
//		
//		/* again, value and address should be the same as local dummy */
//		EXPECT_EQ (dummy, mDummyPtr.get());
//		EXPECT_EQ (3, mDummyPtr.get()->val);
//		
//		
//		/* clean up */
//		delete dummy;
//	}
//	
//	
//	
//	
//	
//	/* primitive property accessing through operators */
//	TEST_F (PropertyTest, GetSetPrimitivesWithOperators)
//	{
//		/* set values through assignment operator */
//		mInt = 1;
//		mUInt = 1;
//		mBool = true;
//		
//		
//		/* test assignment operator */
//		EXPECT_EQ (1, mInt.get());
//		EXPECT_EQ (1, mUInt.get());
//		EXPECT_EQ (true, mBool.get());
//		
//		
//		/* test function operator */
//		EXPECT_EQ (1, mInt());
//		EXPECT_EQ (1, mUInt());
//		EXPECT_EQ (true, mBool());
//		
//		
//		/* test reference operator */
//		EXPECT_EQ (&mInt.get(), &mInt);
//		EXPECT_EQ (&mUInt.get(), &mUInt);
//		EXPECT_EQ (&mBool.get(), &mBool);
//		
//		
//		/* test dereference operator */
//		EXPECT_EQ (1, *mInt);
//		EXPECT_EQ (1, *mUInt);
//		EXPECT_EQ (true, *mBool);
//		
//		
//		/* test conversion operator */
//		EXPECT_EQ (1, mInt);
//		EXPECT_EQ (1, mUInt);
//		EXPECT_EQ (true, mBool);
//	}
//	
//	
//	
//	/* class property accessing through operators */
//	TEST_F (PropertyTest, GetSetClassesWithOperators)
//	{
//		PropertyTestDummy dummy;
//		
//		/* set dummy class to the property */
//		dummy.val = 1;
//		mDummy = dummy;
//		
//		
//		/* test assignment operator */
//		EXPECT_NE (&dummy, &mDummy.get());
//		EXPECT_EQ (1, mDummy.get().val);
//		
//		
//		/* change local dummy value */
//		dummy.val = 2;
//		
//		/* property value should neither be changed nor the address
//		 * match the local dummy since it should invoke the copy constructor
//		 * when we set it on the property */
//		
//		/* test function operator */
//		EXPECT_EQ (1, mDummy().val);
//		
//		
//		/* test reference and dereference operator */
//		EXPECT_EQ (&mDummy.get(), &mDummy);
//		EXPECT_EQ (1, (*mDummy).val);
//		
//		/* test indirection operator */
//		EXPECT_EQ (1, mDummy->val);
//		
//		
//		/* test conversion operator */
//		PropertyTestDummy convertedDummy = mDummy;
//		EXPECT_NE (&convertedDummy, &mDummy.get());
//		EXPECT_EQ (1, convertedDummy.val);
//	}
//	
//	
//	
//	/* pointer property accessing through operators */
//	TEST_F (PropertyTest, GetSetPointersWithOperators)
//	{
//		PropertyTestDummy* dummy = new PropertyTestDummy ();
//		
//		/* set dummy class to the property */
//		dummy->val = 1;
//		mDummyPtr = dummy;
//		
//		
//		/* test assignment operator */
//		EXPECT_EQ (dummy, mDummyPtr.get());
//		EXPECT_EQ (1, mDummyPtr.get()->val);
//		
//		
//		/* change local dummy value */
//		dummy->val = 2;
//		
//		/* dummy pointer should copy the pointer address and the value
//		 * should be changed to the local dummy version */
//		
//		/* test function operator */
//		EXPECT_EQ (2, mDummyPtr()->val);
//		
//		
//		/* test reference and dereference operator */
//		EXPECT_EQ (&mDummyPtr.get(), &mDummyPtr);
//		EXPECT_EQ (mDummyPtr.get(), *mDummyPtr);
//		
//		/* test indirection operator */
//		EXPECT_EQ (2, (*mDummyPtr)->val);
//		
//		
//		/* test conversion operator */
//		PropertyTestDummy* convertedDummy = mDummyPtr;
//		EXPECT_EQ (convertedDummy, mDummyPtr.get());
//		
//		
//		/* clean up */
//		delete dummy;
//	}
	
	
	
	
	TEST_F (PropertyTest, AbstractReadOnly)
	{
		PropertyTestDummy dummy;
		Delegate<PropertyTestDummy, int, int> del1 (&dummy, &PropertyTestDummy::getProp);
		Delegate<PropertyTestDummy, void, int> del2 (&dummy, &PropertyTestDummy::setProp);
		
		AbstractProperty<int, ReadWrite> prop1 (&del1, &del2);
		
		prop1.set (6);
		EXPECT_EQ (6, prop1.get());
	}


}}
