
#ifndef SOMA_MOCK_CONFIG_H_
#define SOMA_MOCK_CONFIG_H_


#include <gmock/gmock.h>


namespace Soma {
namespace Test {


class MockConfig : public ConfigInterface
{
	MOCK_METHOD0 (save, bool ());
	MOCK_METHOD0 (restore, bool ());
	
	MOCK_CONST_METHOD1 (exists, bool (const String& name));
	
	MOCK_CONST_METHOD2 (get, int    (const String& name, int defaultValue));
	MOCK_CONST_METHOD2 (get, uint   (const String& name, uint defaultValue));
	MOCK_CONST_METHOD2 (get, bool   (const String& name, bool defaultValue));
	MOCK_CONST_METHOD2 (get, float  (const String& name, float defaultValue));
	MOCK_CONST_METHOD2 (get, String (const String& name, const char* defaultValue));
	MOCK_CONST_METHOD2 (get, String (const String& name, const String& defaultValue));
	
	MOCK_METHOD2 (set, bool (const String& name, int value));
	MOCK_METHOD2 (set, bool (const String& name, uint value));
	MOCK_METHOD2 (set, bool (const String& name, bool value));
	MOCK_METHOD2 (set, bool (const String& name, float value));
	MOCK_METHOD2 (set, bool (const String& name, const char* value));
	MOCK_METHOD2 (set, bool (const String& name, const String& value));
};

}}


#endif /* SOMA_MOCK_CONFIG_H_ */
