
#ifndef RUNTIME_TEST_H_
#define RUNTIME_TEST_H_


#include <Soma/Soma.h>


/**
 * Base class for runtime tests.
 * Handles engine initialisation and test framework setup.
 */
class RuntimeTest
{
public:
	/**
	 * Constructor.
	 */
	RuntimeTest ();
	
	/**
	 * Destructor.
	 */
	virtual ~RuntimeTest ();
	
	
	/**
	 * Initialise the runtime test.
	 */
	void initialise ();
	
	/**
	 * Start the runtime main loop.
	 */
	void run ();
	
	
	
protected:
	/**
	 * The engine.
	 */
	Soma::Engine* mEngine;
	
	/**
	 * The main engine context to use.
	 */
	Soma::Context* mContext;
	
	/**
	 * The main world to use.
	 */
	Soma::World* mWorld;
	
	
	/**
	 * Setup the test environment.
	 */
	virtual void setup () {}
	
	/**
	 * Tear down the test environment.
	 */
	virtual void teardown () {}
	
	
	
	/**
	 * Engine loop tick.
	 */
	virtual void onTick (const Soma::TickEventArgs& args) {}
};



#endif /* RUNTIME_TEST_H_ */
