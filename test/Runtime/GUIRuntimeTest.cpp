
#include "RuntimeTest.h"

#include <Ogre.h>



class GUIRuntimeTest : public RuntimeTest
{
private:
	void setup ();
	void onTick (const Soma::TickEventArgs& args);
	
	
	/* entities */
	Soma::smart_ptr<Soma::Entity> mCamera;
	Soma::smart_ptr<Soma::AssetEntity> mObject;
	
	/* fps */
	Soma::smart_ptr<Soma::GUI::Label> mFPS;
	
	
	float mCount;
	Ogre::RenderTarget* mTarget;
};




/* setup environment */
void GUIRuntimeTest::setup ()
{
	using namespace Soma;
	
	
	mContext->addComponents ("context.rendertarget.window,"
	                         "context.viewport");
	
	mWorld->addComponents ("world.visual");
	
	
	/* create camera entity */
	mCamera = new Entity ("Camera", "entity.visual.node, entity.visual.camera");
	mCamera->setPosition (0, 0, 5);
	
	
	/* create an entity to control */
	mObject = new AssetEntity ("Object", "cube.dae");
	
	
	mWorld->add (mObject);
	mWorld->add (mCamera);
	
	mContext->setProperty ("context.viewport", "camera-entity", mCamera);
	mCamera->setProperty ("entity.visual.camera", "near-clip-distance", 0.1f);
	mCamera->setProperty ("entity.visual.camera", "look-at", Vector::ZERO);
	
	
	
	Ogre::ResourceGroupManager::getSingleton().addResourceLocation ("/devel/data", "FileSystem");
	Ogre::ResourceGroupManager::getSingleton().initialiseAllResourceGroups();
	
	
	smart_ptr<Component> component = mContext->getComponent ("context.viewport");
	smart_ptr<Visual::Viewport> viewport = ref_cast<Visual::Viewport> (component);
	
	
	
	/* create gui */
	smart_ptr<GUI::Screen> screen = GUI::Manager::instance().createScreen ("fps", "dejavu", viewport);
	smart_ptr<GUI::Layer> layer = screen->createLayer ("layer", 0);
	
	mFPS = new GUI::Label ("fps_label", layer);
	mFPS->setPosition (10, viewport->getViewport()->getActualHeight() - 25);
	
	
	
	smart_ptr<Component> com2 = mContext->getComponent ("context.rendertarget");
	smart_ptr<Visual::RenderTarget> target = ref_cast<Visual::RenderTarget> (com2);
	mTarget = target->getTarget();
}




/* game tick */
void GUIRuntimeTest::onTick (const Soma::TickEventArgs& args)
{
	mCount += args.elapsed;
	
	if (mCount > 1)
	{
		std::stringstream stream;
		stream << "FPS: " << mTarget->getLastFPS() << "\n";
		
		mFPS->setText (stream.str());
		mCount = 0;
	}
}





/* program entry */
int main (int argc, char** argv)
{
	GUIRuntimeTest test;
	
	test.initialise();
	test.run();
}
