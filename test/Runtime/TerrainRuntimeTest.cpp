
#include "RuntimeTest.h"

#include <Ogre.h>



class TerrainRuntimeTest : public RuntimeTest
{
private:
	void setup ();
	void onTick (const Soma::TickEventArgs& args);
	
	void bind ();
	
	
	void onActivated (const Soma::weak_ptr<Soma::Input::Action>& action);
	
	
	/* entities */
	Soma::smart_ptr<Soma::Entity> mCamera;
	Soma::smart_ptr<Soma::Entity> mLight;
	Soma::smart_ptr<Soma::AssetEntity> mObject;
	
	
	/* bindings */
	Soma::Input::TriggerAction mQuit;
	Soma::Input::TriggerAction mForward;
	Soma::Input::TriggerAction mBack;
	Soma::Input::TriggerAction mLeft;
	Soma::Input::TriggerAction mRight;
};




/* setup environment */
void TerrainRuntimeTest::setup ()
{
	using namespace Soma;
	
	
	mContext->addComponents ("context.rendertarget.window,"
	                         "context.viewport,"
	                         "context.input");
	
	mWorld->addComponents ("world.visual, world.visual.terrain");
	
	
	/* create camera entity */
	mCamera = new Entity ("Camera", "entity.visual.node, entity.visual.camera");
	mCamera->setPosition (1683, 50, 2116);
	
	
	/* create an entity to control */
	mObject = new AssetEntity ("Object", "cube.dae");
	
	
	mWorld->add (mObject);
	mWorld->add (mCamera);
	
	mContext->setProperty ("context.viewport", "camera-entity", mCamera);
	mCamera->setProperty ("entity.visual.camera", "near-clip-distance", 0.1f);
	mCamera->setProperty ("entity.visual.camera", "look-at", Vector::ZERO);
//	mCamera->setProperty ("entity.visual.camera", "look-at", Vector(1963, 50, 1660));
	
	smart_ptr<Component> com = mCamera->getComponent("entity.visual.camera");
	smart_ptr<Visual::Camera> cam = ref_cast<Visual::Camera> (com);
	
	cam->getCamera()->setFarClipDistance (50000);
	
	
	bind ();
	
	
	
	mWorld->setProperty ("world.visual", "ambient-light", Colour(0.2, 0.2, 0.2));
	
	/* create directional light */
	mLight = new Entity ("Light", "entity.visual.node, entity.visual.light");
	
	mWorld->add (mLight);
	
	mLight->setProperty ("entity.visual.light", "type", String("directional"));
	mLight->setProperty ("entity.visual.light", "direction", Vector(0.55, -0.3, -0.75));
	
	
	
	
	Ogre::ResourceGroupManager::getSingleton().addResourceLocation ("/devel/data", "FileSystem");
	Ogre::ResourceGroupManager::getSingleton().initialiseAllResourceGroups();
	
	
	com = mWorld->getComponent ("world.visual.terrain");
	smart_ptr<Visual::Terrain> terrain = ref_cast<Visual::Terrain> (com);
	
	com = mLight->getComponent ("entity.visual.light");
	smart_ptr<Visual::Light> light = ref_cast<Visual::Light> (com);
	
	terrain->setLight (light);
	terrain->setHeightMap ("/devel/data/terrain.dds");
	terrain->load2 ();
}




/* engine main loop tick */
void TerrainRuntimeTest::onTick (const Soma::TickEventArgs& args)
{
	float distance = 100 * args.elapsed;
	
	
	if (mForward.isActive()) mCamera->translate (0, 0, -distance);
	if (mBack.isActive())    mCamera->translate (0, 0, distance);
	if (mLeft.isActive())    mCamera->translate (-distance, 0, 0);
	if (mRight.isActive())   mCamera->translate (distance, 0, 0);
}





/* bind input controls */
void TerrainRuntimeTest::bind ()
{
	using namespace Soma;
	
	
	smart_ptr<Component> com = mContext->getComponent ("context.input");
	smart_ptr<Input::Context> ctx = ref_cast<Input::Context> (com);
	
	mQuit.setContext (ctx);
	mForward.setContext (ctx);
	mBack.setContext (ctx);
	mLeft.setContext (ctx);
	mRight.setContext (ctx);
	
	mQuit.bind ("Keyboard/Escape");
	mQuit.bind ("Keyboard/Left Control", "Keyboard/Q");
	
	mForward.bind ("Keyboard/W");
	mBack.bind ("Keyboard/S");
	mLeft.bind ("Keyboard/A");
	mRight.bind ("Keyboard/D");
	
	
	mQuit.Activated += delegate (this, &TerrainRuntimeTest::onActivated);
}





/* an input binding was actived */
void TerrainRuntimeTest::onActivated (const Soma::weak_ptr<Soma::Input::Action>& action)
{
	if (action == &mQuit)
		mEngine->quit ();
}





/* program entry */
int main (int argc, char** argv)
{
	TerrainRuntimeTest test;
	
	test.initialise();
	test.run();
}
