
#include "RuntimeTest.h"


/* constructor */
RuntimeTest::RuntimeTest ()
{
	mEngine = new Soma::Engine ();
}



/* destructor */
RuntimeTest::~RuntimeTest ()
{
	delete mEngine;
}



/* main loop */
void RuntimeTest::run ()
{
	mEngine->run ();
	teardown ();
}




/* initialise runtime */
void RuntimeTest::initialise ()
{
	mEngine->mount ("/devel/data");
	mEngine->Tick += delegate (this, &RuntimeTest::onTick);
	
	
	/* initialise engine subsystems */
	if (!mEngine->initialise ())
		SOMA_EXCEPTION ("RuntimeTest", "Failed to initialise engine");
	
	
	
	/* create and load context */
	mContext = mEngine->createContext ("MainContext");
	
	if (!mContext->load ())
		SOMA_EXCEPTION ("RuntimeTest", "Failed to load context 'MainContext'");
	
	
	
	/* create and load world */
	mWorld = mEngine->createWorld ("MainWorld");
	
	if (!mWorld->load ())
		SOMA_EXCEPTION ("RuntimeTest", "Failed to load world 'MainWorld'");
	
	
	setup ();
}
