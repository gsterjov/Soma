
#include "RuntimeTest.h"



class InputRuntimeTest : public RuntimeTest
{
private:
	void setup ();
	void onTick (const Soma::TickEventArgs& args);
	
	void bind ();
	
	
	void onActivated (const Soma::weak_ptr<Soma::Input::Action>& action);
	
	
	/* entities */
	Soma::smart_ptr<Soma::Entity> mCamera;
	Soma::smart_ptr<Soma::AssetEntity> mObject;
	
	
	/* bindings */
	Soma::Input::TriggerAction mQuit;
	Soma::Input::TriggerAction mForward;
	Soma::Input::TriggerAction mBack;
	Soma::Input::TriggerAction mLeft;
	Soma::Input::TriggerAction mRight;
};




/* setup environment */
void InputRuntimeTest::setup ()
{
	using namespace Soma;
	
	
	mContext->addComponents ("context.rendertarget.window,"
	                         "context.viewport,"
	                         "context.input");
	
	mWorld->addComponents ("world.visual");
	
	
	/* create camera entity */
	mCamera = new Entity ("Camera", "entity.visual.node, entity.visual.camera");
	mCamera->setPosition (0, 0, 3);
	
	
	/* create an entity to control */
	mObject = new AssetEntity ("Object", "cube.dae");
	
	
	mWorld->add (mObject);
	mWorld->add (mCamera);
	
	mContext->setProperty ("context.viewport", "camera-entity", mCamera);
	mCamera->setProperty ("entity.visual.camera", "near-clip-distance", 0.1f);
	
	
	bind ();
}




/* engine main loop tick */
void InputRuntimeTest::onTick (const Soma::TickEventArgs& args)
{
	float distance = 1 * args.elapsed;
	
	
	if (mForward.isActive()) mCamera->translate (0, 0, -distance);
	if (mBack.isActive())    mCamera->translate (0, 0, distance);
	if (mLeft.isActive())    mCamera->translate (-distance, 0, 0);
	if (mRight.isActive())   mCamera->translate (distance, 0, 0);
}





/* bind input controls */
void InputRuntimeTest::bind ()
{
	using namespace Soma;
	
	
	smart_ptr<Component> com = mContext->getComponent ("context.input");
	smart_ptr<Input::Context> ctx = ref_cast<Input::Context> (com);
	
	mQuit.setContext (ctx);
	mForward.setContext (ctx);
	mBack.setContext (ctx);
	mLeft.setContext (ctx);
	mRight.setContext (ctx);
	
	mQuit.bind ("Keyboard/Escape");
	mQuit.bind ("Keyboard/Left Control", "Keyboard/Q");
	
	mForward.bind ("Keyboard/W");
	mBack.bind ("Keyboard/S");
	mLeft.bind ("Keyboard/A");
	mRight.bind ("Keyboard/D");
	
	
	mQuit.Activated += delegate (this, &InputRuntimeTest::onActivated);
}




/* an input binding was actived */
void InputRuntimeTest::onActivated (const Soma::weak_ptr<Soma::Input::Action>& action)
{
	if (action == &mQuit)
		mEngine->quit ();
}





/* program entry */
int main (int argc, char** argv)
{
	InputRuntimeTest test;
	
	test.initialise();
	test.run();
}
