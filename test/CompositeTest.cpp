
#include <gtest/gtest.h>

#include <Soma/Registry.h>
#include <Soma/Composite.h>
#include <Soma/Component.h>


namespace Soma {
namespace Test {


	/* mock composite for testing */
	class MockComposite : public Composite
	{
	public:
		const String& getName() const { static String name = "MockComposite"; return name; }
	};
	
	
	
	/* mock registry component for testing */
	class MockComponent : public Component
	{
	public:
		String mType;
		weak_ptr<Composite> mOwner;
		
		String mValue;
		
		
		MockComponent () : mType("mock.component") {}
		MockComponent (const String& type) : mType(type) {}
		
		
		const String&       getType()  const { return mType; }
		weak_ptr<Composite> getOwner() const { return mOwner; }
		
		
		void setOwner (const weak_ptr<Composite>& owner)
		{
			mOwner = owner;
			
			if (mOwner)
			{
				mOwner->Request += delegate (this, &MockComponent::onRequest);
				mOwner->Broadcast += delegate (this, &MockComponent::onBroadcast);
			}
		}
		
		
		bool load () { return true; }
		bool reload () { return true; }
		void unload () {}
		bool isLoaded() const { return true; }
		
		
		
		
		
		
		void onRequest (CompositeMessage& message)
		{
			if (message.type == "mock")
				message.value = String("mock value");
		}
		
		void onBroadcast (const CompositeMessage& message)
		{
			if (message.type == "mock")
				mValue = message.value.get<String>();
		}
		
	};
	
	
	/* mocks with alternate name */
	class MockComponent1 : public MockComponent
	{
	public:
		MockComponent1 () : MockComponent("mock.component1") {}
	};
	
	
	class MockComponent2 : public MockComponent
	{
	public:
		MockComponent2 () : MockComponent("mock.component2") {}
	};
	
	
	
	/* mock component type for registry retrieval testing */
	REGISTER_TYPE (Component, MockComponent, "mock.component");
	
	/* alternate mocks */
	REGISTER_TYPE (Component, MockComponent1, "mock.component1");
	REGISTER_TYPE (Component, MockComponent2, "mock.component2");
	
	
	
	
	
	/* test component adding */
	TEST (CompositeTest, AddComponents)
	{
		using namespace testing;
		
		
		MockComposite composite;
		
		smart_ptr<Component> component1 = new MockComponent ("component1");
		smart_ptr<Component> component2 = new MockComponent ("component2");
		
		
		
		
		/* explicit adding */
		composite.addComponent (component1);
		EXPECT_EQ (1, composite.getComponents().size());
		
		composite.addComponent (component2);
		EXPECT_EQ (2, composite.getComponents().size());
		
		
		/* adding from registry */
		composite.addComponents ("mock.component");
		EXPECT_EQ (3, composite.getComponents().size());
		
		/* adding many from registry */
		composite.addComponents ("mock.component1, mock.component2");
		EXPECT_EQ (5, composite.getComponents().size());
	}
	
	
	
	
	
	/* test component removal */
	TEST (CompositeTest, RemoveComponents)
	{
		using namespace testing;
		
		
		MockComposite composite;
		
		smart_ptr<Component> component1 = new MockComponent ("component1");
		smart_ptr<Component> component2 = new MockComponent ("component2");
		
		
		composite.addComponent (component1);
		composite.addComponent (component2);
		composite.addComponents ("mock.component");
		composite.addComponents ("mock.component1, mock.component2");
		
		ASSERT_EQ (5, composite.getComponents().size());
		
		
		/* explicit removal */
		composite.removeComponent (component1);
		EXPECT_EQ (4, composite.getComponents().size());
		
		/* remove explicitly added component by name */
		composite.removeComponents (component2->getType());
		EXPECT_EQ (3, composite.getComponents().size());
		
		
		/* remove component by name */
		composite.removeComponents ("mock.component");
		EXPECT_EQ (2, composite.getComponents().size());
		
		/* remove many components by name */
		composite.removeComponents ("mock.component1, mock.component2");
		EXPECT_EQ (0, composite.getComponents().size());
	}
	
	
	
	
	
	/* test component retrieval */
	TEST (CompositeTest, RetrieveComponents)
	{
		using namespace testing;
		
		
		MockComposite composite;
		
		smart_ptr<Component> component1 = new MockComponent ("component1");
		smart_ptr<Component> component2 = new MockComponent ("component2");
		
		
		composite.addComponent (component1);
		composite.addComponent (component2);
		composite.addComponents ("mock.component");
		composite.addComponents ("mock.component1, mock.component2");
		
		ASSERT_EQ (5, composite.getComponents().size());
		
		
		/* retrieve explicit components */
		EXPECT_EQ (component1, composite.getComponent ("component1"));
		EXPECT_EQ (component2, composite.getComponent ("component2"));
		
		/* retrieve registry components */
		EXPECT_EQ ("mock.component", composite.getComponent ("mock.component")->getType());
		EXPECT_EQ ("mock.component1", composite.getComponent ("mock.component1")->getType());
		EXPECT_EQ ("mock.component2", composite.getComponent ("mock.component2")->getType());
	}
	
	
	
	
	
	/* test component broadcast */
	TEST (CompositeTest, ComponentBroadcast)
	{
		using namespace testing;
		
		
		MockComposite composite;
		
		smart_ptr<Component> component1 = new MockComponent ("component1");
		smart_ptr<Component> component2 = new MockComponent ("component2");
		
		
		composite.addComponent (component1);
		composite.addComponent (component2);
		composite.addComponents ("mock.component");
		composite.addComponents ("mock.component1, mock.component2");
		
		ASSERT_EQ (5, composite.getComponents().size());
		
		
		/* get components */
		smart_ptr<MockComponent> com1 = ref_cast<MockComponent> (composite.getComponent ("component1"));
		smart_ptr<MockComponent> com2 = ref_cast<MockComponent> (composite.getComponent ("component2"));
		smart_ptr<MockComponent> com3 = ref_cast<MockComponent> (composite.getComponent ("mock.component"));
		smart_ptr<MockComponent> com4 = ref_cast<MockComponent> (composite.getComponent ("mock.component1"));
		smart_ptr<MockComponent> com5 = ref_cast<MockComponent> (composite.getComponent ("mock.component2"));
		
		
		/* send an invalid broadcast message */
		composite.broadcast ("bad mock", String("mock value"));
		
		/* component values shouldn't have changed */
		EXPECT_EQ ("", com1->mValue);
		EXPECT_EQ ("", com2->mValue);
		EXPECT_EQ ("", com3->mValue);
		EXPECT_EQ ("", com4->mValue);
		EXPECT_EQ ("", com5->mValue);
		
		
		/* send a valid broadcast message */
		composite.broadcast ("mock", String("mock value"));
		
		/* component values shouldn't have changed */
		EXPECT_EQ ("mock value", com1->mValue);
		EXPECT_EQ ("mock value", com2->mValue);
		EXPECT_EQ ("mock value", com3->mValue);
		EXPECT_EQ ("mock value", com4->mValue);
		EXPECT_EQ ("mock value", com5->mValue);
	}
	
	
	
	
	
	/* test component request */
	TEST (CompositeTest, ComponentRequest)
	{
		using namespace testing;
		
		
		MockComposite composite;
		
		smart_ptr<Component> component1 = new MockComponent ("component1");
		smart_ptr<Component> component2 = new MockComponent ("component2");
		
		
		composite.addComponent (component1);
		composite.addComponent (component2);
		composite.addComponents ("mock.component");
		composite.addComponents ("mock.component1, mock.component2");
		
		ASSERT_EQ (5, composite.getComponents().size());
		
		
		/* send an invalid request */
		CompositeMessage msg = composite.request ("bad mock");
		
		EXPECT_EQ (msg.type, "bad mock");
		EXPECT_TRUE (msg.value.isEmpty());
		
		
		/* send a valid request */
		msg = composite.request ("mock");
		
		EXPECT_EQ (msg.type, "mock");
		EXPECT_EQ (msg.value.get<String>(), "mock value");
	}

}}
