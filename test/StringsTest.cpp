
#include <gtest/gtest.h>
#include <Soma/String.h>


namespace Soma {
namespace Test {


	/* test string trimming */
	TEST (StringsTest, TrimString)
	{
		String str;
		
		/* end whitespace */
		str = "test string  ";
		EXPECT_EQ ("test string", Strings::trim (str));
		
		/* start whitespace */
		str = "  test string";
		EXPECT_EQ ("test string", Strings::trim (str));
		
		/* both whitespace */
		str = "  test string  ";
		EXPECT_EQ ("test string", Strings::trim (str));
		
		
		/* end tab */
		str = "test string\t";
		EXPECT_EQ ("test string", Strings::trim (str));
		
		/* start tab */
		str = "\ttest string";
		EXPECT_EQ ("test string", Strings::trim (str));
		
		/* both tab */
		str = "\ttest string\t";
		EXPECT_EQ ("test string", Strings::trim (str));
		
		
		/* end newline */
		str = "test string\n";
		EXPECT_EQ ("test string", Strings::trim (str));
		
		/* start newline */
		str = "\ntest string";
		EXPECT_EQ ("test string", Strings::trim (str));
		
		/* both newline */
		str = "\ntest string\n";
		EXPECT_EQ ("test string", Strings::trim (str));
		
		
		/* end all */
		str = "test string  \t\n";
		EXPECT_EQ ("test string", Strings::trim (str));
		
		/* start all */
		str = "  \n\ttest string";
		EXPECT_EQ ("test string", Strings::trim (str));
		
		/* both all */
		str = "\t  \n  test string \n\t  ";
		EXPECT_EQ ("test string", Strings::trim (str));
	}
	
	
	
	/* test string transformation */
	TEST (StringsTest, ToLowerString)
	{
		String str;
		
		/* all caps */
		str = "TEST STRING";
		EXPECT_EQ ("test string", Strings::toLower (str));
		
		/* some caps */
		str = "TeST StrinG";
		EXPECT_EQ ("test string", Strings::toLower (str));
		
		/* no caps */
		str = "test string";
		EXPECT_EQ ("test string", Strings::toLower (str));
	}
	
	
	
	/* test string transformation */
	TEST (StringsTest, ToUpperString)
	{
		String str;
		
		/* no caps */
		str = "test string";
		EXPECT_EQ ("TEST STRING", Strings::toUpper (str));
		
		/* some caps */
		str = "TeST StrinG";
		EXPECT_EQ ("TEST STRING", Strings::toUpper (str));
		
		/* all caps */
		str = "TEST STRING";
		EXPECT_EQ ("TEST STRING", Strings::toUpper (str));
	}
	
	
	
	/* test string splitting */
	TEST (StringsTest, SplitString)
	{
		StringList list;
		
		/* trivial split, 1 delimiter */
		list = Strings::split ("test string", " ");
		EXPECT_EQ (2, list.size());
		EXPECT_EQ ("test", list[0]);
		EXPECT_EQ ("string", list[1]);
		
		/* trivial split, many delimiters */
		list = Strings::split ("t\nest s*tr|ing", "*|\n");
		EXPECT_EQ (4, list.size());
		EXPECT_EQ ("t", list[0]);
		EXPECT_EQ ("est s", list[1]);
		EXPECT_EQ ("tr", list[2]);
		EXPECT_EQ ("ing", list[3]);
		
		
		/* trivial split, start delimiter */
		list = Strings::split ("|test", "|");
		EXPECT_EQ (1, list.size());
		EXPECT_EQ ("test", list[0]);
		
		/* trivial split, end delimiter */
		list = Strings::split ("test|", "|");
		EXPECT_EQ (1, list.size());
		EXPECT_EQ ("test", list[0]);
		
		/* trivial split, start and end delimiters */
		list = Strings::split ("|test|", "|");
		EXPECT_EQ (1, list.size());
		EXPECT_EQ ("test", list[0]);
		
		
		/* complex split, 1 delimiter */
		list = Strings::split ("||||te|st| |strin|g|", "|");
		EXPECT_EQ (5, list.size());
		EXPECT_EQ ("te", list[0]);
		EXPECT_EQ ("st", list[1]);
		EXPECT_EQ (" ", list[2]);
		EXPECT_EQ ("strin", list[3]);
		EXPECT_EQ ("g", list[4]);
		
		/* complex split, many delimiters */
		list = Strings::split ("t|est \"strihnghh", "h|\"");
		EXPECT_EQ (4, list.size());
		EXPECT_EQ ("t", list[0]);
		EXPECT_EQ ("est ", list[1]);
		EXPECT_EQ ("stri", list[2]);
		EXPECT_EQ ("ng", list[3]);
		
		
		/* no split, no delimiters */
		list = Strings::split ("test string", "|");
		EXPECT_EQ (0, list.size());
		
		/* no split, empty string */
		list = Strings::split ("", "|");
		EXPECT_EQ (0, list.size());
		
		/* no split, just delimiters */
		list = Strings::split ("||=:'|", "'=:|");
		EXPECT_EQ (0, list.size());
		
		/* no split, one delimiter */
		list = Strings::split ("|", "|");
		EXPECT_EQ (0, list.size());
		
		/* no split, no delimiter */
		list = Strings::split ("test string", "");
		EXPECT_EQ (0, list.size());
	}
	
	
	
	
	/* test string conversion */
	TEST (StringsTest, StringToPrimitive)
	{
		/* int conversion */
		EXPECT_EQ (1, Strings::toInt ("1"));
		EXPECT_EQ (1, Strings::toInt ("  1  "));
		EXPECT_NE (1, Strings::toInt ("x 1 x"));
		
		EXPECT_EQ (-1, Strings::toInt ("-1"));
		EXPECT_EQ (-1, Strings::toInt ("  -1  "));
		EXPECT_NE (-1, Strings::toInt ("x -1 x"));
		
		
		/* uint conversion */
		EXPECT_EQ (2, Strings::toUInt ("2"));
		EXPECT_EQ (2, Strings::toUInt ("  2  "));
		EXPECT_NE (2, Strings::toUInt ("x 2 x"));
		
		
		/* bool conversion */
		EXPECT_EQ (true, Strings::toBool ("true"));
		EXPECT_EQ (true, Strings::toBool ("TRUE"));
		EXPECT_EQ (true, Strings::toBool ("TrUe"));
		EXPECT_EQ (true, Strings::toBool ("  true  "));
		EXPECT_NE (true, Strings::toBool ("x true x"));
		
		EXPECT_EQ (true, Strings::toBool ("yes"));
		EXPECT_EQ (true, Strings::toBool ("YES"));
		EXPECT_EQ (true, Strings::toBool ("YeS"));
		EXPECT_EQ (true, Strings::toBool ("  yes  "));
		EXPECT_NE (true, Strings::toBool ("x yes x"));
		
		EXPECT_EQ (true, Strings::toBool ("on"));
		EXPECT_EQ (true, Strings::toBool ("ON"));
		EXPECT_EQ (true, Strings::toBool ("oN"));
		EXPECT_EQ (true, Strings::toBool ("  on  "));
		EXPECT_NE (true, Strings::toBool ("x on x"));
		
		EXPECT_EQ (false, Strings::toBool ("false"));
		EXPECT_EQ (false, Strings::toBool ("abcdefg"));
		
		
		/* float conversion */
		EXPECT_EQ (1.5f, Strings::toFloat ("1.5"));
		EXPECT_EQ (1.5f, Strings::toFloat ("  1.5  "));
		EXPECT_NE (1.5f, Strings::toFloat ("x 1.5 x"));
		
		EXPECT_EQ (-1.5f, Strings::toFloat ("-1.5"));
		EXPECT_EQ (-1.5f, Strings::toFloat ("  -1.5  "));
		EXPECT_NE (-1.5f, Strings::toFloat ("x -1.5 x"));
	}
	
	
	
	
	/* test string conversion */
	TEST (StringsTest, PrimitiveToString)
	{
		/* convert int */
		EXPECT_EQ ("1", Strings::convert (1));
		EXPECT_EQ ("-2", Strings::convert (-2));
		
		/* convert uint */
		EXPECT_EQ ("3", Strings::convert (static_cast<uint> (3)));
		
		/* convert bool */
		EXPECT_EQ ("true", Strings::convert (true));
		EXPECT_EQ ("false", Strings::convert (false));
		
		/* convert float */
		EXPECT_EQ ("4.5", Strings::convert (4.5f));
		EXPECT_EQ ("-5.5", Strings::convert (-5.5f));
	}


}}
