/*
 * EventTest.cpp
 *
 *  Created on: Jul 27, 2010
 *      Author: sterjov
 */

#include <gtest/gtest.h>
#include <Soma/Event.h>


namespace Soma {
namespace Test {


	/* dummy class for member function testing */
	class EventTestDummy
	{
	public:
		EventTestDummy () : mValue1 (0), mValue2 (0) {}
		
		int mValue1;
		int mValue2;
		
		
		bool test1 (int arg) { mValue1 = arg; return true; }
		bool test2 (int arg) { mValue2 = arg; return true; }
	};
	
	
	
	class EventTestDummyVirtual
	{
	public:
		EventTestDummyVirtual () : mBaseValue (0) {}
		
		int mBaseValue;
		
		
		        bool base     (int arg) { mBaseValue = arg; return true; }
		virtual bool derived1 (int arg) { return false; }
		virtual bool derived2 (int arg) { return false; }
		virtual bool pure1    (int arg) = 0;
		virtual bool pure2    (int arg) = 0;
	};
	
	
	
	class EventTestDummyDerived : public EventTestDummyVirtual
	{
	public:
		EventTestDummyDerived ()
		: mDerivedValue1 (0),
		  mDerivedValue2 (0),
		  mPureValue1 (0),
		  mPureValue2 (0)
		{}
		
		int mDerivedValue1;
		int mDerivedValue2;
		int mPureValue1;
		int mPureValue2;
		
		
		        bool derived1 (int arg) { mDerivedValue1 = arg; return true; }
		virtual bool derived2 (int arg) { mDerivedValue2 = arg; return true; }
		        bool pure1    (int arg) { mPureValue1 = arg; return true; }
		virtual bool pure2    (int arg) { mPureValue2 = arg; return true; }
	};
	
	
	
	class EventTestDummyMultipleA
	{
	public:
		EventTestDummyMultipleA () : mBaseValue1 (0) {}
		
		int mBaseValue1;
		
		
                bool base1    (int arg) { mBaseValue1 = arg; return true; }
		virtual bool derived1 (int arg) { return false; }
		virtual bool pure2    (int arg) = 0;
	};
	
	
	class EventTestDummyMultipleB
	{
	public:
		EventTestDummyMultipleB () : mBaseValue2 (0) {}
		
		int mBaseValue2;
		
		
                bool base2    (int arg) { mBaseValue2 = arg; return true; }
		virtual bool derived2 (int arg) { return false; }
		virtual bool pure1    (int arg) = 0;
	};
	
	
	
	class EventTestDummyMultiple : public EventTestDummyMultipleA,
	                               public EventTestDummyMultipleB
	{
	public:
		EventTestDummyMultiple ()
		: mDerivedValue1 (0),
		  mDerivedValue2 (0),
		  mPureValue1 (0),
		  mPureValue2 (0)
		{}
		
		int mDerivedValue1;
		int mDerivedValue2;
		int mPureValue1;
		int mPureValue2;
		
		
		        bool derived1 (int arg) { mDerivedValue1 = arg; return true; }
		virtual bool derived2 (int arg) { mDerivedValue2 = arg; return true; }
		        bool pure1    (int arg) { mPureValue1 = arg; return true; }
		virtual bool pure2    (int arg) { mPureValue2 = arg; return true; }
	};
	
	
	
	
	
	/* event system test fixture */
	class EventTest : public testing::Test
	{
	protected:
		void SetUp ()
		{
			/* delegates */
			mDel1 = new Delegate<EventTestDummy, bool, int> (&mDummy1, &EventTestDummy::test1);
			mDel2 = new Delegate<EventTestDummy, bool, int> (&mDummy1, &EventTestDummy::test2);
			mDelClone1 = new Delegate<EventTestDummy, bool, int> (&mDummy2, &EventTestDummy::test1);
			mDelClone2 = new Delegate<EventTestDummy, bool, int> (&mDummy2, &EventTestDummy::test2);
			
			/* virtual inheritance */
			mDelBase = new Delegate<EventTestDummyDerived, bool, int> (&mDummyDerived, &EventTestDummyVirtual::base);
			mDelDerived1 = new Delegate<EventTestDummyDerived, bool, int> (&mDummyDerived, &EventTestDummyVirtual::derived1);
			mDelDerived2 = new Delegate<EventTestDummyDerived, bool, int> (&mDummyDerived, &EventTestDummyVirtual::derived2);
			mDelPure1 = new Delegate<EventTestDummyDerived, bool, int> (&mDummyDerived, &EventTestDummyVirtual::pure1);
			mDelPure2 = new Delegate<EventTestDummyDerived, bool, int> (&mDummyDerived, &EventTestDummyVirtual::pure2);
			
			/* multiple inheritance */
			mDelMultipleBase1 = new Delegate<EventTestDummyMultiple, bool, int> (&mDummyMultiple, &EventTestDummyMultipleA::base1);
			mDelMultipleBase2 = new Delegate<EventTestDummyMultiple, bool, int> (&mDummyMultiple, &EventTestDummyMultipleB::base2);
			mDelMultipleDerived1 = new Delegate<EventTestDummyMultiple, bool, int> (&mDummyMultiple, &EventTestDummyMultipleA::derived1);
			mDelMultipleDerived2 = new Delegate<EventTestDummyMultiple, bool, int> (&mDummyMultiple, &EventTestDummyMultipleB::derived2);
			mDelMultiplePure1 = new Delegate<EventTestDummyMultiple, bool, int> (&mDummyMultiple, &EventTestDummyMultipleB::pure1);
			mDelMultiplePure2 = new Delegate<EventTestDummyMultiple, bool, int> (&mDummyMultiple, &EventTestDummyMultipleA::pure2);
		}
		
		
		void TearDown ()
		{
			delete mDel1, mDel2, mDelClone1, mDelClone2;
			delete mDelBase, mDelDerived1, mDelDerived2, mDelPure1, mDelPure2;
			delete mDelMultipleBase1, mDelMultipleBase2, mDelMultipleDerived1, mDelMultipleDerived2, mDelMultiplePure1, mDelMultiplePure2;
		}
		
		
		/* event */
		Event<bool, int> mEvent;
		
		
		/* dummy class for member functions */
		EventTestDummy mDummy1;
		EventTestDummy mDummy2;
		EventTestDummyDerived mDummyDerived;
		EventTestDummyMultiple mDummyMultiple;
		
		
		/* delegates */
		Delegate<EventTestDummy, bool, int>* mDel1;
		Delegate<EventTestDummy, bool, int>* mDel2;
		Delegate<EventTestDummy, bool, int>* mDelClone1;
		Delegate<EventTestDummy, bool, int>* mDelClone2;
		
		/* virtual inheritance */
		Delegate<EventTestDummyDerived, bool, int>* mDelBase;
		Delegate<EventTestDummyDerived, bool, int>* mDelDerived1;
		Delegate<EventTestDummyDerived, bool, int>* mDelDerived2;
		Delegate<EventTestDummyDerived, bool, int>* mDelPure1;
		Delegate<EventTestDummyDerived, bool, int>* mDelPure2;
		
		/* multiple inheritance */
		Delegate<EventTestDummyMultiple, bool, int>* mDelMultipleBase1;
		Delegate<EventTestDummyMultiple, bool, int>* mDelMultipleBase2;
		Delegate<EventTestDummyMultiple, bool, int>* mDelMultipleDerived1;
		Delegate<EventTestDummyMultiple, bool, int>* mDelMultipleDerived2;
		Delegate<EventTestDummyMultiple, bool, int>* mDelMultiplePure1;
		Delegate<EventTestDummyMultiple, bool, int>* mDelMultiplePure2;
	};
	
	
	
	
	/* test event registration */
	TEST_F (EventTest, AddDelegates)
	{
		/* add delegate */
		mEvent.add (*mDel1);
		mEvent.raise (1);
		
		EXPECT_EQ (1, mEvent.getDelegates().size());
		EXPECT_EQ (1, mDummy1.mValue1);
		EXPECT_EQ (0, mDummy1.mValue2);
		EXPECT_EQ (0, mDummy2.mValue1);
		EXPECT_EQ (0, mDummy2.mValue2);
		
		
		/* add without delegate */
		mEvent.add (&mDummy1, &EventTestDummy::test2);
		mEvent.raise (2);
		
		EXPECT_EQ (2, mEvent.getDelegates().size());
		EXPECT_EQ (2, mDummy1.mValue1);
		EXPECT_EQ (2, mDummy1.mValue2);
		EXPECT_EQ (0, mDummy2.mValue1);
		EXPECT_EQ (0, mDummy2.mValue2);
		
		
		/* add with delegate function */
		mEvent.add (delegate (&mDummy2, &EventTestDummy::test1));
		mEvent.raise (3);
		
		EXPECT_EQ (3, mEvent.getDelegates().size());
		EXPECT_EQ (3, mDummy1.mValue1);
		EXPECT_EQ (3, mDummy1.mValue2);
		EXPECT_EQ (3, mDummy2.mValue1);
		EXPECT_EQ (0, mDummy2.mValue2);
		
		
		/* add with inline delegate */
		mEvent.add (Delegate<EventTestDummy, bool, int> (&mDummy2, &EventTestDummy::test2));
		mEvent.raise (4);
		
		EXPECT_EQ (4, mEvent.getDelegates().size());
		EXPECT_EQ (4, mDummy1.mValue1);
		EXPECT_EQ (4, mDummy1.mValue2);
		EXPECT_EQ (4, mDummy2.mValue1);
		EXPECT_EQ (4, mDummy2.mValue2);
	}
	
	
	
	/* test unregistration */
	TEST_F (EventTest, RemoveDelegates)
	{
		/* single delegate */
		mEvent.add (*mDel1);
		mEvent.remove (*mDel1);
		mEvent.raise (1);
		
		EXPECT_EQ (0, mEvent.getDelegates().size());
		EXPECT_EQ (0, mDummy1.mValue1);
		EXPECT_EQ (0, mDummy1.mValue2);
		EXPECT_EQ (0, mDummy2.mValue1);
		EXPECT_EQ (0, mDummy2.mValue2);
		
		
		/* multiple delegates */
		mEvent.add (*mDel1);
		mEvent.add (*mDel2);
		mEvent.add (*mDelClone1);
		mEvent.add (*mDelClone2);
		
		
		/* remove middle */
		mEvent.remove (*mDel2);
		mEvent.raise (1);
		
		EXPECT_EQ (3, mEvent.getDelegates().size());
		EXPECT_EQ (1, mDummy1.mValue1);
		EXPECT_EQ (0, mDummy1.mValue2);
		EXPECT_EQ (1, mDummy2.mValue1);
		EXPECT_EQ (1, mDummy2.mValue2);
		
		
		/* remove same signature, different object */
		mEvent.remove (*mDelClone1);
		mEvent.raise (2);
		
		EXPECT_EQ (2, mEvent.getDelegates().size());
		EXPECT_EQ (2, mDummy1.mValue1);
		EXPECT_EQ (0, mDummy1.mValue2);
		EXPECT_EQ (1, mDummy2.mValue1);
		EXPECT_EQ (2, mDummy2.mValue2);
		
		
		/* remove all */
		mEvent.remove (*mDel1);
		mEvent.remove (*mDelClone2);
		mEvent.raise (3);
		
		EXPECT_EQ (0, mEvent.getDelegates().size());
		EXPECT_EQ (2, mDummy1.mValue1);
		EXPECT_EQ (0, mDummy1.mValue2);
		EXPECT_EQ (1, mDummy2.mValue1);
		EXPECT_EQ (2, mDummy2.mValue2);
		
		
		
		/* remove without delegate */
		mEvent.add (*mDel1);
		mEvent.remove (&mDummy1, &EventTestDummy::test1);
		mEvent.raise (4);
		
		EXPECT_EQ (0, mEvent.getDelegates().size());
		EXPECT_EQ (2, mDummy1.mValue1);
		EXPECT_EQ (0, mDummy1.mValue2);
		EXPECT_EQ (1, mDummy2.mValue1);
		EXPECT_EQ (2, mDummy2.mValue2);
		
		
		/* remove with delegate function */
		mEvent.add (*mDel1);
		mEvent.remove (delegate (&mDummy1, &EventTestDummy::test1));
		mEvent.raise (5);
		
		EXPECT_EQ (0, mEvent.getDelegates().size());
		EXPECT_EQ (2, mDummy1.mValue1);
		EXPECT_EQ (0, mDummy1.mValue2);
		EXPECT_EQ (1, mDummy2.mValue1);
		EXPECT_EQ (2, mDummy2.mValue2);
		
		
		/* remove with inline delegate */
		mEvent.add (*mDel1);
		mEvent.remove (Delegate<EventTestDummy, bool, int> (&mDummy1, &EventTestDummy::test1));
		mEvent.raise (6);
		
		EXPECT_EQ (0, mEvent.getDelegates().size());
		EXPECT_EQ (2, mDummy1.mValue1);
		EXPECT_EQ (0, mDummy1.mValue2);
		EXPECT_EQ (1, mDummy2.mValue1);
		EXPECT_EQ (2, mDummy2.mValue2);
	}
	
	
	
	/* test registration with virtual members */
	TEST_F (EventTest, Add_VirtualMembers)
	{
		/* add base class member */
		mEvent.add (*mDelBase);
		mEvent.raise (1);
		
		EXPECT_EQ (1, mEvent.getDelegates().size());
		EXPECT_EQ (1, mDummyDerived.mBaseValue);
		EXPECT_EQ (0, mDummyDerived.mDerivedValue1);
		EXPECT_EQ (0, mDummyDerived.mDerivedValue2);
		EXPECT_EQ (0, mDummyDerived.mPureValue1);
		EXPECT_EQ (0, mDummyDerived.mPureValue2);
		
		
		/* add derived class member */
		mEvent.add (*mDelDerived1);
		mEvent.raise (2);
		
		EXPECT_EQ (2, mEvent.getDelegates().size());
		EXPECT_EQ (2, mDummyDerived.mBaseValue);
		EXPECT_EQ (2, mDummyDerived.mDerivedValue1);
		EXPECT_EQ (0, mDummyDerived.mDerivedValue2);
		EXPECT_EQ (0, mDummyDerived.mPureValue1);
		EXPECT_EQ (0, mDummyDerived.mPureValue2);
		
		
		/* add derived class member keeping it virtual */
		mEvent.add (*mDelDerived2);
		mEvent.raise (3);
		
		EXPECT_EQ (3, mEvent.getDelegates().size());
		EXPECT_EQ (3, mDummyDerived.mBaseValue);
		EXPECT_EQ (3, mDummyDerived.mDerivedValue1);
		EXPECT_EQ (3, mDummyDerived.mDerivedValue2);
		EXPECT_EQ (0, mDummyDerived.mPureValue1);
		EXPECT_EQ (0, mDummyDerived.mPureValue2);
		
		
		/* add pure class member */
		mEvent.add (*mDelPure1);
		mEvent.raise (4);
		
		EXPECT_EQ (4, mEvent.getDelegates().size());
		EXPECT_EQ (4, mDummyDerived.mBaseValue);
		EXPECT_EQ (4, mDummyDerived.mDerivedValue1);
		EXPECT_EQ (4, mDummyDerived.mDerivedValue2);
		EXPECT_EQ (4, mDummyDerived.mPureValue1);
		EXPECT_EQ (0, mDummyDerived.mPureValue2);
		
		
		/* add pure class member keeping it virtual */
		mEvent.add (*mDelPure2);
		mEvent.raise (5);
		
		EXPECT_EQ (5, mEvent.getDelegates().size());
		EXPECT_EQ (5, mDummyDerived.mBaseValue);
		EXPECT_EQ (5, mDummyDerived.mDerivedValue1);
		EXPECT_EQ (5, mDummyDerived.mDerivedValue2);
		EXPECT_EQ (5, mDummyDerived.mPureValue1);
		EXPECT_EQ (5, mDummyDerived.mPureValue2);
	}
	
	
	
	/* test unregistration with virutal members */
	TEST_F (EventTest, Remove_VirtualMembers)
	{
		/* add all delegates */
		mEvent.add (*mDelBase);
		mEvent.add (*mDelDerived1);
		mEvent.add (*mDelDerived2);
		mEvent.add (*mDelPure1);
		mEvent.add (*mDelPure2);
		
		
		/* remove non virtual inherited base */
		mEvent.remove (*mDelBase);
		mEvent.raise (1);
		
		EXPECT_EQ (4, mEvent.getDelegates().size());
		EXPECT_EQ (0, mDummyDerived.mBaseValue);
		EXPECT_EQ (1, mDummyDerived.mDerivedValue1);
		EXPECT_EQ (1, mDummyDerived.mDerivedValue2);
		EXPECT_EQ (1, mDummyDerived.mPureValue1);
		EXPECT_EQ (1, mDummyDerived.mPureValue2);
		
		
		/* remove derived member */
		mEvent.remove (*mDelDerived1);
		mEvent.raise (2);
		
		EXPECT_EQ (3, mEvent.getDelegates().size());
		EXPECT_EQ (0, mDummyDerived.mBaseValue);
		EXPECT_EQ (1, mDummyDerived.mDerivedValue1);
		EXPECT_EQ (2, mDummyDerived.mDerivedValue2);
		EXPECT_EQ (2, mDummyDerived.mPureValue1);
		EXPECT_EQ (2, mDummyDerived.mPureValue2);
		
		
		/* remove derived member keeping it virtual */
		mEvent.remove (*mDelDerived2);
		mEvent.raise (3);
		
		EXPECT_EQ (2, mEvent.getDelegates().size());
		EXPECT_EQ (0, mDummyDerived.mBaseValue);
		EXPECT_EQ (1, mDummyDerived.mDerivedValue1);
		EXPECT_EQ (2, mDummyDerived.mDerivedValue2);
		EXPECT_EQ (3, mDummyDerived.mPureValue1);
		EXPECT_EQ (3, mDummyDerived.mPureValue2);
		
		
		/* remove pure member */
		mEvent.remove (*mDelPure1);
		mEvent.raise (4);
		
		EXPECT_EQ (1, mEvent.getDelegates().size());
		EXPECT_EQ (0, mDummyDerived.mBaseValue);
		EXPECT_EQ (1, mDummyDerived.mDerivedValue1);
		EXPECT_EQ (2, mDummyDerived.mDerivedValue2);
		EXPECT_EQ (3, mDummyDerived.mPureValue1);
		EXPECT_EQ (4, mDummyDerived.mPureValue2);
		
		
		/* remove pure member keeping it virtual */
		mEvent.remove (*mDelPure2);
		mEvent.raise (5);
		
		EXPECT_EQ (0, mEvent.getDelegates().size());
		EXPECT_EQ (0, mDummyDerived.mBaseValue);
		EXPECT_EQ (1, mDummyDerived.mDerivedValue1);
		EXPECT_EQ (2, mDummyDerived.mDerivedValue2);
		EXPECT_EQ (3, mDummyDerived.mPureValue1);
		EXPECT_EQ (4, mDummyDerived.mPureValue2);
	}
	
	
	
	/* test registration with multiple inheritance */
	TEST_F (EventTest, Add_MultipleInheritance)
	{
		/* add first base class member */
		mEvent.add (*mDelMultipleBase1);
		mEvent.raise (1);
		
		EXPECT_EQ (1, mEvent.getDelegates().size());
		EXPECT_EQ (1, mDummyMultiple.mBaseValue1);
		EXPECT_EQ (0, mDummyMultiple.mBaseValue2);
		EXPECT_EQ (0, mDummyMultiple.mDerivedValue1);
		EXPECT_EQ (0, mDummyMultiple.mDerivedValue2);
		EXPECT_EQ (0, mDummyMultiple.mPureValue1);
		EXPECT_EQ (0, mDummyMultiple.mPureValue2);
		
		
		/* add second base class member */
		mEvent.add (*mDelMultipleBase2);
		mEvent.raise (2);
		
		EXPECT_EQ (2, mEvent.getDelegates().size());
		EXPECT_EQ (2, mDummyMultiple.mBaseValue1);
		EXPECT_EQ (2, mDummyMultiple.mBaseValue2);
		EXPECT_EQ (0, mDummyMultiple.mDerivedValue1);
		EXPECT_EQ (0, mDummyMultiple.mDerivedValue2);
		EXPECT_EQ (0, mDummyMultiple.mPureValue1);
		EXPECT_EQ (0, mDummyMultiple.mPureValue2);
		
		
		/* add derived class member */
		mEvent.add (*mDelMultipleDerived1);
		mEvent.raise (3);
		
		EXPECT_EQ (3, mEvent.getDelegates().size());
		EXPECT_EQ (3, mDummyMultiple.mBaseValue1);
		EXPECT_EQ (3, mDummyMultiple.mBaseValue2);
		EXPECT_EQ (3, mDummyMultiple.mDerivedValue1);
		EXPECT_EQ (0, mDummyMultiple.mDerivedValue2);
		EXPECT_EQ (0, mDummyMultiple.mPureValue1);
		EXPECT_EQ (0, mDummyMultiple.mPureValue2);
		
		
		/* add derived class member keeping it virtual */
		mEvent.add (*mDelMultipleDerived2);
		mEvent.raise (4);
		
		EXPECT_EQ (4, mEvent.getDelegates().size());
		EXPECT_EQ (4, mDummyMultiple.mBaseValue1);
		EXPECT_EQ (4, mDummyMultiple.mBaseValue2);
		EXPECT_EQ (4, mDummyMultiple.mDerivedValue1);
		EXPECT_EQ (4, mDummyMultiple.mDerivedValue2);
		EXPECT_EQ (0, mDummyMultiple.mPureValue1);
		EXPECT_EQ (0, mDummyMultiple.mPureValue2);
		
		
		/* add pure class member */
		mEvent.add (*mDelMultiplePure1);
		mEvent.raise (5);
		
		EXPECT_EQ (5, mEvent.getDelegates().size());
		EXPECT_EQ (5, mDummyMultiple.mBaseValue1);
		EXPECT_EQ (5, mDummyMultiple.mBaseValue2);
		EXPECT_EQ (5, mDummyMultiple.mDerivedValue1);
		EXPECT_EQ (5, mDummyMultiple.mDerivedValue2);
		EXPECT_EQ (5, mDummyMultiple.mPureValue1);
		EXPECT_EQ (0, mDummyMultiple.mPureValue2);
		
		
		/* add pure class member keeping it virtual */
		mEvent.add (*mDelMultiplePure2);
		mEvent.raise (6);
		
		EXPECT_EQ (6, mEvent.getDelegates().size());
		EXPECT_EQ (6, mDummyMultiple.mBaseValue1);
		EXPECT_EQ (6, mDummyMultiple.mBaseValue2);
		EXPECT_EQ (6, mDummyMultiple.mDerivedValue1);
		EXPECT_EQ (6, mDummyMultiple.mDerivedValue2);
		EXPECT_EQ (6, mDummyMultiple.mPureValue1);
		EXPECT_EQ (6, mDummyMultiple.mPureValue2);
	}
	
	
	
	/* test unregistration with virutal members */
	TEST_F (EventTest, Remove_MultipleInheritance)
	{
		/* add all delegates */
		mEvent.add (*mDelMultipleBase1);
		mEvent.add (*mDelMultipleBase2);
		mEvent.add (*mDelMultipleDerived1);
		mEvent.add (*mDelMultipleDerived2);
		mEvent.add (*mDelMultiplePure1);
		mEvent.add (*mDelMultiplePure2);
		
		
		/* remove first base member */
		mEvent.remove (*mDelMultipleBase1);
		mEvent.raise (1);
		
		EXPECT_EQ (5, mEvent.getDelegates().size());
		EXPECT_EQ (0, mDummyMultiple.mBaseValue1);
		EXPECT_EQ (1, mDummyMultiple.mBaseValue2);
		EXPECT_EQ (1, mDummyMultiple.mDerivedValue1);
		EXPECT_EQ (1, mDummyMultiple.mDerivedValue2);
		EXPECT_EQ (1, mDummyMultiple.mPureValue1);
		EXPECT_EQ (1, mDummyMultiple.mPureValue2);
		
		
		/* remove second base member */
		mEvent.remove (*mDelMultipleBase2);
		mEvent.raise (2);
		
		EXPECT_EQ (4, mEvent.getDelegates().size());
		EXPECT_EQ (0, mDummyMultiple.mBaseValue1);
		EXPECT_EQ (1, mDummyMultiple.mBaseValue2);
		EXPECT_EQ (2, mDummyMultiple.mDerivedValue1);
		EXPECT_EQ (2, mDummyMultiple.mDerivedValue2);
		EXPECT_EQ (2, mDummyMultiple.mPureValue1);
		EXPECT_EQ (2, mDummyMultiple.mPureValue2);
		
		
		/* remove derived member */
		mEvent.remove (*mDelMultipleDerived1);
		mEvent.raise (3);
		
		EXPECT_EQ (3, mEvent.getDelegates().size());
		EXPECT_EQ (0, mDummyMultiple.mBaseValue1);
		EXPECT_EQ (1, mDummyMultiple.mBaseValue2);
		EXPECT_EQ (2, mDummyMultiple.mDerivedValue1);
		EXPECT_EQ (3, mDummyMultiple.mDerivedValue2);
		EXPECT_EQ (3, mDummyMultiple.mPureValue1);
		EXPECT_EQ (3, mDummyMultiple.mPureValue2);
		
		
		/* remove derived member keeping it virtual */
		mEvent.remove (*mDelMultipleDerived2);
		mEvent.raise (4);
		
		EXPECT_EQ (2, mEvent.getDelegates().size());
		EXPECT_EQ (0, mDummyMultiple.mBaseValue1);
		EXPECT_EQ (1, mDummyMultiple.mBaseValue2);
		EXPECT_EQ (2, mDummyMultiple.mDerivedValue1);
		EXPECT_EQ (3, mDummyMultiple.mDerivedValue2);
		EXPECT_EQ (4, mDummyMultiple.mPureValue1);
		EXPECT_EQ (4, mDummyMultiple.mPureValue2);
		
		
		/* remove pure member */
		mEvent.remove (*mDelMultiplePure1);
		mEvent.raise (5);
		
		EXPECT_EQ (1, mEvent.getDelegates().size());
		EXPECT_EQ (0, mDummyMultiple.mBaseValue1);
		EXPECT_EQ (1, mDummyMultiple.mBaseValue2);
		EXPECT_EQ (2, mDummyMultiple.mDerivedValue1);
		EXPECT_EQ (3, mDummyMultiple.mDerivedValue2);
		EXPECT_EQ (4, mDummyMultiple.mPureValue1);
		EXPECT_EQ (5, mDummyMultiple.mPureValue2);
		
		
		/* remove pure member keeping it virtual */
		mEvent.remove (*mDelMultiplePure2);
		mEvent.raise (6);
		
		EXPECT_EQ (0, mEvent.getDelegates().size());
		EXPECT_EQ (0, mDummyMultiple.mBaseValue1);
		EXPECT_EQ (1, mDummyMultiple.mBaseValue2);
		EXPECT_EQ (2, mDummyMultiple.mDerivedValue1);
		EXPECT_EQ (3, mDummyMultiple.mDerivedValue2);
		EXPECT_EQ (4, mDummyMultiple.mPureValue1);
		EXPECT_EQ (5, mDummyMultiple.mPureValue2);
	}
	
	
	
	/* test event raising */
	TEST_F (EventTest, RaiseEvent)
	{
		mEvent.add (*mDel1);
		mEvent.add (*mDel2);
		mEvent.add (*mDelClone1);
		mEvent.add (*mDelClone2);
		
		
		/* raise all */
		mEvent.raise (1);
		
		EXPECT_EQ (1, mDummy1.mValue1);
		EXPECT_EQ (1, mDummy1.mValue2);
		EXPECT_EQ (1, mDummy2.mValue1);
		EXPECT_EQ (1, mDummy2.mValue2);
		
		
		/* remove a delegate */
		mEvent.remove (*mDel1);
		mEvent.raise (2);
		
		EXPECT_EQ (1, mDummy1.mValue1);
		EXPECT_EQ (2, mDummy1.mValue2);
		EXPECT_EQ (2, mDummy2.mValue1);
		EXPECT_EQ (2, mDummy2.mValue2);
		
		
		/* remove multiple delegates */
		mEvent.remove (*mDel2);
		mEvent.remove (*mDelClone1);
		mEvent.raise (3);
		
		EXPECT_EQ (1, mDummy1.mValue1);
		EXPECT_EQ (2, mDummy1.mValue2);
		EXPECT_EQ (2, mDummy2.mValue1);
		EXPECT_EQ (3, mDummy2.mValue2);
		
		
		/* no delegates */
		mEvent.remove (*mDelClone2);
		mEvent.raise (4);
		
		EXPECT_EQ (1, mDummy1.mValue1);
		EXPECT_EQ (2, mDummy1.mValue2);
		EXPECT_EQ (2, mDummy2.mValue1);
		EXPECT_EQ (3, mDummy2.mValue2);
	}
	
	
	
	/* test event registration with operators */
	TEST_F (EventTest, Add_WithOperators)
	{
		/* add delegate */
		mEvent += *mDel1;
		EXPECT_EQ (1, mEvent.getDelegates().size());
		
		
		/* add without delegate */
		mEvent += delegate (&mDummy1, &EventTestDummy::test2);
		EXPECT_EQ (2, mEvent.getDelegates().size());
		
		
		/* add inline delegate */
		mEvent += Delegate<EventTestDummy, bool, int> (&mDummy2, &EventTestDummy::test1);
		EXPECT_EQ (3, mEvent.getDelegates().size());
	}
	
	
	
	/* test event unregistration with operators */
	TEST_F (EventTest, Remove_WithOperators)
	{
		mEvent.add (*mDel1);
		mEvent.add (*mDel2);
		mEvent.add (*mDelClone1);
		
		/* remove delegate */
		mEvent -= *mDel1;
		EXPECT_EQ (2, mEvent.getDelegates().size());
		
		
		/* remove without delegate */
		mEvent -= delegate (&mDummy1, &EventTestDummy::test2);
		EXPECT_EQ (1, mEvent.getDelegates().size());
		
		
		/* remove inline delegate */
		mEvent -= Delegate<EventTestDummy, bool, int> (&mDummy2, &EventTestDummy::test1);
		EXPECT_EQ (0, mEvent.getDelegates().size());
	}


}}
