
#include <fstream>

#include <gtest/gtest.h>

#include <Soma/Types.h>
#include <Soma/String.h>
#include <Soma/ConfigFile.h>


namespace Soma {
namespace Test {

// TODO: more thorough ConfigFile tests

	/* 
	 * @file ConfigFile
	 * 
	 * test
	 * {
	 * 		setting1 "test string"
	 * 		setting2 "true"
	 * 		setting3 "-1"
	 * 		setting4 "2"
	 * 		setting5 "3.5"
	 * }
	 * 
	 * @endfile
	 */



	/* test fixture for config file */
	class ConfigFileTest : public testing::Test
	{
	protected:
		std::stringstream mStream;
		ConfigFile mConfig;
		
		
		void SetUp ()
		{
			std::ifstream stream (__FILE__);
			
			/* get file length */
			stream.seekg (0, std::ios::end);
			long length = stream.tellg();
			stream.seekg (0, std::ios::beg);
			
			
			/* create buffer */
			char data[length];
			stream.read (&data[0], length);
			String buf = data;
			
			/* close stream */
			stream.close ();
			
			
			bool isFile = false;
			
			/* parse comments */
			StringList list = Strings::split (buf, "\n");
			
			
			/* loop through each line */
			for (int i = 0; i < list.size(); ++i)
			{
				/* remove whitespace */
				String line = list[i];
				Strings::trim (line);
				
				/* found comment */
				if (line[0] = '*')
				{
					StringList comment = Strings::split (line, " ");
					
					/* possible command */
					if (comment.size() >= 2)
					{
						String command = comment[1];
						
						/* start copying */
						if (command == "@file")
						{
							isFile = true;
							continue;
						}
						
						/* stop copying */
						else if (command == "@endfile")
							isFile = false;
					}
					
					
					/* copy contents to stream */
					if (isFile)
						mStream << line.substr (line.find_last_of ("*") + 1) << std::endl;
				}
			}
		}
		
	};
	
	
	
	/* test restore */
	TEST_F (ConfigFileTest, RestoreConfig)
	{
		/* restore test file in memory stream */
		EXPECT_EQ (true, mConfig.restore (mStream));
		
		/* test config values */
		EXPECT_EQ ("test string", mConfig.get ("test.setting1", ""));
		EXPECT_EQ (true, mConfig.get ("test.setting2", false));
		EXPECT_EQ (-1, mConfig.get ("test.setting3", 0));
		EXPECT_EQ (2, mConfig.get ("test.setting4", static_cast<uint>(0)));
		EXPECT_EQ (3.5f, mConfig.get ("test.setting5", 0.0f));
	}


}}
