
#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include <Soma/ConfigInterface.h>
#include <Soma/Visual/Manager.h>

#include "../MockConfig.h"


namespace Soma {
namespace Test {


	TEST (RendererTest, SaveSettings)
	{
		/* create renderer and mock configuration */
		Visual::Manager manager;
		MockConfig config;
		
		
		/* set renderer properties */
		manager.setRenderSystem (Visual::Manager::RENDER_SYSTEM_OPENGL);
		manager.setFullscreen (true);
		manager.setVSync (true);
		manager.setGammaConversion (true);
		manager.setWidth (100);
		manager.setHeight (100);
		manager.setFSAA (4);
		manager.setFrequency (60);
	}
	
	
	
	
	TEST (RendererTest, Initialisation)
	{
		Visual::Manager manager;
		
		EXPECT_EQ (true, manager.initialise ());
		EXPECT_EQ (true, manager.isInitialised());
		
//		EXPECT_EQ (true, manager.reinitialise ());
//		EXPECT_EQ (true, manager.isInitialised());
		
		manager.shutdown ();
		EXPECT_EQ (false, manager.isInitialised());
	}


}}
