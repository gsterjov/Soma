
#include <gtest/gtest.h>
#include <Soma/SmartPointer.h>


namespace Soma {
namespace Test {


	class TestClass : public ReferenceCounter
	{
	public:
		int val;
	};
	
	
	
	
	/* test smart pointer reference counting */
	TEST (SmartPointerTest, ReferenceCounting)
	{
		TestClass* test = new TestClass();
		smart_ptr<TestClass> ref (test);
		smart_ptr<TestClass> empty;
		
		
		/* check ref is sane */
		EXPECT_EQ (1, ref.getCount());
		EXPECT_TRUE (ref.isValid());
		
		
		/* add weak ref */
		weak_ptr<TestClass> weak (test);
		
		EXPECT_EQ (1, ref.getCount());
		EXPECT_TRUE (ref.isValid());
		
		EXPECT_EQ (1, weak.getCount());
		EXPECT_TRUE (weak.isValid());
		
		
		/* add another ref */
		smart_ptr<TestClass> ref2 (test);
		
		EXPECT_EQ (2, ref.getCount());
		EXPECT_TRUE (ref.isValid());
		
		EXPECT_EQ (2, ref2.getCount());
		EXPECT_TRUE (ref2.isValid());
		
		EXPECT_EQ (2, weak.getCount());
		EXPECT_TRUE (weak.isValid());
		
		
		
		/* modify value through ref */
		ref->val = 1;
		
		EXPECT_EQ (1, ref->val);
		EXPECT_EQ (1, ref2->val);
		EXPECT_EQ (1, weak->val);
		EXPECT_EQ (1, test->val);
		
		
		/* release ref */
		ref.release ();
		
		EXPECT_EQ (0, ref.getCount());
		EXPECT_FALSE (ref.isValid());
		
		EXPECT_EQ (1, ref2.getCount());
		EXPECT_TRUE (ref2.isValid());
		
		EXPECT_EQ (1, weak.getCount());
		EXPECT_TRUE (weak.isValid());
		
		
		
		/* change ref */
		smart_ptr<TestClass> ref3 (test);
		
		EXPECT_EQ (2, ref2.getCount());
		EXPECT_TRUE (ref2.isValid());
		
		EXPECT_EQ (2, weak.getCount());
		EXPECT_TRUE (weak.isValid());
		
		
		ref3 = empty;
		
		EXPECT_EQ (1, ref2.getCount());
		EXPECT_TRUE (ref2.isValid());
		
		EXPECT_EQ (1, weak.getCount());
		EXPECT_TRUE (weak.isValid());
		
		
		/* release all */
		ref2.release ();
		
		EXPECT_EQ (0, ref2.getCount());
		EXPECT_FALSE (ref2.isValid());
		
		EXPECT_EQ (0, weak.getCount());
		EXPECT_FALSE (weak.isValid());
	}
	
	
	
	
	/* test smart pointer copying */
	TEST (SmartPointerTest, Copying)
	{
		TestClass* test = new TestClass();
		smart_ptr<TestClass> ref (test);
		smart_ptr<TestClass> empty;
		
		
		/* copy strong to strong */
		smart_ptr<TestClass> strong1 = ref;
		
		EXPECT_EQ (2, ref.getCount());
		EXPECT_EQ (2, strong1.getCount());
		
		EXPECT_TRUE (strong1.isValid());
		EXPECT_EQ (test, strong1.getPointer());
		EXPECT_EQ (ref.getPointer(), strong1.getPointer());
		
		
		/* copy strong to weak */
		weak_ptr<TestClass> weak1 = ref;
		
		EXPECT_EQ (2, ref.getCount());
		EXPECT_EQ (2, weak1.getCount());
		
		EXPECT_TRUE (weak1.isValid());
		EXPECT_EQ (test, weak1.getPointer());
		EXPECT_EQ (ref.getPointer(), weak1.getPointer());
		
		
		/* copy weak to strong */
		smart_ptr<TestClass> strong2 = weak1;
		
		EXPECT_EQ (3, weak1.getCount());
		EXPECT_EQ (3, strong2.getCount());
		
		EXPECT_TRUE (strong2.isValid());
		EXPECT_EQ (test, strong2.getPointer());
		EXPECT_EQ (weak1.getPointer(), strong2.getPointer());
		
		
		/* copy weak to weak */
		weak_ptr<TestClass> weak2 = weak1;
		
		EXPECT_EQ (3, weak1.getCount());
		EXPECT_EQ (3, weak2.getCount());
		
		EXPECT_TRUE (weak2.isValid());
		EXPECT_EQ (test, weak2.getPointer());
		EXPECT_EQ (weak1.getPointer(), weak2.getPointer());
		
		
		/* copy empty to strong */
		smart_ptr<TestClass> strong3 = empty;
		
		EXPECT_EQ (0, empty.getCount());
		EXPECT_EQ (0, strong3.getCount());
		
		EXPECT_FALSE (strong3.isValid());
		EXPECT_EQ (empty.getPointer(), strong3.getPointer());
		
		
		/* copy empty to weak */
		weak_ptr<TestClass> weak3 = empty;
		
		EXPECT_EQ (0, empty.getCount());
		EXPECT_EQ (0, weak3.getCount());
		
		EXPECT_FALSE (weak3.isValid());
		EXPECT_EQ (empty.getPointer(), weak3.getPointer());
	}
	
	
	
	
	/* test smart pointer assigning */
	TEST (SmartPointerTest, Assigning)
	{
		TestClass* test = new TestClass();
		smart_ptr<TestClass> ref (test);
		smart_ptr<TestClass> empty;
		
		smart_ptr<TestClass> strong1, strong2, strong3;
		weak_ptr<TestClass> weak1, weak2, weak3;
		
		
		/* assign strong to strong */
		strong1 = ref;
		
		EXPECT_EQ (2, ref.getCount());
		EXPECT_EQ (2, strong1.getCount());
		
		EXPECT_TRUE (strong1.isValid());
		EXPECT_EQ (test, strong1.getPointer());
		EXPECT_EQ (ref.getPointer(), strong1.getPointer());
		
		
		/* assign strong to weak */
		weak1 = ref;
		
		EXPECT_EQ (2, ref.getCount());
		EXPECT_EQ (2, weak1.getCount());
		
		EXPECT_TRUE (weak1.isValid());
		EXPECT_EQ (test, weak1.getPointer());
		EXPECT_EQ (ref.getPointer(), weak1.getPointer());
		
		
		/* assign weak to strong */
		strong2 = weak1;
		
		EXPECT_EQ (3, weak1.getCount());
		EXPECT_EQ (3, strong2.getCount());
		
		EXPECT_TRUE (strong2.isValid());
		EXPECT_EQ (test, strong2.getPointer());
		EXPECT_EQ (weak1.getPointer(), strong2.getPointer());
		
		
		/* assign weak to weak */
		weak2 = weak1;
		
		EXPECT_EQ (3, weak1.getCount());
		EXPECT_EQ (3, weak2.getCount());
		
		EXPECT_TRUE (weak2.isValid());
		EXPECT_EQ (test, weak2.getPointer());
		EXPECT_EQ (weak1.getPointer(), weak2.getPointer());
		
		
		/* assign empty to empty strong */
		strong3 = empty;
		
		EXPECT_EQ (0, empty.getCount());
		EXPECT_EQ (0, strong3.getCount());
		
		EXPECT_FALSE (strong3.isValid());
		EXPECT_EQ (empty.getPointer(), strong3.getPointer());
		
		
		
		/* assign empty to empty weak */
		weak3 = empty;
		
		EXPECT_EQ (0, empty.getCount());
		EXPECT_EQ (0, weak3.getCount());
		
		EXPECT_FALSE (weak3.isValid());
		EXPECT_EQ (empty.getPointer(), weak3.getPointer());
		
		
		
		
		/* assign empty to existing strong */
		smart_ptr<TestClass> strong4 (test);
		strong4 = empty;
		
		EXPECT_EQ (0, empty.getCount());
		EXPECT_EQ (0, strong4.getCount());
		
		EXPECT_FALSE (strong4.isValid());
		EXPECT_EQ (empty.getPointer(), strong4.getPointer());
		
		
		/* assign empty to existing weak */
		weak_ptr<TestClass> weak4 (test);
		weak4 = empty;
		
		EXPECT_EQ (0, empty.getCount());
		EXPECT_EQ (0, weak4.getCount());
		
		EXPECT_FALSE (weak4.isValid());
		EXPECT_EQ (empty.getPointer(), weak4.getPointer());
	}


}}
