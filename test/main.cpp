
#include <iostream>
#include <gmock/gmock.h>
#include <Soma/Log.h>


int main (int argc, char** argv)
{
	std::cout << "Running Soma Tests" << std::endl;
	
	
	/* initiate and disable logging */
	Soma::Log* log = new Soma::Log ();
	log->setVerbosity (Soma::Log::NONE);
	
	
	/* start tests */
	testing::InitGoogleMock (&argc, argv);
	return RUN_ALL_TESTS ();
}

