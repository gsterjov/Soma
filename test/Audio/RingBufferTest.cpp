
#include <gtest/gtest.h>

#include <Soma/Audio/RingBuffer.h>


namespace Soma {
namespace Test {


	/* test ring buffer construction */
	TEST (RingBufferTest, Create)
	{
		Audio::RingBuffer<float> buffer (10);
		
		EXPECT_EQ (0, buffer.size());
		EXPECT_EQ (10, buffer.capacity());
		EXPECT_NE (0, buffer.max_size());
		EXPECT_TRUE (buffer.empty());
	}
	
	
	/* test insertion */
	TEST (RingBufferTest, AddItems)
	{
		Audio::RingBuffer<int> buffer (5);
		
		/* test add on empty buffer */
		EXPECT_TRUE (buffer.push_back (1));
		EXPECT_EQ (1, buffer.size());
		EXPECT_EQ (5, buffer.capacity());
		EXPECT_FALSE (buffer.empty());
		EXPECT_EQ (1, buffer.front());
		EXPECT_EQ (1, buffer.back());
		
		
		/* test add with non empty buffer */
		EXPECT_TRUE (buffer.push_back (2));
		EXPECT_EQ (2, buffer.size());
		EXPECT_EQ (5, buffer.capacity());
		EXPECT_FALSE (buffer.empty());
		EXPECT_EQ (1, buffer.front());
		EXPECT_EQ (2, buffer.back());
		
		
		/* test full buffer */
		EXPECT_TRUE (buffer.push_back (3));
		EXPECT_TRUE (buffer.push_back (4));
		EXPECT_TRUE (buffer.push_back (5));
		EXPECT_EQ (5, buffer.size());
		EXPECT_EQ (5, buffer.capacity());
		EXPECT_FALSE (buffer.empty());
		EXPECT_EQ (1, buffer.front());
		EXPECT_EQ (5, buffer.back());
		
		
		/* test overwriting buffer */
		EXPECT_FALSE (buffer.push_back (6));
		EXPECT_EQ (5, buffer.size());
		EXPECT_EQ (5, buffer.capacity());
		EXPECT_FALSE (buffer.empty());
		
		/* 1 should be overwriten and front pushed forward */
		EXPECT_EQ (2, buffer.front());
		EXPECT_EQ (6, buffer.back());
	}
	
	
	
	
	/* test removal */
	TEST (RingBufferTest, RemoveItems)
	{
		Audio::RingBuffer<int> buffer (5);
		
		buffer.push_back (1);
		buffer.push_back (2);
		buffer.push_back (3);
		buffer.push_back (4);
		buffer.push_back (5);
		
		
		/* pop data */
		buffer.pop_front ();
		EXPECT_EQ (4, buffer.size());
		EXPECT_EQ (5, buffer.capacity());
		EXPECT_FALSE (buffer.empty());
		EXPECT_EQ (2, buffer.front());
		EXPECT_EQ (5, buffer.back());
		
		/* pop the rest */
		buffer.pop_front ();
		EXPECT_EQ (3, buffer.size());
		EXPECT_EQ (3, buffer.front());
		EXPECT_EQ (5, buffer.back());
		
		buffer.pop_front ();
		EXPECT_EQ (2, buffer.size());
		EXPECT_EQ (4, buffer.front());
		EXPECT_EQ (5, buffer.back());
		
		buffer.pop_front ();
		EXPECT_EQ (1, buffer.size());
		EXPECT_EQ (5, buffer.front());
		EXPECT_EQ (5, buffer.back());
		
		
		/* pop last item */
		buffer.pop_front ();
		EXPECT_EQ (0, buffer.size());
		EXPECT_EQ (5, buffer.capacity());
		EXPECT_TRUE (buffer.empty());
		
		
		/* push items to empty buffer after removal */
		EXPECT_TRUE (buffer.push_back (6));
		EXPECT_EQ (1, buffer.size());
		EXPECT_EQ (5, buffer.capacity());
		EXPECT_FALSE (buffer.empty());
		EXPECT_EQ (6, buffer.front());
		EXPECT_EQ (6, buffer.back());
		
		
		/* push with existing item after removal */
		EXPECT_TRUE (buffer.push_back (7));
		EXPECT_TRUE (buffer.push_back (8));
		EXPECT_EQ (3, buffer.size());
		EXPECT_FALSE (buffer.empty());
		EXPECT_EQ (6, buffer.front());
		EXPECT_EQ (8, buffer.back());
		
		
		/* clear buffer */
		buffer.clear ();
		EXPECT_EQ (0, buffer.size());
		EXPECT_EQ (5, buffer.capacity());
		EXPECT_TRUE (buffer.empty());
	}
	
	
	
	
	/* test buffer indexing */
	TEST (RingBufferTest, Indexing)
	{
		Audio::RingBuffer<int> buffer (5);
		const Audio::RingBuffer<int>& const_buffer (buffer);
		
		
		
		/* run tests multiple times to test ring buffer index wrapping */
		for (int i = 0; i < 10; ++i)
		{
			
			/* empty buffer */
			EXPECT_THROW ({ buffer.at(0); }, std::out_of_range);
			EXPECT_THROW ({ buffer.at(1); }, std::out_of_range);
			EXPECT_THROW ({ const_buffer.at(1); }, std::out_of_range);
			EXPECT_THROW ({ const_buffer.at(1); }, std::out_of_range);
			
			
			/* non empty buffer */
			buffer.push_back (1);
			buffer.push_back (2);
			buffer.push_back (3);
			
			EXPECT_EQ (1, buffer.at(0)); EXPECT_EQ (1, const_buffer.at(0));
			EXPECT_EQ (2, buffer.at(1)); EXPECT_EQ (2, const_buffer.at(1));
			EXPECT_EQ (3, buffer.at(2)); EXPECT_EQ (3, const_buffer.at(2));
			
			EXPECT_EQ (1, buffer[0]); EXPECT_EQ (1, const_buffer[0]);
			EXPECT_EQ (2, buffer[1]); EXPECT_EQ (2, const_buffer[1]);
			EXPECT_EQ (3, buffer[2]); EXPECT_EQ (3, const_buffer[2]);
			
			
			/* change first value */
			EXPECT_EQ (1, buffer.front());
			buffer[0] = 4;
			EXPECT_EQ (4, buffer.front());
			
			EXPECT_EQ (4, buffer.at(0)); EXPECT_EQ (4, const_buffer.at(0));
			EXPECT_EQ (2, buffer.at(1)); EXPECT_EQ (2, const_buffer.at(1));
			EXPECT_EQ (3, buffer.at(2)); EXPECT_EQ (3, const_buffer.at(2));
			
			EXPECT_EQ (4, buffer[0]); EXPECT_EQ (4, const_buffer[0]);
			EXPECT_EQ (2, buffer[1]); EXPECT_EQ (2, const_buffer[1]);
			EXPECT_EQ (3, buffer[2]); EXPECT_EQ (3, const_buffer[2]);
			
			
			/* change middle value */
			buffer[1] = 5;
			
			EXPECT_EQ (4, buffer.at(0)); EXPECT_EQ (4, const_buffer.at(0));
			EXPECT_EQ (5, buffer.at(1)); EXPECT_EQ (5, const_buffer.at(1));
			EXPECT_EQ (3, buffer.at(2)); EXPECT_EQ (3, const_buffer.at(2));
			
			EXPECT_EQ (4, buffer[0]); EXPECT_EQ (4, const_buffer[0]);
			EXPECT_EQ (5, buffer[1]); EXPECT_EQ (5, const_buffer[1]);
			EXPECT_EQ (3, buffer[2]); EXPECT_EQ (3, const_buffer[2]);
			
			
			/* remove values */
			buffer.pop_front ();
			EXPECT_EQ (5, buffer.at(0)); EXPECT_EQ (5, const_buffer.at(0));
			EXPECT_EQ (3, buffer.at(1)); EXPECT_EQ (3, const_buffer.at(1));
			
			buffer.pop_front ();
			EXPECT_EQ (3, buffer.at(0)); EXPECT_EQ (3, const_buffer.at(0));
			
			
			buffer.clear ();
		}
	}


}}
