
#include <gtest/gtest.h>

#include <Soma/FileSystem/Manager.h>

#ifdef SOMA_SYSTEM_UNIX
	#include <Soma/FileSystem/PosixDirectory.h>
#endif

#include <iostream>


namespace Soma {
namespace Test {


	/* test posix directory listing */
	TEST (FileSystemTest, PosixListing)
	{
#ifdef SOMA_SYSTEM_UNIX
		using namespace Soma::FileSystem;
		
		/* get dir */
		PosixDirectory dir ("/devel/data");
		
		FileMap list = dir.getFiles();
		
		FileMap::iterator iter;
		
		for (iter = list.begin(); iter != list.end(); ++iter)
		{
//			std::cout << iter->second->getName() << std::endl;
		}
#endif
	}
	
	
	/* test file system listing */
	TEST (FileSystemTest, DirectoryListing)
	{
		using namespace Soma::FileSystem;
		
		/* get dir */
		Manager sys;
		
		sys.mount ("/devel/data");
		sys.mount ("/devel/source");
		sys.mount ("/home/sterjov");
		
		
		DirectoryMap list = sys.getDirectories();
		DirectoryMap::iterator iter;
		
		for (iter = list.begin(); iter != list.end(); ++iter)
		{
//			std::cout << iter->second->getName() << " - " << iter->second->getPath() << std::endl;
		}
		
		
		FileMap list2 = sys.getFiles();
		FileMap::iterator iter2;
		
		for (iter2 = list2.begin(); iter2 != list2.end(); ++iter2)
		{
//			std::cout << iter2->second->getName() << std::endl;
		}
	}


}}
