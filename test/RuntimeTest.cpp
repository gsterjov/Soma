

#include <iostream>

#include <Soma/Engine.h>
#include <Soma/Exception.h>

#include <Soma/Context.h>
#include <Soma/World.h>
#include <Soma/Entity.h>
#include <Soma/AssetEntity.h>

#include <Soma/FileSystem/Manager.h>


#include <Soma/Scripting/Lua/Material.h>
#include <Soma/Scripting/Lua/VisualMaterial.h>

#include <Soma/Input/Context.h>
#include <Soma/Input/TriggerAction.h>

#include <Soma/GUI/Gorilla.h>
#include <Soma/Visual/Viewport.h>
#include <Soma/Visual/RenderTarget.h>

#include <Ogre.h>
#include <OgreViewport.h>
#include <OgreRenderTarget.h>


namespace Soma {
namespace Test {


	class RuntimeTest
	{
	public:
		RuntimeTest ();
		~RuntimeTest ();
		
		void initialise();
		void run();
		
		
	private:
		Engine* mEngine;
		
		smart_ptr<Context> mContext;
		smart_ptr<World> mWorld;
		
		
		smart_ptr<Scripting::Lua::Material> mMaterial;
		
		
		Input::TriggerAction mQuit;
		
		
		Gorilla::Silverback* mGorilla;
		Gorilla::Screen*     mScreen;
		Gorilla::Layer*      mFPSLayer;
		Gorilla::Caption*    mFPS;
		
		float mCount;
		Ogre::RenderTarget* mTarget;
		
		
		/* setup functions */
		void createScene ();
		void createAudio ();
		void createInput ();
		void createGUI ();
		
		
		/* event handlers */
		void onTick (const TickEventArgs& args);
	};
	
	
	
	
	/**
	 * Constructor.
	 */
	RuntimeTest::RuntimeTest()
	{
		mEngine = new Engine ();
	}
	
	/**
	 * Destructor.
	 */
	RuntimeTest::~RuntimeTest ()
	{
		delete mEngine;
	}
	
	
	
	
	/**
	 * Bring up the entire scene.
	 */
	void RuntimeTest::initialise()
	{
		/* mount directories */
		FileSystem::Manager::instance().mount ("/devel/data");
		
		
		
		/* register events */
		mEngine->Tick += delegate (this, &RuntimeTest::onTick);
		
		
		/* initialise */
		if (!mEngine->initialise ())
			SOMA_EXCEPTION ("RuntimeTest", "Failed to initialise engine.");
		
		
		/* create context */
		mContext = new Context ("Main", "context.rendertarget.window,"
		                                "context.viewport,"
		                                "context.input");
		
		/* load context */
		if (!mContext->load())
			SOMA_EXCEPTION ("RuntimeTest", "Failed to initialise context");
		
		
		/* setup */
		createScene ();
		createInput ();
		
		
		
		Ogre::ResourceGroupManager::getSingleton().addResourceLocation ("/devel/data", "FileSystem");
		Ogre::ResourceGroupManager::getSingleton().initialiseAllResourceGroups();
		
		createGUI ();
	}
	
	
	
	/**
	 * Run the engine loop.
	 */
	void RuntimeTest::run()
	{
		mEngine->run();
	}
	
	
	
	
	/* setup scene components */
	void RuntimeTest::createScene ()
	{
		/* create world */
		mWorld = new World ("World", "world.visual");
		
		/* load world */
//		if (!mWorld->load())
//			SOMA_EXCEPTION ("RuntimeTest", "Failed to load world");
		
		
		
		/* create entities */
		smart_ptr<Entity> parent = new Entity ("Parent", "entity.visual.node");
		
		smart_ptr<Entity> camera = new Entity ("Camera", "entity.visual.node,"
		                                                 "entity.visual.camera");
		
		smart_ptr<Entity> light = new Entity ("Light", "entity.visual.node,"
		                                               "entity.visual.light");
		
//		smart_ptr<Entity> object = new Entity ("Object", "entity.visual.node,"
//		                                                 "entity.visual.mesh");
		
		smart_ptr<AssetEntity> object = new AssetEntity ("Object", "cube.dae");
		
		
		/* set object mesh resource */
//		object->setProperty ("entity.visual.mesh", "resource-path", String("cube.dae"));
		
		
		/* setup parent */
		parent->addChild (camera);
		parent->addChild (object);
		parent->addChild (light);
		
		
		/* add the parent to the world */
		mWorld->add (parent);
		
		
		/* attach camera to viewport so we can see something */
		mContext->setProperty ("context.viewport", "camera-entity", camera);
//		mContext->setProperty ("context.viewport", "background-colour", Colour(0, 0, 1));
		
		
		/* camera properties */
		camera->setProperty ("entity.visual.camera", "near-clip-distance", 0.1f);
		
		
		/* position entities */
		camera->translate (0, 2, 4);
		light->translate (1, 2, 1);
		
		
		
		if (!mWorld->load())
			SOMA_EXCEPTION ("RuntimeTest", "Failed to load world");
		
		
		/* set ambient light */
		mWorld->setProperty ("world.visual", "ambient-light", Colour(0, 0, 0, 0));
		
		
		/* set light */
		light->setProperty ("entity.visual.light", "type", String("directional"));
		light->setProperty ("entity.visual.light", "direction", Vector(-0.2, -0.2, -0.2));
		
		
		/* face camera */
		camera->setProperty ("entity.visual.camera", "look-at", Vector(0, 0, 0));
		
		
		
//		mMaterial = new Scripting::Lua::Material (String("/devel/data/material.lua"));
//		
//		assert (mMaterial->getVisualMaterial()->load ());
//		smart_ptr<Visual::Material> mat = mMaterial->getVisualMaterial();
//		
//		object->getChild("head")->setProperty ("entity.visual.mesh", "material", mat);
	}
	
	
	
	
	
	/* setup input components */
	void RuntimeTest::createInput ()
	{
		smart_ptr<Component> component = mContext->getComponent ("context.input");
		smart_ptr<Input::Context> ctx = ref_cast<Input::Context> (component);
		
		/* quit bindings */
		mQuit.setContext (ctx);
		
		mQuit.bind ("Keyboard/Escape");
		mQuit.bind ("Keyboard/Left Control", "Keyboard/Q");
	}
	
	
	
	
	
	/* setup gui components */
	void RuntimeTest::createGUI ()
	{
		smart_ptr<Component> component = mContext->getComponent ("context.viewport");
		smart_ptr<Visual::Viewport> viewport = ref_cast<Visual::Viewport> (component);
		
		
		mGorilla = new Gorilla::Silverback ();
		mGorilla->loadAtlas ("dejavu");
		
		mScreen = mGorilla->createScreen (viewport->getViewport(), "dejavu");
		
		mFPSLayer = mScreen->createLayer (14);
		mFPS = mFPSLayer->createCaption (14, 10, 10, "");
		
		mFPS->top (viewport->getViewport()->getActualHeight() - 25);
		
		
		
		smart_ptr<Component> com2 = mContext->getComponent ("context.rendertarget");
		smart_ptr<Visual::RenderTarget> target = ref_cast<Visual::RenderTarget> (com2);
		mTarget = target->getTarget();
	}
	
	
	
	
	/* game tick */
	void RuntimeTest::onTick (const TickEventArgs& args)
	{
		if (mQuit.isActive())
			mEngine->quit ();
		
		
		mCount += args.elapsed;
		
		if (mCount > 1)
		{
			std::stringstream stream;
			stream << "FPS: " << mTarget->getLastFPS() << "\n";
			
			mFPS->text (stream.str());
			mCount = 0;
		}
	}

}}




int main (int argc, char** argv)
{
	Soma::Test::RuntimeTest test;
	
	test.initialise();
	test.run();
}

