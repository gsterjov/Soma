

#include <Soma/Engine.h>
#include <Soma/Graphics/Window.h>
#include <Soma/Graphics/World.h>
#include <Soma/Graphics/Camera.h>

#include <Soma/Graphics/DeferredShading/System.h>

/* filesystem */
#include <Soma/FileSystem/FileSystem.h>

/* graphics */
#include <Soma/Graphics/MeshObject.h>
#include <Soma/Graphics/Material.h>
#include <Soma/Graphics/Light.h>
#include <Soma/Graphics/Node.h>

/* audio */
#include <Soma/Audio/Manager.h>
#include <Soma/Audio/Mixer.h>
#include <Soma/Audio/Source.h>
#include <Soma/Audio/Listener.h>

/* physics */
#include <Soma/Physics/Manager.h>
#include <Soma/Physics/World.h>
#include <Soma/Physics/RigidBody.h>
#include <Soma/Physics/RigidBodyObject.h>

/* input */
#include <Soma/Input/Manager.h>
#include <Soma/Input/TriggerAction.h>


#include <Soma/Object.h>

#include <Ogre.h>
#include <btBulletDynamicsCommon.h>


namespace Soma {
namespace Test {


	class RuntimeTest : public Engine::Listener
	{
	public:
		RuntimeTest ();
		
		void initialise();
		void run();
		
		
	private:
		Engine* mEngine;
		
		/* graphics components */
		Graphics::Window* mWindow;
		
		Graphics::WorldRef mWorld;
		Graphics::CameraRef mCamera;
		
		Graphics::NodeRef mCamNode;
		
		
		Graphics::DeferredShading* mSystem;
		
		
		/* audio */
		Audio::SourceRef mSource1;
		Audio::SourceRef mSource2;
		Audio::ListenerRef mListener;
		
		
		/* input */
		Input::Manager* mInput;
		Input::TriggerAction mQuit;
		
		Input::TriggerAction mForward;
		Input::TriggerAction mBack;
		Input::TriggerAction mLeft;
		Input::TriggerAction mRight;
		
		Input::TriggerAction mPlay;
		Input::TriggerAction mStop;
		
		
		void createAudio ();
		void createInput ();
		
		void onTick (const Engine::TickEventArgs& args);
	};
	
	
	
	
	/**
	 * Constructor.
	 */
	RuntimeTest::RuntimeTest()
	{
		mEngine = new Engine();
	}
	
	
	/**
	 * Create engine components.
	 */
	void RuntimeTest::initialise()
	{
		FileSystem::FileSystem::instance().mount ("/devel/");
		
		
		mEngine->initialise();
		mEngine->addListener (this);
		
		
		/* create window */
		mWindow = new Graphics::Window ("Soma Runtime Test");
		mWindow->create();
		
		
		/* create input system */
		mInput = new Input::Manager (mWindow, false);
		mInput->initialise ();
		
		createInput ();
		
		
		/* create world */
		mWorld = Graphics::WorldRef (new Graphics::World ());
		mWorld->getSceneManager()->setAmbientLight (Ogre::ColourValue::Blue);
		
		/* create camera */
		mCamera = Graphics::CameraRef (new Graphics::Camera (mWorld));
		
		
		/* add camera to window viewport */
		mWindow->addViewport (mCamera);
		
		
		
		/* add resource path */
#ifdef SOMA_UNIX
		Ogre::ResourceGroupManager::getSingleton().addResourceLocation ("/devel/data", "FileSystem");
#else
		Ogre::ResourceGroupManager::getSingleton().addResourceLocation ("C:\\devel\\data", "FileSystem");
#endif
		Ogre::ResourceGroupManager::getSingleton().initialiseAllResourceGroups ();
		
		
		
		/* enable deferred shading */
		Ogre::Viewport* vp = mWindow->getTarget()->getViewport(0);

		mSystem = new Graphics::DeferredShading (vp);
//		mSystem->initialise ();

		mWorld->getSceneManager()->setShadowTechnique (Ogre::SHADOWTYPE_NONE);



		/* create light */
		Graphics::LightRef light (new Graphics::Light(mWorld));
		light->getLight()->setType (Ogre::Light::LT_SPOTLIGHT);
		light->getLight()->setCastShadows (false);


		/* attach light to node */
		Graphics::NodeRef lightnode (new Graphics::Node (mWorld));
		lightnode->attach (light);
		mWorld->add (lightnode);
//		lightnode->setPosition (4, 6, 0.5);
		lightnode->setPosition (1, 2, 0.5);


		Ogre::Vector3 vec = Ogre::Vector3 (0, 0, 0) - lightnode->getPosition();
		vec.normalise();

		light->getLight()->setDirection (vec);


		/* attach camera to node */
		mCamNode = Graphics::NodeRef (new Graphics::Node (mWorld));
		mCamNode->attach (mCamera);
		mWorld->add (mCamNode);

		/* position camera */
//		camnode->setPosition (1, 1.5, 1.5);
//		mCamera->lookAt (-0.5, 0.5, -0.5);

		mCamNode->setPosition (2, 1, 4);
//		camnode->setPosition (0.8, 1.8, 0.3);
		mCamera->lookAt (0, 0, 0);

		mCamera->setNearClipDistance (0.1);



		/* create object */
		ObjectRef obj (new Object (mWorld, "cube.dae"));
		mWorld->add (obj->getNode());

		obj->getMeshObj()->getEntity()->setCastShadows (false);
//		obj->getMeshObj()->getEntity()->setMaterialName ("CubeMaterial");

		
		
		
		/* create audio */
		createAudio ();
		
		
//		/* create physics object */
//		Physics::WorldRef phyWorld (new Physics::World());
//		Physics::Manager::instance().add (phyWorld);
//
//		/* manual rigid body resource */
//		btCollisionShape* shape = new btBoxShape (btVector3 (0.5, 0.5, 0.5));
//		Physics::RigidBodyRef phyBody (new Physics::RigidBody (shape, 1));
//
//		/* create rigid body */
//		Physics::RigidBodyObjectRef body (new Physics::RigidBodyObject (phyWorld, phyBody));
//
//
//		/* add to world */
//		phyWorld->addRigidBody (body);
//
//		/* attach object */
////		body->attach (obj->getNode().getPointer());
	}
	
	
	
	/**
	 * Run the engine loop.
	 */
	void RuntimeTest::run()
	{
		mEngine->run();
	}
	
	
	
	
	/* create audio */
	void RuntimeTest::createAudio ()
	{
		mSource1 = Audio::SourceRef (new Audio::Source ("source1"));
		mSource2 = Audio::SourceRef (new Audio::Source ("source2"));
		mListener = Audio::ListenerRef (new Audio::Listener ("listener"));
	}
	
	
	
	/* create input */
	void RuntimeTest::createInput ()
	{
		/* quit bindings */
		mQuit.bind ("Keyboard/Escape");
		mQuit.bind ("Keyboard/Left Control", "Keyboard/Q");
		
		/* movement bindings */
		mForward.bind ("Keyboard/W");
		mBack.bind ("Keyboard/S");
		mLeft.bind ("Keyboard/A");
		mRight.bind ("Keyboard/D");
		
		mPlay.bind ("Keyboard/P");
		mStop.bind ("Keyboard/O");
	}
	
	
	
	
	/* game tick */
	void RuntimeTest::onTick (const Engine::TickEventArgs& args)
	{
		mInput->step ();
		
		
		if (mQuit.isActive())
			mEngine->quit ();
		
		
		float disp = 3 * args.elapsed;
		
		if (mForward.isActive()) mCamNode->translate (0, 0, -disp);
		if (mBack.isActive())    mCamNode->translate (0, 0, disp);
		if (mLeft.isActive())    mCamNode->translate (-disp, 0, 0);
		if (mRight.isActive())   mCamNode->translate (disp, 0, 0);
		
		
		if (mPlay.isActive() && !mSource1->isPlaying())
		{
			mSource1->play ("/devel/data/sample1.ogg");
			mSource2->play ("/devel/data/sample2.ogg");
		}
		
		if (mStop.isActive() && mSource1->isPlaying())
		{
			mSource1->stop ();
			mSource2->stop ();
		}
	}

}}



int main (int argc, char** argv)
{
	Soma::Test::RuntimeTest test;
	test.initialise();
	test.run();
}

