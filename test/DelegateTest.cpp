/*
 * DelegateTest.cpp
 *
 *  Created on: Jul 31, 2010
 *      Author: sterjov
 */

#include <gtest/gtest.h>
#include <Soma/Delegate.h>


namespace Soma {
namespace Test {


	/* dummy class for delegate member wrapping */
	class DelegateTestDummy
	{
	public:
		int test1 (int arg) { return arg; }
		int test2 (int arg) { return arg; }
	};
	
	
	
	class DelegateTestDummyVirtual
	{
	public:
				int base     (int arg) { return arg; }
		virtual int derived1 (int arg) { return -1; }
		virtual int derived2 (int arg) { return 01; }
		virtual int pure1    (int arg) = 0;
		virtual int pure2    (int arg) = 0;
	};
	
	
	
	class DelegateTestDummyDerived : public DelegateTestDummyVirtual
	{
	public:
				int derived1 (int arg) { return arg; }
		virtual int derived2 (int arg) { return arg; }
				int pure1    (int arg) { return arg; }
		virtual int pure2    (int arg) { return arg; }
	};
	
	
	
	class DelegateTestDummyMultipleA
	{
	public:
				int base1    (int arg) { return arg; }
		virtual int derived1 (int arg) { return -1; }
		virtual int pure2    (int arg) = 0;
	};
	
	
	class DelegateTestDummyMultipleB
	{
	public:
				int base2    (int arg) { return arg; }
		virtual int derived2 (int arg) { return -1; }
		virtual int pure1    (int arg) = 0;
	};
	
	
	
	class DelegateTestDummyMultiple : public DelegateTestDummyMultipleA,
								      public DelegateTestDummyMultipleB
	{
	public:
				int derived1 (int arg) { return arg; }
		virtual int derived2 (int arg) { return arg; }
				int pure1    (int arg) { return arg; }
		virtual int pure2    (int arg) { return arg; }
	};
	
	
	
	
	/* compare non-static member delegates */
	TEST (DelegateTest, Compare_MemberDelegates)
	{
		typedef Delegate<DelegateTestDummy, int, int> delType;
		
		DelegateTestDummy dummy;
		
		
		/* create delegates */
		delType del1 (&dummy, &DelegateTestDummy::test1);
		delType del2 (&dummy, &DelegateTestDummy::test2);
		delType delClone (&dummy, &DelegateTestDummy::test1);
		
		
		/* smae delegate reference */
		EXPECT_EQ (true, del1.equals (&del1));
		
		/* same callback, same object */
		EXPECT_EQ (true, del1.equals (&delClone));
		
		/* different callback, same object */
		EXPECT_EQ (false, del1.equals (&del2));
		
		
		/* create new object */
		DelegateTestDummy dummy_clone;
		
		delType delNew1 (&dummy_clone, &DelegateTestDummy::test1);
		delType delNew2 (&dummy_clone, &DelegateTestDummy::test2);
		
		
		/* same callback, different object */
		EXPECT_EQ (false, del1.equals (&delNew1));
		
		/* different callback, different object */
		EXPECT_EQ (false, del1.equals (&delNew2));
	}
	
	
	
	/* compare non-static member delegates */
	TEST (DelegateTest, Compare_VirtualDelegates_SingleInheritance)
	{
		typedef Delegate<DelegateTestDummyVirtual, int, int> delType;
		
		DelegateTestDummyDerived dummy;
		
		
		/* create delegates */
		delType base     (&dummy, &DelegateTestDummyVirtual::base);
		delType derived1 (&dummy, &DelegateTestDummyVirtual::derived1);
		delType derived2 (&dummy, &DelegateTestDummyVirtual::derived2);
		delType pure1    (&dummy, &DelegateTestDummyVirtual::pure1);
		delType pure2    (&dummy, &DelegateTestDummyVirtual::pure2);
		
		/* create clone delegates */
		delType base_clone     (&dummy, &DelegateTestDummyVirtual::base);
		delType derived1_clone (&dummy, &DelegateTestDummyVirtual::derived1);
		delType derived2_clone (&dummy, &DelegateTestDummyVirtual::derived2);
		delType pure1_clone    (&dummy, &DelegateTestDummyVirtual::pure1);
		delType pure2_clone    (&dummy, &DelegateTestDummyVirtual::pure2);
		
		
		/* smae delegate reference */
		EXPECT_EQ (true, base.equals (&base));
		EXPECT_EQ (true, derived1.equals (&derived1));
		EXPECT_EQ (true, derived2.equals (&derived2));
		EXPECT_EQ (true, pure1.equals (&pure1));
		EXPECT_EQ (true, pure2.equals (&pure2));
		
		
		/* same callback, same object, different delegate */
		EXPECT_EQ (true, base.equals (&base_clone));
		EXPECT_EQ (true, derived1.equals (&derived1_clone));
		EXPECT_EQ (true, derived2.equals (&derived2_clone));
		EXPECT_EQ (true, pure1.equals (&pure1_clone));
		EXPECT_EQ (true, pure2.equals (&pure2_clone));
		
		
		/* different callback, same object, same delegate */
		EXPECT_EQ (false, base.equals (&derived1));
		EXPECT_EQ (false, base.equals (&derived2));
		EXPECT_EQ (false, base.equals (&pure1));
		EXPECT_EQ (false, base.equals (&pure2));
		
		EXPECT_EQ (false, derived1.equals (&base));
		EXPECT_EQ (false, derived1.equals (&derived2));
		EXPECT_EQ (false, derived1.equals (&pure1));
		EXPECT_EQ (false, derived1.equals (&pure2));
		
		EXPECT_EQ (false, derived2.equals (&base));
		EXPECT_EQ (false, derived2.equals (&derived1));
		EXPECT_EQ (false, derived2.equals (&pure1));
		EXPECT_EQ (false, derived2.equals (&pure2));
		
		EXPECT_EQ (false, pure1.equals (&base));
		EXPECT_EQ (false, pure1.equals (&derived1));
		EXPECT_EQ (false, pure1.equals (&derived2));
		EXPECT_EQ (false, pure1.equals (&pure2));
		
		EXPECT_EQ (false, pure2.equals (&base));
		EXPECT_EQ (false, pure2.equals (&derived1));
		EXPECT_EQ (false, pure2.equals (&derived2));
		EXPECT_EQ (false, pure2.equals (&pure1));
		
		
		/* create new object */
		DelegateTestDummyDerived dummy_clone;
		
		delType base_new     (&dummy_clone, &DelegateTestDummyVirtual::base);
		delType derived1_new (&dummy_clone, &DelegateTestDummyVirtual::derived1);
		delType derived2_new (&dummy_clone, &DelegateTestDummyVirtual::derived2);
		delType pure1_new    (&dummy_clone, &DelegateTestDummyVirtual::pure1);
		delType pure2_new    (&dummy_clone, &DelegateTestDummyVirtual::pure2);
		
		
		/* same callback, different object */
		EXPECT_EQ (false, base.equals (&base_new));
		EXPECT_EQ (false, derived1.equals (&derived1_new));
		EXPECT_EQ (false, derived2.equals (&derived2_new));
		EXPECT_EQ (false, pure1.equals (&pure1_new));
		EXPECT_EQ (false, pure2.equals (&pure2_new));
	}
	
	
	
	/* compare virtual member delegates with multiple inheritance */
	TEST (DelegateTest, Compare_VirtualDelegates_MultipleInheritance)
	{
		typedef Delegate<DelegateTestDummyMultipleA, int, int> delTypeA;
		typedef Delegate<DelegateTestDummyMultipleB, int, int> delTypeB;
		
		DelegateTestDummyMultiple dummy;
		
		
		/* create delegates */
		delTypeA base1    (&dummy, &DelegateTestDummyMultipleA::base1);
		delTypeB base2    (&dummy, &DelegateTestDummyMultipleB::base2);
		delTypeA derived1 (&dummy, &DelegateTestDummyMultipleA::derived1);
		delTypeB derived2 (&dummy, &DelegateTestDummyMultipleB::derived2);
		delTypeB pure1    (&dummy, &DelegateTestDummyMultipleB::pure1);
		delTypeA pure2    (&dummy, &DelegateTestDummyMultipleA::pure2);
		
		/* create clone delegates */
		delTypeA base1_clone    (&dummy, &DelegateTestDummyMultipleA::base1);
		delTypeB base2_clone    (&dummy, &DelegateTestDummyMultipleB::base2);
		delTypeA derived1_clone (&dummy, &DelegateTestDummyMultipleA::derived1);
		delTypeB derived2_clone (&dummy, &DelegateTestDummyMultipleB::derived2);
		delTypeB pure1_clone    (&dummy, &DelegateTestDummyMultipleB::pure1);
		delTypeA pure2_clone    (&dummy, &DelegateTestDummyMultipleA::pure2);
		
		
		/* smae delegate reference */
		EXPECT_EQ (true, base1.equals (&base1));
		EXPECT_EQ (true, base2.equals (&base2));
		EXPECT_EQ (true, derived1.equals (&derived1));
		EXPECT_EQ (true, derived2.equals (&derived2));
		EXPECT_EQ (true, pure1.equals (&pure1));
		EXPECT_EQ (true, pure2.equals (&pure2));
		
		
		/* same callback, same object, different delegate */
		EXPECT_EQ (true, base1.equals (&base1_clone));
		EXPECT_EQ (true, base2.equals (&base2_clone));
		EXPECT_EQ (true, derived1.equals (&derived1_clone));
		EXPECT_EQ (true, derived2.equals (&derived2_clone));
		EXPECT_EQ (true, pure1.equals (&pure1_clone));
		EXPECT_EQ (true, pure2.equals (&pure2_clone));
		
		
		/* different callback, same object, same delegate */
		EXPECT_EQ (false, base1.equals (&base2));
		EXPECT_EQ (false, base1.equals (&derived1));
		EXPECT_EQ (false, base1.equals (&derived2));
		EXPECT_EQ (false, base1.equals (&pure1));
		EXPECT_EQ (false, base1.equals (&pure2));
		
		EXPECT_EQ (false, base2.equals (&base1));
		EXPECT_EQ (false, base2.equals (&derived1));
		EXPECT_EQ (false, base2.equals (&derived2));
		EXPECT_EQ (false, base2.equals (&pure1));
		EXPECT_EQ (false, base2.equals (&pure2));
		
		EXPECT_EQ (false, derived1.equals (&base1));
		EXPECT_EQ (false, derived1.equals (&base2));
		EXPECT_EQ (false, derived1.equals (&derived2));
		EXPECT_EQ (false, derived1.equals (&pure1));
		EXPECT_EQ (false, derived1.equals (&pure2));

		EXPECT_EQ (false, derived2.equals (&base1));
		EXPECT_EQ (false, derived2.equals (&base2));
		EXPECT_EQ (false, derived2.equals (&derived1));
		EXPECT_EQ (false, derived2.equals (&pure1));
		EXPECT_EQ (false, derived2.equals (&pure2));

		EXPECT_EQ (false, pure1.equals (&base1));
		EXPECT_EQ (false, pure1.equals (&base2));
		EXPECT_EQ (false, pure1.equals (&derived1));
		EXPECT_EQ (false, pure1.equals (&derived2));
		EXPECT_EQ (false, pure1.equals (&pure2));

		EXPECT_EQ (false, pure2.equals (&base1));
		EXPECT_EQ (false, pure2.equals (&base2));
		EXPECT_EQ (false, pure2.equals (&derived1));
		EXPECT_EQ (false, pure2.equals (&derived2));
		EXPECT_EQ (false, pure2.equals (&pure1));
		
		
		/* create new object */
		DelegateTestDummyMultiple dummy_clone;
		
		delTypeA base1_new    (&dummy_clone, &DelegateTestDummyMultipleA::base1);
		delTypeB base2_new    (&dummy_clone, &DelegateTestDummyMultipleB::base2);
		delTypeA derived1_new (&dummy_clone, &DelegateTestDummyMultipleA::derived1);
		delTypeB derived2_new (&dummy_clone, &DelegateTestDummyMultipleB::derived2);
		delTypeB pure1_new    (&dummy_clone, &DelegateTestDummyMultipleB::pure1);
		delTypeA pure2_new    (&dummy_clone, &DelegateTestDummyMultipleA::pure2);
		
		
		/* same callback, different object */
		EXPECT_EQ (false, base1.equals (&base1_new));
		EXPECT_EQ (false, base2.equals (&base2_new));
		EXPECT_EQ (false, derived1.equals (&derived1_new));
		EXPECT_EQ (false, derived2.equals (&derived2_new));
		EXPECT_EQ (false, pure1.equals (&pure1_new));
		EXPECT_EQ (false, pure2.equals (&pure2_new));
	}
	
	
	
	
	
	
	/* call non-static member delegates */
	TEST (DelegateTest, Call_MemberDelegates)
	{
		typedef Delegate<DelegateTestDummy, int, int> delType;
		
		DelegateTestDummy dummy;
		
		
		/* create delegates */
		delType test1 (&dummy, &DelegateTestDummy::test1);
		delType test2 (&dummy, &DelegateTestDummy::test2);
		
		
		/* call discrete delegates */
		EXPECT_EQ (1, test1.call (1));
		EXPECT_EQ (2, test2.call (2));
	}
	
	
	
	/* call virtual member delegates */
	TEST (DelegateTest, Call_VirtualDelegates_SingleInheritance)
	{
		typedef Delegate<DelegateTestDummyVirtual, int, int> delType;
		
		DelegateTestDummyDerived dummy;
		
		
		/* create delegates */
		delType base     (&dummy, &DelegateTestDummyVirtual::base);
		delType derived1 (&dummy, &DelegateTestDummyVirtual::derived1);
		delType derived2 (&dummy, &DelegateTestDummyVirtual::derived2);
		delType pure1    (&dummy, &DelegateTestDummyVirtual::pure1);
		delType pure2    (&dummy, &DelegateTestDummyVirtual::pure2);
		
		
		/* call discrete delegates */
		EXPECT_EQ (1, base.call (1));
		EXPECT_EQ (2, derived2.call (2));
		EXPECT_EQ (3, derived1.call (3));
		EXPECT_EQ (4, pure1.call (4));
		EXPECT_EQ (5, pure2.call (5));
	}
	
	
	
	/* call virtual member delegates with multiple inheritance */
	TEST (DelegateTest, Call_VirtualDelegates_MultipleInheritance)
	{
		typedef Delegate<DelegateTestDummyMultipleA, int, int> delTypeA;
		typedef Delegate<DelegateTestDummyMultipleB, int, int> delTypeB;
		
		DelegateTestDummyMultiple dummy;
		
		
		/* create delegates */
		delTypeA base1    (&dummy, &DelegateTestDummyMultipleA::base1);
		delTypeB base2    (&dummy, &DelegateTestDummyMultipleB::base2);
		delTypeA derived1 (&dummy, &DelegateTestDummyMultipleA::derived1);
		delTypeB derived2 (&dummy, &DelegateTestDummyMultipleB::derived2);
		delTypeB pure1    (&dummy, &DelegateTestDummyMultipleB::pure1);
		delTypeA pure2    (&dummy, &DelegateTestDummyMultipleA::pure2);
		
		
		/* call discrete delegates */
		EXPECT_EQ (1, base1.call (1));
		EXPECT_EQ (2, base2.call (2));
		EXPECT_EQ (3, derived2.call (3));
		EXPECT_EQ (4, derived1.call (4));
		EXPECT_EQ (5, pure1.call (5));
		EXPECT_EQ (6, pure2.call (6));
	}


}}
