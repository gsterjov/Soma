
#include "Soma/Asset.h"

#include "Soma/Log.h"


namespace Soma
{

	/* log source */
	const String Asset::LogSource = "Asset";
	
	/* resource type */
	const String Asset::Type = "asset";
	
	
	
	
	/* load the asset resource */
	bool Asset::load ()
	{
		ResourceList::iterator iter;
		
		/* load all resources */
		for (iter = mResources.begin(); iter != mResources.end(); ++iter)
		{
			/* resource failed to load */
			if (!iter->resource->load ())
				return Log::Error (LogSource, "Cannot load the asset because "
						"it failed to load the resource: " + iter->name);
		}
		
		return true;
	}
	
	
	
	/* reload the asset resource */
	bool Asset::reload ()
	{
		ResourceList::iterator iter;
		
		/* load all resources */
		for (iter = mResources.begin(); iter != mResources.end(); ++iter)
		{
			/* resource failed to load */
			if (!iter->resource->reload ())
				return Log::Error (LogSource, "Cannot reload the asset because "
						"it failed to reload the resource: " + iter->name);
		}
		
		return true;
	}
	
	
	
	/* unload the asset resource */
	void Asset::unload ()
	{
		ResourceList::iterator iter;
		
		/* load all resources */
		for (iter = mResources.begin(); iter != mResources.end(); ++iter)
			iter->resource->unload ();
	}
	
	
	
	/* get asset resource size */
	size_t Asset::getSize () const
	{
		size_t size = 0;
		return size;
	}

}
