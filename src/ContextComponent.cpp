
#include "Soma/ContextComponent.h"

#include "Soma/Context.h"


namespace Soma
{

	/* get the component owner */
	weak_ptr<Composite> ContextComponent::getOwner() const
	{
		return mOwner;
	}
	
	
	
	/* set the component owner by down casting the composite reference to
	 * a context if the type matches. By doing so we allow all context
	 * components to freely reference their owner without typecasting each
	 * and every time a component is added. */
	void ContextComponent::setOwner (const weak_ptr<Composite>& owner)
	{
		if (owner == mOwner) return;
		
		/* unload before we change owners */
		bool loaded = isLoaded();
		unload ();
		
		
		/* set new owner */
		mOwner.release();
		if (owner) mOwner = ref_cast<Context> (owner);
		
		
		/* load again */
		if (loaded) load ();
	}

}
