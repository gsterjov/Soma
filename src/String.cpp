
#include "Soma/String.h"

#include <algorithm>
#include <cctype>
#include <sstream>


namespace Soma
{

	/**
	 * Trims the whitespace at the start and end of the specified string.
	 * This will effectively remove spaces, tabs and newlines from the string.
	 * This works on the given string, it does not make a local copy.
	 * 
	 * @param str The string to trim.
	 * @return The same string converted to lowercase.
	 */
	String& Strings::trim (String& str)
	{
		str.erase (0, str.find_first_not_of (" \t\n"));
		str.erase (str.find_last_not_of (" \t\n") + 1);
		return str;
	}
	
	
	
	/**
	 * Converts all uppercase characters of the string to its lowercase
	 * equivalent. This works on the given string, it does not make a local
	 * copy.
	 * 
	 * @param str The string to convert.
	 * @return The same string converted to lowercase.
	 */
	String& Strings::toLower (String& str)
	{
		std::transform (str.begin(), str.end(), str.begin(), tolower);
		return str;
	}
	
	
	/**
	 * Converts all lowercase characters of the string to its uppercase
	 * equivalent. This works on the given string, it does not make a local
	 * copy.
	 * 
	 * @param str The string to convert.
	 * @return The same string converted to lowercase.
	 */
	String& Strings::toUpper (String& str)
	{
		std::transform (str.begin(), str.end(), str.begin(), toupper);
		return str;
	}
	
	
	
	/**
	 * Splits a given string at the specified delimiter.
	 * 
	 * If there are no characters between delimiters (as would be the case if
	 * two or more delimiters are found one after the other) then it will be
	 * ignored. This is handy as it will on return real string splits that
	 * have content as opposed to empty splits which provide little to no
	 * benefit. If no delimiters are found or the string is comprised entirely
	 * of delimiters then an empty list will be returned.
	 * 
	 * @param str The string to split.
	 * @param delimiters The characters to split that string at.
	 * 
	 * @return A list of strings split from the given string.
	 */
	StringList Strings::split (const String& str, const String& delimiters)
	{
		StringList list;
		
		/* index variables */
		String::size_type pos = 0;
		String::size_type start = 0;
		
		
		while (pos != String::npos)
		{
			/* find first occurence of a delimiter */
			pos = str.find_first_of (delimiters, start);
			
			
			/* we got nowhere */
			if (pos == start) ++start;
			
			/* add token to the list */
			else if (pos != String::npos)
				list.push_back (str.substr (start, pos - start));
			
			/* add the rest if a delimiter was found */
			else if (start > 0) list.push_back (str.substr (start));
			
			
			/* find the start of the next token */
			start = str.find_first_not_of (delimiters, pos + 1);
		}
		
		
		return list;
	}
	
	
	
	
	/**
	 * Converts a given string into an integer.
	 * 
	 * @param str The string to convert.
	 * @return The integer interpreted from the string.
	 */
	int Strings::toInt (const String& str)
	{
		int val;
		std::istringstream stream (str);
		
		stream >> val;
		return val;
	}
	
	
	/**
	 * Converts a given string into an unsigned integer.
	 * 
	 * @param str The string to convert.
	 * @return The unsigned integer interpreted from the string.
	 */
	uint Strings::toUInt (const String& str)
	{
		uint val;
		std::istringstream stream (str);
		
		stream >> val;
		return val;
	}
	
	
	/**
	 * Converts a given string of "true", "yes" and "on" into a boolean.
	 * 
	 * @param str The string to convert.
	 * @return The boolean interpreted from the string.
	 */
	bool Strings::toBool (const String& str)
	{
		String val = str;
		trim (val);
		toLower (val);
		
		if (val == "true" || val == "yes" || val == "on")
			return true;
		
		return false;
	}
	
	
	/**
	 * Converts a given string into a float.
	 * 
	 * @param str The string to convert.
	 * @return The float interpreted from the string.
	 */
	float Strings::toFloat (const String& str)
	{
		float val;
		std::istringstream stream (str);
		
		stream >> val;
		return val;
	}

}
