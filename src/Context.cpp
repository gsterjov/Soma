
#include "Soma/Context.h"

#include "Soma/Log.h"
#include "Soma/Registry.h"


namespace Soma
{

	/* log source */
	const String Context::LogSource = "Context";
	
	
	
	/* named constructor */
	Context::Context (const String& name) : mName(name)
	{
		Log::Debug (LogSource, "Creating context: " + mName);
	}
	
	
	/* constructor with components */
	Context::Context (const String& name, const String& types) : mName (name)
	{
		Log::Debug (LogSource, "Creating context: " + mName);
		
		addComponents (types);
	}
	
	
	/* destructor */
	Context::~Context ()
	{
		Log::Debug (LogSource, "Destroying context: " + mName);
	}


}
