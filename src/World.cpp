
#include "Soma/World.h"

#include "Soma/Log.h"
#include "Soma/Registry.h"
#include "Soma/Entity.h"


namespace Soma
{

	/* log source */
	const String World::LogSource = "World";
	
	
	
	/* named constructor */
	World::World (const String& name) : mName(name)
	{
		Log::Debug (LogSource, "Creating world: " + mName);
	}
	
	
	/* constructor with components */
	World::World (const String& name, const String& types) : mName(name)
	{
		Log::Debug (LogSource, "Creating world: " + mName);
		
		addComponents (types);
	}
	
	
	/* when the world is destroyed we have to unload all its entities since
	 * it is no longer valid without a world to operate in. we dont destory the
	 * entities because they can still be used simply by adding them to another
	 * world */
	World::~World ()
	{
		Log::Debug (LogSource, "Destroying world: " + mName);
		
		unload ();
		
		
		EntityMap::iterator iter;
		
		/* remove world from entities */
		for (iter = mEntities.begin(); iter != mEntities.end(); ++iter)
			iter->second->setWorld (0);
	}
	
	
	
	/* get entity */
	smart_ptr<Entity> World::getEntity (const String& name) const
	{
		/* look for entity */
		EntityMap::const_iterator iter = mEntities.find (name);
		
		if (iter != mEntities.end()) return iter->second;
		else return 0;
	}
	
	
	
	/* get all entities */
	EntityList World::getEntities() const
	{
		EntityList list;;
		EntityMap::const_iterator iter;
		
		/* add entities to list */
		for (iter = mEntities.begin(); iter != mEntities.end(); ++iter)
			list.push_back (iter->second);
		
		return list;
	}
	
	
	
	
	/* add entity */
	void World::add (const smart_ptr<Entity>& entity)
	{
		Log::Trace (LogSource, "Adding entity: " + entity->getName());
		
		
		/* set entity world and add to store */
		entity->setWorld (this);
		mEntities[entity->getName()] = entity;
		
		
		/* load entity if world is loaded */
		if (isLoaded())
		{
			if (!entity->load ())
				Log::Error (LogSource, "Failed to load entity '" +
						entity->getName() + "' in the world: " + mName);
		}
		
		EntityAdded.raise (entity);
	}
	
	
	
	
	/* remove entity */
	void World::remove (const smart_ptr<Entity>& entity)
	{
		remove (entity->getName());
	}
	
	
	/* remove entity by name */
	void World::remove (const String& name)
	{
		EntityMap::iterator iter = mEntities.find (name);
		
		/* found entity */
		if (iter != mEntities.end())
		{
			Log::Trace (LogSource, "Removing entity: " + name);
			
			iter->second->unload ();
			iter->second->setWorld (0);
			
			EntityRemoved.raise (iter->second);
			mEntities.erase (iter);
		}
		
		/* not found */
		else
			Log::Error (LogSource, "Failed to remove entity '" + name + "' "
					"because it cannot be found within the world: " + mName);
	}
	
	
	
	
	/* load the world composite by loading all root entities */
	bool World::load ()
	{
		Log::Debug (LogSource, "Loading world entities: " + mName);
		
		if (mLoaded) return true;
		
		
		/* load components first */
		if (!loadComponents ())
			return false;
		
		
		EntityMap::iterator iter;
		
		/* load all entities */
		for (iter = mEntities.begin(); iter != mEntities.end(); ++iter)
		{
			/* failed to load root entity */
			if (!iter->second->load ())
			{
				Log::Error (LogSource, "Failed to load root entity '" +
						iter->second->getName() + "' in the world: " + mName);
				
				mLoaded = false;
				break;
			}
		}
		
		
		/* unload all loaded root entities */
		if (!mLoaded) unload ();
		else Loaded.raise (this);
		
		return mLoaded;
	}
	
	
	/* reload the world composite by reloading all root entities */
	bool World::reload ()
	{
		Log::Debug (LogSource, "Reloading world entities: " + mName);
		
		
		/* reload components first */
		if (!reloadComponents ())
		{
			unload ();
			return false;
		}
		
		
		EntityMap::iterator iter;
		
		/* reload all entities */
		for (iter = mEntities.begin(); iter != mEntities.end(); ++iter)
		{
			/* failed to reload root entity */
			if (!iter->second->reload ())
			{
				Log::Error (LogSource, "Failed to reload root entity '" +
						iter->second->getName() + "' in the world: " + mName);
				
				mLoaded = false;
				break;
			}
		}
		
		
		/* unload all loaded root entities */
		if (!mLoaded) unload ();
		else Reloaded.raise (this);
		
		return mLoaded;
	}
	
	
	/* unload the world composite by unloading all root entities */
	void World::unload ()
	{
		Log::Debug (LogSource, "Unloading world entities: " + mName);
		
		
		EntityMap::iterator iter;
		
		/* unload all entities */
		for (iter = mEntities.begin(); iter != mEntities.end(); ++iter)
			iter->second->unload ();
		
		unloadComponents ();
		Unloaded.raise (this);
	}

}
