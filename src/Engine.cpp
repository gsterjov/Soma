
#include "Soma/Engine.h"

#include "Soma/Log.h"
#include "Soma/Timer.h"
#include "Soma/SubSystem.h"
#include "Soma/Registry.h"

#include "Soma/Context.h"
#include "Soma/World.h"

#include "Soma/Asset.h"
#include "Soma/FileSystem/Manager.h"


namespace Soma
{

	/* constructor */
	Engine::Engine ()
	: mInitialised (false),
	  mRunning (false),
	  mThrottle (0.015)
	{
		/* get existing log or create on if none exist */
		mLog = Log::instancePtr() ? Log::instancePtr() : new Log ();
		
		
		new AssetManager ();
		
		
		Log::Message ("Engine", "Creating core subsystems");
		
		/* create core systems */
		new FileSystem::Manager ();
		
		
		Log::Message ("Engine", "Creating engine subsystems");
		
		/* get all available subsystems */
		Registry<SubSystem>::TypeMap::iterator iter;
		Registry<SubSystem>::TypeMap types = Registry<SubSystem>::getTypes();
		
		/* log registered subsystems */
		for (iter = types.begin(); iter != types.end(); ++iter)
			Log::Message ("Engine", "Found subsystem: " + iter->first);
		
		/* create subsystems and add to the list */
		for (iter = types.begin(); iter != types.end(); ++iter)
			mSubSystems.push_back (iter->second->create());
		
		
		
		/* create game loop timer */
		mTimer = new Timer ();
	}
	
	
	/* destructor */
	Engine::~Engine ()
	{
		Log::Message ("Engine", "Shutting down engine subsystems");
		
		/* make sure we are shut down */
		if (mInitialised) shutdown ();
		
		
		std::vector<SubSystem*>::iterator iter;
		
		/* free all subsystems */
		for (iter = mSubSystems.begin(); iter != mSubSystems.end(); ++iter)
			delete *iter;
		
		
		/* destroy core systems */
		delete FileSystem::Manager::instancePtr();
		
		delete mLog;
		delete mTimer;
	}
	
	
	
	/* initialise engine */
	bool Engine::initialise ()
	{
		Log::Message ("Engine", "Initialising engine");
		
		
		mInitialised = true;
		std::vector<SubSystem*>::iterator iter;
		
		/* initialise all subsystems */
		for (iter = mSubSystems.begin(); iter != mSubSystems.end(); ++iter)
		{
			if (!(*iter)->initialise ())
			{
				Log::Error ("Engine", "Failed to initialise subsystem: ");
				
				mInitialised = false;
				break;
			}
		}
		
		
		/* shutdown all initialised subsystems if we failed */
		if (!mInitialised) shutdown ();
		return mInitialised;
	}
	
	
	
	/* reinitialise engine */
	bool Engine::reinitialise ()
	{
		Log::Message ("Engine", "Reinitialising engine");
		
		if (!mInitialised)
			return Log::Error ("Engine", "Engine must first be initialised "
					"before it can be reinitialised");
		
		
		std::vector<SubSystem*>::iterator iter;
		
		/* reinitialise all subsystems */
		for (iter = mSubSystems.begin(); iter != mSubSystems.end(); ++iter)
		{
			Log::Error ("Engine", "Failed to reinitialise subsystem: ");
			
			if (!(*iter)->reinitialise ())
				mInitialised = false;
		}
		
		
		/* provide new delta times by resetting */
		mTimer->reset ();
		
		/* shutdown all initialised subsystems if we failed */
		if (!mInitialised) shutdown ();
		return mInitialised;
	}
	
	
	
	/* shutdown engine */
	void Engine::shutdown ()
	{
		Log::Message ("Engine", "Shutting down engine");
		
		
		std::vector<SubSystem*>::iterator iter;
		
		/* shutdown all subsystems */
		for (iter = mSubSystems.begin(); iter != mSubSystems.end(); ++iter)
		{
			if ((*iter)->isInitialised())
				(*iter)->shutdown ();
		}
		
		
		mInitialised = false;
	}
	
	
	
	/* start the game loop */
	void Engine::run ()
	{
		Log::Debug ("Engine", "Starting game loop");
		
		assert (mInitialised);
		
		mRunning = true;
		
		/* the game loop */
		while (mRunning)
			step ();
	}
	
	
	/* stop the game loop */
	void Engine::quit ()
	{
		Log::Debug ("Engine", "Stopping game loop");
		
		assert (mInitialised);
		mRunning = false;
	}
	
	
	
	/* process one frame */
	void Engine::step ()
	{
		assert (mInitialised);
		
		/* create tick event */
		TickEventArgs ev;
		ev.elapsed = mTimer->getSeconds (true);
		
		/* throttle rendering */
		if (ev.elapsed < mThrottle)
			ev.elapsed += mTimer->sleep (mThrottle - ev.elapsed);
		
		
		std::vector<SubSystem*>::iterator iter;
		
		/* synchronise all subsystems */
		for (iter = mSubSystems.begin(); iter != mSubSystems.end(); ++iter)
			(*iter)->step ();
		
		
		/* raise the tick event */
		Tick.raise (ev);
	}
	
	
	
	
	/* mount resource path */
	void Engine::mount (const String& path)
	{
		FileSystem::Manager::instance().mount (path);
	}
	
	
	/* unmount resource path */
	void Engine::unmount (const String& path)
	{
		FileSystem::Manager::instance().unmount (path);
	}
	
	
	
	
	
	/* create engine context */
	Context* Engine::createContext (const String& name)
	{
		/* context name already exists */
		if (mContexts.find (name) != mContexts.end())
			SOMA_EXCEPTION ("Engine", "An engine context by the name of '" +
					name + " already exists");
		
		Context* ctx = new Context (name);
		mContexts[name] = ctx;
		return ctx;
	}
	
	
	
	/* destroy engine context by name */
	void Engine::destroyContext (const String& name)
	{
		ContextMap::iterator iter = mContexts.find (name);
		
		if (iter != mContexts.end())
		{
			delete iter->second;
			mContexts.erase (iter);
		}
	}
	
	
	
	/* destroy engine context */
	void Engine::destroyContext (Context* context)
	{
		destroyContext (context->getName());
	}
	
	
	
	/* get engine context by name */
	Context* Engine::getContext (const String& name) const
	{
		ContextMap::const_iterator iter = mContexts.find (name);
		return iter != mContexts.end() ? iter->second : NULL;
	}
	
	
	
	
	
	/* create world */
	World* Engine::createWorld (const String& name)
	{
		/* world name already exists */
		if (mWorlds.find (name) != mWorlds.end())
			SOMA_EXCEPTION ("Engine", "A world by the name of '" +
					name + " already exists");
		
		World* world = new World (name);
		mWorlds[name] = world;
		return world;
	}
	
	
	
	/* destroy world by name */
	void Engine::destroyWorld (const String& name)
	{
		WorldMap::iterator iter = mWorlds.find (name);
		
		if (iter != mWorlds.end())
		{
			delete iter->second;
			mWorlds.erase (iter);
		}
	}
	
	
	
	/* destroy world */
	void Engine::destroyWorld (World* world)
	{
		destroyWorld (world->getName());
	}
	
	
	
	/* get world by name */
	World* Engine::getWorld (const String& name) const
	{
		WorldMap::const_iterator iter = mWorlds.find (name);
		return iter != mWorlds.end() ? iter->second : NULL;
	}

}
