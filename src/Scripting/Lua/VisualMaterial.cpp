
#include "Soma/Scripting/Lua/VisualMaterial.h"


extern "C" {
#include "lua.h"
}


#include <OgreMaterial.h>
#include <OgreTechnique.h>
#include <OgrePass.h>

#include "Soma/Log.h"
#include "Soma/Registry.h"
#include "Soma/Exception.h"
#include "Soma/Scripting/Lua/Manager.h"

#include "Soma/Visual/Material.h"
#include "Soma/Visual/Technique.h"
#include "Soma/Visual/Pass.h"


namespace Soma {
namespace Scripting {
namespace Lua {


	/* log source */
	String VisualMaterial::LogSource = "Scripting::Lua::VisualMaterial";
	
	
	/* register format type */
	REGISTER_TYPE (Visual::MaterialManager::Format, MaterialFormat, "Lua Material");
	
	
	
	
	/* constructor */
	VisualMaterial::VisualMaterial (const Vri& vri) : mVri(vri)
	{
		clear ();
	}
	
	
	/* name constructor */
	VisualMaterial::VisualMaterial (const String& name) : Visual::Material(name)
	{
		clear ();
	}
	
	
	/* destructor */
	VisualMaterial::~VisualMaterial ()
	{
		
	}
	
	
	
	
	/* load the material by reading the lua table and filling in the visual
	 * material class */
	bool VisualMaterial::loadMaterial (lua_State* state)
	{
		Log::Debug (LogSource, "Loading visual Lua material");
		
		
		/* invalid technique */
		if (!lua_istable (state, -1))
		{
			return Log::Error (LogSource, "Cannot load the visual material "
					"because the stack does not contain a material table");
		}
		
		
		lua_pushnil (state);
		
		/* get all techniques */
		while (lua_next (state, -2))
		{
			/* get key value */
			if (lua_type (state, -2) == LUA_TSTRING)
			{
				String name = lua_tostring (state, -2);
				getTechnique (state, name);
			}
			
			/* remove value but keep key iterator */
			lua_pop (state, 1);
		}
		
		
		/* chain up */
		return true;
	}
	
	
	
	
	
	/* gets a colour from the table at the top of the stack */
	Colour VisualMaterial::getColour (lua_State* state)
	{
		/* temporary colour array */
		float col[4] = {0, 0, 0, 1};
		
		
		/* get table size */
		int len = lua_objlen (state, -1);
		if (len > 4) len = 4;
		
		
		/* get all components */
		for (int i = 0; i < len; ++i)
		{
			/* get component at index. Lua indexes start at 1 */
			lua_pushinteger (state, i+1);
			lua_gettable (state, -2);
			
			/* invalid component value */
			if (!lua_isnumber (state, -1))
			{
				Log::Error (LogSource, "Failed to get colour as component '" +
						Strings::convert(i) + "' is an invalid value");
				
				lua_pop (state, 1);
				return Colour ();
			}
			
			/* store colour component */
			col[i] = lua_tonumber (state, -1);
			lua_pop (state, 1);
		}
		
		
		return Colour (col[0], col[1], col[2], col[3]);
	}
	
	
	
	
	/* get the technique table at the top of the stack */
	void VisualMaterial::getTechnique (lua_State* state, const String& name)
	{
		/* invalid technique */
		if (!lua_istable (state, -1))
		{
			Log::Error(LogSource, "Technique '" + name + "' is not a valid Lua table");
			return;
		}
		
		
		/* create technique */
		smart_ptr<Visual::Technique> technique = add (name);
		
		
		/* get all passes */
		lua_pushnil (state);
		
		while (lua_next (state, -2))
		{
			/* get key value */
			if (lua_type (state, -2) == LUA_TSTRING)
			{
				String name = lua_tostring (state, -2);
				getPass (state, name, technique);
			}
			
			/* remove value but keep key iterator */
			lua_pop (state, 1);
		}
	}
	
	
	
	
	/* get the pass table at the top of the stack */
	void VisualMaterial::getPass (lua_State* state,
	                              const String& name,
	                              smart_ptr<Visual::Technique>& technique)
	{
		/* invalid pass */
		if (!lua_istable (state, -1))
		{
			Log::Error(LogSource, "Pass '" + name + "' is not a valid Lua table");
			return;
		}
		
		
		/* create pass */
		smart_ptr<Visual::Pass> pass = technique->add (name);
		
		
		/* lighting */
		lua_getfield (state, -1, "lighting");
		
		if (lua_isboolean (state, -1))
		{
			bool enabled = lua_toboolean (state, -1);
			pass->setLighting (enabled);
		}
		
		lua_pop (state, 1);
		
		
		
		/* ambient */
		lua_getfield (state, -1, "ambient");
		
		if (lua_istable (state, -1))
			pass->setAmbient (getColour (state));
		
		lua_pop (state, 1);
		
		
		/* diffuse */
		lua_getfield (state, -1, "diffuse");
		
		if (lua_istable (state, -1))
			pass->setDiffuse (getColour (state));
		
		lua_pop (state, 1);
		
		
		/* specular */
		lua_getfield (state, -1, "specular");
		
		if (lua_istable (state, -1))
			pass->setSpecular (getColour (state));
		
		lua_pop (state, 1);
		
		
		/* emissive */
		lua_getfield (state, -1, "emissive");
		
		if (lua_istable (state, -1))
			pass->setEmissive (getColour (state));
		
		lua_pop (state, 1);
	}


}}}
