
#include "Soma/Scripting/Lua/Manager.h"


extern "C" {
#include "lua.h"
#include "lualib.h"
#include "lauxlib.h"
}


#include "Soma/Log.h"
#include "Soma/Registry.h"
#include "Soma/Exception.h"


namespace Soma {
namespace Scripting {
namespace Lua {


	/* log source */
	String Manager::LogSource = "Scripting::Lua::Manager";
	
	
	/* subsystem type */
	REGISTER_TYPE (SubSystem, Manager, "Lua");
	
	
	
	
	/* constructor */
	Manager::Manager () : mInitialised(false), mState(0)
	{
		Log::Debug (LogSource, "Creating Lua manager");
	}
	
	
	/* destructor */
	Manager::~Manager ()
	{
		Log::Debug (LogSource, "Destroying Lua manager");
		
		if (mInitialised)
			shutdown ();
	}
	
	
	
	
	/* initialise host */
	bool Manager::initialise ()
	{
		Log::Message (LogSource, "Initialising Lua manager");
		
		
		mState = lua_open ();
		luaL_openlibs (mState);
		
		mInitialised = true;
		return mInitialised;
	}
	
	
	
	/* reinitialise host */
	bool Manager::reinitialise ()
	{
		Log::Message (LogSource, "Reinitialising Lua manager");
		
		shutdown ();
		return initialise ();
	}
	
	
	
	/* shutdown host */
	void Manager::shutdown ()
	{
		Log::Message (LogSource, "Shutting down Lua manager");
		
		lua_close (mState);
		
		mInitialised = false;
		mState = 0;
	}
	
	
	
	
	/* load and execute the lua script */
	bool Manager::runScript (const Vri& vri)
	{
		int ret = luaL_dofile (mState, vri.path.c_str());
		return !checkError (ret);
	}
	
	
	
	
	
	/* dump the contents of the stack */
	void Manager::dumpStack ()
	{
		/* loop through stack */
		for (int i = 1; i <= lua_gettop (mState); ++i)
		{
			/* get type */
			int type = lua_type (mState, i);
			
			switch (type)
			{
				
				case LUA_TSTRING:
					std::cout << lua_tostring (mState, i) << std::endl;
					break;
					
				case LUA_TBOOLEAN:
					if (lua_toboolean (mState, i))
						std::cout << "true" << std::endl;
					else
						std::cout << "false" << std::endl;
					break;
					
				case LUA_TNUMBER:
					std::cout << lua_tonumber (mState, i) << std::endl;
					break;
					
				default:
					std::cout << lua_typename (mState, i) << std::endl;
					break;
			}
			
		}
	}
	
	
	
	
	
	/* error reporter */
	bool Manager::checkError (int status)
	{
		if (status != 0)
		{
			String err = lua_tostring (mState, -1);
			Log::Error (LogSource, err);
			
			lua_pop (mState, 1);
			return true;
		}
		
		return false;
	}


}}}
