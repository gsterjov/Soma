
#include "Soma/Scripting/Lua/Material.h"


extern "C" {
#include "lua.h"
}


#include "Soma/Log.h"
#include "Soma/Exception.h"
#include "Soma/Scripting/Lua/Manager.h"
#include "Soma/Scripting/Lua/VisualMaterial.h"


#ifdef SOMA_SUBSYSTEM_ENABLE_VISUAL
#include "Soma/Visual/Material.h"
#include "Soma/Visual/Technique.h"
#include "Soma/Visual/Pass.h"
#endif



namespace Soma {
namespace Scripting {
namespace Lua {


	/* log source */
	String Material::LogSource = "Scripting::Lua::Material";
	
	
	
	/* constructor */
	Material::Material (const Vri& vri) : mVri(vri)
	{
		if (!Manager::instance().isInitialised())
			SOMA_EXCEPTION (LogSource, "Cannot execute the Lua script because "
					"the Lua manager has not yet been initialised: " + vri.path);
		
		
		load (vri);
	}
	
	
	/* destructor */
	Material::~Material ()
	{
		Log::Debug (LogSource, "Unloading material script: " + mVri.path);
	}
	
	
	
	
	
	/* load the material properties from the Lua material table which should
	 * be in the globals table */
	void Material::load (const Vri& vri)
	{
		Log::Debug (LogSource, "Loading material script: " + vri.path);
		
		/* execute material script */
		if (!Manager::instance().runScript (vri))
			SOMA_EXCEPTION (LogSource, "Failed to execute Lua script: " + vri.path);
		
		
		
		lua_State* state = Manager::instance().getState();
		
		/* get material table */
		lua_getglobal (state, "material");
		
		
		
		String name = "Material";
		
		/* get material name */
		lua_getfield (state, -1, "name");
		
		if (lua_type (state, -1) == LUA_TSTRING)
			name = lua_tostring (state, -1);
		
		lua_pop (state, 1);
		
		
		
		/* get all materials */
		lua_pushnil (state);
		
		while (lua_next (state, -2))
		{
			/* get key value */
			if (lua_type (state, -2) == LUA_TSTRING)
			{
				String key = lua_tostring (state, -2);
				
				if (key == "visual")
				{
					mVisualMaterial = new VisualMaterial (name);
					mVisualMaterial->loadMaterial (state);
				}
			}
			
			/* remove value but keep key iterator */
			lua_pop (state, 1);
		}
		
		
		/* remove material table */
		lua_pop (state, 1);
	}


}}}
