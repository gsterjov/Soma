
#include "Soma/ConfigFile.h"

#include "Soma/Log.h"
#include <fstream>


namespace Soma
{


	/* file constructor */
	ConfigFile::ConfigFile (const String& file) : mFile(file)
	{
	}
	
	
	/* empty constructor */
	ConfigFile::ConfigFile ()
	{
	}
	
	
	/* destructor */
	ConfigFile::~ConfigFile ()
	{
	}
	
	
	
	/* save to file */
	bool ConfigFile::save ()
	{
		/* TODO: implement ConfigFile saving */
		Log::Debug ("ConfigFile", "Saving config file: " + mFile);
		return false;
	}
	
	
	/* save to stream */
	bool ConfigFile::save (std::ostream& stream)
	{
		/* TODO: implement ConfigFile saving */
		Log::Debug ("ConfigFile", "Saving config file: " + mFile);
		return false;
	}
	
	
	
	
	/* restore from file */
	bool ConfigFile::restore ()
	{
		Log::Message ("ConfigFile", "Restoring configuration file: " + mFile);
		
		/* open stream */
		std::ifstream stream (mFile.c_str());
		
		/* restore file stream */
		bool restored = restore (stream);
		
		/* close stream */
		stream.close ();
		
		return restored;
	}
	
	
	/* restore from stream */
	bool ConfigFile::restore (std::istream& stream)
	{
		Log::Debug ("ConfigFile", "Reading configuration stream");
		
		
		/* get file length */
		stream.seekg (0, std::ios::end);
		long length = stream.tellg();
		stream.seekg (0, std::ios::beg);
		
		
		/* create buffer */
		std::vector<char> data;
		data.reserve (length);
		
		stream.read (&data[0], length);
		String buf = &data[0];
		
		
		/* parse config file */
		parse (buf);
		
		
		/* successfully restored configuration */
		return true;
	}
	
	
	
	
	
	/* ConfigInterface implementation: config exists */
	bool ConfigFile::exists (const String& name) const
	{
		return mContents.find (name) != mContents.end();
	}
	
	
	
	/* ConfigInterface implementation: get int */
	int ConfigFile::get (const String& name, int defaultValue) const
	{
		/* cant use [] operator as its not const. Find name instead */
		std::map<String, String>::const_iterator iter = mContents.find (name);
		
		if (iter != mContents.end()) return Strings::toInt (iter->second);
		else return defaultValue;
	}
	
	
	/* ConfigInterface implementation: get uint */
	uint ConfigFile::get (const String& name, uint defaultValue) const
	{
		/* cant use [] operator as its not const. Find name instead */
		std::map<String, String>::const_iterator iter = mContents.find (name);
		
		if (iter != mContents.end()) return Strings::toUInt (iter->second);
		else return defaultValue;
	}
	
	
	/* ConfigInterface implementation: get bool */
	bool ConfigFile::get (const String& name, bool defaultValue) const
	{
		/* cant use [] operator as its not const. Find name instead */
		std::map<String, String>::const_iterator iter = mContents.find (name);
		
		if (iter != mContents.end()) return Strings::toBool (iter->second);
		else return defaultValue;
	}
	
	
	/* ConfigInterface implementation: get float */
	float ConfigFile::get (const String& name, float defaultValue) const
	{
		/* cant use [] operator as its not const. Find name instead */
		std::map<String, String>::const_iterator iter = mContents.find (name);
		
		if (iter != mContents.end()) return Strings::toFloat (iter->second);
		else return defaultValue;
	}
	
	
	/* ConfigInterface implementation: get char string */
	String ConfigFile::get (const String& name, const char* defaultValue) const
	{
		String val = defaultValue;
		return get (name, val);
	}
	
	
	/* ConfigInterface implementation: get string */
	String ConfigFile::get (const String& name, const String& defaultValue) const
	{
		/* cant use [] operator as its not const. Find name instead */
		std::map<String, String>::const_iterator iter = mContents.find (name);
		
		if (iter != mContents.end()) return iter->second;
		else return defaultValue;
	}
	
	
	
	
	/* ConfigInterface implementation: set int */
	bool ConfigFile::set (const String& name, int value)
	{
		mContents[name] = Strings::convert (value);
		return true;
	}
	
	
	/* ConfigInterface implementation: set uint */
	bool ConfigFile::set (const String& name, uint value)
	{
		mContents[name] = Strings::convert (value);
		return true;
	}
	
	
	/* ConfigInterface implementation: set bool */
	bool ConfigFile::set (const String& name, bool value)
	{
		mContents[name] = Strings::convert (value);
		return true;
	}
	
	
	/* ConfigInterface implementation: set float */
	bool ConfigFile::set (const String& name, float value)
	{
		mContents[name] = Strings::convert (value);
		return true;
	}
	
	
	/* ConfigInterface implementation: set char string */
	bool ConfigFile::set (const String& name, const char* value)
	{
		mContents[name] = value;
		return true;
	}
	
	
	/* ConfigInterface implementation: set string */
	bool ConfigFile::set (const String& name, const String& value)
	{
		mContents[name] = value;
		return true;
	}
	
	
	
	/* parse the string */
	void ConfigFile::parse (const String& buf)
	{
		Log::Debug ("ConfigFile", "Parsing config file");
		
		
		/* split the buffer into groups */
		StringList list = Strings::split (buf, "{}");
		
		/* loop through each group */
		for (int i = 0; i < list.size()-1; ++i)
		{
			/* get the group name */
			String group = list[i];
			Strings::trim (group);
			
			/* empty group names not allowed */
			if (group.size() == 0)
			{
				Log::Warning ("ConfigFile", "Group with no name found. Skipping");
				continue;
			}
			
			
			/* get the group settings */
			StringList values = Strings::split (list[++i]);
			
			/* add settings to the map */
			for (int j = 0; j < values.size(); ++j)
			{
				/* keys are defined as $group.$option */
				StringList option = Strings::split (values[j], "\"");
				
				/* add valid options */
				if (option.size() >= 2)
				{
					String opt = group + "." + Strings::trim (option[0]);
					mContents[opt] = Strings::trim (option[1]);
				}
			}
		}
	}

}
