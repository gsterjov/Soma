
#include "Soma/Composite.h"

#include "Soma/Log.h"
#include "Soma/Registry.h"
#include "Soma/Component.h"


namespace Soma
{

	/* log source */
	const String Composite::LogSource = "Composite";
	
	
	
	
	/* constructor */
	Composite::Composite () : mLoaded(false)
	{
		
	}
	
	
	/* destructor */
	Composite::~Composite ()
	{
		Log::Trace (LogSource, "Removing all components");
		
		
		/* its entirely possible for a component to still be referenced despite
		 * the composite it was added to is being destroyed. As such we need
		 * to unset this composite as the owner for all components added to it */
		ComponentMap::const_iterator iter;
		
		/* remove owner for all components */
		for (iter = mComponents.begin(); iter != mComponents.end(); ++iter)
		{
			iter->second->unload ();
			iter->second->setOwner (0);
		}
	}
	
	
	
	
	/* get component by type */
	smart_ptr<Component> Composite::getComponent (const String& type) const
	{
		/* look for component */
		ComponentMap::const_iterator iter = mComponents.find (type);
		
		if (iter != mComponents.end()) return iter->second;
		else return 0;
	}
	
	
	
	/* get all components */
	ComponentList Composite::getComponents () const
	{
		ComponentList list;
		ComponentMap::const_iterator iter;
		
		/* add components to list */
		for (iter = mComponents.begin(); iter != mComponents.end(); ++iter)
			list.push_back (iter->second);
		
		return list;
	}
	
	
	
	
	
	/* get property from the specified component */
	Value Composite::getProperty (const String& type, const String& name) const
	{
		/* look for component */
		ComponentMap::const_iterator iter = mComponents.find (type);
		
		/* found component */
		if (iter != mComponents.end())
			return iter->second->getProperty (name);
		
		/* return null value */
		else
		{
			Log::Warning (LogSource, "Failed to get the property '" + name +
					"' on the component '" + type + "' because no such "
					"component exists in the composite: " + getName());
			
			return Value();
		}
	}
	
	
	
	/* set property on the specified component */
	void Composite::setProperty (const String& type,
	                             const String& name,
	                             const Value& value)
	{
		/* look for component */
		ComponentMap::const_iterator iter = mComponents.find (type);
		
		/* found component */
		if (iter != mComponents.end())
			iter->second->setProperty (name, value);
		
		else
			Log::Warning (LogSource, "Failed to set the property '" + name +
					"' on the component '" + type + "' because no such "
					"component exists in the composite: " + getName());
	}
	
	
	
	
	
	/* add component */
	void Composite::addComponent (const smart_ptr<Component>& component)
	{
		Log::Trace (LogSource, "Adding component '" + component->getType() +
				"' to the composite: " + getName());
		
		
		/* set owner and add to store */
		component->setOwner (this);
		mComponents[component->getType()] = component;
		
		/* load component if composite is loaded */
		if (mLoaded) component->load ();
		
		ComponentAdded.raise (component);
	}
	
	
	
	/* add components by type */
	void Composite::addComponents (const String& types)
	{
		/* split types */
		StringList::iterator iter;
		StringList list = Strings::split (types, ",");
		
		
		/* no delimiter found so just push the string if it has characters.
		 * this makes it possible to add just one component */
		if (list.size() == 0 && types.size() > 0)
			list.push_back (types);
		
		
		/* look for all types */
		for (iter = list.begin(); iter != list.end(); ++iter)
		{
			String name = Strings::trim (*iter);
			
			/* get component type from the registry */
			Registry<Component>::Type* type = Registry<Component>::find (name);
			
			/* found type */
			if (type)
			{
				smart_ptr<Component> component (type->create());
				addComponent (component);
			}
			
			/* no type found */
			else
				Log::Error (LogSource, "Cannot find a component type by the name: " + name);
		}
	}
	
	
	
	/* remove component */
	void Composite::removeComponent (const smart_ptr<Component>& component)
	{
		Log::Trace (LogSource, "Removing component '" + component->getType() +
				"' from the composite: " + getName());
		
		
		/* unload component */
		if (component->isLoaded()) component->unload ();
		
		/* nullify owner and remove from store */
		component->setOwner (0);
		mComponents.erase (component->getType());
		
		ComponentRemoved.raise (component);
	}
	
	
	
	/* remove components by type */
	void Composite::removeComponents (const String& types)
	{
		/* split types */
		StringList::iterator iter;
		StringList list = Strings::split (types, ",");
		
		
		/* no delimiter found so just push the string if it has characters.
		 * this makes it possible to remove just one component */
		if (list.size() == 0 && types.size() > 0)
			list.push_back (types);
		
		
		/* look for all types */
		for (iter = list.begin(); iter != list.end(); ++iter)
		{
			String name = Strings::trim (*iter);
			
			/* look for component */
			ComponentMap::iterator it = mComponents.find (name);
			
			/* found component */
			if (it != mComponents.end())
			{
				/* we make a copy here otherwise it will corrupt memory as
				 * we'd be passing in the store reference and then trying to
				 * use it after it is removed */
				smart_ptr<Component> component = it->second;
				removeComponent (component);
			}
			
		}
	}
	
	
	
	
	/* load the composite by loading all its components */
	/* TODO: decide if composite loading should be 'best effort' or 'all or nothing' */
	bool Composite::loadComponents ()
	{
		if (mLoaded) return true;
		
		Log::Trace (LogSource, "Loading composite: " + getName());
		
		
		mLoaded = true;
		ComponentMap::iterator iter;
		
		/* load components */
		for (iter = mComponents.begin(); iter != mComponents.end(); ++iter)
		{
			/* failed to load component */
			if (!iter->second->load ())
			{
				Log::Error (LogSource, "Failed to load component '" +
						iter->second->getType() + "' in the composite: " + getName());
				
				mLoaded = false;
				break;
			}
		}
		
		
		/* unload all loaded components */
		if (!mLoaded) unload ();
		return mLoaded;
	}
	
	
	
	/* reload the composite by reloading all its components */
	bool Composite::reloadComponents ()
	{
		if (!mLoaded)
			return Log::Error (LogSource, "Cannot reload the composite '" +
					getName() + "' because it has yet to be loaded.");
		
		
		Log::Trace (LogSource, "Reloading composite: " + getName());
		
		
		mLoaded = true;
		ComponentMap::iterator iter;
		
		/* load components */
		for (iter = mComponents.begin(); iter != mComponents.end(); ++iter)
		{
			/* failed to load component */
			if (!iter->second->reload ())
			{
				Log::Error (LogSource, "Failed to reload component '" +
						iter->second->getType() + "' in the composite: " + getName());
				
				mLoaded = false;
				break;
			}
		}
		
		
		/* unload all loaded components */
		if (!mLoaded) unload ();
		return mLoaded;
	}
	
	
	
	/* unload the composite by unloading all its components */
	void Composite::unloadComponents ()
	{
		if (!mLoaded) return;
		
		Log::Trace (LogSource, "Unloading composite: " + getName());
		
		
		ComponentMap::iterator iter;
		
		/* unload components */
		for (iter = mComponents.begin(); iter != mComponents.end(); ++iter)
		{
			if (iter->second->isLoaded())
				iter->second->unload ();
		}
		
		mLoaded = false;
	}

}
