
#include "Soma/WorldComponent.h"

#include "Soma/World.h"


namespace Soma
{

	/* get the component owner */
	weak_ptr<Composite> WorldComponent::getOwner() const
	{
		return mOwner;
	}
	
	
	
	/* set the component owner by down casting the composite reference to
	 * a world if the type matches. By doing so we allow all world
	 * components to freely reference their owner without typecasting each
	 * and every time a component is added. */
	void WorldComponent::setOwner (const weak_ptr<Composite>& owner)
	{
		if (owner == mOwner) return;
		
		/* unload before we change owners */
		bool loaded = isLoaded();
		unload ();
		
		
		/* set new owner */
		mOwner.release();
		if (owner) mOwner = ref_cast<World> (owner);
		
		
		/* load again */
		if (loaded) load ();
	}

}
