
#include "Soma/Math/Vector3.h"


namespace Soma {
namespace Math {

	/* predefined vectors */
	const Vector3 Vector3::ZERO (0, 0, 0);
	const Vector3 Vector3::UNIT_SCALE (1, 1, 1);
	
	const Vector3 Vector3::UNIT_X (1, 0, 0);
	const Vector3 Vector3::UNIT_Y (0, 1, 0);
	const Vector3 Vector3::UNIT_Z (0, 0, 1);
	
	const Vector3 Vector3::NEGATIVE_UNIT_X (-1, 0, 0);
	const Vector3 Vector3::NEGATIVE_UNIT_Y (0, -1, 0);
	const Vector3 Vector3::NEGATIVE_UNIT_Z (0, 0, -1);
	
	
	
	
	/* constructor */
	Vector3::Vector3 (float x, float y, float z) : x(x), y(y), z(z)
	{
	}
	
	/* empty constructor */
	Vector3::Vector3 () : x(0), y(0), z(0)
	{
	}
	
	
	
	/* vector assignment */
	Vector3& Vector3::operator= (const Vector3& rhs)
	{
		x = rhs.x;
		y = rhs.y;
		z = rhs.z;
		return *this;
	}
	
	/* scalar assignment */
	Vector3& Vector3::operator= (const float rhs)
	{
		x = y = z = rhs;
		return *this;
	}
	
	
	
	
	/* equality operator */
	bool Vector3::operator== (const Vector3& rhs) const
	{
		return (x == rhs.x && y == rhs.y && z == rhs.z);
	}
	
	/* inequality operator */
	bool Vector3::operator!= (const Vector3& rhs) const
	{
		return !(*this == rhs);
	}
	
	
	
	
	/* addition operator */
	Vector3 Vector3::operator+ (const Vector3& rhs) const
	{
		return Vector3 (x + rhs.x, y + rhs.y, z + rhs.z);
	}
	
	/* addition operator */
	Vector3 Vector3::operator+ (const float rhs) const
	{
		return Vector3 (x + rhs, y + rhs, z + rhs);
	}
	
	
	/* subtraction operator */
	Vector3 Vector3::operator- (const Vector3& rhs) const
	{
		return Vector3 (x - rhs.x, y - rhs.y, z - rhs.z);
	}
	
	/* subtraction operator */
	Vector3 Vector3::operator- (const float rhs) const
	{
		return Vector3 (x - rhs, y - rhs, z - rhs);
	}
	
	
	/* multiplication operator */
	Vector3 Vector3::operator* (const Vector3& rhs) const
	{
		return Vector3 (x * rhs.x, y * rhs.y, z * rhs.z);
	}
	
	/* multiplication operator */
	Vector3 Vector3::operator* (const float rhs) const
	{
		return Vector3 (x * rhs, y * rhs, z * rhs);
	}
	
	
	/* division operator */
	Vector3 Vector3::operator/ (const Vector3& rhs) const
	{
		return Vector3 (x / rhs.x, y / rhs.y, z / rhs.z);
	}
	
	/* division operator */
	Vector3 Vector3::operator/ (const float rhs) const
	{
		return Vector3 (x / rhs, y / rhs, z / rhs);
	}
	
	
	
	
	/* addition operator */
	Vector3& Vector3::operator+= (const Vector3& rhs)
	{
		x += rhs.x;
		y += rhs.y;
		z += rhs.z;
		
		return *this;
	}
	
	/* addition operator */
	Vector3& Vector3::operator+= (const float rhs)
	{
		x += rhs;
		y += rhs;
		z += rhs;
		
		return *this;
	}
	
	
	/* subtraction operator */
	Vector3& Vector3::operator-= (const Vector3& rhs)
	{
		x -= rhs.x;
		y -= rhs.y;
		z -= rhs.z;
		
		return *this;
	}
	
	/* subtraction operator */
	Vector3& Vector3::operator-= (const float rhs)
	{
		x -= rhs;
		y -= rhs;
		z -= rhs;
		
		return *this;
	}
	
	
	/* multiplication operator */
	Vector3& Vector3::operator*= (const Vector3& rhs)
	{
		x *= rhs.x;
		y *= rhs.y;
		z *= rhs.z;
		
		return *this;
	}
	
	/* multiplication operator */
	Vector3& Vector3::operator*= (const float rhs)
	{
		x *= rhs;
		y *= rhs;
		z *= rhs;
		
		return *this;
	}
	
	
	/* division operator */
	Vector3& Vector3::operator/= (const Vector3& rhs)
	{
		x /= rhs.x;
		y /= rhs.y;
		z /= rhs.z;
		
		return *this;
	}
	
	/* division operator */
	Vector3& Vector3::operator/= (const float rhs)
	{
		x /= rhs;
		y /= rhs;
		z /= rhs;
		
		return *this;
	}
	
	
	
	
	/* negative operator */
	Vector3 Vector3::operator- () const
	{
		return Vector3 (-x, -y, -z);
	}
	
	
	
	
	/* stream out operator */
	std::ostream& operator<< (std::ostream& ostream, const Vector3& ref)
	{
		ostream << "Vector3: {" << ref.x << ", " << ref.y << ", " << ref.z << "}";
		return ostream;
	}

}}
