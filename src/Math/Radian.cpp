
#include "Soma/Math/Radian.h"

#include "Soma/Math/Math.h"
#include "Soma/Math/Degree.h"


namespace Soma {
namespace Math {


	/* constructor */
	Radian::Radian (float radian) : mRadian(radian)
	{
	}
	
	
	/* degree constructor */
	Radian::Radian (const Degree& degree) : mRadian(degree.toRadians())
	{
	}
	
	
	/* empty constructor */
	Radian::Radian () : mRadian(0)
	{
	}
	
	
	
	
	/* get value */
	float Radian::getValue () const
	{
		return mRadian;
	}
	
	
	/* convert to degrees */
	float Radian::toDegrees () const
	{
		return Math::radiansToDegrees (mRadian);
	}
	
	
	
	/* conversion operator */
	Radian::operator float () const
	{
		return mRadian;
	}
	
	
	
	
	/* vector assignment */
	Radian& Radian::operator= (const Radian& rhs)
	{
		mRadian = rhs.mRadian;
		return *this;
	}
	
	/* radian assignment */
	Radian& Radian::operator= (const Degree& rhs)
	{
		mRadian = rhs.toRadians ();
		return *this;
	}
	
	/* scalar assignment */
	Radian& Radian::operator= (const float rhs)
	{
		mRadian = rhs;
		return *this;
	}
	
	
	
	
	/* equality operator */
	bool Radian::operator== (const Radian& rhs) const
	{
		return mRadian == rhs.mRadian;
	}
	
	/* inequality operator */
	bool Radian::operator!= (const Radian& rhs) const
	{
		return mRadian != rhs.mRadian;
	}
	
	/* less than operator */
	bool Radian::operator< (const Radian& rhs) const
	{
		return mRadian < rhs.mRadian;
	}
	
	/* less than equal to operator */
	bool Radian::operator<= (const Radian& rhs) const
	{
		return mRadian <= rhs.mRadian;
	}
	
	/* greater than operator */
	bool Radian::operator> (const Radian& rhs) const
	{
		return mRadian > rhs.mRadian;
	}
	
	/* greater than equal to operator */
	bool Radian::operator>= (const Radian& rhs) const
	{
		return mRadian >= rhs.mRadian;
	}
	
	
	
	
	/* addition operator */
	Radian Radian::operator+ (const Radian& rhs) const
	{
		return Radian (mRadian + rhs.mRadian);
	}
	
	/* addition operator */
	Radian Radian::operator+ (const Degree& rhs) const
	{
		return Radian (mRadian + rhs.toRadians());
	}
	
	
	/* subtraction operator */
	Radian Radian::operator- (const Radian& rhs) const
	{
		return Radian (mRadian - rhs.mRadian);
	}
	
	/* subtraction operator */
	Radian Radian::operator- (const Degree& rhs) const
	{
		return Radian (mRadian - rhs.toRadians());
	}
	
	
	/* multiplication operator */
	Radian Radian::operator* (const float rhs) const
	{
		return Radian (mRadian * rhs);
	}
	
	
	/* division operator */
	Radian Radian::operator/ (const float rhs) const
	{
		return Radian (mRadian / rhs);
	}
	
	
	
	
	/* addition operator */
	Radian& Radian::operator+= (const Radian& rhs)
	{
		mRadian += rhs.mRadian;
		return *this;
	}
	
	/* addition operator */
	Radian& Radian::operator+= (const Degree& rhs)
	{
		mRadian += rhs.toRadians();
		return *this;
	}
	
	
	/* subtraction operator */
	Radian& Radian::operator-= (const Radian& rhs)
	{
		mRadian -= rhs.mRadian;
		return *this;
	}
	
	/* subtraction operator */
	Radian& Radian::operator-= (const Degree& rhs)
	{
		mRadian -= rhs.toRadians();
		return *this;
	}
	
	
	/* multiplication operator */
	Radian& Radian::operator*= (const float rhs)
	{
		mRadian *= rhs;
		return *this;
	}
	
	
	/* division operator */
	Radian& Radian::operator/= (const float rhs)
	{
		mRadian /= rhs;
		return *this;
	}
	
	
	
	
	/* negative operator */
	Radian Radian::operator- () const
	{
		return Radian (-mRadian);
	}


}}
