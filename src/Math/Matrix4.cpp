
#include "Soma/Math/Matrix4.h"

#include <cassert>

#include "Soma/Math/Vector3.h"
#include "Soma/Math/Matrix3.h"
#include "Soma/Math/Quaternion.h"


namespace Soma {
namespace Math {

	/* predefined matrices */
	const Matrix4 Matrix4::ZERO (
			0, 0, 0, 0,
			0, 0, 0, 0,
			0, 0, 0, 0,
			0, 0, 0, 0);
	
	const Matrix4 Matrix4::IDENTITY (
			1, 0, 0, 0,
			0, 1, 0, 0,
			0, 0, 1, 0,
			0, 0, 0, 1);
	
	
	
	
	/* constructor */
	Matrix4::Matrix4 (float m1_1, float m1_2, float m1_3, float m1_4,
	                  float m2_1, float m2_2, float m2_3, float m2_4,
	                  float m3_1, float m3_2, float m3_3, float m3_4,
	                  float m4_1, float m4_2, float m4_3, float m4_4)
	{
		m[0][0] = m1_1; m[0][1] = m1_2; m[0][2] = m1_3; m[0][3] = m1_4;
		m[1][0] = m2_1; m[1][1] = m2_2; m[1][2] = m2_3; m[1][3] = m2_4;
		m[2][0] = m3_1; m[2][1] = m3_2; m[2][2] = m3_3; m[2][3] = m3_4;
		m[3][0] = m4_1; m[3][1] = m4_2; m[3][2] = m4_3; m[3][3] = m4_4;
	}
	
	
	
	/* 3x3 matrix constructor */
	Matrix4::Matrix4 (const Matrix3& mat)
	{
		operator= (IDENTITY);
		operator= (mat);
	}
	
	
	/* quaternion constructor */
	Matrix4::Matrix4 (const Quaternion& quat)
	{
		operator= (IDENTITY);
		operator= (quat.toRotationMatrix());
	}
	
	
	
	
	/* is matrix affine */
	bool Matrix4::isAffine () const
	{
		return m[3][0] == 0 &&
		       m[3][1] == 0 &&
		       m[3][2] == 0 &&
		       m[3][3] == 1;
	}
	
	
	
	
	/* transpose the matrix */
	Matrix4 Matrix4::transpose () const
	{
		return Matrix4 (
				m[0][0], m[1][0], m[2][0], m[3][0],
				m[0][1], m[1][1], m[2][1], m[3][1],
				m[0][2], m[1][2], m[2][2], m[3][2],
				m[0][3], m[1][3], m[2][3], m[3][3]);
	}
	
	
	
	/* set translation */
	void Matrix4::setTranslation (const Vector3& vec)
	{
		m[0][3] = vec.x;
		m[1][3] = vec.y;
		m[2][3] = vec.z;
	}
	
	
	
	/* get translation */
	Vector3 Matrix4::getTranslation () const
	{
		return Vector3 (m[0][3], m[1][3], m[2][3]);
	}
	
	
	
	/* set scale */
	void Matrix4::setScale (const Vector3& vec)
	{
		m[0][0] = vec.x;
		m[1][1] = vec.y;
		m[2][2] = vec.z;
	}
	
	
	
	
	/* decompose the matrix */
	void Matrix4::decompose (Vector3&    position,
	                         Vector3&    scale,
	                         Quaternion& orientation) const
	{
		assert (isAffine());
		
		Matrix3 mat (*this);
		
		Matrix3 rot;
		Vector3 shear;
		mat.decompose (rot, scale, shear);
		
		orientation = Quaternion (rot);
		position = Vector3 (m[0][3], m[1][3], m[2][3]);
	}
	
	
	
	
	
	/* 3x3 matrix assignment */
	Matrix4& Matrix4::operator= (const Matrix3& mat)
	{
		/* set all components */
		for (int row = 0; row < 3; ++row)
		{
			for (int col = 0; col < 3; ++col)
				m[row][col] = mat[row][col];
		}
		
		return *this;
	}
	
	
	
	
	/* index operator */
	float* Matrix4::operator[] (int index)
	{
		assert (index < 4);
		return m[index];
	}
	
	/* index operator */
	const float* Matrix4::operator[] (int index) const
	{
		assert (index < 4);
		return m[index];
	}
	
	
	
	/* equality operator */
	bool Matrix4::operator== (const Matrix4& mat) const
	{
		/* check all components */
		for (int row = 0; row < 4; ++row)
		{
			for (int col = 0; col < 4; ++col)
			{
				/* matrix component not the same */
				if (m[row][col] != mat.m[row][col])
					return false;
			}
		}
		
		return true;
	}
	
	
	/* inequality operator */
	bool Matrix4::operator!= (const Matrix4& mat) const
	{
		return !(*this == mat);
	}
	
	
	
	/* multiplication operator */
	Matrix4 Matrix4::operator* (const Matrix4& mat) const
	{
		Matrix4 ret;
		
		for (int row = 0; row < 4; ++row)
		{
			for (int col = 0; col < 4; ++col)
			{
				ret.m[row][col] =
						m[row][0] * mat.m[0][col] +
						m[row][1] * mat.m[1][col] +
						m[row][2] * mat.m[2][col] +
						m[row][3] * mat.m[3][col];
			}
		}
		
		return ret;
	}
	
	
	/* multiplication operator */
	Vector3 Matrix4::operator* (const Vector3& vec) const
	{
		Vector3 ret;
		
		float inv = 1.0f / (m[3][0] * vec.x + m[3][1] * vec.y + m[3][2] * vec.z + m[3][3]);
		
		ret.x = (m[0][0] * vec.x + m[0][1] * vec.y + m[0][2] * vec.z + m[0][3]) * inv;
		ret.y = (m[1][0] * vec.x + m[1][1] * vec.y + m[1][2] * vec.z + m[1][3]) * inv;
		ret.z = (m[2][0] * vec.x + m[2][1] * vec.y + m[2][2] * vec.z + m[2][3]) * inv;
		
		return ret;
	}
	
	
	
	/* addition operator */
	Matrix4 Matrix4::operator+ (const Matrix4& mat) const
	{
		Matrix4 ret;
		
		/* add all components */
		for (int row = 0; row < 4; ++row)
		{
			for (int col = 0; col < 4; ++col)
				ret.m[row][col] = m[row][col] + mat.m[row][col];
		}
		
		return ret;
	}
	
	
	
	/* subtraction operator */
	Matrix4 Matrix4::operator- (const Matrix4& mat) const
	{
		Matrix4 ret;
		
		/* add all components */
		for (int row = 0; row < 4; ++row)
		{
			for (int col = 0; col < 4; ++col)
				ret.m[row][col] = m[row][col] - mat.m[row][col];
		}
		
		return ret;
	}
	
	
	
	
	/* stream out operator */
	std::ostream& operator<< (std::ostream& ostream, const Matrix4& ref)
	{
		ostream << "Matrix4: ";
		
		for (int row = 0; row < 4; ++row)
		{
			for (int col = 0; col < 4; ++col)
				ostream << ref[row][col] << " ";
			
			ostream << "\n         ";
		}
		
		return ostream;
	}


}}
