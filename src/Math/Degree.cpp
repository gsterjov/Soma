
#include "Soma/Math/Degree.h"

#include "Soma/Math/Math.h"
#include "Soma/Math/Radian.h"


namespace Soma {
namespace Math {



	/* constructor */
	Degree::Degree (float degree) : mDegree(degree)
	{
	}
	
	
	/* radian constructor */
	Degree::Degree (const Radian& radian) : mDegree(radian.toDegrees())
	{
	}
	
	
	/* empty constructor */
	Degree::Degree () : mDegree(0)
	{
	}
	
	
	
	
	/* get value */
	float Degree::getValue() const
	{
		return mDegree;
	}
	
	
	/* convert to radians */
	float Degree::toRadians() const
	{
		return Math::degreesToRadians (mDegree);
	}
	
	
	
	/* conversion operator */
	Degree::operator float () const
	{
		return mDegree;
	}
	
	
	
	
	/* vector assignment */
	Degree& Degree::operator= (const Degree& rhs)
	{
		mDegree = rhs.mDegree;
		return *this;
	}
	
	/* radian assignment */
	Degree& Degree::operator= (const Radian& rhs)
	{
		mDegree = rhs.toDegrees();
		return *this;
	}
	
	/* scalar assignment */
	Degree& Degree::operator= (const float rhs)
	{
		mDegree = rhs;
		return *this;
	}
	
	
	
	
	/* equality operator */
	bool Degree::operator== (const Degree& rhs) const
	{
		return mDegree == rhs.mDegree;
	}
	
	/* inequality operator */
	bool Degree::operator!= (const Degree& rhs) const
	{
		return mDegree != rhs.mDegree;
	}
	
	/* less than operator */
	bool Degree::operator< (const Degree& rhs) const
	{
		return mDegree < rhs.mDegree;
	}
	
	/* less than equal to operator */
	bool Degree::operator<= (const Degree& rhs) const
	{
		return mDegree <= rhs.mDegree;
	}
	
	/* greater than operator */
	bool Degree::operator> (const Degree& rhs) const
	{
		return mDegree > rhs.mDegree;
	}
	
	/* greater than equal to operator */
	bool Degree::operator>= (const Degree& rhs) const
	{
		return mDegree >= rhs.mDegree;
	}
	
	
	
	
	/* addition operator */
	Degree Degree::operator+ (const Degree& rhs) const
	{
		return Degree (mDegree + rhs.mDegree);
	}
	
	/* addition operator */
	Degree Degree::operator+ (const Radian& rhs) const
	{
		return Degree (mDegree + rhs.toDegrees());
	}
	
	
	/* subtraction operator */
	Degree Degree::operator- (const Degree& rhs) const
	{
		return Degree (mDegree - rhs.mDegree);
	}
	
	/* subtraction operator */
	Degree Degree::operator- (const Radian& rhs) const
	{
		return Degree (mDegree - rhs.toDegrees());
	}
	
	
	/* multiplication operator */
	Degree Degree::operator* (const float rhs) const
	{
		return Degree (mDegree * rhs);
	}
	
	
	/* division operator */
	Degree Degree::operator/ (const float rhs) const
	{
		return Degree (mDegree / rhs);
	}
	
	
	
	
	/* addition operator */
	Degree& Degree::operator+= (const Degree& rhs)
	{
		mDegree += rhs.mDegree;
		return *this;
	}
	
	/* addition operator */
	Degree& Degree::operator+= (const Radian& rhs)
	{
		mDegree += rhs.toDegrees();
		return *this;
	}
	
	
	/* subtraction operator */
	Degree& Degree::operator-= (const Degree& rhs)
	{
		mDegree -= rhs.mDegree;
		return *this;
	}
	
	/* subtraction operator */
	Degree& Degree::operator-= (const Radian& rhs)
	{
		mDegree -= rhs.toDegrees();
		return *this;
	}
	
	
	/* multiplication operator */
	Degree& Degree::operator*= (const float rhs)
	{
		mDegree *= rhs;
		return *this;
	}
	
	
	/* division operator */
	Degree& Degree::operator/= (const float rhs)
	{
		mDegree /= rhs;
		return *this;
	}
	
	
	
	
	/* negative operator */
	Degree Degree::operator- () const
	{
		return Degree (-mDegree);
	}


}}
