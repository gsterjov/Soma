
#include "Soma/Math/Matrix3.h"

#include <cassert>

#include "Soma/Math/Math.h"
#include "Soma/Math/Matrix4.h"
#include "Soma/Math/Vector3.h"


namespace Soma {
namespace Math {

	/* predefined matrices */
	const Matrix3 Matrix3::ZERO (
			0, 0, 0,
			0, 0, 0,
			0, 0, 0);
	
	const Matrix3 Matrix3::IDENTITY (
			1, 0, 0,
			0, 1, 0,
			0, 0, 1);
	
	
	
	
	/* constructor */
	Matrix3::Matrix3 (float m1_1, float m1_2, float m1_3,
	                  float m2_1, float m2_2, float m2_3,
	                  float m3_1, float m3_2, float m3_3)
	{
		m[0][0] = m1_1; m[0][1] = m1_2; m[0][2] = m1_3;
		m[1][0] = m2_1; m[1][1] = m2_2; m[1][2] = m2_3;
		m[2][0] = m3_1; m[2][1] = m3_2; m[2][2] = m3_3;
	}
	
	
	
	/* 4x4 matrix constructor */
	Matrix3::Matrix3 (const Matrix4& mat)
	{
		operator= (mat);
	}
	
	
	
	
	/* transpose the matrix */
	Matrix3 Matrix3::transpose () const
	{
		return Matrix3 (
				m[0][0], m[1][0], m[2][0],
				m[0][1], m[1][1], m[2][1],
				m[0][2], m[1][2], m[2][2]);
	}
	
	
	
	
	/* decompose the matrix */
	void Matrix3::decompose (Matrix3& rotation,
	                         Vector3& scale,
	                         Vector3& shear) const
	{
		/* build orthogonal matrix */
		float inverseLength = Math::invSqrt (m[0][0] * m[0][0] +
		                                     m[1][0] * m[1][0] +
		                                     m[2][0] * m[2][0]);
		
		rotation[0][0] = m[0][0] * inverseLength;
		rotation[1][0] = m[1][0] * inverseLength;
		rotation[2][0] = m[2][0] * inverseLength;
		
		float dot = rotation[0][0] * m[0][1] +
		            rotation[1][0] * m[1][1] +
		            rotation[2][0] * m[2][1];
		
		rotation[0][1] = m[0][1] - dot * rotation[0][0];
		rotation[1][1] = m[1][1] - dot * rotation[1][0];
		rotation[2][1] = m[2][1] - dot * rotation[2][0];
		
		
		inverseLength = Math::invSqrt (rotation[0][1] * rotation[0][1] +
		                               rotation[1][1] * rotation[1][1] +
		                               rotation[2][1] * rotation[2][1]);
		
		rotation[0][1] *= inverseLength;
		rotation[1][1] *= inverseLength;
		rotation[2][1] *= inverseLength;
		
		dot = rotation[0][0] * m[0][2] +
		      rotation[1][0] * m[1][2] +
		      rotation[2][0] * m[2][2];
		
		rotation[0][2] = m[0][2] - dot * rotation[0][0];
		rotation[1][2] = m[1][2] - dot * rotation[1][0];
		rotation[2][2] = m[2][2] - dot * rotation[2][0];
		
		dot = rotation[0][1] * m[0][2] +
		      rotation[1][1] * m[1][2] +
		      rotation[2][1] * m[2][2];
		
		rotation[0][2] -= dot * rotation[0][1];
		rotation[1][2] -= dot * rotation[1][1];
		rotation[2][2] -= dot * rotation[2][1];
		
		
		inverseLength = Math::invSqrt (rotation[0][2] * rotation[0][2] +
		                               rotation[1][2] * rotation[1][2] +
		                               rotation[2][2] * rotation[2][2]);
		
		rotation[0][2] *= inverseLength;
		rotation[1][2] *= inverseLength;
		rotation[2][2] *= inverseLength;
		
		
		
		/* guarantee that the orthogonal matrix had a determinant of 1 which
		 * ensures that there are no reflections */
		float det = rotation[0][0] * rotation[1][1] * rotation[2][2] +
		            rotation[0][1] * rotation[1][2] * rotation[2][0] +
		            rotation[0][2] * rotation[1][0] * rotation[2][1] -
		            rotation[0][2] * rotation[1][1] * rotation[2][0] -
		            rotation[0][1] * rotation[1][0] * rotation[2][2] -
		            rotation[0][0] * rotation[1][2] * rotation[2][1];
		
		
		/* determinant has reflections */
		if (det < 0.0)
		{
			/* flip all components */
			for (int row = 0; row < 3; ++row)
			{
				for (int col = 0; col < 3; ++col)
					rotation[row][col] = -rotation[row][col];
			}
		}
		
		
		
		/* build 'right' matrix */
		Matrix3 mat;
		mat[0][0] = rotation[0][0] * m[0][0] + rotation[1][0] * m[1][0] + rotation[2][0] * m[2][0];
		mat[0][1] = rotation[0][0] * m[0][1] + rotation[1][0] * m[1][1] + rotation[2][0] * m[2][1];
		mat[1][1] = rotation[0][1] * m[0][1] + rotation[1][1] * m[1][1] + rotation[2][1] * m[2][1];
		mat[0][2] = rotation[0][0] * m[0][2] + rotation[1][0] * m[1][2] + rotation[2][0] * m[2][2];
		mat[1][2] = rotation[0][1] * m[0][2] + rotation[1][1] * m[1][2] + rotation[2][1] * m[2][2];
		mat[2][2] = rotation[0][2] * m[0][2] + rotation[1][2] * m[1][2] + rotation[2][2] * m[2][2];
		
		
		/* scale component */
		scale.x = mat[0][0];
		scale.y = mat[1][1];
		scale.z = mat[2][2];
		
		
		/* shear component */
		float inv = 1.0f / scale.x;
		shear.x = mat[0][1] * inv;
		shear.y = mat[0][2] * inv;
		shear.z = mat[1][1] / scale.y;
	}
	
	
	
	
	
	/* 4x4 matrix assignment */
	Matrix3& Matrix3::operator= (const Matrix4& mat)
	{
		/* set all components */
		for (int row = 0; row < 3; ++row)
		{
			for (int col = 0; col < 3; ++col)
				m[row][col] = mat[row][col];
		}
		
		return *this;
	}
	
	
	
	
	/* index operator */
	float* Matrix3::operator[] (int index)
	{
		assert (index < 3);
		return m[index];
	}
	
	/* index operator */
	const float* Matrix3::operator[] (int index) const
	{
		assert (index < 3);
		return m[index];
	}
	
	
	
	/* equality operator */
	bool Matrix3::operator== (const Matrix3& mat) const
	{
		/* check all components */
		for (int row = 0; row < 3; ++row)
		{
			for (int col = 0; col < 3; ++col)
			{
				/* matrix component not the same */
				if (m[row][col] != mat.m[row][col])
					return false;
			}
		}
		
		return true;
	}
	
	
	/* inequality operator */
	bool Matrix3::operator!= (const Matrix3& mat) const
	{
		return !(*this == mat);
	}
	
	
	
	/* multiplication operator */
	Matrix3 Matrix3::operator* (const Matrix3& mat) const
	{
		Matrix3 ret;
		
		/* multiply all components */
		for (int row = 0; row < 3; ++row)
		{
			for (int col = 0; col < 3; ++col)
			{
				ret.m[row][col] =
						m[row][0] * mat.m[0][col] +
						m[row][1] * mat.m[1][col] +
						m[row][2] * mat.m[2][col];
			}
		}
		
		return ret;
	}
	
	
	/* multiplication operator */
	Vector3 Matrix3::operator* (const Vector3& vec) const
	{
		Vector3 ret;
		
		ret.x = m[0][0] * vec.x + m[0][1] * vec.y + m[0][2] * vec.z;
		ret.y = m[1][0] * vec.x + m[1][1] * vec.y + m[1][2] * vec.z;
		ret.z = m[2][0] * vec.x + m[2][1] * vec.y + m[2][2] * vec.z;
		
		return ret;
	}
	
	
	
	/* addition operator */
	Matrix3 Matrix3::operator+ (const Matrix3& mat) const
	{
		Matrix3 ret;
		
		/* add all components */
		for (int row = 0; row < 3; ++row)
		{
			for (int col = 0; col < 3; ++col)
				ret.m[row][col] = m[row][col] + mat.m[row][col];
		}
		
		return ret;
	}
	
	
	
	/* subtraction operator */
	Matrix3 Matrix3::operator- (const Matrix3& mat) const
	{
		Matrix3 ret;
		
		/* subtract all components */
		for (int row = 0; row < 3; ++row)
		{
			for (int col = 0; col < 3; ++col)
				ret.m[row][col] = m[row][col] - mat.m[row][col];
		}
		
		return ret;
	}

}}
