
#include "Soma/Math/Math.h"

#include <cmath>


namespace Soma {
namespace Math {

	/* constant math values */
	const float Math::PI   = atan (1.0f) * 4.0f;
	const float Math::LOG2 = log (2.0f);
	
	const float Math::mRadiansPerDegree = PI / 180.0f;
	const float Math::mDegreesPerRadian = 180.0f / PI;


}}
