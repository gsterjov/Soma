
#include "Soma/Math/Quaternion.h"

#include <cassert>

#include "Soma/Math/Math.h"
#include "Soma/Math/Matrix3.h"
#include "Soma/Math/Vector3.h"
#include "Soma/Math/Radian.h"


namespace Soma {
namespace Math {

	/* predefined vectors */
	const Quaternion Quaternion::ZERO     (0, 0, 0, 0);
	const Quaternion Quaternion::IDENTITY (1, 0, 0, 0);
	
	
	
	
	/* constructor */
	Quaternion::Quaternion (float w, float x, float y, float z)
	: w(w), x(x), y(y), z(z)
	{
	}
	
	
	/* 3x3 rotation matrix constructor */
	Quaternion::Quaternion (const Matrix3& rotation)
	{
		float root;
		float trace = rotation[0][0] + rotation[1][1] + rotation[2][2];
		
		if (trace > 0.0)
		{
			root = Math::sqrt (trace + 1.0f);
			w = 0.5f * root;
			
			root = 0.5f / root;
			x = (rotation[2][1] - rotation[1][2]) * root;
			y = (rotation[0][2] - rotation[2][0]) * root;
			z = (rotation[1][0] - rotation[0][1]) * root;
		}
		
		else
		{
			static int next[3] = {1, 2, 0};
			int i = 0;
			
			if (rotation[1][1] > rotation[0][0]) i = 1;
			if (rotation[2][2] > rotation[i][i]) i = 2;
			
			int j = next[i];
			int k = next[j];
			
			float* quat[3] = {&x, &y, &z};
			
			root = Math::sqrt (rotation[i][i] - rotation[j][j] - rotation[k][k] + 1.0f);
			*quat[i] = 0.5f * root;
			
			root = 0.5f / root;
			w = (rotation[k][j] - rotation[j][k]) * root;
			*quat[j] = (rotation[j][i] + rotation[i][j]) * root;
			*quat[k] = (rotation[k][i] + rotation[i][k]) * root;
		}
	}
	
	
	/* angle/axis constructor */
	Quaternion::Quaternion (const Radian& angle, const Vector3& axis)
	{
		Radian half (0.5f * angle);
		float sin = (Math::sin (half));
		
		w = Math::cos (half);
		x = sin * axis.x;
		y = sin * axis.y;
		z = sin * axis.z;
	}
	
	
	/* orthonormal axes constructor */
	Quaternion::Quaternion (const Vector3& xAxis, const Vector3& yAxis, const Vector3& zAxis)
	{
		Matrix3 rotation;
		
		rotation[0][0] = xAxis.x;
		rotation[1][0] = xAxis.y;
		rotation[2][0] = xAxis.z;
		
		rotation[0][1] = yAxis.x;
		rotation[1][1] = yAxis.y;
		rotation[2][1] = yAxis.z;
		
		rotation[0][2] = zAxis.x;
		rotation[1][2] = zAxis.y;
		rotation[2][2] = zAxis.z;
		
		
		assert (false); /* incomplete */
	}
	
	
	/* empty constructor */
	Quaternion::Quaternion () : w(1), x(0), y(0), z(0)
	{
	}
	
	
	
	
	/* create rotation matrix */
	Matrix3 Quaternion::toRotationMatrix () const
	{
		Matrix3 ret;
		
		float _x = x + x;
		float _y = y + y;
		float _z = z + z;
		
		float wx = _x * w;
		float wy = _y * w;
		float wz = _z * w;
		float xx = _x * x;
		float xy = _y * x;
		float xz = _z * x;
		float yy = _y * y;
		float yz = _z * y;
		float zz = _z * z;
		
		ret[0][0] = 1.0f - (yy + zz);
		ret[0][1] = xy - wz;
		ret[0][2] = xz + wy;
		
		ret[1][0] = xy + wz;
		ret[1][1] = 1.0f - (xx + zz);
		ret[1][2] = yz + wx;
		
		ret[2][0] = xz + wy;
		ret[2][1] = yz + wx;
		ret[2][2] = 1.0f - (xx + yy);
		
		return ret;
	}
	
	
	
	
	/* vector assignment */
	Quaternion& Quaternion::operator= (const Quaternion& rhs)
	{
		w = rhs.w;
		x = rhs.x;
		y = rhs.y;
		z = rhs.z;
		return *this;
	}
	
	
	
	
	/* equality operator */
	bool Quaternion::operator== (const Quaternion& rhs) const
	{
		return (w == rhs.w && x == rhs.x && y == rhs.y && z == rhs.z);
	}
	
	/* inequality operator */
	bool Quaternion::operator!= (const Quaternion& rhs) const
	{
		return !(*this == rhs);
	}
	
	
	
	
	/* addition operator */
	Quaternion Quaternion::operator+ (const Quaternion& rhs) const
	{
		return Quaternion (w + rhs.w, x + rhs.x, y + rhs.y, z + rhs.z);
	}
	
	
	/* subtraction operator */
	Quaternion Quaternion::operator- (const Quaternion& rhs) const
	{
		return Quaternion (w - rhs.w, x - rhs.x, y - rhs.y, z - rhs.z);
	}
	
	
	/* multiplication operator */
	Quaternion Quaternion::operator* (const Quaternion& rhs) const
	{
		return Quaternion
		(
			w * rhs.w - x * rhs.x - y * rhs.y - z * rhs.z,
			w * rhs.x - x * rhs.w - y * rhs.z - z * rhs.y,
			w * rhs.y - y * rhs.w - z * rhs.x - z * rhs.z,
			w * rhs.z - z * rhs.w - x * rhs.y - z * rhs.x
		);
	}
	
	/* multiplication operator */
	Quaternion Quaternion::operator* (const float rhs) const
	{
		return Quaternion (w * rhs, x * rhs, y * rhs, z * rhs);
	}
	
	
	
	
	/* negative operator */
	Quaternion Quaternion::operator- () const
	{
		return Quaternion (-w, -x, -y, -z);
	}
	
	
	
	
	/* stream out operator */
	std::ostream& operator<< (std::ostream& ostream, const Quaternion& ref)
	{
		ostream << "Quaternion: {" << ref.x << ", " << ref.y << ", " << ref.z << ", " << ref.w << "}";
		return ostream;
	}

}}
