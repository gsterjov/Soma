
#include "Soma/Audio/Input/Vorbis.h"

#include <vorbis/codec.h>
#include <vorbis/vorbisfile.h>

#include "Soma/Log.h"
#include "Soma/Audio/InputType.h"


namespace Soma {
namespace Audio {

	/* log source */
	const String Vorbis::LogSource = "Audio::Vorbis";
	
	
	
	/* audio system subsystem type */
	struct VorbisType : InputType
	{
		String getName() { return "Vorbis"; }
		Input* create() { return new Vorbis(); }
		void destroy (Input* input) { delete input; }
	};
	
	/* create and register type */
	static VorbisType type;
	
	
	
	
	/* constructor */
	Vorbis::Vorbis ()
	{
		Log::Debug (LogSource, "Creating Vorbis input decoder");
		mFile = new OggVorbis_File ();
	}
	
	
	/* destructor */
	Vorbis::~Vorbis ()
	{
		Log::Debug (LogSource, "Destroying Vorbis input decoder");
	}
	
	
	
	/* open media */
	bool Vorbis::open (const String& path)
	{
		Log::Debug (LogSource, "Opening Vorbis input decoder");
		
		
		/* open file */
		ov_fopen (path.c_str(), mFile);
		
		
		/* get audio properties */
		vorbis_comment* comment = ov_comment (mFile, -1);
		vorbis_info* info = ov_info (mFile, -1);
		long bitrate = ov_bitrate (mFile, -1);
		int streams = ov_streams (mFile);
		bool seekable = ov_seekable (mFile);
		ogg_int64_t samples = ov_pcm_total (mFile, -1);
		double time = ov_time_total (mFile, -1);
		
		
		String debug;
		
		
		/* add audio info */
		debug += "Encoded by: " + String(comment->vendor) + " " + Strings::convert(info->version) + "\n";
		
		debug += "Channels: " + Strings::convert (info->channels) + "\n";
		debug += "Sample Rate: " + Strings::convert (info->rate) + "Hz\n";
		debug += "Bitrate: " + Strings::convert (bitrate/1024) + "kbps\n";
		debug += "Streams: " + Strings::convert (streams) + "\n";
		debug += "Seekable: " + Strings::convert (seekable) + "\n";
		debug += "Time: " + Strings::convert (time) + "\n";
		debug += "Total Samples: " + Strings::convert (samples) + "\n";
		
		
		/* add all comments to debug message */
		for (int i = 0; i < comment->comments; ++i)
		{
			String msg (comment->user_comments[i], comment->comment_lengths[i]);
			debug += "Comment: " + msg + "\n";
		}
		
		
		/* dump media info */
		Log::Debug (LogSource, "Vorbis media info:\n" + debug);
		
		
		mOpened = true;
		return mOpened;
	}
	
	
	
	/* close media */
	void Vorbis::close ()
	{
		Log::Debug (LogSource, "Closing Vorbis input decoder");
		ov_clear (mFile);
		
		mOpened = false;
	}
	
	
	
	
	/* read a frame */
	int Vorbis::read (float* buffer, int samples)
	{
		int stream;
		int len = samples / 2;
		
		float** buf; /* array[channels][samples] */
		
		
		/* decode samples into buffer */
		int read = ov_read_float (mFile, &buf, len, &stream);
		
		
		/* error reading */
		if (read < 0)
		{
			Log::Error (LogSource, "Failed to read the stream");
			return read;
		}
		
		
		int j = 0;
		
		/* copy samples into interleaved array */
		for (int i = 0; i < read; ++i)
		{
			buffer[j++] = buf[0][i];
			buffer[j++] = buf[1][i];
		}
		
		
		/* interleaved sample length */
		return read * 2;
	}


}}
