
#include "Soma/Audio/Manager.h"

#include "Soma/Log.h"
#include "Soma/Registry.h"
#include "Soma/Audio/OutputType.h"
#include "Soma/Audio/Mixer.h"


namespace Soma {
namespace Audio {

	/* log source */
	String Manager::LogSource = "Audio::Manager";
	
	
	/* subsystem type */
//	REGISTER_TYPE (SubSystem, Manager, "Audio");
	
	
	
	
	/* constructor */
	Manager::Manager () : mInitialised(false)
	{
		Log::Debug (LogSource, "Creating audio manager");
		
		
		/* get all available outputs */
		OutputTypes::iterator iter;
		OutputTypes& types = Soma::Audio::getOutputTypes();
		
		/* find alsa type */
		for (iter = types.begin(); iter != types.end(); ++iter)
		{
			OutputType* output = *iter;
			
			/* TODO: create output class based on preferences */
			if (output->getName() == "ALSA")
				mOutput = smart_ptr<Output> (output->create ());
		}
	}
	
	
	/* destructor */
	Manager::~Manager ()
	{
		Log::Debug (LogSource, "Destroying audio manager");
		
		if (mInitialised)
			shutdown();
	}
	
	
	
	/* initialise system */
	bool Manager::initialise ()
	{
		Log::Message (LogSource, "Initialising audio manager");
		
		
		/* create pipeline */
		mMixer = smart_ptr<Mixer> (new Mixer ());
		
		/* initialise pipeline */
		mMixer->setOutput (mOutput);
		mInitialised = mOutput->open();
		
		return mInitialised;
	}
	
	
	/* reinitialise system */
	bool Manager::reinitialise ()
	{
		Log::Message (LogSource, "Reinitialising audio manager");
		return mInitialised;
	}
	
	
	/* shutdown system */
	void Manager::shutdown ()
	{
		Log::Message (LogSource, "Shutting down audio manager");
		
		/* free pipeline */
		mMixer.release ();
		mOutput->close ();
		
		mInitialised = false;
	}
	
	
	
	/* update sources and listeners */
	void Manager::step ()
	{
		
	}
	
	
	
	
	/* Configurable implementation: save audio settings */
	bool Manager::save (ConfigInterface& config)
	{
		Log::Debug (LogSource, "Saving audio manager settings");
		return true;
	}
	
	
	
	/* Configurable implementation: restore audio settings */
	bool Manager::restore (const ConfigInterface& config)
	{
		Log::Debug (LogSource, "Restoring audio manager settings");
		return true;
	}


}}
