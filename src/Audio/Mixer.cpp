
#include "Soma/Audio/Mixer.h"

#include "Soma/Types.h"
#include "Soma/Log.h"
#include "Soma/Audio/Output.h"
#include "Soma/Audio/SourceComponent.h"


#define INT16_MIN -32767
#define INT16_MAX 32767


namespace Soma {
namespace Audio {


	/* start device thread */
	static void* start_thread (void* obj)
	{
		static_cast<Mixer*> (obj)->_drainLoop ();
	}
	
	
	
	/* constructor */
	Mixer::Mixer () : mRunning(true), mBuffer(4096)
	{
		Log::Debug ("Audio::Mixer", "Creating audio mixer");
		
		
		pthread_cond_init (&mCond, 0);
		pthread_mutex_init (&mMutex, 0);
		
		/* create drain thread */
		pthread_create (&mThread, 0, &start_thread, this);
	}
	
	
	/* destructor */
	Mixer::~Mixer ()
	{
		Log::Debug ("Audio::Mixer", "Shutting down audio mixer");
		
		
		/* terminate thread */
		mRunning = false;
		pthread_join (mThread, 0);
	}
	
	
	
	/* add source */
	void Mixer::addSource (const smart_ptr<SourceComponent>& source)
	{
		Log::Trace ("Audio::Mixer", "Adding source: ");
		
		
		pthread_mutex_lock (&mMutex);
		
		/* add source and signal thread */
		mSources.push_back (source);
		pthread_cond_signal (&mCond);
		
		pthread_mutex_unlock (&mMutex);
	}
	
	
	
	/* remove source */
	void Mixer::removeSource (const smart_ptr<SourceComponent>& source)
	{
		Log::Trace ("Audio::Mixer", "Removing source: ");
		
		
		pthread_mutex_lock (&mMutex);
		
		std::vector<smart_ptr<SourceComponent> >::iterator iter;
		
		/* remove all matching sources */
		for (iter = mSources.begin(); iter != mSources.end(); ++iter)
		{
			if (*iter == source)
			{
				mSources.erase (iter);
				break;
			}
		}
		
		pthread_mutex_unlock (&mMutex);
	}
	
	
	
	
	/* mix sources */
	void Mixer::mix ()
	{
		/* short circuit if nothing is playing */
		if (mSources.size() == 0)
			return;
		
		/* FIXME: this simply does not work */
		int id = 0;
		int min = 2048;
		std::vector<smart_ptr<SourceComponent> >::iterator iter;
		
		/* combine sources into the final buffer */
		for (iter = mSources.begin(); iter != mSources.end(); ++iter)
		{
			int left = mBuffer.capacity() - mBuffer.size();
			int wrote = mLengths[id];
			
			
			int size = (mBuffer.size() - wrote) + left;
			
			
			/* get source samples */
			const SampleBuffer& buffer = (*iter)->read (size);
			
			
			/* get minimum buffer size */
			if (buffer.length < min) min = buffer.length;
			mLengths[id] = buffer.length;
			
			
			/* mix with all the samples already in the buffer */
			for (int i = 0, j = wrote; j < mBuffer.size(); ++i, ++j)
			{
				/* (s1 + s2) - (s1 * s2) */
				mBuffer[j] = (mBuffer[j] + buffer.samples[i]) -
				             (mBuffer[j] * buffer.samples[i]);
			}
			
			/* add all remaining samples */
			for (int i = mBuffer.size(); i < buffer.length; ++i)
			{
				mBuffer.push_back (buffer.samples[i]);
			}
			
			
			++id;
		}
		
		
		
		/* output buffer */
		int16 out[2048] = {};
		
		/* convert to output sample format */
		for (int i = 0; i < min; ++i)
		{
			out[i] = mBuffer[0] * INT16_MAX;
			mBuffer.pop_front ();
			
			if (out[i] > INT16_MAX) out[i] = INT16_MAX;
			else if (out[i] < INT16_MIN) out[i] = INT16_MIN;
		}
		
		
		/* write mixed buffer to sound device */
		mOutput->write (reinterpret_cast<char*> (out), min * 2);
	}
	
	
	
	
	
	/* drain buffer */
	void Mixer::_drainLoop ()
	{
		Log::Debug ("Audio::Mixer", "Starting audio thread");
		
		/* FIXME: mutex locking is too aggressive. lag when removing sources */
		while (mRunning)
		{
			pthread_mutex_lock (&mMutex);
			
			/* wait till we have a source */
			if (mSources.size() == 0)
				pthread_cond_wait (&mCond, &mMutex);
			
			
			/* mix sources */
			mix ();
			
			
			pthread_mutex_unlock (&mMutex);
		}
	}

}}
