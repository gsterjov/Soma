
#include "Soma/Audio/InputType.h"


namespace Soma {
namespace Audio {


	/* get input types */
	InputTypes& getInputTypes ()
	{
		static InputTypes types;
		return types;
	}


}}
