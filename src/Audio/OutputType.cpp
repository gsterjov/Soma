
#include "Soma/Audio/OutputType.h"


namespace Soma {
namespace Audio {


	/* get output types */
	OutputTypes& getOutputTypes ()
	{
		static OutputTypes types;
		return types;
	}


}}
