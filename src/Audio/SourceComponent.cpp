
#include "Soma/Audio/SourceComponent.h"

#include "Soma/Log.h"
#include "Soma/Registry.h"
#include "Soma/Audio/Input.h"
#include "Soma/Audio/InputType.h"
#include "Soma/Audio/Manager.h"
#include "Soma/Audio/Mixer.h"

#include "Soma/Entity.h"


namespace Soma {
namespace Audio {

	/* log source */
	const String SourceComponent::LogSource = "Audio::SourceComponent";
	
	/* component name */
	const String SourceComponent::Type = "entity.audio.source";
	
	
	/* component type */
	REGISTER_TYPE (Component, SourceComponent, SourceComponent::Type);
	
	
	
	
	/* constructor */
	SourceComponent::SourceComponent ()
	{
		Log::Debug (LogSource, "Creating source: ");
		
		
		/* get all available inputs */
		InputTypes::iterator iter;
		InputTypes& types = Soma::Audio::getInputTypes();
		
		/* find vorbis type */
		for (iter = types.begin(); iter != types.end(); ++iter)
		{
			InputType* type = *iter;
			
			/* TODO: create input class based on file extension */
			if (type->getName() == "Vorbis")
				mInput = smart_ptr<Input> (type->create ());
		}
		
		
		/* set buffer size */
		mBuffer.samples = new float[2048];
	}
	
	
	/* destructor */
	SourceComponent::~SourceComponent ()
	{
//		Log::Debug (LogSource, "Destroying source component on entity:" + mOwner->getName());
	}
	
	
	
	
	/* play media */
	bool SourceComponent::play (const String& path)
	{
		if (!mInput) return false;
		
		
//		Log::Trace (LogSource, "Playing media '" + path + "' at the "
//				"entity '" + mOwner->getName() + "'");
		
		
		mInput->open (path);
		Manager::instance().getMixer()->addSource (weak_ptr<SourceComponent> (this));
		
		
		mPlaying = true;
		return mPlaying;
	}
	
	
	
	/* read samples */
	const SampleBuffer& SourceComponent::read (int samples)
	{
		mBuffer.length = mInput->read (mBuffer.samples, samples);
		return mBuffer;
	}
	
	
	
	/* stop playing */
	void SourceComponent::stop ()
	{
		if (!mPlaying) return;
		
		Log::Trace (LogSource, "Stopping source:");
		
		
		Manager::instance().getMixer()->removeSource (weak_ptr<SourceComponent> (this));
		mInput->close ();
		mPlaying = false;
	}


}}
