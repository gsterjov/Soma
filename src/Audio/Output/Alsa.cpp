
#include "Soma/Audio/Output/Alsa.h"

#include "Soma/Log.h"
#include "Soma/Audio/OutputType.h"


namespace Soma {
namespace Audio {

	/* log source */
	const String Alsa::LogSource = "Audio::ALSA";
	
	/* device name */
	const String Alsa::mName = "ALSA";
	
	
	
	/* audio output type */
	struct AlsaType : OutputType
	{
		String getName() { return "ALSA"; }
		Output* create() { return new Alsa(); }
		void destroy (Output* output) { delete output; }
	};
	
	/* create and register type */
	static AlsaType type;
	
	
	
	
	/* constructor */
	Alsa::Alsa () : mOpened(false)
	{
		Log::Debug (LogSource, "Creating ALSA output: " + mName);
	}
	
	
	/* destructor */
	Alsa::~Alsa ()
	{
		Log::Debug (LogSource, "Destroying ALSA output: " + mName);
		
		if (mOpened) close ();
	}
	
	
	
	
	/* open device */
	bool Alsa::open ()
	{
		Log::Debug (LogSource, "Opening ALSA device: " + mName);
		
		
		int err = 0;
		
		/* open device */
		err = snd_pcm_open (&mPcm, "plughw", SND_PCM_STREAM_PLAYBACK, 0);
		
		/* failed to open device */
		if (err < 0)
			return Log::Error (LogSource, "Failed to open the Alsa "
					"device '" + mName + "'. " + snd_strerror (err));
		
		
		/* configure device */
		if (!set_hw_params ()) return false;
		if (!set_sw_params ()) return false;
		
		Log::Debug (LogSource, "Configured Alsa device: " + mName);
		
		/* debug */
		snd_output_t* setup;
		snd_output_stdio_attach (&setup, stdout, 0);
		
		snd_pcm_dump_setup (mPcm, setup);
		
		
		
		/* get frame size in bytes */
		mFrameSize = snd_pcm_frames_to_bytes (mPcm, mPeriodSize) / mPeriodSize;
		
		
		/* prepare device */
		snd_pcm_prepare (mPcm);
		
		
		mOpened = true;
		return mOpened;
	}
	
	
	
	/* close device */
	void Alsa::close ()
	{
		Log::Debug (LogSource, "Closing ALSA device: " + mName);
		
		snd_pcm_close (mPcm);
		mPcm = 0;
		
		mOpened = false;
	}
	
	
	
	
	/* write frames */
	int Alsa::write (char* buffer, int length)
	{
		snd_pcm_uframes_t frames = length / mFrameSize;
		
		
		/* try to write all frames */
		while (frames > 0)
		{
			/* write to alsa buffer */
			snd_pcm_sframes_t ret = snd_pcm_writei (mPcm, buffer, frames);
			
			
			/* no data written */
			if (ret == -EAGAIN || ret == -EINTR)
				continue;
			
			/* recover from error */
			else if (ret < 0)
			{
				Log::Error (LogSource, snd_strerror (ret));
				snd_pcm_prepare (mPcm);
			}
			
			else
			{
				frames -= ret;
				buffer += ret * mFrameSize;
			}
		}
		
		
		return length;
	}
	
	
	
	
	/* set hw params */
	bool Alsa::set_hw_params ()
	{
		int err = 0;
		
		
		/* device configuration */
		snd_pcm_hw_params_t* params;
		snd_pcm_hw_params_alloca (&params);
		
		
		/* fill configuration */
		err = snd_pcm_hw_params_any (mPcm, params);
		
		if (err < 0)
			return Log::Error (LogSource, "Failed to configure the "
					"device '" + mName + "'. " + snd_strerror (err));
		
		
		
		/* set access type */
		err = snd_pcm_hw_params_set_access (mPcm, params, SND_PCM_ACCESS_RW_INTERLEAVED);
		
		if (err < 0)
			return Log::Error (LogSource, "Failed to set the access "
					"type to 'interleaved' on device '" + mName + "'. "
					+ snd_strerror (err));
		
		
		
		/* set sample format */
		err = snd_pcm_hw_params_set_format (mPcm, params, SND_PCM_FORMAT_S16_LE);
		
		if (err < 0)
			return Log::Error (LogSource, "Failed to set the sample "
					"format type to 'S16_LE' on device '" + mName + "'. "
					+ snd_strerror (err));
		
		
	    
		/* set sample rate */
		unsigned int rate = 44100;
		err = snd_pcm_hw_params_set_rate_near (mPcm, params, &rate, 0);
		
		if (err < 0)
			return Log::Error (LogSource, "Failed to set sample "
					"rate to '" + Strings::convert (rate) + "' on device '"
					+ mName + "'. " + snd_strerror (err));
		
		
		
		/* set channels */
		err = snd_pcm_hw_params_set_channels (mPcm, params, 2);
		
		if (err < 0)
			return Log::Error (LogSource, "Failed to set number of "
					"channels to '" + Strings::convert (2) + "' on device '"
					+ mName + "'. " + snd_strerror (err));
		
		
		
		/* set buffer time to 0.5 seconds */
		uint time = 500000;
		err = snd_pcm_hw_params_set_buffer_time_near (mPcm, params, &time, 0);
		
		if (err < 0)
			return Log::Error (LogSource, "Failed to set buffer time "
					"to '" + Strings::convert ((float)time/(float)1000000) +
					"s' on device '" + mName + "'. " + snd_strerror (err));
		
		
		
		
		/* apply configuration */
		err = snd_pcm_hw_params (mPcm, params);
		
		if (err < 0)
			return Log::Error (LogSource, "Failed to apply hardware "
					"configuration to device '" + mName + "'. "
					+ snd_strerror (err));
		
		
		
		
		/* get buffer size in frames */
		err = snd_pcm_hw_params_get_buffer_size (params, &mBufferSize);
		
		if (err < 0)
			return Log::Error (LogSource, "Failed to read the buffer "
					"size from device '" + mName + "'. " + snd_strerror (err));
		
		
		
		/* get period size in frames */
		err = snd_pcm_hw_params_get_period_size (params, &mPeriodSize, 0);
		
		if (err < 0)
			return Log::Error (LogSource, "Failed to read the period "
					"size from device '" + mName + "'. " + snd_strerror (err));
		
		
		return true;
	}
	
	
	
	
	/* set sw params */
	bool Alsa::set_sw_params ()
	{
		int err = 0;
		
		/* software configuration */
		snd_pcm_sw_params_t* params;
		snd_pcm_sw_params_alloca (&params);
		
		
		/* fill configuration */
		err = snd_pcm_sw_params_current (mPcm, params);
		
		if (err < 0)
			return Log::Error (LogSource, "Failed to configure the "
					"library settings '" + mName + "'. " + snd_strerror (err));
		
		
		
		/* set start threshold */
		err = snd_pcm_sw_params_set_start_threshold (mPcm, params, mPeriodSize);
		
		if (err < 0)
			return Log::Error (LogSource, "Failed to set the start "
					"threshold to '" + Strings::convert (mPeriodSize) + "' on "
					"the device '" + mName + "'. " + snd_strerror (err));
		
		
		
		/* set minimum frames */
		err = snd_pcm_sw_params_set_avail_min (mPcm, params, mPeriodSize);
		
		if (err < 0)
			return Log::Error (LogSource, "Failed to set minimum "
					"frames to '" + Strings::convert (mPeriodSize) + "' on "
					"the device '" + mName + "'. " + snd_strerror (err));
		
		
		
		
		/* apply configuration */
		err = snd_pcm_sw_params (mPcm, params);
		
		if (err < 0)
			return Log::Error (LogSource, "Failed to apply software "
					"configuration to device '" + mName + "'. "
					+ snd_strerror (err));
		
		
		return true;
	}


}}
