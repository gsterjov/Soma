
#include "Soma/FileSystem/Win32Directory.h"


namespace Soma {
namespace FileSystem {


	/* constructor */
	Win32Directory::Win32Directory (const String& path)
	: mPath (path),
	  mCached (false)
	{
		/* make sure there is a trailing slash */
		if (mPath[mPath.size()-1] != '/')
			mPath += '/';
		
		
		/* get directory name */
		int start = mPath.find_last_of ('/', mPath.size() - 2) + 1;
		int end = mPath.size() - start - 1;
		
		mName = mPath.substr (start, end);
	}
	
	
	/* destructor */
	Win32Directory::~Win32Directory ()
	{
		DirectoryMap::iterator iter;
		
		/* delete subdirs */
		for (iter = mDirCache.begin(); iter != mDirCache.end(); ++iter)
			delete iter->second;
		
		
		FileMap::iterator it;
		
		/* delete files */
		for (it = mFileCache.begin(); it != mFileCache.end(); ++it)
			delete it->second;
	}
	
	
	
	/* build cache */
	void Win32Directory::buildCache ()
	{
		
		
		mCached = true;
	}
	
	
	
	
	/* get sub directories */
	DirectoryMap& Win32Directory::getDirectories()
	{
		/* scan the directory */
		if (!mCached) buildCache ();
		return mDirCache;
	}
	
	
	
	/* get files */
	FileMap& Win32Directory::getFiles()
	{
		/* scan the directory */
		if (!mCached) buildCache ();
		return mFileCache;
	}

}}
