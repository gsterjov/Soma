
#include "Soma/FileSystem/PosixFile.h"


namespace Soma {
namespace FileSystem {


	/* constructor */
	PosixFile::PosixFile (const String& path) : mPath(path)
	{
		mName = mPath.substr (mPath.find_last_of ('/') + 1);
	}
	
	
	/* destructor */
	PosixFile::~PosixFile ()
	{
	}
	

}}
