
#include "Soma/FileSystem/Manager.h"

#include "Soma/Log.h"


/* platform specific includes */
#ifdef SOMA_SYSTEM_UNIX
	#include "Soma/FileSystem/PosixDirectory.h"
#elif defined SOMA_SYSTEM_WIN32
	#include "Soma/FileSystem/Win32Directory.h"
#endif



namespace Soma {
namespace FileSystem {


	/* constructor */
	Manager::Manager () : mCached(false)
	{
		Log::Debug ("FileSystem::Manager", "Creating virtual file system manager");
	}
	
	
	/* destructor */
	Manager::~Manager ()
	{
		Log::Debug ("FileSystem::Manager", "Destroying virtual file system manager");
		
		
		DirectoryMap::iterator iter;
		
		/* free mount directories */
		for (iter = mMounts.begin(); iter != mMounts.end(); ++iter)
			delete iter->second;
	}
	
	
	
	
	/* build cache */
	void Manager::buildCache ()
	{
		DirectoryMap::iterator iter;
		
		/* get entries from all mounts */
		for (iter = mMounts.begin(); iter != mMounts.end(); ++iter)
		{
			Directory* dir = iter->second;
			
			/* get mount directories and files */
			DirectoryMap& dirs = dir->getDirectories();
			FileMap& files = dir->getFiles();
			
			mDirCache.insert (dirs.begin(), dirs.end());
			mFileCache.insert (files.begin(), files.end());
		}
		
		mCached = true;
	}
	
	
	
	/* mount path */
	void Manager::mount (const String& path)
	{
		Log::Trace ("FileSystem::Manager", "Mounting path: " + path);
		
		
#ifdef SOMA_SYSTEM_UNIX
		if (mMounts.find (path) == mMounts.end())
			mMounts[path] = new PosixDirectory (path);
#elif defined SOMA_SYSTEM_WIN32
		if (mMounts.find (path) == mMounts.end())
			mMounts[path] = new Win32Directory (path);
#endif
	}
	
	
	/* unmount path */
	void Manager::unmount (const String& path)
	{
		Log::Trace ("FileSystem::Manager", "Unmounting path: " + path);
		
		
		Directory* dir = mMounts[path];
		
		if (dir) delete dir;
		mMounts.erase (path);
	}
	
	
	
	/* find file */
	File* Manager::findFile (const String& path)
	{
		if (!mCached) buildCache();
		
		
		/* assume path is a file name */
		String file = path;
		
		
		/* split path into its components */
		StringList list = Strings::split (path, "/");
		
		/* got directory */
		if (list.size() > 0)
		{
			String file = list.back();
			list.pop_back ();
		}
		
		
		Directory* dir = 0;
		StringList::iterator iter;
		
		/* recurse into the directory path */
		for (iter = list.begin(); iter != list.end(); ++iter)
		{
			if (dir)
				dir = dir->getDirectories()[*iter];
			else
				dir = mDirCache[*iter];
			
			if (!dir) return 0;
		}
		
		
		if (dir)
			return dir->getFiles()[file];
		else
			return mFileCache[file];
	}
	
	
	
	/* get all directories */
	DirectoryMap& Manager::getDirectories ()
	{
		if (!mCached) buildCache();
		return mDirCache;
	}
	
	
	/* get all files */
	FileMap& Manager::getFiles ()
	{
		if (!mCached) buildCache();
		return mFileCache;
	}


}}
