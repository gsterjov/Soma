
#include "Soma/FileSystem/PosixDirectory.h"

#include <dirent.h>
#include <errno.h>
#include <cstdio>  /* perror */

#ifndef _DIRENT_HAVE_D_TYPE
#include <sys/stat.h>
#endif

#include "Soma/FileSystem/PosixFile.h"


namespace Soma {
namespace FileSystem {


	/* constructor */
	PosixDirectory::PosixDirectory (const String& path)
	: mPath (path),
	  mCached (false)
	{
		/* make sure there is a trailing slash */
		if (mPath[mPath.size()-1] != '/')
			mPath += '/';
		
		
		/* get directory name */
		int start = mPath.find_last_of ('/', mPath.size() - 2) + 1;
		int end = mPath.size() - start - 1;
		
		mName = mPath.substr (start, end);
	}
	
	
	/* destructor */
	PosixDirectory::~PosixDirectory ()
	{
		DirectoryMap::iterator iter;
		
		/* delete subdirs */
		for (iter = mDirCache.begin(); iter != mDirCache.end(); ++iter)
			delete iter->second;
		
		
		FileMap::iterator it;
		
		/* delete files */
		for (it = mFileCache.begin(); it != mFileCache.end(); ++it)
			delete it->second;
	}
	
	
	
	/* build cache */
	void PosixDirectory::buildCache ()
	{
		/* open this directory */
		DIR* dir = opendir (mPath.c_str());
		
		/* open failed */
		if (!dir)
		{
			perror (String("Error reading directory '" + mPath + "'").c_str());
			return;
		}
		
		
		/* clear error buffer */
		errno = 0;
		struct dirent* entry;
		
		
		/* add all subdirs to the list */
		while (entry = readdir (dir))
		{
			/* get full path */
			String path = mPath + entry->d_name;
			
/* use filesystem entry type */
#ifdef _DIRENT_HAVE_D_TYPE
			
			/* entry type is a file */
			if (entry->d_type == DT_REG)
				mFileCache[entry->d_name] = new PosixFile (path);
			
			/* entry type is a directory */
			else if (entry->d_type == DT_DIR)
				mDirCache[entry->d_name] = new PosixDirectory (path);
			
/* otherwise make a stat call */
#else
			struct stat s;
			
			/* reading entry failed */
			if (stat (path.c_str(), &s) != 0)
				perror (String("Error reading '" + path + "'").c_str());
			
			/* got a file */
			else if (S_ISREG (s.st_mode))
				mFileCache[entry->d_name] = new PosixFile (path);
			
			/* got a directory */
			else if (S_ISDIR (s.st_mode))
				mDirCache[entry->d_name] = new PosixDirectory (path);
#endif
		}
		
		
		
		/* catch any errors */
		if (errno != 0)
			perror (String("Error reading directory '" + mPath + "'").c_str());
		
		
		/* close this directory */
		closedir (dir);
		
		mCached = true;
	}
	
	
	
	
	/* get sub directories */
	DirectoryMap& PosixDirectory::getDirectories()
	{
		/* scan the directory */
		if (!mCached) buildCache ();
		return mDirCache;
	}
	
	
	
	/* get files */
	FileMap& PosixDirectory::getFiles()
	{
		/* scan the directory */
		if (!mCached) buildCache ();
		return mFileCache;
	}

}}
