
#include "Soma/LinkedComponent.h"

#include "Soma/Log.h"
#include "Soma/Entity.h"


namespace Soma
{

	/* log source */
	const String LinkedComponent::LogSource = "LinkedComponent";
	
	
	
	/* set owner */
	void LinkedComponent::bind (const weak_ptr<Entity>& entity)
	{
		/* unbind old entity */
		if (mEntity) unbind ();
		
		
		mEntity = entity;
		if (!mEntity) return;
		
		
		Log::Debug (LogSource, "Binding entity: " + mEntity->getName());
		
		
		/* link existing sibling components */
		ComponentList::iterator iter;
		ComponentList siblings = mEntity->getComponents();
		
		for (iter = siblings.begin(); iter != siblings.end(); ++iter)
			link (*iter);
		
		
		
		/* link existing parent */
		ParentChangedEventArgs args;
		args.newParent = mEntity->getParent();
		
		onParentChanged (args);
		
		
		
		/* link existing children */
		EntityList::iterator it;
		EntityList children = mEntity->getChildren();
		
		for (it = children.begin(); it != children.end(); ++it)
			onChildAdded (*it);
		
		
		
		/* monitor sibling component changes */
		mEntity->ComponentAdded   += delegate (this, &LinkedComponent::onComponentAdded);
		mEntity->ComponentRemoved += delegate (this, &LinkedComponent::onComponentRemoved);
		
		/* monitor parent changes */
		mEntity->ParentChanged += delegate (this, &LinkedComponent::onParentChanged);
		
		/* monitor child changes */
		mEntity->ChildAdded   += delegate (this, &LinkedComponent::onChildAdded);
		mEntity->ChildRemoved += delegate (this, &LinkedComponent::onChildRemoved);
	}
	
	
	
	
	/* unbind the entity */
	void LinkedComponent::unbind ()
	{
		if (!mEntity) return;
		
		Log::Debug (LogSource, "Unbinding entity: " + mEntity->getName());
		
		
		/* stop monitoring sibling component changes */
		mEntity->ComponentAdded   -= delegate (this, &LinkedComponent::onComponentAdded);
		mEntity->ComponentRemoved -= delegate (this, &LinkedComponent::onComponentRemoved);
		
		/* stop monitoring parent changes */
		mEntity->ParentChanged -= delegate (this, &LinkedComponent::onParentChanged);
		
		/* stop monitoring child changes */
		mEntity->ChildAdded   -= delegate (this, &LinkedComponent::onChildAdded);
		mEntity->ChildRemoved -= delegate (this, &LinkedComponent::onChildRemoved);
		
		
		
		/* unlink all sibling components */
		ComponentList::iterator iter;
		ComponentList siblings = mEntity->getComponents();
		
		for (iter = siblings.begin(); iter != siblings.end(); ++iter)
			unlink (*iter);
		
		
		
		/* unlink parent */
		ParentChangedEventArgs args;
		args.oldParent = mEntity->getParent();
		
		onParentChanged (args);
		
		
		
		/* unlink all children */
		EntityList::iterator it;
		EntityList children = mEntity->getChildren();
		
		for (it = children.begin(); it != children.end(); ++it)
			onChildRemoved (*it);
		
		
		mEntity.release ();
	}
	
	
	
	
	
	
	/* sibling component added */
	void LinkedComponent::onComponentAdded (const smart_ptr<Component>& component)
	{
		link (component);
	}
	
	
	/* sibling component removed */
	void LinkedComponent::onComponentRemoved (const smart_ptr<Component>& component)
	{
		unlink (component);
	}
	
	
	
	
	
	/* parent entity changed */
	void LinkedComponent::onParentChanged (const ParentChangedEventArgs& args)
	{
		/* unlink old parent */
		if (args.oldParent)
		{
			/* stop monitoring component changes */
			args.oldParent->ComponentAdded   -= delegate (this, &LinkedComponent::onParentComponentAdded);
			args.oldParent->ComponentRemoved -= delegate (this, &LinkedComponent::onParentComponentRemoved);
			
			
			ComponentList::iterator iter;
			ComponentList components = args.oldParent->getComponents();
			
			/* unlink all parent components */
			for (iter = components.begin(); iter != components.end(); ++iter)
				unlinkParent (*iter);
		}
		
		
		/* link new parent */
		if (args.newParent)
		{
			/* get new parent components */
			ComponentList::iterator iter;
			ComponentList components = args.newParent->getComponents();
			
			/* link existing components */
			for (iter = components.begin(); iter != components.end(); ++iter)
				linkParent (*iter);
			
			
			/* monitor component changes */
			args.newParent->ComponentAdded   += delegate (this, &LinkedComponent::onParentComponentAdded);
			args.newParent->ComponentRemoved += delegate (this, &LinkedComponent::onParentComponentRemoved);
		}
	}
	
	
	
	/* parent component added */
	void LinkedComponent::onParentComponentAdded (const smart_ptr<Component>& component)
	{
		linkParent (component);
	}
	
	
	/* parent component removed */
	void LinkedComponent::onParentComponentRemoved (const smart_ptr<Component>& component)
	{
		unlinkParent (component);
	}
	
	
	
	
	
	
	/* child entity added */
	void LinkedComponent::onChildAdded (const weak_ptr<Entity>& child)
	{
		ComponentList::iterator iter;
		ComponentList components = child->getComponents();
		
		/* link existing components */
		for (iter = components.begin(); iter != components.end(); ++iter)
			linkChild (*iter);
		
		
		/* monitor component changes */
		child->ComponentAdded   += delegate (this, &LinkedComponent::onChildComponentAdded);
		child->ComponentRemoved += delegate (this, &LinkedComponent::onChildComponentRemoved);
	}
	
	
	/* child entity removed */
	void LinkedComponent::onChildRemoved (const weak_ptr<Entity>& child)
	{
		ComponentList::iterator iter;
		ComponentList components = child->getComponents();
		
		/* unlink existing components */
		for (iter = components.begin(); iter != components.end(); ++iter)
			unlinkChild (*iter);
		
		
		/* stop monitoring component changes */
		child->ComponentAdded   -= delegate (this, &LinkedComponent::onChildComponentAdded);
		child->ComponentRemoved -= delegate (this, &LinkedComponent::onChildComponentRemoved);
	}
	
	
	
	/* child component added */
	void LinkedComponent::onChildComponentAdded (const smart_ptr<Component>& component)
	{
		linkChild (component);
	}
	
	
	/* child component removed */
	void LinkedComponent::onChildComponentRemoved (const smart_ptr<Component>& component)
	{
		unlinkChild (component);
	}

}
