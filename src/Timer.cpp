
#include "Soma/Timer.h"


namespace Soma
{

	/* constructor */
	Timer::Timer ()
	{
		reset ();
	}
	
	
	/* destructor */
	Timer::~Timer ()
	{
	}
	
	
	
	
#ifdef SOMA_SYSTEM_UNIX
	timespec time_diff (const timespec& start, const timespec& end)
	{
		timespec diff;
		
		if (end.tv_nsec - start.tv_nsec < 0)
		{
			diff.tv_sec = end.tv_sec - start.tv_sec - 1;
			diff.tv_nsec = 1000000000L + end.tv_nsec - start.tv_nsec;
		}
		else
		{
			diff.tv_sec = end.tv_sec - start.tv_sec;
			diff.tv_nsec = end.tv_nsec - start.tv_nsec;
		}
		
		return diff;
	}
#endif
	
	
	
	
	/* reset timer */
	void Timer::reset ()
	{
#ifdef SOMA_SYSTEM_UNIX
		/* get latest time */
		clock_gettime (CLOCK_MONOTONIC, &mStart);
		mLast = mStart;
#endif
	}
	
	
	
	/* block thread */
	float Timer::sleep (float time)
	{
#ifdef SOMA_SYSTEM_UNIX
		timespec start, end;
		
		/* get the start time of sleep */
		clock_gettime (CLOCK_MONOTONIC, &start);
		
		long sec = 0;
		long nsec = 0;
		
		/* calculate sleep time from seconds */
		nsec = time * 1000000000L;
		sec = nsec / 1000000000L;
		
		
		timespec td;
		td.tv_sec = sec;
		td.tv_nsec = nsec;
		
		/* sleep */
		nanosleep (&td, NULL);
		
		
		/* get the end time of sleep */
		clock_gettime (CLOCK_MONOTONIC, &end);
		
		/* return actual time spent sleeping */
		timespec diff = time_diff (start, end);
		return diff.tv_sec + (diff.tv_nsec / 1000000000.0f);
#endif
	}
	
	
	
	
	/* time in microseconds */
	float Timer::getMicroseconds (bool splitTime)
	{
#ifdef SOMA_SYSTEM_UNIX
		timespec delta = mLast;
		
		/* get latest time */
		clock_gettime (CLOCK_MONOTONIC, &mLast);
		
		/* get appropriate time difference */
		timespec diff = splitTime ? time_diff (delta, mLast) : time_diff (mStart, mLast);
		return (diff.tv_sec / 1000000.0f) + (diff.tv_nsec / 1000.0f);
#endif
	}
	
	
	
	/* time in milliseconds */
	float Timer::getMilliseconds (bool splitTime)
	{
#ifdef SOMA_SYSTEM_UNIX
		timespec delta = mLast;
		
		/* get latest time */
		clock_gettime (CLOCK_MONOTONIC, &mLast);
		
		/* get appropriate time difference */
		timespec diff = splitTime ? time_diff (delta, mLast) : time_diff (mStart, mLast);
		return (diff.tv_sec / 1000.0f) + (diff.tv_nsec / 1000000.0f);
#endif
	}
	
	
	
	/* time in seconds */
	float Timer::getSeconds (bool splitTime)
	{
#ifdef SOMA_SYSTEM_UNIX
		timespec delta = mLast;
		
		/* get latest time */
		clock_gettime (CLOCK_MONOTONIC, &mLast);
		
		/* get appropriate time difference */
		timespec diff = splitTime ? time_diff (delta, mLast) : time_diff (mStart, mLast);
		return diff.tv_sec + (diff.tv_nsec / 1000000000.0f);
#endif
	}

}
