
#include "Soma/AssetEntity.h"

#include "Soma/Log.h"
#include "Soma/Asset.h"


namespace Soma
{

	/* log source */
	const String AssetEntity::LogSource = "AssetEntity";
	
	
	
	/* named constructor */
	AssetEntity::AssetEntity (const String& name) : Entity(name)
	{
		
	}
	
	
	
	/* path constructor */
	AssetEntity::AssetEntity (const String& name, const Vri& vri)
	: Entity (name)
	{
		setAsset (vri);
	}
	
	
	
	/* asset constructor */
	AssetEntity::AssetEntity (const String& name, const smart_ptr<Asset>& asset)
	: Entity (name)
	{
		setAsset (asset);
	}
	
	
	
	/* destructor */
	AssetEntity::~AssetEntity ()
	{
		
	}
	
	
	
	
	/* set asset by path */
	void AssetEntity::setAsset (const Vri& vri)
	{
		setAsset (AssetManager::instance().get(vri));
	}
	
	
	
	/* set asset */
	void AssetEntity::setAsset (const smart_ptr<Asset>& asset)
	{
		Log::Debug (LogSource, "Setting asset resource on asset entity: " + getName());
		
		/* load asset */
		mAsset = asset;
		mAsset->load ();
		
		
		bool hasMesh = false;
		
		
		/* get asset resources */
		Asset::ResourceList::iterator iter;
		Asset::ResourceList resources = mAsset->getResources();
		
		/* add appropriate asset components */
		for (iter = resources.begin(); iter != resources.end(); ++iter)
		{
			smart_ptr<Entity> child = new Entity (iter->name);
			
			
			/* get transformation */
			Vector pos; Vector scale; Quaternion rot;
			iter->transform.decompose (pos, scale, rot);
			
			/* apply transformation */
			child->setOrientation (rot);
			child->setPosition (pos);
			
			
			/* found visual mesh */
			if (iter->resource->getType() == "visual/mesh")
			{
				child->addComponents ("entity.visual.node, entity.visual.mesh");
				
				child->setProperty ("entity.visual.node", "scale", scale);
				child->setProperty ("entity.visual.mesh", "resource", iter->resource);
				
				hasMesh = true;
			}
			
			
			/* link sub entity */
			addChild (child);
		}
		
		
		/* add root visual node */
		if (hasMesh)
			addComponents ("entity.visual.node");
	}

}
