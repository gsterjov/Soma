
#include "Soma/GUI/Layer.h"

#include "Soma/GUI/Gorilla.h"

#include "Soma/Log.h"
#include "Soma/Registry.h"


namespace Soma {
namespace GUI {

	/* log source */
	const String Layer::LogSource = "GUI::Layer";
	
	
	
	/* constructor */
	Layer::Layer (const String& name, Gorilla::Layer* layer)
	: mName (name),
	  mLayer (layer)
	{
		Log::Debug (LogSource, "Creating layer: " + mName);
	}
	
	
	
	/* destructor */
	Layer::~Layer ()
	{
		if (mLayer)
		{
			Log::Debug (LogSource, "Destroying layer: " + mName);
		}
	}
	
	
}}
