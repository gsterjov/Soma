
#include "Soma/GUI/Screen.h"

#include "Soma/GUI/Gorilla.h"

#include "Soma/Log.h"
#include "Soma/Registry.h"

#include "Soma/GUI/Manager.h"
#include "Soma/GUI/Layer.h"
#include "Soma/Visual/Viewport.h"


namespace Soma {
namespace GUI {

	/* log source */
	const String Screen::LogSource = "GUI::Screen";
	
	
	
	/* constructor */
	Screen::Screen (const String& name,
	                const String& atlas,
	                const smart_ptr<Visual::Viewport>& viewport)
	: mName (name)
	{
		Log::Debug (LogSource, "Creating the screen '" + mName + "' with the atlas: " + atlas);
		
		Gorilla::Silverback* gorilla = Manager::instance().getGorilla();
		
		gorilla->loadAtlas (atlas);
		mScreen = gorilla->createScreen (viewport->getViewport(), atlas);
	}
	
	
	
	/* destructor */
	Screen::~Screen ()
	{
		if (mScreen)
		{
			Log::Debug (LogSource, "Destroying screen: " + mName);
			
			Gorilla::Silverback* gorilla = Manager::instance().getGorilla();
			gorilla->destroyScreen (mScreen);
		}
	}
	
	
	
	
	/* create a layer */
	smart_ptr<Layer> Screen::createLayer (const String& name, int index)
	{
		Layer* layer = new Layer (name, mScreen->createLayer (index));
		mLayers[name] = layer;
		
		return layer;
	}
	
	
}}
