
#include "Soma/GUI/Manager.h"

#include "Soma/GUI/Gorilla.h"

#include "Soma/Log.h"
#include "Soma/Registry.h"

#include "Soma/GUI/Screen.h"


namespace Soma {
namespace GUI {

	/* log source */
	const String Manager::LogSource = "GUI::Manager";
	
	
	/* subsystem type */
	REGISTER_TYPE (SubSystem, Manager, "GUI");
	
	
	
	
	/* constructor */
	Manager::Manager () : mInitialised(false)
	{
		Log::Debug (LogSource, "Creating GUI manager");
	}
	
	
	/* destructor */
	Manager::~Manager()
	{
		Log::Debug (LogSource, "Destroying GUI manager");
		
		if (mInitialised)
			shutdown ();
	}
	
	
	
	/* initialise gorilla */
	bool Manager::initialise ()
	{
		Log::Message (LogSource, "Initialising GUI manager");
		
		mGorilla = new Gorilla::Silverback ();
		
		mInitialised = true;
		return true;
	}
	
	
	
	/* reinitialise gorilla */
	bool Manager::reinitialise ()
	{
		Log::Message (LogSource, "Reinitialising GUI manager");
		
		shutdown ();
		return initialise ();
	}
	
	
	
	/* shut down gorilla */
	void Manager::shutdown ()
	{
		Log::Message (LogSource, "Shutting down GUI manager");
		
		delete mGorilla;
		mGorilla = 0;
	}
	
	
	
	
	/* create a new screen */
	smart_ptr<Screen> Manager::createScreen (const String name,
	                                         const String atlas,
	                                         const smart_ptr<Visual::Viewport>& viewport)
	{
		Screen* screen = new Screen (name, atlas, viewport);
		mScreens[name] = screen;
		
		return screen;
	}
	
	
	
	
	/* save gui settings */
	bool Manager::save (ConfigInterface& config)
	{
		Log::Debug (LogSource, "Saving GUI manager settings");
		return true;
	}
	
	
	
	/* restore gui settings */
	bool Manager::restore (const ConfigInterface& config)
	{
		Log::Debug (LogSource, "Restoring GUI manager settings");
		return true;
	}

}}
