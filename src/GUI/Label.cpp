
#include "Soma/GUI/Label.h"

#include <cassert>
#include "Soma/GUI/Gorilla.h"

#include "Soma/Log.h"
#include "Soma/GUI/Layer.h"


namespace Soma {
namespace GUI {

	/* log source */
	const String Label::LogSource = "GUI::Label";
	
	
	
	/* constructor */
	Label::Label (const String& name, const smart_ptr<Layer>& layer)
	: mCaption (0),
	  Widget (name)
	{
		Log::Debug (LogSource, "Creating label '" + mName + "' on the layer: " + layer->getName());
		
		mCaption = layer->getLayer()->createCaption (14, 0, 0, "");
	}
	
	
	
	/* destructor */
	Label::~Label ()
	{
		if (mCaption)
		{
			Log::Debug (LogSource, "Destroying label: " + mName);
		}
	}
	
	
	
	
	/* get label text */
	String Label::getText () const
	{
		assert (mCaption);
		return mCaption->text();
	}
	
	
	
	/* set label text */
	void Label::setText (const String& text)
	{
		assert (mCaption);
		mCaption->text (text);
	}
	
	
	
	/* set label position */
	void Label::setPosition (float left, float top)
	{
		assert (mCaption);
		mCaption->left (left);
		mCaption->top (top);
	}
	
	
	
	/* set label left position */
	void Label::setLeft (float left)
	{
		assert (mCaption);
		mCaption->left (left);
	}
	
	
	
	/* set label top position */
	void Label::setTop (float top)
	{
		assert (mCaption);
		mCaption->top (top);
	}
	
	
	
	/* set label width */
	void Label::setWidth (float width)
	{
		assert (mCaption);
		mCaption->width (width);
	}
	
	
	
	/* set label height */
	void Label::setHeight (float height)
	{
		assert (mCaption);
		mCaption->height (height);
	}

}}
