
#include "Soma/Formats/Collada/Mesh.h"

#include <ColladaParser/Document.h>

#include <OgreMesh.h>
#include <OgreSubMesh.h>
#include <OgreMeshManager.h>
#include <OgreHardwareBufferManager.h>
#include <OgreMaterialManager.h>

#include "Soma/Log.h"
#include "Soma/Registry.h"


namespace Soma {
namespace Formats {
namespace Collada {


	/* register format type */
	REGISTER_TYPE (Visual::MeshManager::Format, MeshFormat, "Collada Mesh");
	
	
	
	/* constructor */
	Mesh::Mesh (const Vri& vri) : mVri(vri), mMesh(0)
	{
	}
	
	
	/* empty constructor */
	Mesh::Mesh () : mMesh (0)
	{
	}
	
	
	/* destructor */
	Mesh::~Mesh ()
	{
	}
	
	
	
	
	/* bind material symbol */
	void Mesh::bindMaterial (const String& symbol, const String& material)
	{
		mMaterials[symbol] = material;
	}
	
	
	
	
	/* create ogre mesh */
	Ogre::Mesh* Mesh::createMesh ()
	{
		/* already loaded */
		if (mMesh) return mMesh;
		
		
		using namespace ColladaParser;
		
		/* open document */
		Document doc (mVri.asset);
		if (!doc.open()) return 0;
		
		
		GeometryList::iterator iter;
		GeometryList geoms = doc.getGeometries();
		
		/* find wanted geometry */
		for (iter = geoms.begin(); iter != geoms.end(); ++iter)
		{
			Geometry* geom = *iter;
			
			/* found matching geometry */
			if (mVri.resource.empty() || geom->getID() == mVri.resource)
			{
				loadGeometry (geom, mVri.path);
				break;
			}
		}
		
		
		return mMesh;
	}
	
	
	
	/* handle collada geometry */
	void Mesh::loadGeometry (const ColladaParser::Geometry* geometry,
	                         const String& name)
	{
		Log::Debug ("ColladaMesh", "Loading mesh from Collada geometry: " + name);
		
		
		/* create manual mesh */
		Ogre::MeshPtr mesh = Ogre::MeshManager::getSingleton().createManual (name, "General");
		
		/* bounding box */
		Ogre::AxisAlignedBox box;
		
		
		
		using namespace ColladaParser;
		
		/* get submesh primitives */
		const std::vector<Primitive*>& primitives = geometry->getPrimitives();
		std::vector<Primitive*>::const_iterator iter;
		
		
		/* add all submesh primitives */
		for (iter = primitives.begin(); iter != primitives.end(); ++iter)
		{
			Primitive* primitive = *iter;
			
			
			/* vertex and index count */
			size_t vertexCount = primitive->getIndexCount();
			size_t indexCount  = primitive->getIndexCount();
			
			/* create submesh */
			Ogre::SubMesh* submesh = mesh->createSubMesh ();
			
			String mat = primitive->getMaterial();
			
			
			/* set bound material */
			if (mMaterials.find (mat) != mMaterials.end())
			{
				Log::Debug ("ColladaMesh", "Setting material on submesh: " + mMaterials[mat]);
				submesh->setMaterialName (mMaterials[mat]);
			}
			
			
			/* vertex data */
			Ogre::VertexData* data = new Ogre::VertexData ();
			data->vertexCount = vertexCount;
			
			submesh->vertexData = data;
			submesh->useSharedVertices = false;
			
			
			/* vertex declaration */
			size_t offset = 0;
			Ogre::VertexDeclaration* decl = data->vertexDeclaration;
			
			/* position and normals */
			offset += decl->addElement (0, offset, Ogre::VET_FLOAT3, Ogre::VES_POSITION).getSize();
			
			if (primitive->hasNormals())
				offset += decl->addElement (0, offset, Ogre::VET_FLOAT3, Ogre::VES_NORMAL).getSize();
			
			if (primitive->hasTexCoords())
				offset += decl->addElement (0, offset, Ogre::VET_FLOAT2, Ogre::VES_TEXTURE_COORDINATES).getSize();
			
			
			
			/* vertex buffer */
			Ogre::HardwareVertexBufferSharedPtr vbuf = Ogre::HardwareBufferManager::getSingleton().createVertexBuffer (
				decl->getVertexSize (0),
				vertexCount,
				Ogre::HardwareBuffer::HBU_STATIC_WRITE_ONLY,
				true
			);
			
			
			/* bind vertex buffer */
			data->vertexBufferBinding->setBinding (0, vbuf);
			
			
			
			/* index buffer */
			Ogre::HardwareIndexBufferSharedPtr ibuf = Ogre::HardwareBufferManager::getSingleton().createIndexBuffer (
				Ogre::HardwareIndexBuffer::IT_16BIT,
				indexCount,
				Ogre::HardwareBuffer::HBU_STATIC_WRITE_ONLY,
				true
			);
			
			
			/* link to submesh */
			submesh->indexData->indexBuffer = ibuf;
			submesh->indexData->indexCount = indexCount;
			submesh->indexData->indexStart = 0;
			
			
			
			
			/* lock vertex buffer and get the data pointer */
			float* vbuffer = static_cast<float*> (vbuf->lock (Ogre::HardwareBuffer::HBL_DISCARD));
			
			
			
			/* populate vertex buffer */
			for (int i = 0; i < vertexCount; i++)
			{
				/* get vertex and normal vector */
				ColladaParser::Vector vert = primitive->getVertex (i);
				
				/* add to vertex buffer */
				*vbuffer++ = vert.x;
				*vbuffer++ = vert.y;
				*vbuffer++ = vert.z;
				
				
				if (primitive->hasNormals())
				{
					ColladaParser::Vector norm = primitive->getNormal (i);
					
					*vbuffer++ = norm.x;
					*vbuffer++ = norm.y;
					*vbuffer++ = norm.z;
				}
				
				if (primitive->hasTexCoords())
				{
					ColladaParser::TexCoord tex = primitive->getTexCoord (i);
					
					*vbuffer++ = tex.u;
					*vbuffer++ = 1-tex.v;
				}
				
				
				/* update bounding box dimensions */
				box.merge (Ogre::Vector3 (vert.x, vert.y, vert.z));
			}
			
			
			
			/* unlock vertex buffer */
			vbuf->unlock ();
			
			
			
			/* lock index buffer and get the data pointer */
			Ogre::uint16* ibuffer = static_cast<Ogre::uint16*> (ibuf->lock (Ogre::HardwareBuffer::HBL_DISCARD));
			
			
			/* populate index buffer */
			for (int i = 0; i < indexCount; i++)
			{
				/* add index */
				*ibuffer++ = static_cast<Ogre::uint16> (i);
			}
			
			
			/* unlock index buffer */
			ibuf->unlock ();
		}
		
		
		
		/* set mesh bounds */
		Ogre::Real radius = (box.getMaximum() - box.getMinimum()).length() / 2.0;
		mesh->_setBounds (box);
		mesh->_setBoundingSphereRadius (radius);
		
		
		/* set resource mesh */
		mMesh = mesh.get();
	}


}}}
