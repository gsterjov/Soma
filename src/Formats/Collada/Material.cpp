
#include "Soma/Formats/Collada/Material.h"

#include <ColladaParser/Document.h>

#include "Soma/Log.h"
#include "Soma/Registry.h"

#include "Soma/Visual/Technique.h"
#include "Soma/Visual/Pass.h"


namespace Soma {
namespace Formats {
namespace Collada {


	/* register format type */
	REGISTER_TYPE (Visual::MaterialManager::Format, MaterialFormat, "Collada Material");
	
	
	
	/* constructor */
	Material::Material (const Vri& vri) : mVri(vri)
	{
	}
	
	
	/* empty constructor */
	Material::Material ()
	{
	}
	
	
	/* destructor */
	Material::~Material ()
	{
	}
	
	
	
	/* load material */
	bool Material::load ()
	{
		if (mVri.path.empty()) return true;
		
		Log::Debug ("ColladaMaterial", "Loading material from Collada document: " + mVri.path);
		
		
		using namespace ColladaParser;
		
		
		/* open document */
		Document doc (mVri.asset);
		if (!doc.open()) return false;
		
		
		MaterialList::iterator iter;
		MaterialList materials = doc.getMaterials();
		
		/* find wanted material */
		for (iter = materials.begin(); iter != materials.end(); ++iter)
		{
			ColladaParser::Material* mat = *iter;
			
			/* found matching material */
			if (mVri.resource.empty() || mat->getID() == mVri.resource)
			{
				loadMaterial (doc, mat);
				break;
			}
		}
		
		
		return Soma::Visual::Material::load ();
	}
	
	
	
	/* handle collada material */
	void Material::loadMaterial (const ColladaParser::Document& doc,
	                             const ColladaParser::Material* material)
	{
		Log::Debug ("ColladaMaterial", "Loading material from Collada material: "
				+ material->getID());
		
		
		using namespace ColladaParser;
		
		
		/* create default material if we dont have one */
		if (!mMaterial) create (material->getID());
		
		
		/* get effect instance */
		StringList url = Strings::split (material->getEffect().url, "#");
		String effect_name = !url.empty() ? url[0] : "";
		
		
		/* get list of effects */
		EffectList::iterator iter;
		EffectList effects = doc.getEffects();
		
		Effect* effect = 0;
		
		/* get instanced effect */
		for (iter = effects.begin(); iter != effects.end(); ++iter)
		{
			/* found matching effect */
			if (effect_name == (*iter)->getID())
			{
				effect = *iter;
				break;
			}
		}
		
		
		/* no effect found */
		if (!effect) return;
		
		
		ProfileCommonList::iterator it;
		ProfileCommonList profiles = effect->getCommonProfiles();
		
		/* add all common profile techniques */
		for (it = profiles.begin(); it != profiles.end(); ++it)
		{
			ProfileCommon* profile = *it;
			ProfileCommon::Technique tech = profile->getTechnique();
			
			
			/* create technique */
			smart_ptr<Visual::Technique> technique = add (tech.sid);
			smart_ptr<Visual::Pass> pass = technique->add ("pass1");
			
			
			/* constant technique */
			if (tech.constant)
			{
				ColladaParser::Colour col = tech.constant->emission;
				pass->setEmissive (col.r, col.g, col.b);
			}
			
			/* lambert technique */
			else if (tech.lambert)
			{
				ColladaParser::Colour col = tech.lambert->emission;
				pass->setEmissive (col.r, col.g, col.b);
				
				col = tech.lambert->ambient;
				pass->setAmbient (col.r, col.g, col.b);
				
				col = tech.lambert->diffuse;
				pass->setDiffuse (col.r, col.g, col.b, col.a);
			}
			
			/* phong technique */
			else if (tech.phong)
			{
				ColladaParser::Colour col = tech.phong->emission;
				pass->setEmissive (col.r, col.g, col.b);
				
				col = tech.phong->ambient;
				pass->setAmbient (col.r, col.g, col.b);
				
				col = tech.phong->diffuse;
				pass->setDiffuse (col.r, col.g, col.b, col.a);
				
				col = tech.phong->specular;
				pass->setSpecular (col.r, col.g, col.b, col.a);
			}
			
			/* blinn technique */
			else if (tech.blinn)
			{
				ColladaParser::Colour col = tech.phong->emission;
				pass->setEmissive (col.r, col.g, col.b);
				
				col = tech.phong->ambient;
				pass->setAmbient (col.r, col.g, col.b);
				
				col = tech.phong->diffuse;
				pass->setDiffuse (col.r, col.g, col.b, col.a);
				
				col = tech.phong->specular;
				pass->setSpecular (col.r, col.g, col.b, col.a);
			}
		}
		
		
		/* load material */
		Soma::Visual::Material::load ();
	}


}}}
