
#include "Soma/Formats/Collada/Asset.h"

#include <ColladaParser/Document.h>
#include <ColladaParser/Transform.h>

#include "Soma/Log.h"
#include "Soma/Registry.h"

#include "Soma/Formats/Collada/Mesh.h"
#include "Soma/Formats/Collada/Material.h"


namespace Soma {
namespace Formats {
namespace Collada {
	
	
	/* register format type */
	REGISTER_TYPE (AssetManager::Format, AssetFormat, "Collada Asset");
	
	
	
	/* constructor */
	Asset::Asset (const Vri& vri) : mVri(vri)
	{
		
	}
	
	
	/* destructor */
	Asset::~Asset ()
	{
		unload ();
	}
	
	
	
	/* load asset */
	bool Asset::load ()
	{
		Log::Debug ("ColladaAsset", "Loading asset from Collada file: " + mVri.asset);
		
		
		using namespace ColladaParser;
		
		
		/* open document */
		Document doc (mVri.asset);
		
		if (!doc.open())
			return Log::Error ("ColladaAsset", "Failed to open "
				"the Collada document: " + mVri.asset);
		
		
		/* get all visual scenes */
		VisualSceneList::iterator iter;
		VisualSceneList scenes = doc.getVisualScenes();
		
		/* process all nodes in all visual scenes */
		for (iter = scenes.begin(); iter != scenes.end(); ++iter)
		{
			VisualScene* scene = *iter;
			
			NodeList::iterator it;
			NodeList nodes = scene->getNodes();
			
			/* parse all nodes */
			for (it = nodes.begin(); it != nodes.end(); ++it)
				parseNode (doc, *it);
		}
		
		
		/* chain up to asset */
		return Soma::Asset::load();
	}
	
	
	
	
	/* parse node */
	void Asset::parseNode (const ColladaParser::Document& doc,
	                       const ColladaParser::Node* node)
	{
		Log::Trace ("ColladaAsset", "Loading node: " + node->getID());
		
		
		using namespace ColladaParser;
		
		
		/* local node transformation matrix */
		Matrix transform = parseTransforms (node);
		
		
		
		/* get geometries */
		GeometryInstanceList::iterator iter;
		GeometryInstanceList geoms = node->getGeometries();
		
		/* add all geometry instances */
		for (iter = geoms.begin(); iter != geoms.end(); ++iter)
		{
			GeometryInstance* instance = *iter;
			
			/* matching geometry */
			Geometry* geometry = 0;
			
			
			/* mesh name */
			StringList str = Strings::split (instance->url, "#");
			if (str.empty()) continue;
			
			String name = str[0];
			
			
			GeometryList::iterator it;
			GeometryList list = doc.getGeometries();
			
			/* find matching geometry */
			for (it = list.begin(); it != list.end(); ++it)
			{
				if ((*it)->getID() == name)
				{
					geometry = *it;
					break;
				}
			}
			
			/* no geometry found */
			if (!geometry) continue;
			
			
			/* create mesh resource identifier */
			Vri mesh_vri = mVri.asset + "#" + geometry->getID();
			
			/* look for existing mesh */
			Visual::MeshManager* mesh_man = Visual::MeshManager::instancePtr();
			smart_ptr<Visual::Mesh> mesh_res = mesh_man->find (mesh_vri);
			
			/* found existing mesh */
			if (mesh_res)
			{
				/* shortcut loading process */
				Instance inst;
				inst.name = node->getID();
				inst.transform = transform;
				inst.resource = mesh_res;
				
				mResources.push_back (inst);
				return;
			}
			
			/* no mesh found, create mesh */
			smart_ptr<Collada::Mesh> mesh = new Collada::Mesh ();
			mesh_man->add (mesh_vri, mesh);
			
			
			
			/* load and bind materials */
			if (instance->materials)
			{
				StringMap::iterator i;
				StringMap symbols = instance->materials->materials;
				
				/* bind materials to mesh */
				for (i = symbols.begin(); i != symbols.end(); ++i)
				{
					/* matching material */
					ColladaParser::Material* material = 0;
					
					/* get material name from symbol */
					StringList tmp = Strings::split (i->second, "#");
					if (tmp.empty()) continue;
					
					String mat = tmp[0];
					
					
					MaterialList::iterator mat_iter;
					MaterialList materials = doc.getMaterials();
					
					/* find matching material */
					for (mat_iter = materials.begin(); mat_iter != materials.end(); ++mat_iter)
					{
						if ((*mat_iter)->getID() == mat)
						{
							material = *mat_iter;
							break;
						}
					}
					
					/* no material found */
					if (!material) continue;
					
					
					/* create material resource identifier */
					Vri mat_vri = mVri.asset + "#" + material->getID();
					
					/* look for existing material */
					Visual::MaterialManager* mat_man = Visual::MaterialManager::instancePtr();
					smart_ptr<Visual::Material> mat_res = mat_man->find (mat_vri);
					
					/* no existing material found */
					if (!mat_res)
					{
						Collada::Material* m = new Collada::Material ();
						m->loadMaterial (doc, material);
						mat_man->add (mat_vri, m);
					}
					
					
					/* bind to mesh */
					mesh->bindMaterial (i->first, material->getID());
					
				} /* materials */
			}
			
			
			/* load geometry */
			mesh->loadGeometry (geometry, name);
			
			/* add mesh instance */
			Instance inst;
			inst.name = node->getID();
			inst.transform = transform;
			inst.resource = mesh;
			
			mResources.push_back (inst);
			
		} /* geometry instances */
		
	}
	
	
	
	
	/* parse node transforms */
	Matrix Asset::parseTransforms (const ColladaParser::Node* node)
	{
		using namespace ColladaParser;
		
		
		/* local node transformation matrix */
		Matrix transform = Matrix::IDENTITY;
		
		
		TransformList::iterator i;
		TransformList transforms = node->getTransforms();
		
		/* get complete transformation */
		for (i = transforms.begin(); i != transforms.end(); ++i)
		{
			Transform* tform = *i;
			
			
			/* got translation */
			if (tform->getType() == Transform::TRANSLATE)
			{
				ColladaParser::Vector vec = tform->getTranslation();
				
				/* create translation matrix */
				Matrix mat = Matrix::IDENTITY;
				mat.setTranslation (Vector (vec.x, vec.y, vec.z));
				transform = transform * mat;
			}
			
			/* got scale */
			else if (tform->getType() == Transform::SCALE)
			{
				ColladaParser::Vector vec = tform->getScale();
				
				/* create scale matrix */
				Matrix mat = Matrix::IDENTITY;
				mat.setScale (Vector (vec.x, vec.y, vec.z));
				transform = transform * mat;
			}
			
			/* got rotation */
			else if (tform->getType() == Transform::ROTATE)
			{
				ColladaParser::Vector vec = tform->getRotation();
				
				Quaternion quat = Quaternion::IDENTITY;
				
				/* create quaternion from angle/exis */
				if (vec.x) quat = Quaternion (Degree(vec.x), Vector::UNIT_X);
				if (vec.y) quat = Quaternion (Degree(vec.y), Vector::UNIT_Y);
				if (vec.z) quat = Quaternion (Degree(vec.z), Vector::UNIT_Z);
				
				/* create rotation matrix */
				Matrix mat (quat);
				transform = transform * mat;
			}
		}
		
		
		return transform;
	}

}}}
