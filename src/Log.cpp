
#include "Soma/Log.h"

#include <iostream>


namespace Soma
{

	/* log source */
	String Log::LogSource = "Log";
	
	
	
	/* constructor */
	Log::Log (const String& filename)
	{
		mFile.open (filename.c_str());
		Message (LogSource, "Logging at '" + filename + "'");
	}
	
	
	/* destructor */
	Log::~Log ()
	{
		mFile.close ();
		Message (LogSource, "Closing log");
	}
	
	
	
	/* trace message */
	void Log::sendTrace (const String& source, const String& trace)
	{
		if (mVerbosity <= TRACE)
		{
			std::cout << trace << std::endl;
			mFile << "(Soma::" << source << ") - Trace: " << trace << std::endl;
		}
	}
	
	
	/* debug message */
	void Log::sendDebug (const String& source, const String& debug)
	{
		if (mVerbosity <= DEBUG)
		{
			std::cout << debug << std::endl;
			mFile << "(Soma::" << source << ") - Debug: " << debug << std::endl;
		}
	}
	
	
	/* information message */
	void Log::sendMessage (const String& source, const String& message)
	{
		if (mVerbosity <= NOTIFY)
		{
			std::cout << message << std::endl;
			mFile << "(Soma::" << source << ") - Information: " << message << std::endl;
		}
	}
	
	
	/* warning message */
	void Log::sendWarning (const String& source, const String& warning)
	{
		if (mVerbosity <= WARNING)
		{
			std::clog << warning << std::endl;
			mFile << "(Soma::" << source << ") - Warning: " << warning << std::endl;
		}
	}
	
	
	/* error message */
	void Log::sendError (const String& source, const String& error)
	{
		if (mVerbosity <= ERROR)
		{
			std::cerr << error << std::endl;
			mFile << "(Soma::" << source << ") - Error: " << error << std::endl;
		}
	}

}
