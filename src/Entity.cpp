
#include "Soma/Entity.h"

#include "Soma/Log.h"
#include "Soma/Registry.h"
#include "Soma/Exception.h"
#include "Soma/EntityComponent.h"

#include "Soma/World.h"


namespace Soma
{

	/* log source */
	const String Entity::LogSource = "Entity";
	
	
	
	/* named constructor */
	Entity::Entity (const String& name) : mName(name)
	{
		Log::Debug (LogSource, "Creating entity: " + mName);
	}
	
	
	/* constructor with components */
	Entity::Entity (const String& name, const String& types) : mName(name)
	{
		Log::Debug (LogSource, "Creating entity: " + mName);
		
		addComponents (types);
	}
	
	
	/* we unload and nullify the parent for all the children within the entity
	 * so they can be used elsewhere without problems of it being 'orphaned'.
	 * the same is done for components so that they are in an unloaded state
	 * when the entity hierarchy is changed and they are no longer a part of it
	 * 
	 * we dont have to worry about the parent containing an invalid reference
	 * to this entity because child maps are strongly referenced meaning that
	 * so long as this entity has a parent it cannot be destroyed, unless it
	 * is done so outside of the SmartPointer context, which is undefined */
	Entity::~Entity ()
	{
		Log::Debug (LogSource, "Destroying entity: " + mName);
		
		unload ();
		
		
		EntityMap::const_iterator iter;
		
		/* clear child parent field */
		for (iter = mChildren.begin(); iter != mChildren.end(); ++iter)
		{
			iter->second->setWorld (0);
			iter->second->setParent (0);
		}
	}
	
	
	
	
	/* get child */
	smart_ptr<Entity> Entity::getChild (const String& name) const
	{
		/* look for entity */
		EntityMap::const_iterator iter = mChildren.find (name);
		
		/* found entity */
		if (iter != mChildren.end())
			return iter->second;
		
		/* return null reference */
		else return smart_ptr<Entity>();
	}
	
	
	/* get all children */
	EntityList Entity::getChildren () const
	{
		EntityList list;
		EntityMap::const_iterator iter;
		
		/* add entities to list */
		for (iter = mChildren.begin(); iter != mChildren.end(); ++iter)
			list.push_back (iter->second);
		
		return list;
	}
	
	
	
	
	/* change parent */
	void Entity::setParent (const weak_ptr<Entity>& parent)
	{
		/* already set to the parent */
		if (parent == mParent) return;
		
		
		if (parent) Log::Trace (LogSource, mName + " - Setting parent: " + parent->getName());
		else Log::Trace (LogSource, mName + " - Setting parent: (NULL)");
		
		
		/* parent already exists */
		if (parent && mParent)
			SOMA_EXCEPTION (LogSource, "Failed to set the parent of the "
				"entity '" + mName + "' to '" + parent->getName() + "' because "
				"it already has the parent '" + mParent->getName() + "'. You "
				"must first remove the entity from the parent before a new "
				"parent can be set.");
		
		
		/* detach from position events */
		if (mParent)
		{
			mParent->PositionChanged -= delegate (this, &Entity::onParentPositionChanged);
			mParent->OrientationChanged -= delegate (this, &Entity::onParentOrientationChanged);
			
			if (mLoaded) unload ();
		}
		
		
		/* set parent */
		ParentChangedEventArgs args;
		args.oldParent = mParent;
		args.newParent = parent;
		
		mParent = parent;
		ParentChanged.raise (args);
		
		
		/* attach to position events */
		if (mParent)
		{
			mParent->PositionChanged += delegate (this, &Entity::onParentPositionChanged);
			mParent->OrientationChanged += delegate (this, &Entity::onParentOrientationChanged);
			
			if (mParent->isLoaded()) load ();
		}
	}
	
	
	
	
	/* set the world this entity should operate in. This needs to propagate
	 * down to child entities so that all components within the entity hierarchy
	 * can have access to the world once it is associated with one */
	void Entity::setWorld (const weak_ptr<World>& world)
	{
		if (world == mWorld) return;
		
		Log::Trace (LogSource, "Setting world to '" + world->getName() +
				"' on the entity: " + mName);
		
		
		/* unload since we are getting a new world reference */
		bool loaded = mLoaded;
		unload ();
		
		
		mWorld = world;
		
		EntityMap::iterator iter;
		
		/* propagate down to children */
		for (iter = mChildren.begin(); iter != mChildren.end(); ++iter)
			iter->second->setWorld (world);
		
		
		/* load again with new world reference */
		if (loaded) load ();
	}
	
	
	
	
	
	/* add child */
	void Entity::addChild (const smart_ptr<Entity>& child)
	{
		Log::Trace (LogSource, mName + " - Adding child entity: " + child->getName());
		
		
		child->setWorld (mWorld);
		child->setParent (this);
		mChildren[child->getName()] = child;
		
		ChildAdded.raise (child);
		
		/* load child if we are loaded */
		if (mLoaded) child->load ();
	}
	
	
	/* remove child */
	void Entity::removeChild (const smart_ptr<Entity>& child)
	{
		removeChild (child->getName());
	}
	
	
	/* remove child by name */
	void Entity::removeChild (const String& name)
	{
		Log::Trace (LogSource, mName + " - Removing child entity: " + name);
		
		/* look for child */
		EntityMap::iterator iter = mChildren.find (name);
		
		/* found entity */
		if (iter != mChildren.end())
		{
			smart_ptr<Entity> child = iter->second;
			
			child->setWorld (0);
			child->setParent (0);
			mChildren.erase (iter);
			
			ChildRemoved.raise (child);
			
			/* unload child if we are loaded */
			if (mLoaded) child->unload ();
		}
	}
	
	
	
	
	/* recursively load all its child entities to ensure that the entity DAG
	 * is in sync */
	bool Entity::load ()
	{
		if (!mWorld)
			return Log::Error (LogSource, "Failed to load entity '" + mName +
					"' since it hasn't been added to a world yet");
		
		if (mLoaded) return true;
		
		
		Log::Trace (LogSource, "Loading entity tree: " + mName);
		
		
		/* load components first */
		if (!loadComponents ())
			return false;
		
		
		EntityMap::iterator iter;
		
		/* load child entities */
		for (iter = mChildren.begin(); iter != mChildren.end(); ++iter)
		{
			/* failed to load child entity */
			if (!iter->second->load ())
			{
				Log::Error (LogSource, "Failed to load child entity '" +
						iter->second->getName() + "' in the entity: " + mName);
				
				mLoaded = false;
				break;
			}
		}
		
		
		/* unload all loaded entities */
		if (!mLoaded) unload ();
		else Loaded.raise (this);
		
		return mLoaded;
	}
	
	
	/* recursively reload all its child entities to ensure that the entity DAG
	 * is in sync */
	bool Entity::reload ()
	{
		Log::Trace (LogSource, "Reloading entity tree: " + mName);
		
		
		/* reload components first */
		if (!reloadComponents ())
		{
			unload ();
			return false;
		}
		
		
		EntityMap::iterator iter;
		
		/* reload child entities */
		for (iter = mChildren.begin(); iter != mChildren.end(); ++iter)
		{
			/* failed to reload child entity */
			if (!iter->second->load ())
			{
				Log::Error (LogSource, "Failed to reload child entity '" +
						iter->second->getName() + "' in the entity: " + mName);
				
				mLoaded = false;
				break;
			}
		}
		
		
		/* unload all loaded entities */
		if (!mLoaded) unload ();
		else Reloaded.raise (this);
		
		return mLoaded;
	}
	
	
	/* recursively unload all child entites. This will ensure that the entity
	 * still maintains its state but no longer operates in the world it was
	 * loaded in. */
	void Entity::unload ()
	{
		if (!mLoaded) return;
		
		Log::Trace (LogSource, "Unloading entity tree: " + mName);
		
		
		EntityMap::iterator iter;
		
		/* unload child entities */
		for (iter = mChildren.begin(); iter != mChildren.end(); ++iter)
			iter->second->unload ();
		
		
		/* unload components after all children have been unloaded so that
		 * any child needing a component from this entity can still access
		 * it without having to worry about monitoring its load state */
		unloadComponents ();
		Unloaded.raise (this);
	}

}
