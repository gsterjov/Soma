
#include "Soma/Visual/Mesh.h"

#include <cassert>
#include <OgreMesh.h>
#include <OgreMeshManager.h>

#include "Soma/Log.h"


namespace Soma {
namespace Visual {

	/* log source */
	const String Mesh::LogSource = "Visual::Mesh";
	
	/* resource type */
	const String Mesh::Type = "visual/mesh";
	
	
	
	/* constructor */
	Mesh::Mesh (Ogre::Mesh* mesh) : mMesh(mesh)
	{
	}
	
	
	/* empty constructor */
	Mesh::Mesh () : mMesh(0)
	{
		
	}
	
	
	/* destructor */
	Mesh::~Mesh ()
	{
		if (mMesh)
		{
			mMesh->unload();
			Ogre::MeshManager::getSingleton().remove (mMesh->getName());
		}
	}
	
	
	
	/* load the mesh resource */
	bool Mesh::load ()
	{
		/* create mesh */
		if (!mMesh) mMesh = createMesh();
		
		assert (mMesh);
		mMesh->load ();
		return mMesh->isLoaded();
	}
	
	
	/* reload the mesh resource */
	bool Mesh::reload ()
	{
		assert (mMesh);
		mMesh->reload ();
		return mMesh->isLoaded();
	}
	
	
	/* unload the mesh resource */
	void Mesh::unload ()
	{
		assert (mMesh);
		mMesh->unload ();
	}
	
	
	
	/* get mesh resource size */
	size_t Mesh::getSize () const
	{
		return mMesh->getSize ();
	}

}}
