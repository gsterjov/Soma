
#include "Soma/Visual/World.h"

#include <cassert>

#include <OgreRoot.h>
#include <OgreSceneManager.h>
#include <OgreSceneNode.h>

#include "Soma/Log.h"
#include "Soma/Registry.h"
#include "Soma/World.h"
#include "Soma/Entity.h"

#include "Soma/Visual/Manager.h"
#include "Soma/Visual/Node.h"


namespace Soma {
namespace Visual {

	/* log source */
	const String World::LogSource = "Visual::World";
	
	/* world type */
	const String World::Type = "world.visual";
	
	
	/* visual world component type */
	REGISTER_TYPE (Component, World, World::Type);
	
	
	
	/* constructor */
	World::World ()
	{
		
	}
	
	
	/* destructor */
	World::~World ()
	{
		unload ();
	}
	
	
	
	
	/* get ambient colour */
	Colour World::getAmbientLight () const
	{
		assert (mSceneManager);
		
		Ogre::ColourValue col = mSceneManager->getAmbientLight();
		return Colour (col.r, col.g, col.b, col.a);
	}
	
	
	/* set ambient colour */
	void World::setAmbientLight (const Colour& colour)
	{
		assert (mSceneManager);
		
		Ogre::ColourValue col (colour.r, colour.g, colour.b, colour.a);
		mSceneManager->setAmbientLight (col);
	}
	
	
	
	
	/* add scene node */
	void World::add (const smart_ptr<Node>& node)
	{
		Log::Debug (LogSource, "Adding node to world: " + node->getOwner()->getName());
		
		mNodes.push_back (node);
		
		/* add to scene manager if we are already loaded */
		if (node->isLoaded())
			onNodeLoad (node);
		
		/* attach to node events */
		node->Loaded   += delegate (this, &World::onNodeLoad);
		node->Unloaded += delegate (this, &World::onNodeUnload);
	}
	
	
	/* remove scene node */
	void World::remove (const smart_ptr<Node>& node)
	{
		Log::Debug (LogSource, "Removing node from world: " + node->getSceneNode()->getName());
		
		mNodes.remove (node);
		
		/* detach from node events */
		node->Loaded   -= delegate (this, &World::onNodeLoad);
		node->Unloaded -= delegate (this, &World::onNodeUnload);
		
		/* remove from scene manager if we are already loaded */
		if (node->isLoaded())
			onNodeUnload (node);
	}
	
	
	
	
	/* load world */
	bool World::load ()
	{
		if (!mOwner)
			return Log::Error (LogSource, "Cannot load visual world component "
					"because it has not been added to a world yet");
		
		/* already loaded */
		if (mSceneManager) return true;
		
		
		Log::Debug (LogSource, "Creating visual scene in world: " + mOwner->getName());
		
		
		/* create new scene manager */
		Manager* manager = Manager::instancePtr();
		mSceneManager = manager->getRoot()->createSceneManager (Ogre::ST_GENERIC, mOwner->getName());
		
		
		
		EntityList::iterator iter;
		EntityList entities = mOwner->getEntities();
		
		/* add existing entities */
		for (iter = entities.begin(); iter != entities.end(); ++iter)
			onEntityAdded (*iter);
		
		
		/* attach to owner events */
		mOwner->EntityAdded   += delegate (this, &World::onEntityAdded);
		mOwner->EntityRemoved += delegate (this, &World::onEntityRemoved);
		
		return mSceneManager;
	}
	
	
	
	/* reload world */
	bool World::reload ()
	{
		Log::Debug (LogSource, "Recreating visual scene in world: " + mOwner->getName());
		
		unload ();
		return load ();
	}
	
	
	
	/* unload world */
	void World::unload ()
	{
		if (!mSceneManager) return;
		
		Log::Debug (LogSource, "Destroying visual scene in world: " + mOwner->getName());
		
		
		/* detach from owner events */
		mOwner->EntityAdded   -= delegate (this, &World::onEntityAdded);
		mOwner->EntityRemoved -= delegate (this, &World::onEntityRemoved);
		
		
		/* destroy scene manager */
		Manager* manager = Manager::instancePtr();
		manager->getRoot()->destroySceneManager (mSceneManager);
		
		mSceneManager = 0;
	}
	
	
	
	
	
	/* get property */
	Value World::getProperty (const String& name) const
	{
		if (name == "ambient-light")
			return getAmbientLight();
	}
	
	
	
	/* set property */
	void World::setProperty (const String& name, const Value& value)
	{
		if (name == "ambient-light")
			setAmbientLight (value.get<Colour>());
	}
	
	
	
	
	/* attach to added entity and wait for any loaded nodes to arrive. once
	 * we have a loaded node we can add it to the scene manager, thus any
	 * entity with a entity.visual.node component can easily be added to the
	 * scene manager without any extra effort outside the component interface */
	void World::onEntityAdded (const smart_ptr<Entity>& entity)
	{
		smart_ptr<Component> component = entity->getComponent (Node::Type);
		
		if (component)
		{
			smart_ptr<Node> node = ref_cast<Node> (component);
			add (node);
		}
	}
	
	
	/* remove the entity and its node component from the scene manager */
	void World::onEntityRemoved (const smart_ptr<Entity>& entity)
	{
		smart_ptr<Component> component = entity->getComponent (Node::Type);
		
		if (component)
		{
			smart_ptr<Node> node = ref_cast<Node> (component);
			remove (node);
		}
	}
	
	
	
	/* node component loaded, add it to the scene manager */
	void World::onNodeLoad (const weak_ptr<Component>& component)
	{
		weak_ptr<Node> node = ref_cast<Node> (component);
		mSceneManager->getRootSceneNode()->addChild (node->getSceneNode());
	}
	
	
	/* node component unloaded, remove it from the scene manager */
	void World::onNodeUnload (const weak_ptr<Component>& component)
	{
		weak_ptr<Node> node = ref_cast<Node> (component);
		mSceneManager->getRootSceneNode()->removeChild (node->getSceneNode());
	}


}}
