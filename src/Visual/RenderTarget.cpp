
#include "Soma/Visual/RenderTarget.h"

#include <OgreRoot.h>
#include <OgreCamera.h>
#include <OgreRenderWindow.h>

#include "Soma/Log.h"
#include "Soma/Context.h"

#include "Soma/Visual/Manager.h"
#include "Soma/Visual/Camera.h"


namespace Soma {
namespace Visual {

	/* log source */
	const String RenderTarget::LogSource = "Visual::RenderTarget";
	
	/* subcontext type */
	const String RenderTarget::Type = "context.rendertarget";
	
	
	
	
	/* constructor */
	RenderTarget::RenderTarget () : mRenderWindow(0)
	{
		
	}
	
	
	/* destructor */
	RenderTarget::~RenderTarget ()
	{
		
	}
	
	
	
	
	/* a message request was sent */
	void RenderTarget::onRequest (CompositeMessage& message)
	{
		if (mRenderWindow && message.type == "WindowID")
			message.value.set (getWindowID());
	}
	
	
	
	/* create render window */
	void RenderTarget::createTarget (const String& title,
	                                 const RenderTargetProperties& props)
	{
		Manager* manager = Manager::instancePtr();
		assert (manager);
		
		
		/* fill params */
		Ogre::NameValuePairList params;
		
		if (props.useCurrentGLContext)
			params["currentGLContext"] = "True";
		
		
		/* create target */
		mRenderWindow = manager->getRoot()->createRenderWindow (
				title,
				props.width,
				props.height,
				props.fullscreen,
				&params);
		
		
		/* listen to requests */
		mOwner->Request += delegate (this, &RenderTarget::onRequest);
		
		/* get window id to broadcast */
		mOwner->broadcast ("WindowID", getWindowID());
	}
	
	
	
	/* get the window id */
	size_t RenderTarget::getWindowID ()
	{
		size_t id = 0;
		mRenderWindow->getCustomAttribute ("WINDOW", &id);
		
		return id;
	}
	
	
	
	/* update render target */
	void RenderTarget::update ()
	{
		mRenderWindow->update();
	}
	
	
	/* resize render target */
	void RenderTarget::resize (int width, int height)
	{
		mRenderWindow->resize (width, height);
	}

}}
