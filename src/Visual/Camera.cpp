
#include "Soma/Visual/Camera.h"

#include <OgreCamera.h>
#include <OgreSceneManager.h>
#include <OgreVector3.h>
#include <OgreQuaternion.h>

#include "Soma/Log.h"
#include "Soma/Registry.h"

#include "Soma/Entity.h"
#include "Soma/Visual/World.h"
#include "Soma/Visual/Node.h"


namespace Soma {
namespace Visual {

	/* log source */
	const String Camera::LogSource = "Visual::Camera";
	
	/* component name */
	const String Camera::Type = "entity.visual.camera";
	
	
	/* component type */
	REGISTER_TYPE (Component, Camera, Camera::Type);
	
	
	
	/* constructor */
	Camera::Camera () : mCamera(0)
	{
		
	}
	
	
	/* destructor */
	Camera::~Camera ()
	{
		unload ();
	}
	
	
	
	
	
	/*  change camera direction */
	void Camera::lookAt (const Vector& point)
	{
		assert (mCamera);
		
		Ogre::Vector3 vec (point.x, point.y, point.z);
		mCamera->lookAt (vec);
	}
	
	
	
	/* set near clip distance */
	void Camera::setNearClipDistance (float distance)
	{
		assert (mCamera);
		mCamera->setNearClipDistance (distance);
	}
	
	
	
	/* object implementaiton */
	Ogre::MovableObject* Camera::getMovableObject ()
	{
		return mCamera;
	}
	
	
	
	
	/* create an ogre camera instance by using the visual world provided by the
	 * base object class */
	bool Camera::create (const weak_ptr<Visual::World>& world)
	{
		Log::Debug (LogSource, "Creating camera in entity: " + mOwner->getName());
		
		/* create camera */
		mCamera = world->getSceneManager()->createCamera (mOwner->getName());
		
		
		/* load properties */
		PropertyList::iterator iter;
		
		for (iter = mProps.begin(); iter != mProps.end(); ++iter)
			setProperty (iter->first, iter->second);
		
		
		/* look for sibling node */
		smart_ptr<Component> component = mOwner->getComponent (Node::Type);
		if (component) onComponentAdded (component);
		
		/* entity events */
		mOwner->ComponentAdded   += delegate (this, &Camera::onComponentAdded);
		mOwner->ComponentRemoved += delegate (this, &Camera::onComponentRemoved);
		
		return true;
	}
	
	
	
	/* destroy the ogre camera instance by using the visual world store in the
	 * base object class */
	void Camera::destroy ()
	{
		if (!mCamera) return;
		
		/* if the visual world isnt valid anymore then the camera should have
		 * been destroyed when it became so */
		assert (mVisualWorld);
		
		
		Log::Debug (LogSource, "Destroying camera in entity: " + mOwner->getName());
		
		/* entity events */
		mOwner->ComponentAdded   -= delegate (this, &Camera::onComponentAdded);
		mOwner->ComponentRemoved -= delegate (this, &Camera::onComponentRemoved);
		
		/* look for sibling node */
		smart_ptr<Component> component = mOwner->getComponent (Node::Type);
		if (component) onComponentRemoved (component);
		
		
		/* destroy camera */
		mVisualWorld->getSceneManager()->destroyCamera (mCamera);
		mCamera = 0;
	}
	
	
	
	
	
	/* get property */
	Value Camera::getProperty (const String& name) const
	{
		/* camera */
		if (name == "near-clip-distance")
			return mCamera ? mCamera->getNearClipDistance() : 0;
	}
	
	
	
	/* set property */
	void Camera::setProperty (const String& name, const Value& value)
	{
		/* camera */
		if (name == "near-clip-distance")
		{
			if (mCamera) setNearClipDistance (value.get<float>());
			else mProps.push_back (std::make_pair (name, value));
		}
		
		else if (name == "look-at")
		{
			if (mCamera) lookAt (value.get<Vector>());
			else mProps.push_back (std::make_pair (name, value));
		}
	}
	
	
	
	
	
	/* attach to sibling node if it was added */
	void Camera::onComponentAdded (const smart_ptr<Component>& component)
	{
		if (component->getType() == Node::Type)
		{
			smart_ptr<Node> node = ref_cast<Node> (component);
			node->attach (this);
		}
	}
	
	
	/* detach from sibling node if it was removed */
	void Camera::onComponentRemoved (const smart_ptr<Component>& component)
	{
		if (component->getType() == Node::Type)
		{
			smart_ptr<Node> node = ref_cast<Node> (component);
			node->detach (this);
		}
	}

}}
