
#include "Soma/Visual/Terrain.h"

#include <OgreTerrain.h>
#include <OgreTerrainGroup.h>
#include <OgreTerrainMaterialGeneratorA.h>

#include "Soma/Log.h"
#include "Soma/Registry.h"
#include "Soma/World.h"

#include "Soma/Visual/Light.h"
#include "Soma/Visual/World.h"


namespace Soma {
namespace Visual {

	/* log source */
	const String Terrain::LogSource = "Visual::Terrain";
	
	/* world type */
	const String Terrain::Type = "world.visual.terrain";
	
	
	/* visual world component type */
	REGISTER_TYPE (Component, Terrain, Terrain::Type);
	
	
	
	/* constructor */
	Terrain::Terrain () : mLoaded(false), mImported(false)
	{
		
	}
	
	
	/* destructor */
	Terrain::~Terrain ()
	{
		unload ();
	}
	
	
	
	
	/* load terrain */
	bool Terrain::load2 ()
	{
		if (!mOwner)
			return Log::Error (LogSource, "Cannot load visual terrain component "
					"because it has not been added to a world yet");
		
		/* already laoded */
		if (mLoaded) return true;
		
		
		Log::Debug (LogSource, "Creating visual terrain in world: " + mOwner->getName());
		
		
		
		smart_ptr<Component> com = mOwner->getComponent ("world.visual");
		smart_ptr<Visual::World> world = ref_cast<Visual::World> (com);
		
		
		/* create terrain */
		mTerrainGlobals = new Ogre::TerrainGlobalOptions ();
		mTerrainGroup = new Ogre::TerrainGroup (world->getSceneManager(), Ogre::Terrain::ALIGN_X_Z, 513, 12000.0f);
		
		/* terrain save name */
		mTerrainGroup->setFilenameConvention ("terrain", "dat");
		mTerrainGroup->setOrigin (Ogre::Vector3::ZERO);
		
		
		/* cofigure terrain defaults */
		configureTerrain (mLight);
		defineTerrain (0, 0);
		
		std::cout << "LOADING" << std::endl;
		/* load terrain */
		mTerrainGroup->loadAllTerrains (true);
		
		
		/* generate blend maps */
		if (mImported)
		{
			Ogre::TerrainGroup::TerrainIterator iter = mTerrainGroup->getTerrainIterator();
			
			while (iter.hasMoreElements())
			{
				Ogre::Terrain* terrain = iter.getNext()->instance;
				initBlendMaps (terrain);
			}
		}
		
		
		/* clean up */
		mTerrainGroup->freeTemporaryResources ();
//		mTerrainGroup->saveAllTerrains (true);
		std::cout << "LOADED!" << std::endl;
		
		mLoaded = true;
		return mLoaded;
	}
	
	
	
	/* reload terrain */
	bool Terrain::reload ()
	{
		assert (mLoaded);
		
		Log::Debug (LogSource, "Recreating visual terrain in world: " + mOwner->getName());
		
		unload ();
		return load ();
	}
	
	
	
	/* unload terrain */
	void Terrain::unload ()
	{
		if (!mLoaded) return;
		
		assert (mOwner);
		Log::Debug (LogSource, "Destroying visual terrain in world: " + mOwner->getName());
		
		mLoaded = false;
	}
	
	
	
	
	
	/* load terrain image */
	static void getTerrainImage (bool flipX, bool flipY, Ogre::Image& img, const Vri& vri)
	{
		img.load (vri.path, Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);
		if (flipX) img.flipAroundY ();
		if (flipY) img.flipAroundX ();
	}
	
	
	
	/* define the terrain */
	void Terrain::defineTerrain (long x, long y)
	{
		Soma::String filename = mTerrainGroup->generateFilename (x, y);
		
		if (Ogre::ResourceGroupManager::getSingleton().resourceExists (mTerrainGroup->getResourceGroup(), filename))
		{
			mTerrainGroup->defineTerrain (x, y);
		}
		
		else
		{
			Ogre::Image img;
			getTerrainImage (x % 2 != 0, y % 2 != 0, img, mHeightMap);
			mTerrainGroup->defineTerrain (x, y, &img);
			
			mImported = true;;
		}
	}
	
	
	
	/* initiate blend maps */
	void Terrain::initBlendMaps (Ogre::Terrain* terrain)
	{
		/* get blend maps */
		Ogre::TerrainLayerBlendMap* blendmap0 = terrain->getLayerBlendMap (1);
		Ogre::TerrainLayerBlendMap* blendmap1 = terrain->getLayerBlendMap (2);
		
		/* set blend triggers */
		float height0 = 70;
		float dist0 = 40;
		
		float height1 = 70;
		float dist1 = 15;
		
		
		/* blend */
		float* blend1 = blendmap1->getBlendPointer();
		
		for (int y = 0; y < terrain->getLayerBlendMapSize(); ++y)
		{
			for (int x = 0; x < terrain->getLayerBlendMapSize(); ++x)
			{
				float tx, ty;
				blendmap0->convertImageToTerrainSpace (x, y, &tx, &ty);
				
				float height = terrain->getHeightAtTerrainPosition (tx, ty);
				float val = (height - height0) / dist0;
				val = Ogre::Math::Clamp (val, 0.0f, 1.0f);
				
				val = (height - height1) / dist1;
				val = Ogre::Math::Clamp (val, 0.0f, 1.0f);
				*blend1++ = val;
			}
		}
		
		
		blendmap0->dirty ();
		blendmap1->dirty ();
		blendmap0->update ();
		blendmap1->update ();
	}
	
	
	
	/* configure terrain properties */
	void Terrain::configureTerrain (const smart_ptr<Light>& light)
	{
		Ogre::Light* l = light->getLight();
		
		
		mTerrainGlobals->setMaxPixelError (8);
		mTerrainGlobals->setCompositeMapDistance (3000);
		
		/* lighting */
		mTerrainGlobals->setLightMapDirection (l->getDerivedDirection());
		mTerrainGlobals->setCompositeMapAmbient (Ogre::ColourValue (0.2, 0.2, 0.2));
		mTerrainGlobals->setCompositeMapDiffuse (l->getDiffuseColour());
		
		
		/* material profile settings */
		Ogre::TerrainMaterialGeneratorA::SM2Profile* prof =
				static_cast<Ogre::TerrainMaterialGeneratorA::SM2Profile*> (mTerrainGlobals->getDefaultMaterialGenerator()->getActiveProfile());
		
		prof->setLightmapEnabled (false);
		
		
		
		/* imports */
		Ogre::Terrain::ImportData& data = mTerrainGroup->getDefaultImportSettings();
		
		data.terrainSize = 513;
		data.worldSize = 12000.0f;
		data.inputScale = 600;
		data.minBatchSize = 33;
		data.maxBatchSize = 65;
		
		
		/* textures */
		data.layerList.resize (3);
		data.layerList[0].worldSize = 100;
		data.layerList[0].textureNames.push_back ("dirt_grayrocky_diffusespecular.dds");
		data.layerList[0].textureNames.push_back ("dirt_grayrocky_normalheight.dds");
		
		data.layerList[1].worldSize = 30;
		data.layerList[1].textureNames.push_back ("grass_green-01_diffusespecular.dds");
		data.layerList[1].textureNames.push_back ("grass_green-01_normalheight.dds");
		
		data.layerList[2].worldSize = 200;
		data.layerList[2].textureNames.push_back ("growth_weirdfungus-03_diffusespecular.dds");
		data.layerList[2].textureNames.push_back ("growth_weirdfungus-03_normalheight.dds");
	}

}}
