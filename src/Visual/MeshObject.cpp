
#include "Soma/Visual/MeshObject.h"

#include <stdexcept>
#include <OgreEntity.h>
#include <OgreSceneManager.h>

#include "Soma/Log.h"
#include "Soma/Registry.h"
#include "Soma/Exception.h"

#include "Soma/Entity.h"
#include "Soma/Visual/World.h"
#include "Soma/Visual/Node.h"
#include "Soma/Visual/Material.h"
#include "Soma/Visual/Mesh.h"


namespace Soma {
namespace Visual {

	/* log source */
	const String MeshObject::LogSource = "Visual::MeshObject";
	
	/* component name */
	const String MeshObject::Type = "entity.visual.mesh";
	
	
	/* component type */
	REGISTER_TYPE (Component, MeshObject, MeshObject::Type);
	
	
	
	/* constructor */
	MeshObject::MeshObject (const smart_ptr<Mesh>& mesh) : mEntity (0)
	{
		setMesh (mesh);
	}
	
	
	/* path constructor */
	MeshObject::MeshObject (const Vri& vri)
	: mEntity (0)
	{
		setMesh (vri);
	}
	
	
	/* no resource constructor */
	MeshObject::MeshObject () : mEntity(0)
	{
		
	}
	
	
	
	MeshObject::~MeshObject ()
	{
		if (mEntity) destroy ();
	}
	
	
	
	/* set mesh resource */
	void MeshObject::setMesh (const smart_ptr<Mesh>& mesh)
	{
		assert (!mEntity);
		mMesh = mesh;
	}
	
	
	/* set mesh by path */
	void MeshObject::setMesh (const Vri& vri)
	{
		/* get the specified mesh */
		smart_ptr<Mesh> mesh = MeshManager::instance().get (vri);
		
		/* no mesh resource found */
		if (!mesh)
			SOMA_EXCEPTION (LogSource, "Cannot find the asset '" + 
					vri.asset + "' defining a mesh with the name '" +
					vri.resource + "'. Mesh object creation failed.");
		
		/* cannot load resource */
		else if (!mesh->load())
			SOMA_EXCEPTION (LogSource, "Cannot load the mesh '" + vri.resource +
					"' found in the asset '" + vri.asset + "'. Loading "
					"failed.");
		
		/* finish loading resource */
		setMesh (mesh);
	}
	
	
	
	/* set material */
	void MeshObject::setMaterial (const smart_ptr<Material>& material)
	{
		Log::Debug (LogSource, "Setting material '" + material->getName() +
				"' on the mesh object in the entity: " + mOwner->getName());
		
		mMaterial = material;
		
		if (mEntity)
			mEntity->setMaterialName (mMaterial->getMaterial()->getName());
	}
	
	
	
	/* object implementaiton */
	Ogre::MovableObject* MeshObject::getMovableObject() { return mEntity; }
	
	
	
	
	/* create an ogre entity by using the visual world provided by the
	 * base object class */
	bool MeshObject::create (const weak_ptr<Visual::World>& world)
	{
		if (!mMesh)
			return Log::Error (LogSource, "No mesh has been defined for the "
					"mesh object in the entity: " + mOwner->getName());
		
		
		Log::Debug (LogSource, "Creating an instance of mesh '' in entity: " +
				mOwner->getName());
		
		/* create mesh object */
		mEntity = mVisualWorld->getSceneManager()->createEntity (
				mOwner->getName(),
				mMesh->getMesh()->getName());
		
		/* set material */
		if (mMaterial)
			mEntity->setMaterialName (mMaterial->getMaterial()->getName());
		
		
		/* look for sibling node */
		smart_ptr<Component> component = mOwner->getComponent (Node::Type);
		if (component) onComponentAdded (component);
		
		/* entity events */
		mOwner->ComponentAdded   += delegate (this, &MeshObject::onComponentAdded);
		mOwner->ComponentRemoved += delegate (this, &MeshObject::onComponentRemoved);
		
		return true;
	}
	
	
	
	/* destroy the ogre entity by using the visual world store in the
	 * base object class */
	void MeshObject::destroy ()
	{
		if (!mEntity) return;
		
		/* if the visual world isnt valid anymore then the object should have
		 * been destroyed when it became so */
		assert (mVisualWorld);
		
		
		Log::Debug (LogSource, "Destroying mesh object in entity: " + mOwner->getName());
		
		/* entity events */
		mOwner->ComponentAdded   -= delegate (this, &MeshObject::onComponentAdded);
		mOwner->ComponentRemoved -= delegate (this, &MeshObject::onComponentRemoved);
		
		/* look for sibling node */
		smart_ptr<Component> component = mOwner->getComponent (Node::Type);
		if (component) onComponentRemoved (component);
		
		
		/* destroy mesh object */
		mVisualWorld->getSceneManager()->destroyEntity (mEntity);
		mEntity = 0;
	}
	
	
	
	
	
	/* get property */
	Value MeshObject::getProperty (const String& name) const
	{
		/* mesh */
		if (name == "resource")
			return ref_cast<Resource> (mMesh);
	}
	
	
	
	/* set property */
	void MeshObject::setProperty (const String& name, const Value& value)
	{
		/* mesh */
		if (name == "resource")
		{
			smart_ptr<Resource> resource = value.get<smart_ptr<Resource> >();
			
			/* get mesh resource */
			if (resource->getType() == Mesh::Type)
				setMesh (ref_cast<Mesh> (resource));
			
			/* invalid resource type */
			else
				Log::Error (LogSource, "Cannot set the mesh resource '" +
						resource->getVri().path + "' because it is not of the "
						"resource type: " + Mesh::Type);
		}
		
		else if (name == "resource-path")
			setMesh (value.get<String>());
		
		
		else if (name == "material")
			setMaterial (value.get<smart_ptr<Material> >());
	}
	
	
	
	
	/* attach to sibling node if it was added */
	void MeshObject::onComponentAdded (const smart_ptr<Component>& component)
	{
		if (component->getType() == Node::Type)
		{
			smart_ptr<Node> node = ref_cast<Node> (component);
			node->attach (this);
		}
	}
	
	
	/* detach from sibling node if it was removed */
	void MeshObject::onComponentRemoved (const smart_ptr<Component>& component)
	{
		if (component->getType() == Node::Type)
		{
			smart_ptr<Node> node = ref_cast<Node> (component);
			node->detach (this);
		}
	}

}}
