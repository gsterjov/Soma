
#include "Soma/Visual/Manager.h"

#include <cassert>

#include <OgreLogManager.h>
#include <OgreRoot.h>
#include <OgreRenderSystem.h>  /* initialise() */
#include <OgreWindowEventUtilities.h>

#include "Soma/Log.h"
#include "Soma/Registry.h"
#include "Soma/ConfigFile.h"

#include "Soma/Visual/Mesh.h"  /* MeshManager */
#include "Soma/Visual/Material.h"  /* MaterialManager */


namespace Soma {
namespace Visual {

	/* log source */
	const String Manager::LogSource = "Visual::Manager";
	
	
	/* subsystem type */
	REGISTER_TYPE (SubSystem, Manager, "Renderer");
	
	
	
	
	/* constructor */
	Manager::Manager ()
	: mRenderSystem (RENDER_SYSTEM_OPENGL),
	  mFullscreen (false),
	  mVSync (false),
	  mGammaConversion (false),
	  mWidth (640),
	  mHeight (480),
	  mFSAA (0),
	  mFrequency (50)
	{
		Log::Debug (LogSource, "Creating OGRE and loading plugins");
		
		
		/* intercept ogre log */
		Ogre::LogManager* log = new Ogre::LogManager ();
		log->createLog ("ogre.log", true, false, true);
		
		
		/* bring up ogre */
		mRoot = new Ogre::Root ();
		
#ifdef SOMA_SYSTEM_UNIX
		mRoot->loadPlugin ("libRenderSystem_GL.so");
		mRoot->loadPlugin ("libPlugin_CgProgramManager.so");
		
#elif defined SOMA_SYSTEM_WIN32
		mRoot->loadPlugin ("RenderSystem_GL.dll");
		mRoot->loadPlugin ("RenderSystem_Direct3D9.dll");
		mRoot->loadPlugin ("Plugin_CgProgramManager.dll");
		
		mRenderSystem = RENDER_SYSTEM_DIRECT_X;
#endif
		
		
		/* create resource managers */
		Log::Debug (LogSource, "Creating resource managers");
		new MeshManager ();
		new MaterialManager ();
	}
	
	
	/* destructor */
	Manager::~Manager ()
	{
		/* destroy resource managers */
		Log::Debug (LogSource, "Destroying resource managers");
		delete MeshManager::instancePtr();
		delete MaterialManager::instancePtr();
		
		/* destroy ogre */
		Log::Debug (LogSource, "Destroying OGRE");
		delete mRoot;  /* ogre cleans up for us */
		delete Ogre::LogManager::getSingletonPtr();
	}
	
	
	
	/* initialise renderer */
	bool Manager::initialise ()
	{
		Log::Message (LogSource, "Initialising renderer");
		
		
		Ogre::RenderSystem* system = 0;
		
		/* create the rendering system */
		switch (mRenderSystem)
		{
		case RENDER_SYSTEM_OPENGL:
			system = mRoot->getRenderSystemByName ("OpenGL Rendering Subsystem");
			
			if (!system)
				return Log::Error (LogSource, "Could not find the OpenGL rendering system.");
			break;

		case RENDER_SYSTEM_DIRECT_X:
			system = mRoot->getRenderSystemByName ("Direct3D9 Rendering Subsystem");
			
			if (!system)
				return Log::Error (LogSource, "Could not find the DirectX rendering system.");
			break;
			
		default:
			return Log::Error (LogSource, "No supported rendering system found");
		}
		
		
		/* set render system */
		mRoot->setRenderSystem (system);
		applySettings ();
		
		
		/* initialise rendering system */
		Log::Debug (LogSource, "Initialising OGRE");
		mRoot->initialise (false);
		
		return mRoot->isInitialised();
	}
	
	
	
	/* reinitialise renderer */
	bool Manager::reinitialise ()
	{
		Log::Message (LogSource, "Reinitialising renderer");
		
		applySettings ();
		mRoot->getRenderSystem()->reinitialise ();
		
		return mRoot->isInitialised();
	}
	
	
	
	/* shut down renderer */
	void Manager::shutdown ()
	{
		Log::Message (LogSource, "Shutting down renderer");
		mRoot->shutdown ();
	}
	
	
	
	/* initialisation state */
	bool Manager::isInitialised ()
	{
		return mRoot->isInitialised();
	}
	
	
	
	/* render a frame */
	void Manager::step ()
	{
		Ogre::WindowEventUtilities::messagePump ();
		mRoot->renderOneFrame ();
	}
	
	
	
	
	/* apply renderer system settings */
	void Manager::applySettings ()
	{
		Log::Message (LogSource, "Applying renderer settings");
		
		
		/* get rendering system */
		Ogre::RenderSystem* system = mRoot->getRenderSystem();
		assert (system);
		
#if SOMA_WIN32
		system->setConfigOption ("Allow NVPerfHUD", "Yes");
#endif
		
		/* boolean conversion */
		//system->setConfigOption ("Full Screen", mFullscreen ? "Yes" : "No");
		//system->setConfigOption ("VSync", mVSync ? "Yes" : "No");
//		system->setConfigOption ("sRGB Gamma Conversion", mGammaConversion ? "Yes" : "No");
		
		/* string conversion */
		std::stringstream stream;
		
		/* set resolution */
		stream.str ("");
		stream << mWidth << " x " << mHeight;
		//system->setConfigOption ("Video Mode", stream.str());
		
		/* set antialiasing */
		stream.str ("");
		stream << mFSAA;
		//system->setConfigOption ("FSAA", stream.str());
		
		/* set monitor frequency */
		stream.str ("");
		stream << mFrequency << " MHz";
		//system->setConfigOption ("Display Frequency", stream.str());
	}
	
	
	
	
	
	/* Configurable implementation: save renderer settings */
	bool Manager::save (ConfigInterface& config)
	{
		/* set render system */
		switch (mRenderSystem)
		{
		case RENDER_SYSTEM_OPENGL:
			config.set ("graphics.system", "OpenGL");
			break;
			
		case RENDER_SYSTEM_DIRECT_X:
			config.set ("graphics.system", "Direct X");
			break;
			
		default:
			return Log::Error (LogSource, "Unsupported rendering system");
		}
		
		
		/* set resolution */
		std::stringstream stream;
		stream << mWidth << "x" << mHeight;
		config.set ("graphics.resolution", stream.str());
		
		
		/* set render system options */
		config.set ("graphics.fullscreen", mFullscreen);
		config.set ("graphics.vsync", mVSync);
		config.set ("graphics.gamma_conversion", mGammaConversion);
		config.set ("graphics.fsaa", mFSAA);
		config.set ("graphics.frequency", mFrequency);
		
		
		/* successfully saved */
		return true;
	}
	
	
	
	/* Configurable implementation: restore renderer settings */
	bool Manager::restore (const ConfigInterface& config)
	{
		Log::Debug (LogSource, "Restoring renderer subsystem settings");
		
		
		/* get render system */
		String str = config.get ("graphics.system", "OpenGL");
		Log::Debug ("Renderer", "Restoring rendering system: " + str);
		
		/* found opengl */
		if      (str == "OpenGL")   mRenderSystem = RENDER_SYSTEM_OPENGL;
		else if (str == "Direct X") mRenderSystem = RENDER_SYSTEM_DIRECT_X;
		else
		{
			Log::Warning (LogSource, "'" + str + "' is not a valid renderering"
					"system. Using default renderering system");
			mRenderSystem = RENDER_SYSTEM_OPENGL;
		}
		
		
		
		/* get resolution */
		str = config.get ("graphics.resolution", "640x480");
		Log::Debug (LogSource, "Restoring resolution: " + str);
		
		
		StringList list = Strings::split (str, " x");
		
		/* individual width and height */
		if (list.size() == 2)
		{
			mWidth  = Strings::toInt (list[0]);
			mHeight = Strings::toInt (list[1]);
		}
		
		/* couldn't parse resolution */
		else
		{
			Log::Warning (LogSource, "Failed to parse the resolution '" + str + "'");
			
			/* default resolution */
			mWidth  = 640;
			mHeight = 480;
		}
		
		
		Log::Debug (LogSource, "Restoring graphic options");
		
		/* get render system options */
		mFullscreen      = config.get ("graphics.fullscreen", false);
		mVSync           = config.get ("graphics.vsync", false);
		mGammaConversion = config.get ("graphics.gamma_conversion", false);
		mFSAA            = config.get ("graphics.fsaa", 0);
		mFrequency       = config.get ("graphics.frequency", 50);
		
		
		/* successfully restored */
		return true;
	}

}}
