
#include "Soma/Visual/Viewport.h"

#include <OgreViewport.h>
#include <OgreCamera.h>
#include <OgreRenderWindow.h>

#include "Soma/Log.h"
#include "Soma/Registry.h"
#include "Soma/Exception.h"
#include "Soma/Context.h"
#include "Soma/Entity.h"

#include "Soma/Visual/RenderTarget.h"
#include "Soma/Visual/Camera.h"


namespace Soma {
namespace Visual {

	/* log source */
	const String Viewport::LogSource = "Visual::Viewport";
	
	/* subcontext type */
	const String Viewport::Type = "context.viewport";
	
	
	/* viewport subcontext type */
	REGISTER_TYPE (Component, Viewport, Viewport::Type);
	
	
	
	
	/* constructor */
	Viewport::Viewport () : mLoaded(false), mViewport(0)
	{
		
	}
	
	
	/* destructor */
	Viewport::~Viewport ()
	{
		unload ();
	}
	
	
	
	/* set viewport camera */
	void Viewport::setCamera (const smart_ptr<Camera>& camera)
	{
		Log::Debug (LogSource, "Adding camera to viewport");
		
		mCamera = camera;
		
		/* add to render target if we are already loaded */
		if (mCamera->isLoaded())
			onCameraLoad (mCamera);
		
		/* attach to camera events */
		mCamera->Loaded   += delegate (this, &Viewport::onCameraLoad);
		mCamera->Unloaded += delegate (this, &Viewport::onCameraUnload);
	}
	
	
	
	/* set viewport camera from entity */
	void Viewport::setCamera (const smart_ptr<Entity>& entity)
	{
		/* look for a camera component in the entity */
		smart_ptr<Component> component = entity->getComponent (Camera::Type);
		
		if (component)
		{
			smart_ptr<Camera> cam = ref_cast<Camera> (component);
			setCamera (cam);
		}
		
		/* no camera found */
		else
			SOMA_EXCEPTION (LogSource, "Cannot find a camera component "
					"within the entity: " + entity->getName());
	}
	
	
	
	
	/* change background colour */
	void Viewport::setBackgroundColour (const Colour& colour)
	{
		mBackColour = colour;
		
		if (mViewport)
			mViewport->setBackgroundColour (Ogre::ColourValue (colour.r,
			                                                   colour.g,
			                                                   colour.b));
	}
	
	
	
	
	/* loading viewport */
	bool Viewport::load ()
	{
		if (!mOwner)
			return Log::Error (LogSource, "Cannot load viewport context component "
					"because it has not been added to a context yet");
		
		/* already loaded */
		if (mLoaded) return true;
		
		
		Log::Debug (LogSource, "Creating viewport in context: " + mOwner->getName());
		
		
		/* look for render target component */
		smart_ptr<Component> component = mOwner->getComponent (RenderTarget::Type);
		
		if (component)
		{
			mRenderTarget = ref_cast<RenderTarget> (component);
			
			/* add viewport if we have a camera */
			if (mCamera && mCamera->isLoaded())
				onCameraLoad (mCamera);
		}
		
		
		/* attach to owner component events */
		mOwner->ComponentAdded   += delegate (this, &Viewport::onComponentAdded);
		mOwner->ComponentRemoved += delegate (this, &Viewport::onComponentRemoved);
		
		
		mLoaded = true;
		return mLoaded;
	}
	
	
	
	/* reload viewport */
	bool Viewport::reload ()
	{
		Log::Debug (LogSource, "Recreating viewport in context: " + mOwner->getName());
		
		unload ();
		return load ();
	}
	
	
	
	/* unload viewport */
	void Viewport::unload ()
	{
		if (!mLoaded) return;
		
		Log::Debug (LogSource, "Destroying viewport in context: " + mOwner->getName());
		
		
		/* detach from owner component events */
		mOwner->ComponentAdded   -= delegate (this, &Viewport::onComponentAdded);
		mOwner->ComponentRemoved -= delegate (this, &Viewport::onComponentRemoved);
		
		
		/* remove viewport */
		if (mViewport)
			mRenderTarget->getTarget()->removeViewport (0);
		
		mViewport = 0;
		mRenderTarget.release();
		
		mLoaded = false;
	}
	
	
	
	
	
	/* get property */
	Value Viewport::getProperty (const String& name) const
	{
		/* camera */
		if (name == "camera")
			return mCamera;
		
		/* background colour */
		if (name == "background-colour")
			return getBackgroundColour();
	}
	
	
	
	/* set property */
	void Viewport::setProperty (const String& name, const Value& value)
	{
		/* camera */
		if (name == "camera")
			setCamera (value.get<smart_ptr<Camera> >());
		
		else if (name == "camera-entity")
			setCamera (value.get<smart_ptr<Entity> >());
		
		
		/* background colour */
		else if (name == "background-colour")
			setBackgroundColour (value.get<Colour>());
	}
	
	
	
	
	/* component added in owner context */
	void Viewport::onComponentAdded (const smart_ptr<Component>& component)
	{
		if (component->getType() == RenderTarget::Type)
			mRenderTarget = ref_cast<RenderTarget> (component);
		
		
		/* add viewport if we are initialised */
		if (mCamera && mCamera->isLoaded())
			onCameraLoad (mCamera);
	}
	
	
	
	/* component removed in owner context */
	void Viewport::onComponentRemoved (const smart_ptr<Component>& component)
	{
		if (component->getType() == RenderTarget::Type)
		{
			/* remove viewport if we have one */
			onCameraUnload (mCamera);
			
			/* release render target reference */
			mRenderTarget.release();
		}
	}
	
	
	
	
	/* camera component has been loaded */
	void Viewport::onCameraLoad (const weak_ptr<Component>& component)
	{
		/* viewport is loaded and attached to render target */
		if (mLoaded && mRenderTarget)
		{
			Ogre::RenderTarget* target = mRenderTarget->getTarget();
			mViewport = target->addViewport (mCamera->getCamera());
			
			mViewport->setBackgroundColour (Ogre::ColourValue (mBackColour.r,
			                                                   mBackColour.g,
			                                                   mBackColour.b));
		}
	}
	
	
	
	/* camera component has been unloaded */
	void Viewport::onCameraUnload (const weak_ptr<Component>& component)
	{
		/* viewport is loaded and attached to render target */
		if (mViewport)
			mRenderTarget->getTarget()->removeViewport (0);
	}

}}
