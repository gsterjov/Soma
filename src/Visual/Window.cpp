
#include "Soma/Visual/Window.h"

#include <OgreRenderWindow.h>
#include <OgreRoot.h>

#include "Soma/Log.h"
#include "Soma/Registry.h"
#include "Soma/Context.h"

#include "Soma/Visual/Manager.h"


namespace Soma {
namespace Visual {

	/* log source */
	const String Window::LogSource = "Visual::Window";
	
	
	/* window subcontext type */
	REGISTER_TYPE (Component, Window, "context.rendertarget.window");
	
	
	
	
	/* constructor */
	Window::Window ()
	{
		
	}
	
	
	/* title constructor */
	Window::Window (const String& title) : mTitle(title)
	{
		
	}
	
	
	/* destructor */
	Window::~Window ()
	{
		unload ();
	}
	
	
	
	
	/* load the window */
	bool Window::load ()
	{
		if (!mOwner)
			return Log::Error (LogSource, "Cannot load window context component "
					"because it has not been added to a context yet");
		
		
		if (mRenderWindow) return true;
		
		Log::Debug (LogSource, "Creating render window in context: " + mOwner->getName());
		
		
		Manager* manager = Manager::instancePtr();
		assert (manager);
		
		
		/* create target properties */
		RenderTargetProperties props;
		props.width = manager->getWidth();
		props.height = manager->getHeight();
		props.fullscreen = manager->getFullscreen();
		
		
		Log::Debug (LogSource, "Window properties:\n\t"
				"Width: " + Strings::convert (props.width) + "\n\t"
				"Height: " + Strings::convert (props.height) + "\n\t"
				"Fullscreen: " + Strings::convert (props.fullscreen));
		
		/* create target */
		createTarget (mTitle, props);
		
		return mRenderWindow != 0;
	}
	
	
	
	/* reload the window */
	bool Window::reload ()
	{
		Log::Debug (LogSource, "Recreating render window in context: " + mOwner->getName());
		
		unload ();
		return load ();
	}
	
	
	
	/* unload the window */
	void Window::unload ()
	{
		if (!mRenderWindow) return;
		
		Log::Debug (LogSource, "Destroying render window in context: " + mOwner->getName());
		
		mRenderWindow->destroy ();
		delete mRenderWindow;
		mRenderWindow = 0;
	}
	

}}
