
#include "Soma/Visual/Technique.h"

#include <OgreMaterial.h>
#include <OgreTechnique.h>
#include <OgrePass.h>

#include "Soma/Log.h"
#include "Soma/Visual/Material.h"
#include "Soma/Visual/Pass.h"


namespace Soma {
namespace Visual {

	/* log source */
	const String Technique::LogSource = "Visual::Technique";
	
	
	
	
	/* name constructor */
	Technique::Technique (const weak_ptr<Material>& material, const String& name)
	{
		Log::Debug (LogSource, "Creating technique: " + name);
		
		
		assert (material);
		
		/* create technique */
		Ogre::Material* mat = material->getMaterial();
		mTechnique = mat->createTechnique ();
		mTechnique->setName (name);
	}
	
	
	
	/* technique constructor */
	Technique::Technique (Ogre::Technique* technique) : mTechnique(technique)
	{
		Log::Debug (LogSource, "Creating technique: " + technique->getName());
	}
	
	
	
	
	
	/* get technique name */
	const String& Technique::getName () const
	{
		return mTechnique->getName();
	}
	
	
	
	/* get the pass by looking into the OGRE technique and wrapping it up
	 * in our pass class */
	smart_ptr<Pass> Technique::getPass (const String& name) const
	{
		Ogre::Pass* pass = mTechnique->getPass (name);
		return pass ? new Pass (pass) : 0;
	}
	
	
	
	/* iterate through all the passes and add it into a list wrapped with
	 * our pass class */
	PassList Technique::getPasses () const
	{
		PassList list;
		
		Ogre::Technique::PassIterator iter = mTechnique->getPassIterator();
		
		/* add all techniques to the list */
		while (iter.hasMoreElements())
		{
			Ogre::Pass* pass = iter.getNext();
			list.push_back (new Pass (pass));
		}
		
		return list;
	}
	
	
	
	
	/* adding a pass involves creating and returning it so it can be
	 * modified. since the pass class makes sure it is created within
	 * the technique we only need to call its constructor here */
	smart_ptr<Pass> Technique::add (const String& name)
	{
		return new Pass (this, name);
	}
	
	
	
	/* ogre doesnt have a remove-by-name function so we iterate through the
	 * passes to find a pass with a matching name, remove it with an index
	 * and then break out of the loop */
	void Technique::remove (const String& name)
	{
		/* look for pass index */
		for (int i = 0; i < mTechnique->getNumPasses(); ++i)
		{
			Ogre::Pass* pass = mTechnique->getPass(i);
			
			/* found matching pass */
			if (pass->getName() == name)
			{
				Log::Debug (LogSource, "Removing pass '" + name + "' from the "
						"technique: " + mTechnique->getName());
				
				mTechnique->removePass (i);
				break;
			}
		}
	}
	
	
	
	/* since we have no actual pass store we just call remove() with the
	 * pass name instead. this is primarily a convenient function */
	void Technique::remove (const smart_ptr<Pass>& pass)
	{
		remove (pass->getName());
	}
	

}}
