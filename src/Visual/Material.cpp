
#include "Soma/Visual/Material.h"

#include <list>
#include <cassert>

#include <OgreMaterial.h>
#include <OgreMaterialManager.h>
#include <OgreTechnique.h>

#include "Soma/Log.h"
#include "Soma/Visual/Technique.h"


namespace Soma {
namespace Visual {

	/* log source */
	const String Material::LogSource = "Visual::Material";
	
	/* resource type */
	const String Material::Type = "visual/material";
	
	
	
	
	/* name constructor */
	Material::Material (const String& name)
	{
		create (name);
	}
	
	
	
	/* material constructor */
	Material::Material (Ogre::Material* material) : mMaterial(material)
	{
		Log::Debug (LogSource, "Creating material: " + material->getName());
	}
	
	
	
	/* copy constructor */
	Material::Material (const Material& material, const String& name)
	{
		Log::Debug (LogSource, "Creating material: " + name);
		
		/* clone material */
		mMaterial = material.mMaterial->clone(name).get();
	}
	
	
	
	/* destructor */
	Material::~Material ()
	{
		Log::Debug (LogSource, "Destroying material: " + mMaterial->getName());
		Ogre::MaterialManager::getSingleton().remove (mMaterial->getHandle());
	}
	
	
	
	
	/* get material name */
	const String& Material::getName () const
	{
		return mMaterial->getName();
	}
	
	
	
	/* get the technique by looking into the OGRE material and wrapping it up
	 * in our technique class */
	smart_ptr<Technique> Material::getTechnique (const String& name) const
	{
		Ogre::Technique* technique = mMaterial->getTechnique (name);
		return technique ? new Technique (technique) : 0;
	}
	
	
	
	/* iterate through all the techniques and add it into a list wrapped with
	 * our technique class */
	TechniqueList Material::getTechniques () const
	{
		TechniqueList list;
		
		Ogre::Material::TechniqueIterator iter = mMaterial->getTechniqueIterator();
		
		/* add all techniques to the list */
		while (iter.hasMoreElements())
		{
			Ogre::Technique* technique = iter.getNext();
			list.push_back (new Technique (technique));
		}
		
		return list;
	}
	
	
	
	
	/* adding a technique involves creating and returning it so it can be
	 * modified. since the technique class makes sure it is created within
	 * the material we only need to call its constructor here */
	smart_ptr<Technique> Material::add (const String& name)
	{
		return new Technique (this, name);
	}
	
	
	
	/* ogre doesnt have a remove-by-name function so we iterate through the
	 * techniques to find a technique with a matching name, remove it with
	 * an index and then break out of the loop */
	void Material::remove (const String& name)
	{
		/* look for technique index */
		for (int i = 0; i < mMaterial->getNumTechniques(); ++i)
		{
			Ogre::Technique* technique = mMaterial->getTechnique(i);
			
			/* found matching technique */
			if (technique->getName() == name)
			{
				Log::Debug (LogSource, "Removing technique '" + name + "' from "
						"the material: " + mMaterial->getName());
				
				mMaterial->removeTechnique (i);
				break;
			}
		}
	}
	
	
	
	/* since we have no actual technique store we just call remove() with the
	 * technique name instead. this is primarily a convenient function */
	void Material::remove (const smart_ptr<Technique>& technique)
	{
		remove (technique->getName());
	}
	
	
	
	/* clear material by removing all techniques */
	void Material::clear ()
	{
		mMaterial->removeAllTechniques ();
	}
	
	
	
	
	
	/* load the material resource */
	bool Material::load ()
	{
		assert (mMaterial);
		mMaterial->load ();
		return mMaterial->isLoaded();
	}
	
	
	/* reload the material resource */
	bool Material::reload ()
	{
		assert (mMaterial);
		mMaterial->reload ();
		return mMaterial->isLoaded();
	}
	
	
	/* unload the material resource */
	void Material::unload ()
	{
		assert (mMaterial);
		mMaterial->unload ();
	}
	
	
	
	/* get material resource size */
	size_t Material::getSize () const
	{
		return mMaterial->getSize ();
	}
	
	
	
	
	
	/* create the internal material */
	void Material::create (const String& name)
	{
		Log::Debug (LogSource, "Creating material: " + name);
		
		/* create material */
		Ogre::MaterialManager* manager = Ogre::MaterialManager::getSingletonPtr();
		mMaterial = manager->getDefaultSettings()->clone(name).get();
	}

}}
