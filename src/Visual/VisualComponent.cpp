
#include "Soma/Visual/VisualComponent.h"

#include "Soma/Log.h"
#include "Soma/World.h"
#include "Soma/Entity.h"
#include "Soma/Visual/World.h"


namespace Soma {
namespace Visual {

	/* log source */
	const String VisualComponent::LogSource = "Visual::VisualComponent";
	
	
	
	/* constructor */
	VisualComponent::VisualComponent () : mLoaded(false)
	{
		
	}
	
	
	/* destructor */
	VisualComponent::~VisualComponent ()
	{
		
	}
	
	
	
	/* load the visual component. we do this by extracting the visual world
	 * (if any) from the world reference and then pass it to the create
	 * function in order to create an actual instance of the object. since
	 * the visual world contains the scene manager which is needed by all
	 * visual objects using OGRE, and that a visual world is not required to
	 * be in a world reference, we have to manage this separately, refusing to
	 * load if a visual world isnt present within the entity */
	bool VisualComponent::load ()
	{
		if (!mOwner)
			return Log::Error (LogSource, "Cannot load the visual component '" +
					getType() + "because it has not been added to an entity yet");
		
		/* already loaded */
		if (mLoaded) return true;
		
		
		weak_ptr<Soma::World> world = mOwner->getWorld();
		
		/* get world component */
		smart_ptr<Component> component = world->getComponent (Visual::World::Type);
		
		/* no world.visual component found */
		if (!component)
			return Log::Error (LogSource, "Cannot load the visual component '" +
					getType() + "' on the entity '" + mOwner->getName() + "' "
					"because the world '" + world->getName() + "' does not "
					"have the necessary visual world component: " + Visual::World::Type);
		
		
		/* cast world and create object */
		mVisualWorld = ref_cast<Visual::World> (component);
		
		mLoaded = create (mVisualWorld);
		
		if (mLoaded) Loaded.raise (this);
		return mLoaded;
	}
	
	
	
	/* reload the visual component */
	bool VisualComponent::reload ()
	{
		Log::Debug (LogSource, "Reloading visual component in entity: " + mOwner->getName());
		
		unload ();
		return load ();
	}
	
	
	
	/* unload the visual component by destroying any ogre instance it may have.
	 * this allows us to reuse the component in another world without having
	 * to create a whole new visual component */
	void VisualComponent::unload ()
	{
		if (!mLoaded) return;
		
		destroy ();
		mLoaded = false;
		
		Unloaded.raise (this);
	}

}}
