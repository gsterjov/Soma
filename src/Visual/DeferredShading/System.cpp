
#include "Soma/Visual/DeferredShading/System.h"

#include <OgreMaterialManager.h>
#include <OgreCompositorManager.h>
#include <OgreCompositorInstance.h>

#include "Soma/Visual/DeferredShading/LightCompositor.h"


namespace Soma {
namespace Visual {


	/* constructor */
	DeferredShading::DeferredShading (Ogre::Viewport* viewport)
	: mInitialised (false),
	  mViewport (viewport)
	{
		
	}
	
	
	/* destructor */
	DeferredShading::~DeferredShading ()
	{
		
	}
	
	
	
	/* start deferred shading */
	bool DeferredShading::initialise ()
	{
		if (mInitialised) return true;
		
		/* get compositor manager */
		Ogre::CompositorManager& manager = Ogre::CompositorManager::getSingleton();
		
		
		/* register custom composition passes */
		manager.registerCustomCompositionPass ("DeferredLight", new LightCompositor());
		
		
		/* create compositors */
		mGBuffer = manager.addCompositor (mViewport, "deferred/gbuffer");
		mLighting = manager.addCompositor (mViewport, "deferred/lights");
		
		mGBuffer->setEnabled (true);
		mLighting->setEnabled (true);
		
		
		mInitialised = true;
		return mInitialised;
	}
	
	/* restart deferred shading */
	bool DeferredShading::reinitialise ()
	{
		mInitialised = true;
		return mInitialised;
	}
	
	/* stop deferred shading */
	void DeferredShading::shutdown ()
	{
		mInitialised = false;
	}

}}
