
#include "Soma/Visual/DeferredShading/LightCompositor.h"

#include <Ogre.h>


namespace Soma {
namespace Visual {

	/* constructor */
	LightRenderOperation::LightRenderOperation (Ogre::CompositorInstance* instance,
	                                            const Ogre::CompositionPass* pass)
	{
		/* get viewport */
		mViewport = instance->getChain()->getViewport();
	}
	
	
	/* destructor */
	LightRenderOperation::~LightRenderOperation ()
	{
		LightMap::iterator iter;
		
		for (iter = mLights.begin(); iter != mLights.end(); ++iter)
			delete iter->second;
		
		mLights.clear ();
	}
	
	
	
	
	/* add custom render pass */
	void inject (Ogre::SceneManager* sm,
	             Ogre::Technique* tech,
	             Ogre::Renderable* rend,
	             const Ogre::LightList* list)
	{
		for (int i = 0; i < tech->getNumPasses(); ++i)
		{
			Ogre::Pass* pass = tech->getPass (i);
			sm->_injectRenderWithPass (pass, rend, false, false, list);
		}
	}
	
	
	
	
	/* execute operation */
	void LightRenderOperation::execute (Ogre::SceneManager* sm,
	                                    Ogre::RenderSystem* rs)
	{
		Ogre::Camera* cam = mViewport->getCamera ();
		
		
		/* update deferred lights */
		Ogre::LightList::const_iterator iter;
		const Ogre::LightList& list = sm->_getLightsAffectingFrustum ();
		
		/* render deferred lights */
		for (iter = list.begin(); iter != list.end(); ++iter)
		{
			Ogre::Light* light = *iter;
			
			Ogre::LightList lights;
			lights.push_back (light);
			
			LightMap::iterator it = mLights.find (light);
			DeferredLight* dlight = 0;
			
			/* no deferred light yet */
			if (it == mLights.end())
			{
				dlight = new DeferredLight (light);
				mLights[light] = dlight;
			}
			
			/* found deffered light */
			else
				dlight = it->second;
			
			
			Ogre::MaterialPtr mat = Ogre::MaterialManager::getSingleton().getByName ("deferred/geometry");
			
			if (!mat->isLoaded())
				mat->load();
			
			Ogre::Technique* tech = mat->getTechnique (0);
			
			inject (sm, tech, dlight, &lights);
		}
	}

}}
