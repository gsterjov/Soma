
#include "Soma/Visual/DeferredShading/GeomUtils.h"

#include <OgreMeshManager.h>
#include <OgreSubMesh.h>
#include <OgreHardwareBufferManager.h>


using namespace Ogre;


namespace Soma {
namespace Visual {


	void GeomUtils::createCone (Ogre::VertexData* vertexData,
	                            Ogre::IndexData* indexData,
	                            float radius,
	                            float height,
	                            int nVerticesInBase)
	{
		assert (vertexData);
		assert (indexData);
		
		
		/* vertex declaration */
		size_t offset = 0;
		Ogre::VertexDeclaration* decl = vertexData->vertexDeclaration;
		decl->addElement (0, 0, Ogre::VET_FLOAT3, Ogre::VES_POSITION);
		
		
		/* vertex buffer */
		vertexData->vertexCount = nVerticesInBase + 1;
		
		Ogre::HardwareVertexBufferSharedPtr vbuf = Ogre::HardwareBufferManager::getSingleton().createVertexBuffer (
				decl->getVertexSize(0),
				vertexData->vertexCount,
				Ogre::HardwareBuffer::HBU_STATIC_WRITE_ONLY,
				false
		);
		
		/* bind vertex buffer */
		vertexData->vertexBufferBinding->setBinding (0, vbuf);
		
		
		
		/* index buffer */
		indexData->indexCount = (3 * nVerticesInBase) + (3 * (nVerticesInBase - 2));
		
		Ogre::HardwareIndexBufferSharedPtr ibuf = Ogre::HardwareBufferManager::getSingleton().createIndexBuffer (
				Ogre::HardwareIndexBuffer::IT_16BIT,
				indexData->indexCount,
				Ogre::HardwareBuffer::HBU_STATIC_WRITE_ONLY,
				false
		);
		
		
		/* link to mesh */
		indexData->indexBuffer = ibuf;
		
		
		
		/* lock vertex buffer */
		float* vbuffer = static_cast<float*> (vbuf->lock (Ogre::HardwareBuffer::HBL_DISCARD));
		
		
		/* cone head and base */
		for (int i = 0; i < 3; ++i)
			*vbuffer++ = 0.0f;
		
		/* angle to tip delta */
		float fDeltaBaseAngle = (2 * Ogre::Math::PI) / nVerticesInBase;
		
		/* cone base */
		for (int i = 0; i < nVerticesInBase; ++i)
		{
			float angle = i * fDeltaBaseAngle;
			
			*vbuffer++ = radius * Ogre::Math::Cos (angle);
			*vbuffer++ = height;
			*vbuffer++ = radius * Ogre::Math::Sin (angle);
		}
		
		
		/* unlock vertex buffer */
		vbuf->unlock();
		
		
		
		/* lock index buffer */
		unsigned short* ibuffer = static_cast<unsigned short*> (ibuf->lock (Ogre::HardwareBuffer::HBL_DISCARD));
		
		
		/* cone head */
		for (int i = 0; i < nVerticesInBase; ++i)
		{
			*ibuffer++ = 0;
			*ibuffer++ = (i % nVerticesInBase) + 1;
			*ibuffer++ = ((i + 1) % nVerticesInBase) + 1;
		}
		
		/* cone base */
		for (int i = 0; i < nVerticesInBase - 2; ++i)
		{
			*ibuffer++ = 1;
			*ibuffer++ = i + 3;
			*ibuffer++ = i + 2;
		}
		
		
		/* unlock index buffer */
		ibuf->unlock();
	}


}}
