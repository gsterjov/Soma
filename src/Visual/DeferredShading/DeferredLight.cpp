
#include "Soma/Visual/DeferredShading/DeferredLight.h"

#include <OgreHardwareBufferManager.h>
#include <OgreCamera.h>
#include <OgreSceneNode.h>
#include <OgreLight.h>

#include "Soma/Visual/DeferredShading/GeomUtils.h"


namespace Soma {
namespace Visual {

	/* constructor */
	DeferredLight::DeferredLight (Ogre::Light* light) : mLight(light)
	{
		createCone ();
	}
	
	
	/* destructor */
	DeferredLight::~DeferredLight ()
	{
		delete mRenderOp.indexData;
		delete mRenderOp.vertexData;
	}
	
	
	
	
	/* get radius */
	Ogre::Real DeferredLight::getBoundingRadius () const
	{
		return mRadius;
	}
	
	
	/* get view depth */
	Ogre::Real DeferredLight::getSquaredViewDepth (const Ogre::Camera* camera) const
	{
		Ogre::Vector3 dist = camera->getDerivedPosition() - getParentSceneNode()->_getDerivedPosition();
		return dist.squaredLength ();
	}
	
	
	/* get transform */
	void DeferredLight::getWorldTransforms (Ogre::Matrix4* trans) const
	{
		Ogre::Quaternion quat = Ogre::Vector3::UNIT_Y.getRotationTo (mLight->getDerivedDirection());
		trans->makeTransform (mLight->getDerivedPosition(), Ogre::Vector3::UNIT_SCALE, quat);
	}
	
	
	
	/* create cone */
	void DeferredLight::createCone ()
	{
		Ogre::Real height = mLight->getAttenuationRange ();
		Ogre::Radian angle = mLight->getSpotlightOuterAngle () / 2;
		Ogre::Real radius = Ogre::Math::Tan (angle) * height;
		
		Ogre::Real baseVertices = 20;
		
		
		/* prepare geometry buffer */
		mRenderOp.operationType = Ogre::RenderOperation::OT_TRIANGLE_LIST;
		mRenderOp.indexData = new Ogre::IndexData ();
		mRenderOp.vertexData = new Ogre::VertexData ();
		mRenderOp.useIndexes = true;
		
		/* create geometry */
		GeomUtils::createCone (mRenderOp.vertexData,
		                       mRenderOp.indexData,
		                       radius,
		                       height,
		                       baseVertices);
		
		
		/* set bound box */
		setBoundingBox (Ogre::AxisAlignedBox (Ogre::Vector3 (-radius, 0, -radius),
		                                      Ogre::Vector3 (radius, height, radius)));
		
		mRadius = radius;
	}

}}
