
#include "Soma/Visual/Pass.h"

#include <OgreTechnique.h>
#include <OgrePass.h>

#include "Soma/Log.h"
#include "Soma/Visual/Technique.h"


namespace Soma {
namespace Visual {

	/* log source */
	const String Pass::LogSource = "Visual::Pass";
	
	
	
	
	/* name constructor */
	Pass::Pass (const weak_ptr<Technique>& technique, const String& name)
	{
		Log::Debug (LogSource, "Creating pass: " + name);
		
		/* create pass */
		Ogre::Technique* tech = technique->getTechnique();
		mPass = tech->createPass ();
		mPass->setName (name);
	}
	
	
	
	/* pass constructor */
	Pass::Pass (Ogre::Pass* pass) : mPass(pass)
	{
		Log::Debug (LogSource, "Creating pass: " + pass->getName());
	}
	
	
	
	
	/* get pass name */
	const String& Pass::getName () const
	{
		return mPass->getName();
	}
	
	
	
	
	/* get receives lighitng */
	bool Pass::getLighting ()
	{
		return mPass->getLightingEnabled();
	}
	
	
	/* set receives lighting */
	void Pass::setLighting (bool enabled)
	{
		mPass->setLightingEnabled (enabled);
	}
	
	
	
	
	/* get ambient colour */
	Colour Pass::getAmbient ()
	{
		Ogre::ColourValue col = mPass->getAmbient();
		return Colour (col.r, col.g, col.b, col.a);
	}
	
	
	/* get diffuse colour */
	Colour Pass::getDiffuse ()
	{
		Ogre::ColourValue col = mPass->getDiffuse();
		return Colour (col.r, col.g, col.b, col.a);
	}
	
	
	/* get specular colour */
	Colour Pass::getSpecular ()
	{
		Ogre::ColourValue col = mPass->getSpecular();
		return Colour (col.r, col.g, col.b, col.a);
	}
	
	
	/* get emissive colour */
	Colour Pass::getEmissive ()
	{
		Ogre::ColourValue col = mPass->getSelfIllumination();
		return Colour (col.r, col.g, col.b, col.a);
	}
	
	
	
	
	/* set ambient colour */
	void Pass::setAmbient (const Colour& colour)
	{
		mPass->setAmbient (colour.r, colour.g, colour.b);
	}
	
	
	/* set diffuse colour */
	void Pass::setDiffuse (const Colour& colour)
	{
		mPass->setDiffuse (colour.r, colour.g, colour.b, colour.a);
	}
	
	
	/* set specular colour */
	void Pass::setSpecular (const Colour& colour)
	{
		mPass->setSpecular (colour.r, colour.g, colour.b, colour.a);
	}
	
	
	/* set emissive colour */
	void Pass::setEmissive (const Colour& colour)
	{
		mPass->setSelfIllumination (colour.r, colour.g, colour.b);
	}
	
	
	
	/* set ambient colour */
	void Pass::setAmbient (float r, float g, float b)
	{
		mPass->setAmbient (r, g, b);
	}
	
	
	/* set diffuse colour */
	void Pass::setDiffuse (float r, float g, float b, float a)
	{
		mPass->setDiffuse (r, g, b, a);
	}
	
	
	/* set specular colour */
	void Pass::setSpecular (float r, float g, float b, float a)
	{
		mPass->setSpecular (r, g, b, a);
	}
	
	
	/* set emissive colour */
	void Pass::setEmissive (float r, float g, float b)
	{
		mPass->setSelfIllumination (r, g, b);
	}

}}
