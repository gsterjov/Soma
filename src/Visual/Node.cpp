
#include "Soma/Visual/Node.h"

#include <OgreSceneNode.h>
#include <OgreSceneManager.h>

#include "Soma/Log.h"
#include "Soma/Registry.h"

#include "Soma/Entity.h"
#include "Soma/Visual/World.h"
#include "Soma/Visual/Object.h"


namespace Soma {
namespace Visual {

	/* log source */
	const String Node::LogSource = "Visual::Node";
	
	/* component name */
	const String Node::Type = "entity.visual.node";
	
	
	/* component type */
	REGISTER_TYPE (Component, Node, Node::Type);
	
	
	
	/* constructor */
	Node::Node () : mSceneNode(0), mScale(1, 1, 1)
	{
		
	}
	
	
	/* destructor */
	Node::~Node ()
	{
		unload ();
	}
	
	
	
	/* add child node */
	void Node::add (const smart_ptr<Node>& child)
	{
		Log::Debug (LogSource, "Adding child node component to node in entity: "
				+ mOwner->getName());
		
		
		mChildren.push_back (child);
		
		/* add if loaded */
		if (mSceneNode)
			mSceneNode->addChild (child->mSceneNode);
	}
	
	
	/* remove child node */
	void Node::remove (const smart_ptr<Node>& child)
	{
		Log::Debug (LogSource, "Removing child node component from node in entity: "
				+ mOwner->getName());
		
		
		mChildren.remove (child);
		
		/* remove if loaded */
		if (mSceneNode)
			mSceneNode->removeChild (child->mSceneNode);
	}
	
	
	/* attach movable object */
	void Node::attach (const smart_ptr<Object>& object)
	{
		Log::Debug (LogSource, "Attaching object component '" +
				object->getType() + "' to node in entity: " + mOwner->getName());
		
		
		mObjects.push_back (object);
		
		/* attach if loaded */
		if (mSceneNode)
			mSceneNode->attachObject (object->getMovableObject());
	}
	
	
	/* detach movable object */
	void Node::detach (const smart_ptr<Object>& object)
	{
		Log::Debug (LogSource, "Detaching object component '" +
				object->getType() + "' from node in entity: " + mOwner->getName());
		
		
		mObjects.remove (object);
		
		/* detach if loaded */
		if (mSceneNode)
			mSceneNode->detachObject (object->getMovableObject());
	}
	
	
	
	/* node scale */
	void Node::setScale (const Vector& scale)
	{
		mScale = scale;
		
		if (mSceneNode)
		{
			Ogre::Vector3 vec (scale.x, scale.y, scale.z);
			mSceneNode->setScale (vec);
		}
	}
	
	
	/* scale node */
	void Node::scale (const Vector& scale)
	{
		mScale += scale;
		
		if (mSceneNode)
		{
			Ogre::Vector3 vec (scale.x, scale.y, scale.z);
			mSceneNode->scale (vec);
		}
	}
	
	
	
	
	/* create an ogre scene node instance by using the visual world provided by
	 * the base object class */
	bool Node::create (const weak_ptr<Visual::World>& world)
	{
		Log::Debug (LogSource, "Creating node in entity: " + mOwner->getName());
		
		/* create node */
		mSceneNode = world->getSceneManager()->createSceneNode (mOwner->getName());
		
		
		/* add child nodes */
		std::list<smart_ptr<Node> >::iterator iter;
		
		for (iter = mChildren.begin(); iter != mChildren.end(); ++iter)
			mSceneNode->addChild ((*iter)->getSceneNode());
		
		
		/* attach movable objects */
		std::list<smart_ptr<Object> >::iterator it;
		
		for (it = mObjects.begin(); it != mObjects.end(); ++it)
			mSceneNode->attachObject ((*it)->getMovableObject());
		
		
		/* get current entity position */
		onPositionChanged (mOwner->getPosition());
		onOrientationChanged (mOwner->getOrientation());
		
		/* set scale */
		Ogre::Vector3 vec (mScale.x, mScale.y, mScale.z);
		mSceneNode->setScale (vec);
		
		
		/* attach to entity movable events */
		mOwner->PositionChanged    += delegate (this, &Node::onPositionChanged);
		mOwner->OrientationChanged += delegate (this, &Node::onOrientationChanged);
		
		
		/* bind linked component */
		bind (mOwner);
		
		return true;
	}
	
	
	
	/* destroy the ogre scene node instance by using the visual world store in
	 * the base object class */
	void Node::destroy ()
	{
		if (!mSceneNode) return;
		
		/* if the visual world isnt valid anymore then the scene node should
		 * have been destroyed when it became so */
		assert (mVisualWorld);
		
		
		Log::Debug (LogSource, "Destroying node in entity: " + mOwner->getName());
		
		
		/* detach from entity movable events */
		mOwner->PositionChanged    -= delegate (this, &Node::onPositionChanged);
		mOwner->OrientationChanged -= delegate (this, &Node::onOrientationChanged);
		
		
		/* unbind linked component */
		unbind ();
		
		/* destroy scene node */
		mSceneNode->detachAllObjects ();
		mVisualWorld->getSceneManager()->destroySceneNode (mSceneNode);
		mSceneNode = 0;
	}
	
	
	
	
	
	/* get property */
	Value Node::getProperty (const String& name) const
	{
		/* mesh */
		if (name == "scale")
			return mScale;
		return Value();
	}
	
	
	
	/* set property */
	void Node::setProperty (const String& name, const Value& value)
	{
		/* mesh */
		if (name == "scale")
			setScale (value.get<Vector>());
	}
	
	
	
	
	/* link parent node component */
	void Node::linkParent (const smart_ptr<Component>& parent)
	{
		if (parent->getType() == Type)
		{
			smart_ptr<Node> node = ref_cast<Node> (parent);
			node->add (this);
		}
	}
	
	
	
	/* unlink parent node component */
	void Node::unlinkParent (const smart_ptr<Component>& parent)
	{
		if (parent->getType() == Type)
		{
			smart_ptr<Node> node = ref_cast<Node> (parent);
			node->remove (this);
		}
	}
	
	
	
	
	/* entity position changed */
	void Node::onPositionChanged (const Vector& position)
	{
		if (mSceneNode)
			mSceneNode->setPosition (position.x, position.y, position.z);
	}
	
	
	
	/* entity orientation changed */
	void Node::onOrientationChanged (const Quaternion& orientation)
	{
		if (mSceneNode)
			mSceneNode->setOrientation (orientation.w,
			                            orientation.x,
			                            orientation.y,
			                            orientation.z);
	}


}}
