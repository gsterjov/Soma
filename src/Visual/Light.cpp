
#include "Soma/Visual/Light.h"

#include <cassert>

#include <OgreLight.h>
#include <OgreSceneManager.h>

#include "Soma/Log.h"
#include "Soma/Registry.h"

#include "Soma/Entity.h"
#include "Soma/Visual/World.h"


namespace Soma {
namespace Visual {

	/* log source */
	const String Light::LogSource = "Visual::Light";
	
	/* component name */
	const String Light::Type = "entity.visual.light";
	
	
	/* component type */
	REGISTER_TYPE (Component, Light, Light::Type);
	
	
	
	/* constructor */
	Light::Light () : mLight(0)
	{
		
	}
	
	
	/* destructor */
	Light::~Light ()
	{
		unload ();
	}
	
	
	
	
	/* get light direction */
	Vector Light::getDirection () const
	{
		assert (mLight);
		
		Ogre::Vector3 dir = mLight->getDirection();
		return Vector (dir.x, dir.y, dir.z);
	}
	
	
	
	
	/* set lighting type */
	void Light::setType (LightType type)
	{
		switch (type)
		{
			case POINT:
				mLight->setType (Ogre::Light::LT_POINT);
				break;
				
			case DIRECTIONAL:
				mLight->setType (Ogre::Light::LT_DIRECTIONAL);
				break;
				
			case SPOTLIGHT:
				mLight->setType (Ogre::Light::LT_SPOTLIGHT);
				break;
		}
	}
	
	
	
	/* set direction */
	void Light::setDirection (const Vector& direction)
	{
		assert (mLight);
		mLight->setDirection (direction.x, direction.y, direction.z);
	}
	
	
	
	
	/* object implementaiton */
	Ogre::MovableObject* Light::getMovableObject ()
	{
		return mLight;
	}
	
	
	
	
	/* create an ogre light instance by using the visual world provided by the
	 * base object class */
	bool Light::create (const weak_ptr<Visual::World>& world)
	{
		Log::Debug (LogSource, "Creating light in entity: " + mOwner->getName());
		
		/* create light */
		mLight = world->getSceneManager()->createLight (mOwner->getName());
		return true;
	}
	
	
	
	/* destroy the ogre light instance by using the visual world store in the
	 * base object class */
	void Light::destroy ()
	{
		if (!mLight) return;
		
		/* if the visual world isnt valid anymore then the light should have
		 * been destroyed when it became so */
		assert (mVisualWorld);
		
		
		Log::Debug (LogSource, "Destroying light in entity: " + mOwner->getName());
		
		/* destroy light */
		mVisualWorld->getSceneManager()->destroyLight (mLight);
		mLight = 0;
	}
	
	
	
	
	
	/* get property */
	Value Light::getProperty (const String& name) const
	{
		if (name == "type")
		{
			assert (mLight);
			
			switch (mLight->getType())
			{
				case Ogre::Light::LT_POINT:
					return "point";
					
				case Ogre::Light::LT_DIRECTIONAL:
					return "directional";
					
				case Ogre::Light::LT_SPOTLIGHT:
					return "spotlight";
			}
		}
		
		
		else if (name == "direction")
			return getDirection();
	}
	
	
	
	/* set property */
	void Light::setProperty (const String& name, const Value& value)
	{
		if (name == "type")
		{
			String type = value.get<String>();
			
			if      (type == "point")       setType (POINT);
			else if (type == "directional") setType (DIRECTIONAL);
			else if (type == "spotlight")   setType (SPOTLIGHT);
		}
		
		
		else if (name == "direction")
			setDirection (value.get<Vector>());
	}

}}
