
#include "Soma/Physics/RigidBodyObject.h"

#include <btBulletDynamicsCommon.h>

#include "Soma/Log.h"
#include "Soma/Exception.h"
#include "Soma/Movable.h"
#include "Soma/Physics/World.h"
#include "Soma/Physics/RigidBody.h"


namespace Soma {
namespace Physics {

	/* log source */
	const String RigidBodyObject::LogSource = "Physics::RigidBodyObject";
	
	
	
	/* motion state class for moving abitrary entities */
	class MotionState : public btMotionState
	{
	public:
		/* constructor */
		MotionState (Movable* movable) : mMovable(movable)
		{
			if (mMovable)
			{
				/* get initial transform */
				Vector pos = mMovable->getPosition ();
				Quaternion rot = mMovable->getOrientation ();
				
				mTrans.setOrigin (btVector3 (pos.x, pos.y, pos.z));
				mTrans.setRotation (btQuaternion (rot.x, rot.y, rot.z, rot.w));
			}
		}
		
		/* set movable */
		void setMovable (Movable* movable) { mMovable = movable; }
		
		/* get current transform */
		void getWorldTransform (btTransform& trans) const { trans = mTrans; }
		
		/* set world transform */
		void setWorldTransform (const btTransform& trans)
		{
			if (mMovable)
			{
				/* get transformation */
				btVector3 pos = trans.getOrigin ();
				btQuaternion rot = trans.getRotation ();
				
				/* set transform on movable */
				mMovable->setPosition (Vector (pos.x(), pos.y(), pos.z()));
				mMovable->setOrientation (Quaternion (rot.w(), rot.x(), rot.y(), rot.z()));
			}
		}
		
		
	private:
		Movable* mMovable;
		btTransform mTrans;
	};
	
	
	
	
	
	/* resource constructor */
	RigidBodyObject::RigidBodyObject (const String& name,
	                                  const smart_ptr<RigidBody>& body)
	: mName (name),
	  mBody (0)
	{
		Log::Debug (LogSource, "Creating rigid body object: " + mName);
		setRigidBody (body);
	}
	
	
	/* uri constructor */
	RigidBodyObject::RigidBodyObject (const String& name, const String& uri)
	: mName (name),
	  mBody (0)
	{
		Log::Debug (LogSource, "Creating rigid body object: " + mName);
		setRigidBody (uri);
	}
	
	
	/* empty constructor */
	RigidBodyObject::RigidBodyObject (const String& name)
	: mName (name),
	  mBody (0)
	{
		Log::Debug (LogSource, "Creating rigid body object: " + mName);
	}
	
	
	/* destructor */
	RigidBodyObject::~RigidBodyObject ()
	{
		if (mBody)
		{
			Log::Debug (LogSource, "Destroying rigid body object: " + mName);
			
			mWorld->getDynamicsWorld()->removeRigidBody (mBody);
			delete mBody;
		}
	}
	
	
	
	/* attach the movable */
	void RigidBodyObject::attach (Movable* movable)
	{
		if (mBody) mBody->setMotionState (new MotionState (movable));
	}
	
	
	
	/* set rigid body resource */
	void RigidBodyObject::setRigidBody (const smart_ptr<RigidBody>& body)
	{
		Log::Debug (LogSource, "Setting rigid body on object: " + mName);
		
		
		/* remove last entity */
		if (mBody)
		{
			mWorld->getDynamicsWorld()->removeRigidBody (mBody);
			delete mBody;
		}
		
		
		/* get body properties */
		btVector3 inertia (0, 0, 0);
		float mass = body->getMass();
		
		body->getShape()->calculateLocalInertia (mass, inertia);
		
		/* create rigid body */
		btRigidBody::btRigidBodyConstructionInfo info (mass, NULL, body->getShape());
		mBody = new btRigidBody (info);
		mRigidBody = body;
	}
	
	
	/* set rigid body by name */
	void RigidBodyObject::setRigidBody (const Vri& vri)
	{
		/* get the specified rigid body */
		smart_ptr<RigidBody> body = RigidBodyManager::instance().get (vri);
		
		/* no rigid body resource found */
		if (!body)
			SOMA_EXCEPTION (LogSource, "Cannot find the asset '" + vri.asset +
					"' defining a rigid body with the name '" + vri.resource +
					"'. Rigid body object creation failed.");
		
		/* cannot load resource */
		else if (!body->load())
			SOMA_EXCEPTION (LogSource, "Cannot load the rigid body '" + vri.resource +
					"' found in the asset '" + vri.asset + "'. Loading failed.");
		
		/* finish loading resource */
		setRigidBody (body);
	}

}}
