
#include "Soma/Physics/RigidBody.h"

#include <cassert>
#include <btBulletDynamicsCommon.h>


namespace Soma {
namespace Physics {

	/* log source */
	const String RigidBody::LogSource = "Physics::RigidBody";
	
	/* resource type */
	const String RigidBody::Type = "physics/rigid-body";
	
	
	
	/* constructor */
	RigidBody::RigidBody (btCollisionShape* shape, float mass)
	: mShape (shape),
	  mMass (mass)
	{
		
	}
	
	
	/* empty constructor */
	RigidBody::RigidBody () : mShape(0), mMass(0)
	{
		
	}
	
	
	/* destructor */
	RigidBody::~RigidBody ()
	{
	}
	
	
	
	/* load the rigid body resource */
	bool RigidBody::load ()
	{
		return true;
	}
	
	
	/* reload the rigid body resource */
	bool RigidBody::reload ()
	{
		return true;
	}
	
	
	/* unload the rigid body resource */
	void RigidBody::unload ()
	{
		
	}
	
	
	
	/* get rigid body resource size */
	size_t RigidBody::getSize () const
	{
		return 0;
	}

}}
