
#include "Soma/Physics/Manager.h"

#include <cassert>

#include "Soma/Log.h"
#include "Soma/Registry.h"
#include "Soma/Physics/World.h"


namespace Soma {
namespace Physics {

	/* log source */
	const String Manager::LogSource = "Physics::Manager";
	
	
	/* subsystem type */
	REGISTER_TYPE (SubSystem, Manager, "Physics");
	
	
	
	
	/* constructor */
	Manager::Manager () : mInitialised(false)
	{
		Log::Debug (LogSource, "Creating physics simulation manager");
	}
	
	
	/* destructor */
	Manager::~Manager()
	{
		Log::Debug (LogSource, "Destroying physics simulation manager");
		
		if (mInitialised)
			shutdown ();
	}
	
	
	
	/* initialise physics */
	bool Manager::initialise ()
	{
		Log::Message (LogSource, "Initialising physics simulation manager");
		mInitialised = true;
		return true;
	}
	
	
	
	/* reinitialise physics */
	bool Manager::reinitialise ()
	{
		Log::Message (LogSource, "Reinitialising physics simulation manager");
		return true;
	}
	
	
	
	/* shut down physics */
	void Manager::shutdown ()
	{
		Log::Message (LogSource, "Shutting down physics simulation manager");
	}
	
	
	
	/* simulate a frame */
	void Manager::step ()
	{
		WorldList::iterator iter;
		
		/* step through all added worlds */
		for (iter = mWorlds.begin(); iter != mWorlds.end(); ++iter)
			(*iter)->step ();
	}
	
	
	
	
	/* Configurable implementation: save physics settings */
	bool Manager::save (ConfigInterface& config)
	{
		Log::Debug (LogSource, "Saving physics simulation manager settings");
		return true;
	}
	
	
	
	/* Configurable implementation: restore physics settings */
	bool Manager::restore (const ConfigInterface& config)
	{
		Log::Debug (LogSource, "Restoring physics simulation manager settings");
		return true;
	}

}}
