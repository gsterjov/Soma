
#include "Soma/Physics/World.h"

#include <btBulletDynamicsCommon.h>

#include "Soma/Log.h"
#include "Soma/Timer.h"
#include "Soma/Physics/RigidBodyObject.h"


namespace Soma {
namespace Physics {

	/* log source */
	const String World::LogSource = "Physics::World";
	
	
	
	/* constructor */
	World::World (const String& name) : mName(name)
	{
		Log::Debug (LogSource, "Creating physics world: " + mName);
		
		
		/* config */
		mConfig = new btDefaultCollisionConfiguration ();
		mDispatch = new btCollisionDispatcher (mConfig);
		
		int proxies = 1024;
		
		/* world size */
		btVector3 min (-10000, -10000, -10000);
		btVector3 max (10000, 10000, 10000);
		
		mBroadphase = new btAxisSweep3 (min, max, proxies);
		
		/* solver */
		mSolver = new btSequentialImpulseConstraintSolver ();
		
		
		/* create the world */
		mWorld = new btDiscreteDynamicsWorld (mDispatch,
		                                      mBroadphase,
		                                      mSolver,
		                                      mConfig);
		
		mWorld->setGravity (btVector3 (0, -10, 0));
		
		
		/* create simulation timer */
		mTimer = new Timer ();
	}
	
	
	/* destructor */
	World::~World ()
	{
		Log::Debug (LogSource, "Destroying physics world: " + mName);
		
		delete mWorld;
		delete mSolver;
		delete mDispatch;
		delete mConfig;
		delete mBroadphase;
		
		delete mTimer;
	}
	
	
	
	/* add rigid body */
	void World::addRigidBody (const smart_ptr<RigidBodyObject>& body)
	{
		Log::Debug (LogSource, "Adding rigid body to world: " + body->getName());
		
		mWorld->addRigidBody (body->getBody());
		mRigidBodies.push_back (body);
	}
	
	
	
	/* step through the simulation */
	void World::step ()
	{
		mWorld->stepSimulation (mTimer->getSeconds (true));
	}

}}
