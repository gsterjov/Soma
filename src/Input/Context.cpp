
#include "Soma/Input/Context.h"

#include "OIS.h"

#include "Soma/Log.h"
#include "Soma/Registry.h"
#include "Soma/Context.h"

#include "Soma/Input/Manager.h"
#include "Soma/Input/Keyboard.h"
#include "Soma/Input/Mouse.h"


namespace Soma {
namespace Input {

	/* log source */
	const String Context::LogSource = "Input::Context";
	
	/* subcontext name */
	const String Context::Type = "context.input";
	
	
	/* input subcontext type */
	REGISTER_TYPE (Component, Context, Context::Type);
	
	
	
	
	/* constructor */
	Context::Context () : mGrab(false), mLoaded(false), mManager(0)
	{
	}
	
	
	Context::~Context ()
	{
		unload ();
	}
	
	
	
	
	/* initialise input */
	bool Context::load ()
	{
		if (!mOwner)
			return Log::Error (LogSource, "Cannot load input context component "
					"because it has not been added to a context yet");
		
		/* already loaded */
		if (mLoaded) return true;
		
		
		Log::Debug (LogSource, "Creating input component in context: " + mOwner->getName());
		
		
		/* attach to broadcast for WindowID messages */
		mOwner->Broadcast += delegate (this, &Context::onBroadcast);
		
		
		/* look for existing window id */
		CompositeMessage msg = mOwner->request ("WindowID");
		
		/* got id. create manager */
		if (!msg.value.isEmpty())
			createManager (msg.value.get<size_t>());
		
		
		Manager::instance().addContext (this);
		
		mLoaded = true;
		return mLoaded;
	}
	
	
	
	/* reinitialise input */
	bool Context::reload ()
	{
		Log::Debug (LogSource, "Reloading input component in context: " + mOwner->getName());
		
		unload ();
		return load ();
	}
	
	
	
	/* shutdown input */
	void Context::unload ()
	{
		if (!mLoaded) return;
		
		
		Log::Debug (LogSource, "Unloading input component in context: " + mOwner->getName());
		
		
		Manager::instance().removeContext (this);
		
		/* detach from broadcast */
		mOwner->Broadcast -= delegate (this, &Context::onBroadcast);
		
		
		DeviceMap::iterator iter;
		
		/* free all devices */
		for (iter = mDevices.begin(); iter != mDevices.end(); ++iter)
			delete iter->second;
		
		/* destroy manager */
		if (mManager)
		{
			mManager->destroyInputSystem (mManager);
			mManager = 0;
		}
		
		
		mLoaded = false;
	}
	
	
	
	/* capture device input */
	void Context::capture ()
	{
		DeviceMap::iterator iter;
		
		/* capture input on all devices */
		for (iter = mDevices.begin(); iter != mDevices.end(); ++iter)
			iter->second->capture ();
	}
	
	
	
	
	
	
	/* create input manager */
	void Context::createManager (size_t window_id)
	{
		Log::Debug (LogSource, "Attaching input context to window: "
				+ Strings::convert (window_id));
		
		
		/* create params */
		OIS::ParamList params;
		
		
		/* window id */
		params.insert (std::make_pair ("WINDOW", Strings::convert (window_id)));
		
		
#ifdef SOMA_SYSTEM_UNIX
		/* disable mouse/key lock */
		if (!mGrab)
		{
			params.insert (std::make_pair ("x11_keyboard_grab", "false"));
			params.insert (std::make_pair ("x11_mouse_grab", "false"));
		}
		
		/* other params */
		params.insert (std::make_pair ("x11_mouse_hide", "true"));
#endif
		
		
		/* initialise input */
		mManager = OIS::InputManager::createInputSystem (params);
		
		
		
		/* hook up to the keyboard if one exists */
		if (mManager->getNumberOfDevices (OIS::OISKeyboard) > 0)
		{
			/* create input device */
			OIS::Object* obj = mManager->createInputObject (OIS::OISKeyboard, true);
			OIS::Keyboard* keyboard = static_cast<OIS::Keyboard*> (obj);
			
			/* add keyboard */
			mDevices["Keyboard"] = new Keyboard (keyboard);
		}
		
		
		/* hook up to the mouse if one exists */
		if (mManager->getNumberOfDevices (OIS::OISMouse) > 0)
		{
			/* create input device */
			OIS::Object* obj = mManager->createInputObject (OIS::OISMouse, true);
			OIS::Mouse* mouse = static_cast<OIS::Mouse*> (obj);
			
			/* add mouse */
			mDevices["Mouse"] = new Mouse (mouse);
		}
	}
	
	
	
	
	/* broadcasted message */
	void Context::onBroadcast (const CompositeMessage& message)
	{
		if (mManager) return;
		
		if (message.type == "WindowID")
			createManager (message.value.get<size_t>());
	}

}}
