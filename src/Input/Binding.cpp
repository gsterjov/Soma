
#include "Soma/Input/Binding.h"


namespace Soma {
namespace Input {

	/* log source */
	const String Binding::LogSource = "Input::Binding";
	
	
	
	/* constructor */
	Binding::Binding () : mActive(false), mActiveCount(0)
	{
		
	}
	
	
	/* destructor */
	Binding::~Binding ()
	{
		std::vector<State*>::iterator iter;
		
		/* remove state */
		for (iter = mStates.begin(); iter != mStates.end(); ++iter)
			(*iter)->removeListener (this);
	}
	
	
	
	/* add input */
	void Binding::add (State* state)
	{
		state->addListener (this);
		mStates.push_back (state);
	}
	
	
	/* remove input */
	void Binding::remove (State* state)
	{
		std::vector<State*>::iterator iter;
		
		/* find state */
		for (iter = mStates.begin(); iter != mStates.end(); ++iter)
		{
			if ((*iter) == state)
			{
				state->removeListener (this);
				iter = mStates.erase (iter);
			}
		}
	}
	
	
	
	
	/* activate binding */
	void Binding::activate ()
	{
		mActive = true;
		
		std::vector<Listener*>::iterator iter;
		
		/* notify listeners */
		for (iter = mListeners.begin(); iter != mListeners.end(); ++iter)
			(*iter)->bindingActivated (this);
	}
	
	
	/* deactivate binding */
	void Binding::deactivate ()
	{
		mActive = false;
		
		std::vector<Listener*>::iterator iter;
		
		/* notify listeners */
		for (iter = mListeners.begin(); iter != mListeners.end(); ++iter)
			(*iter)->bindingDeactivated (this);
	}
	
	
	
	/* add listener */
	void Binding::addListener (Listener* listener)
	{
		mListeners.push_back (listener);
	}
	
	
	/* remove listener */
	void Binding::removeListener (Listener* listener)
	{
		std::vector<Listener*>::iterator iter;
		
		/* look for the listener */
		for (iter = mListeners.begin(); iter != mListeners.end(); ++iter)
		{
			if (*iter == listener)
				iter = mListeners.erase (iter);
		}
	}
	
	
	
	
	/* state activated */
	void Binding::stateActivated (State* state)
	{
		++mActiveCount;
		
		/* all states active */
		if (mActiveCount == mStates.size())
			activate ();
	}
	
	
	/* state deactivated */
	void Binding::stateDeactivated (State* state)
	{
		--mActiveCount;
		
		if (mActive)
			deactivate ();
	}

}}
