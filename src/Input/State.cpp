
#include "Soma/Input/State.h"


namespace Soma {
namespace Input {

	/* log source */
	const String State::LogSource = "Input::State";
	
	
	
	/* constructor */
	State::State (Device* device, String name)
	: mDevice (device),
	  mName (name)
	{
		
	}
	
	
	/* destructor */
	State::~State ()
	{
		
	}
	
	
	
	/* activate state */
	void State::activate ()
	{
		mActive = true;
		
		std::vector<Listener*>::iterator iter;
		
		/* notify listeners */
		for (iter = mListeners.begin(); iter != mListeners.end(); ++iter)
			(*iter)->stateActivated (this);
	}
	
	
	/* deactivate state */
	void State::deactivate ()
	{
		mActive = false;
		
		std::vector<Listener*>::iterator iter;
		
		/* notify listeners */
		for (iter = mListeners.begin(); iter != mListeners.end(); ++iter)
			(*iter)->stateDeactivated (this);
	}
	
	
	
	/* add listener */
	void State::addListener (Listener* listener)
	{
		mListeners.push_back (listener);
	}
	
	
	/* remove listener */
	void State::removeListener (Listener* listener)
	{
		std::vector<Listener*>::iterator iter;
		
		/* look for the listener */
		for (iter = mListeners.begin(); iter != mListeners.end(); ++iter)
		{
			if (*iter == listener)
				iter = mListeners.erase (iter);
		}
	}

}}
