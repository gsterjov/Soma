
#include "Soma/Input/Action.h"

#include <stdexcept>

#include "Soma/Log.h"
#include "Soma/Input/Context.h"
#include "Soma/Input/Binding.h"
#include "Soma/Input/Device.h"


namespace Soma {
namespace Input {

	/* log source */
	const String Action::LogSource = "Input::Action";
	
	
	
	/* constructor */
	Action::Action ()
	{
		
	}
	
	
	/* destructor */
	Action::~Action ()
	{
		
	}
	
	
	
	
	/* bind to input */
	void Action::bind (const String& input)
	{
		/* get input state */
		State* state = findState (input);
		
		if (state)
		{
			Binding* binding = new Binding ();
			
			/* add states to the binding */
			binding->add (state);
			addBinding (binding);
		}
	}
	
	
	/* bind to multiple inputs */
	void Action::bind (const String& input1,
	                   const String& input2)
	{
		/* get input state */
		State* state1 = findState (input1);
		State* state2 = findState (input2);
		
		if (state1 && state2)
		{
			Binding* binding = new Binding ();
			
			/* add states to the binding */
			binding->add (state1);
			binding->add (state2);
			addBinding (binding);
		}
	}
	
	
	/* bind to multiple inputs */
	void Action::bind (const String& input1,
	                   const String& input2,
	                   const String& input3)
	{
		/* get input state */
		State* state1 = findState (input1);
		State* state2 = findState (input2);
		State* state3 = findState (input3);
		
		if (state1 && state2 && state3)
		{
			Binding* binding = new Binding ();
			
			/* add states to the binding */
			binding->add (state1);
			binding->add (state2);
			binding->add (state3);
			addBinding (binding);
		}
	}
	
	
	
	
	/* find device specific state */
	State* Action::findState (const String& input)
	{
		/* no attached context means we cant get a device state */
		if (!mContext)
		{
			Log::Error (LogSource, "Cannot bind action '" + getName() +
					"' to the input '" + input + "' because the action hasn't "
					"got a valid context attached.");
			return 0;
		}
		
		
		
		/* [device]/[input] */
		StringList list = Strings::split (input, "/");
		
		/* wrong input format */
		if (list.size() != 2)
		{
			Log::Error (LogSource, "Cannot bind action '" + getName() +
					"' to the input '" + input + "' because it is not in a "
					"valid format.");
			
			return 0;
		}
		
		
		/* get device */
		Device* device = mContext->getDevice (list[0]);
		
		/* no device found */
		if (!device)
		{
			Log::Error (LogSource, "Cannot find an input device by the name "
					"of '" + list[0] + "'");
			return 0;
		}
		
		
		/* get input state */
		State* state = device->getState (list[1]);
		
		/* no state found */
		if (!state)
		{
			Log::Error (LogSource, "Cannot find input by the name of '" +
					list[1] + "'");
			return 0;
		}
		
		
		return state;
	}


}}
