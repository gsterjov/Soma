
#include "Soma/Input/Mouse.h"

#include "Soma/Log.h"


namespace Soma {
namespace Input {

	/* log source */
	const String Mouse::LogSource = "Input::Mouse";
	
	
	
	/* constructor */
	Mouse::Mouse (OIS::Mouse* mouse) : mMouse(mouse)
	{
		mName = "Mouse (" + mMouse->vendor() + ")";
		mMouse->setEventCallback (this);
		
		Log::Debug (LogSource, "Creating mouse device: " + mName);
	}
	
	
	/* destructor */
	Mouse::~Mouse ()
	{
		Log::Debug (LogSource, "Destroying mouse device: " + mName);
	}
	
	
	
	/* mouse moved */
	bool Mouse::mouseMoved (const OIS::MouseEvent& ev)
	{
		return true;
	}
	
	
	/* mouse button pressed */
	bool Mouse::mousePressed (const OIS::MouseEvent& ev, OIS::MouseButtonID id)
	{
		return true;
	}
	
	
	/* mouse button released */
	bool Mouse::mouseReleased (const OIS::MouseEvent& ev, OIS::MouseButtonID id)
	{
		return true;
	}

}}
