
#include "Soma/Input/TriggerAction.h"


namespace Soma {
namespace Input {

	/* log source */
	const String TriggerAction::LogSource = "Input::TriggerAction";
	
	
	
	/* constructor */
	TriggerAction::TriggerAction (const String& name)
	: mName (name),
	  mActive (false)
	{
		
	}
	
	
	/* destructor */
	TriggerAction::~TriggerAction ()
	{
		std::vector<Binding*>::iterator iter;
		
		/* remove state */
		for (iter = mBindings.begin(); iter != mBindings.end(); ++iter)
			(*iter)->removeListener (this);
	}
	
	
	
	/* add binding */
	void TriggerAction::addBinding (Binding* binding)
	{
		binding->addListener (this);
		mBindings.push_back (binding);
	}
	
	
	/* remove binding */
	void TriggerAction::removeBinding (Binding* binding)
	{
		std::vector<Binding*>::iterator iter;
		
		/* look for the binding */
		for (iter = mBindings.begin(); iter != mBindings.end(); ++iter)
		{
			if (*iter == binding)
			{
				binding->removeListener (this);
				iter = mBindings.erase (iter);
			}
		}
	}
	
	
	
	
	/* binding activated */
	void TriggerAction::bindingActivated (Binding* binding)
	{
		mActive = true;
		Activated.raise (this);
	}
	
	
	/* binding deactivated */
	void TriggerAction::bindingDeactivated (Binding* binding)
	{
		mActive = false;
		Deactivated.raise (this);
	}

}}
