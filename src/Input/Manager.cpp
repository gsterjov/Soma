
#include "Soma/Input/Manager.h"

#include "Soma/Log.h"
#include "Soma/Registry.h"

#include "Soma/Input/Context.h"


namespace Soma {
namespace Input {

	/* log source */
	const String Manager::LogSource = "Input::Manager";
	
	
	/* subsystem type */
	REGISTER_TYPE (SubSystem, Manager, "Input");
	
	
	
	
	/* constructor */
	Manager::Manager ()
	: mInitialised (false)
	{
		Log::Debug (LogSource, "Creating input manager");
	}
	
	
	/* destructor */
	Manager::~Manager()
	{
		Log::Debug (LogSource, "Destroying input manager");
		
		if (mInitialised)
			shutdown();
	}
	
	
	
	
	
	/* add context to list */
	void Manager::addContext (Context* context)
	{
		mContexts.push_back (context);
	}
	
	
	
	/* remove context from list */
	void Manager::removeContext (Context* context)
	{
		ContextList::iterator iter;
		
		/* look for context */
		for (iter = mContexts.begin(); iter != mContexts.end(); ++iter)
		{
			/* found context */
			if (*iter == context)
			{
				mContexts.erase (iter);
				break;
			}
		}
	}
	
	
	
	
	/* initialise input system */
	bool Manager::initialise ()
	{
		Log::Message (LogSource, "Initialising input manager");
		
		
		mInitialised = true;
		return true;
	}
	
	
	
	/* reinitialise input system */
	bool Manager::reinitialise ()
	{
		Log::Message (LogSource, "Reinitialising input manager");
		return true;
	}
	
	
	
	/* shut down input system */
	void Manager::shutdown ()
	{
		Log::Message (LogSource, "Shutting down input manager");
	}
	
	
	
	/* read input buffer */
	void Manager::step ()
	{
		ContextList::iterator iter;
		
		/* capture input from all contexts */
		for (iter = mContexts.begin(); iter != mContexts.end(); ++iter)
			(*iter)->capture ();
	}
	
	
	
	
	/* Configurable implementation: save input settings */
	bool Manager::save (ConfigInterface& config)
	{
		Log::Debug (LogSource, "Saving input manager settings");
		return true;
	}
	
	
	
	/* Configurable implementation: restore input settings */
	bool Manager::restore (const ConfigInterface& config)
	{
		Log::Debug (LogSource, "Restoring input manager settings");
		return true;
	}

}}
