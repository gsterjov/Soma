
#include "Soma/Input/Keyboard.h"

#include "Soma/Log.h"
#include "Soma/Input/State.h"


namespace Soma {
namespace Input {

	/* log source */
	const String Keyboard::LogSource = "Input::Keyboard";
	
	
	
	/* constructor */
	Keyboard::Keyboard (OIS::Keyboard* keyboard) : mKeyboard(keyboard)
	{
		mName = "Keyboard (" + mKeyboard->vendor() + ")";
		mKeyboard->setEventCallback (this);
		
		Log::Debug (LogSource, "Creating keyboard device: " + mName);
		
		
		
		using namespace OIS;
		
		/* add key states */
		addState (KC_UNASSIGNED,   "None");
		
		addState (KC_1,            "1");
		addState (KC_2,            "2");
		addState (KC_3,            "3");
		addState (KC_4,            "4");
		addState (KC_5,            "5");
		addState (KC_6,            "6");
		addState (KC_7,            "7");
		addState (KC_8,            "8");
		addState (KC_9,            "9");
		addState (KC_0,            "0");
		
		addState (KC_Q,            "Q");
		addState (KC_W,            "W");
		addState (KC_E,            "E");
		addState (KC_R,            "R");
		addState (KC_T,            "T");
		addState (KC_Y,            "Y");
		addState (KC_U,            "U");
		addState (KC_I,            "I");
		addState (KC_O,            "O");
		addState (KC_P,            "P");
		addState (KC_A,            "A");
		addState (KC_S,            "S");
		addState (KC_D,            "D");
		addState (KC_F,            "F");
		addState (KC_G,            "G");
		addState (KC_H,            "H");
		addState (KC_J,            "J");
		addState (KC_K,            "K");
		addState (KC_L,            "L");
		addState (KC_Z,            "Z");
		addState (KC_X,            "X");
		addState (KC_C,            "C");
		addState (KC_V,            "V");
		addState (KC_B,            "B");
		addState (KC_N,            "N");
		addState (KC_M,            "M");
		
		addState (KC_MINUS,        "Minus");
		addState (KC_EQUALS,       "Equals");
		addState (KC_LBRACKET,     "Left Bracket");
		addState (KC_RBRACKET,     "Right Bracket");
		addState (KC_SEMICOLON,    "Semi Colon");
		addState (KC_APOSTROPHE,   "Apostrophe");
		addState (KC_COMMA,        "Comma");
		addState (KC_PERIOD,       "Period");
		addState (KC_BACKSLASH,    "Back Slash");
		addState (KC_SLASH,        "Forward Slash");
		
		
		addState (KC_ESCAPE,       "Escape");
		addState (KC_TAB,          "Tab");
		addState (KC_CAPITAL,      "Caps Lock");
		addState (KC_LSHIFT,       "Left Shift");
		addState (KC_LCONTROL,     "Left Control");
		addState (KC_LWIN,         "Left Windows");
		addState (KC_LMENU,        "Left Alt");
		
		addState (KC_SPACE,        "Space");
		
		addState (KC_BACK,         "Backspace");
		addState (KC_RETURN,       "Enter");
		addState (KC_RSHIFT,       "Right Shift");
		addState (KC_RCONTROL,     "Right Control");
		addState (KC_RWIN,         "Right Windows");
		addState (KC_RMENU,        "Right Alt");
		
		addState (KC_F1,           "F1");
		addState (KC_F2,           "F2");
		addState (KC_F3,           "F3");
		addState (KC_F4,           "F4");
		addState (KC_F5,           "F5");
		addState (KC_F6,           "F6");
		addState (KC_F7,           "F7");
		addState (KC_F8,           "F8");
		addState (KC_F9,           "F9");
		addState (KC_F10,          "F10");
		addState (KC_F11,          "F11");
		addState (KC_F12,          "F12");
		
		
		addState (KC_INSERT,       "Insert");
		addState (KC_DELETE,       "Delete");
		addState (KC_HOME,         "Home");
		addState (KC_END,          "End");
		addState (KC_PGUP,         "Page Up");
		addState (KC_PGDOWN,       "Page Down");
		
		addState (KC_UP,           "Up");
		addState (KC_LEFT,         "Left");
		addState (KC_RIGHT,        "Right");
		addState (KC_DOWN,         "Down");
		
		
		addState (KC_PAUSE,        "Pause");
		addState (KC_SYSRQ,        "SysRq");
		addState (KC_SCROLL,       "Scroll Lock");
		addState (KC_NUMLOCK,      "Num Lock");
		
		
		/* numpad */
		addState (KC_NUMPAD1,      "Numpad 1");
		addState (KC_NUMPAD2,      "Numpad 2");
		addState (KC_NUMPAD3,      "Numpad 3");
		addState (KC_NUMPAD4,      "Numpad 4");
		addState (KC_NUMPAD5,      "Numpad 5");
		addState (KC_NUMPAD6,      "Numpad 6");
		addState (KC_NUMPAD7,      "Numpad 7");
		addState (KC_NUMPAD8,      "Numpad 8");
		addState (KC_NUMPAD9,      "Numpad 9");
		addState (KC_NUMPAD0,      "Numpad 0");
		addState (KC_ADD,          "Numpad Plus");
		addState (KC_SUBTRACT,     "Numpad Minus");
		addState (KC_MULTIPLY,     "Numpad Multiply");
		addState (KC_DIVIDE,       "Numpad Divide");
		addState (KC_DECIMAL,      "Numpad Decimal");
		addState (KC_NUMPADENTER,  "Numpad Enter");
		
		
		/* media */
		addState (KC_PREVTRACK,    "Media Previous Track"); /* KC_CIRCUMFLEX on Japanese keyboard */
		addState (KC_NEXTTRACK,    "Media Next Track");
		addState (KC_MEDIASELECT,  "Media Select");
		addState (KC_PLAYPAUSE,    "Media Play Pause");
		addState (KC_MEDIASTOP,    "Media Stop");
		addState (KC_VOLUMEUP,     "Media Volume Up");
		addState (KC_VOLUMEDOWN,   "Media Volume Down");
		addState (KC_MUTE,         "Media Mute");
		
		
		/* web */
		addState (KC_WEBHOME,      "Web Home");
		addState (KC_WEBSEARCH,    "Web Search");
		addState (KC_WEBFAVORITES, "Web Favourites");
		addState (KC_WEBREFRESH,   "Web Refresh");
		addState (KC_WEBSTOP,      "Web Stop");
		addState (KC_WEBFORWARD,   "Web Forward");
		addState (KC_WEBBACK,      "Web Back");
		
		
		/* system */
		addState (KC_POWER,        "Power");
		addState (KC_SLEEP,        "Sleep");
		addState (KC_WAKE,         "Wake");
		addState (KC_CALCULATOR,   "Calculator");
		addState (KC_MYCOMPUTER,   "My Computer");
		addState (KC_MAIL,         "Mail");
		addState (KC_APPS,         "Apps");
		
		
		/* NEC PC98 */
		addState (KC_STOP,         "Stop");
		addState (KC_AT,           "At");
		addState (KC_COLON,        "Colon");
		addState (KC_UNDERLINE,    "Underline");
		addState (KC_F13,          "F13");
		addState (KC_F14,          "F14");
		addState (KC_F15,          "F15");
		addState (KC_NUMPADCOMMA,  "Numpad Comma");
		addState (KC_NUMPADEQUALS, "Numpad Equals");
		
		
		/* japanese keyboards */
		addState (KC_KANA,         "Kana");
		addState (KC_KANJI,        "Kanji");
		addState (KC_AX,           "Ax");
		addState (KC_YEN,          "Yen");
		addState (KC_CONVERT,      "Convert");
		addState (KC_NOCONVERT,    "No Convert");
		
		
		/* others */
		addState (KC_GRAVE,        "Grave");     /* accent */
		addState (KC_OEM_102,      "OEM 102");   /* < > | on UK/Germany keyboards */
		addState (KC_ABNT_C1,      "ABNT C1");   /* / ? on Portugese (Brazilian) keyboards */
		addState (KC_ABNT_C2,      "ABNT C2");   /* Numpad . on Portugese (Brazilian) keyboards */
		addState (KC_UNLABELED,    "Unlabeled"); /* (J3100) */
	}
	
	
	
	/* destructor */
	Keyboard::~Keyboard ()
	{
		Log::Debug (LogSource, "Destroying keyboard device: " + mName);
	}
	
	
	
	/* get state from name */
	State* Keyboard::getState (const String& name)
	{
		std::map<String, KeyCode>::iterator iter = mKeyNames.find (name);
		
		/* found matching keycode */
		if (iter != mKeyNames.end())
			return getState (iter->second);
		
		return 0;
	}
	
	
	
	
	/* create state */
	void Keyboard::addState (KeyCode code, const String& name)
	{
		mKeyNames[name] = code;
		mKeyStates[code] = new State (this, mKeyboard->getAsString (code));
	}
	
	
	
	/* key pressed */
	bool Keyboard::keyPressed (const OIS::KeyEvent& ev)
	{
		mKeyStates[ev.key]->activate ();
		return true;
	}
	
	
	/* key released */
	bool Keyboard::keyReleased (const OIS::KeyEvent& ev)
	{
		mKeyStates[ev.key]->deactivate ();
		return true;
	}

}}
