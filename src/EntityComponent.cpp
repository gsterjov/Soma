
#include "Soma/EntityComponent.h"

#include "Soma/Entity.h"


namespace Soma
{

	/* get the component owner */
	weak_ptr<Composite> EntityComponent::getOwner() const
	{
		return mOwner;
	}
	
	
	
	/* set the component owner by down casting the composite reference to
	 * an entity if the type matches. By doing so we allow all entity
	 * components to freely reference their owner without typecasting each
	 * and every time a component is added. */
	void EntityComponent::setOwner (const weak_ptr<Composite>& owner)
	{
		if (owner == mOwner) return;
		
		/* unload before we change owners */
		bool loaded = isLoaded();
		unload ();
		
		
		/* set new owner */
		mOwner.release();
		if (owner) mOwner = ref_cast<Entity> (owner);
		
		
		/* load again */
		if (loaded) load ();
	}

}
